import React from "react"
import { Input } from '@merchstack/react-native-ui'
import { ActionSheetIOS as ActionSheet } from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler"
import { translate } from '../../Translations'

export default function ActionSheetIOS(props) {
    const { list, placeholder, filter, referenceHandler, selectedValue } = props

    showActionSheet = () => {
        let list_array = list.map(item => item[filter])
        list_array.unshift("Cancel")
        ActionSheet.showActionSheetWithOptions(
            {
                options: list_array,
                cancelButtonIndex: 0
            },
            buttonIndex => {
                if(buttonIndex !== 0){
                    let list_id = list.map(item => item["id"])
                    referenceHandler(list_id[buttonIndex - 1])
                }
            }
        )
    }

    return (
        <TouchableOpacity onPress={showActionSheet}>
            <Input
                placeholder={translate(placeholder)}
                value={list.filter(item => item.id == selectedValue).map(item => item[filter]).toString()}
                editable={false}
            />
        </TouchableOpacity>
    )
}
