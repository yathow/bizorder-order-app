import {
  addLocaleChangedListener,
  removeLocaleChangedListener,
  setI18nConfig
} from '../../Translations';

import { PureComponent } from 'react';

export default class BaseComponent extends PureComponent {
  constructor() {
    super();

    setI18nConfig(); // set initial config

    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
  }

  useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
  }

  componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);
  }

  componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);
  }
}
