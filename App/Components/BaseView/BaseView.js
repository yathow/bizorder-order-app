import { ContentView, SafeAreaView, Text } from '@merchstack/react-native-ui';

import BaseComponent from '../BaseComponent/BaseComponent';
import HomeHeader from '../Header/HomeHeader';
import React from 'react';
import Style from './BaseViewStyle';

export default class BaseView extends BaseComponent {
  render() {
    const {
      title,
      showHeader,
      showBackButton,
      showMenuButton,
      showSearchButton,
      showCartButton,
      showFilterButton,
      resetFilters,
      successMessage,
      infoMessage,
      warningMessage,
      errorMessage,
      children
    } = this.props;

    const printHeader = () => {
      if (showHeader === undefined || showHeader) {
        return (
          <HomeHeader
            title={title}
            showBackButton={showBackButton}
            showMenuButton={showMenuButton}
            showSearchButton={showSearchButton}
            showCartButton={showCartButton}
            showFilterButton={showFilterButton}
            resetFilters={resetFilters}
          />
        );
      } else {
        return null;
      }
    };

    return (
      <SafeAreaView style={Style.container}>
        {printHeader()}
        <ContentView
          style={Style.content}
          successMessage={successMessage}
          infoMessage={infoMessage}
          warningMessage={warningMessage}
          errorMessage={errorMessage}
        >
          {children}
        </ContentView>
      </SafeAreaView>
    );
  }
}
