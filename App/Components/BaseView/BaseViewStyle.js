import { StyleSheet } from 'react-native';
import { Styles } from '../../Theme';

export default StyleSheet.create({
  container: {
    ...Styles.container,
    flex: 1
  },
  content: {
    ...Styles.container
  }
});
