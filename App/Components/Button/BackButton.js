import { Icon } from '@merchstack/react-native-ui';
import React from 'react';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';
import { withTheme } from 'react-native-elements';
import { useDispatch } from 'react-redux'
import AppActions from '../../Stores/App/Actions'

const BackButton = props => {
  const { onPress, style } = props;
  const dispatch = useDispatch()

  const back = () => {
    dispatch(AppActions.updateRoute(''))
    props.navigation.goBack();
  };

  const color =
    style !== undefined && style.color !== undefined ? style.color : props.theme.colors.inverse;
  var bStyle = { ...style };
  delete bStyle.color;

  return (
    <TouchableWithoutFeedback onPress={onPress ? onPress : back}>
      <Icon name="arrow-back" color={color} containerStyle={bStyle} />
    </TouchableWithoutFeedback>
  );
};

export default withNavigation(withTheme(BackButton));
