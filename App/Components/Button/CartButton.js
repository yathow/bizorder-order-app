import { Badge, Icon, View } from '@merchstack/react-native-ui';

import PropTypes from 'prop-types';
import React from 'react';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { connect, useDispatch } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { withTheme } from 'react-native-elements';
import AppActions from '../../Stores/App/Actions'

const CartButton = props => {
  const { onPress, style, navigation, count, theme, updateRoute } = props;

  const showCart = () => {
    updateRoute('Cart')
    navigation.navigate('CartScreen');
  };

  const renderBadge = () => {
    if (count > 0) {
      return (
        <Badge
          status="error"
          containerStyle={{ position: 'absolute', bottom: 0, right: 0 }}
          value={count}
        />
      );
    } else {
      return null;
    }
  };

  return (
    <TouchableWithoutFeedback onPress={onPress ? onPress : showCart}>
      <View>
        <Icon name="shopping-cart" color={theme.colors.inverse} containerStyle={style} />
        {renderBadge()}
      </View>
    </TouchableWithoutFeedback>
  );
};

const mapStateToProps = state => ({
  count: state.cart.cart.details.length
});

const mapDispatchToProps = dispatch => ({
  clearRoutes: () => dispatch(AppActions.clearRoutes()),
  updateRoute: route => dispatch(AppActions.updateRoute(route))
});

CartButton.propTypes = {
  count: PropTypes.number
};

CartButton.defaultProps = {
  count: 0
};

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(withTheme(CartButton)));
