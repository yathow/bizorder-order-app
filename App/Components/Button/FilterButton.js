import { Icon, View } from '@merchstack/react-native-ui';

import React from 'react';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { withTheme } from 'react-native-elements';
import OrdersActions from '../../Stores/Orders/Actions';

const FilterButton = props => {
    const { onPress, style, setFilterOrderDialogVisible } = props;

    const filter = () => {
        if (onPress !== undefined) {
            onPress();
        } else {
            setFilterOrderDialogVisible(true)
        }
    };

    const color = style !== undefined && style.color !== undefined ? style.color : props.theme.colors.inverse;
    var bStyle = { ...style };
    delete bStyle.color;

    return (
        <TouchableWithoutFeedback onPress={filter}>
            <View>
                <Icon type="material-community" name="filter" color={color} containerStyle={bStyle} />
            </View>
        </TouchableWithoutFeedback>
    );
};

const mapStateToProps = state => {
    return {
        keyword: state.items.filters.keyword
    };
};

const mapDispatchToProps = dispatch => ({
    setFilterOrderDialogVisible: (visibility) => dispatch(OrdersActions.setFilterOrderDialogVisible(visibility))
});

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(FilterButton));
