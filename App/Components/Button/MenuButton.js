import { Icon } from '@merchstack/react-native-ui';
import React from 'react';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';
import { withTheme } from 'react-native-elements';

const MenuButton = props => {
  const { onPress, style } = props;

  const toggleDrawer = () => {
    props.navigation.toggleDrawer();
  };

  return (
    <TouchableWithoutFeedback onPress={onPress ? onPress : toggleDrawer}>
      <Icon name="menu" color={props.theme.colors.inverse} containerStyle={style} />
    </TouchableWithoutFeedback>
  );
};

export default withNavigation(withTheme(MenuButton));
