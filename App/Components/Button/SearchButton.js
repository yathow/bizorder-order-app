import { Icon, View } from '@merchstack/react-native-ui';

import ItemsActions from '../../Stores/Items/Actions';
import React from 'react';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { withTheme } from 'react-native-elements';

const SearchButton = props => {
  const { onPress, style } = props;

  const search = () => {
    if (onPress !== undefined) {
      onPress();
    } else {
      props.fetchFilters(props.resetFilters);
    }
  };

  const color =
    style !== undefined && style.color !== undefined ? style.color : props.theme.colors.inverse;
  var bStyle = { ...style };
  delete bStyle.color;

  return (
    <TouchableWithoutFeedback onPress={search}>
      <View>
        <Icon name="search" color={color} containerStyle={bStyle} />
      </View>
    </TouchableWithoutFeedback>
  );
};

const mapStateToProps = state => {
  return {
    keyword: state.items.filters.keyword
  };
};

const mapDispatchToProps = dispatch => ({
  fetchFilters: reset => dispatch(ItemsActions.fetchFilters(reset))
});

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(SearchButton));
