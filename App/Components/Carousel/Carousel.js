import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Image, View, ScrollView, Dimensions } from 'react-native'
import { withTheme } from 'react-native-elements'
import Style from './CarouselStyle'

import CarouselActions from '../../Stores/Carousel/Actions'
import PromotionsActions from '../../Stores/Promotions/Actions'
import ItemDetailsActions from '../../Stores/ItemDetails/Actions'
import { TouchableHighlight } from 'react-native-gesture-handler'

const DEVICE_WIDTH = Dimensions.get('screen').width

class Carousel extends Component {
    scrollRef = React.createRef()
    constructor(props) {
        super(props)
    }

    componentDidMount = () => {
        this.actionStartInterval()
    }
    
    componentWillUnmount() {
        this.actionClearInterval()
    }

    actionSetCarouselIndex = event => {
        const viewSize = event.nativeEvent.layoutMeasurement.width
        const contentOffset = event.nativeEvent.contentOffset.x
        const newIndex = Math.floor(contentOffset/viewSize)

        const { setCarouselIndex } = this.props

        setCarouselIndex(newIndex)
        this.forceUpdate()
    }

    actionSetCarouselIndexInterval = () => {
        const { setCarouselIndex, carouselIndex, promotions } = this.props

        let newIndex = carouselIndex === promotions.length - 1 ? 0 : carouselIndex + 1
        setCarouselIndex(newIndex)
        this.scrollRef.current.scrollTo({
            animated: true,
            y: 0,
            x: DEVICE_WIDTH * newIndex
        })
    }

    actionStartInterval = () => {
        this.interval = setInterval(() => {
            this.actionSetCarouselIndexInterval()
        }, 3000)
    }
    
    actionClearInterval = () => {
        clearInterval(this.interval)
    }

    actionGoToPromotion = (promotionId) => {
        const { goToPromotion } = this.props

        goToPromotion(promotionId)
    }

    render() {
        const { promotions, carouselIndex } = this.props
        return(
            <View style={Style.fullSize}>
                <ScrollView 
                    style={Style.container} 
                    horizontal 
                    pagingEnabled 
                    onScrollBeginDrag={() => this.actionClearInterval()}
                    onScrollEndDrag={() => this.actionStartInterval()}
                    onMomentumScrollEnd={this.actionSetCarouselIndex}
                    showsHorizontalScrollIndicator={false}
                    ref={this.scrollRef}
                >
                    {promotions.map(promotion => (
                        promotion.asset_url 
                        ? <TouchableHighlight key={promotion.id} onPress={() => this.actionGoToPromotion(promotion.id)}>
                            <Image style={Style.images} source={{uri: promotion.asset_url}} />
                        </TouchableHighlight>
                        : null
                    ))}
                </ScrollView>
                <View style={Style.paginationContainer}>
                    {promotions.map((promotion, index) => (
                        <View key={promotion.id} style={[Style.pagination, {opacity: index === carouselIndex ? 0.5 : 1}]}/>
                    ))}
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => ({
    carouselIndex: state.carousel.carouselIndex
})

const mapDispatchToProps = dispatch => ({
    setCarouselIndex: index => dispatch(CarouselActions.setCarouselIndex(index)),
    goToPromotion: promotionId => dispatch(PromotionsActions.goToPromotion(promotionId)),
    setExpandPromotion: promotionId => dispatch(ItemDetailsActions.setExpandPromotion(promotionId))
})

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Carousel))
