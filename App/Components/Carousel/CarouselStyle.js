import { Metrics } from '@merchstack/react-native-ui'
import { StyleSheet, Dimensions } from 'react-native'

const screenWidth = Dimensions.get('screen').width
const screenHeight = Dimensions.get('screen').height

export default StyleSheet.create({
    fullSize: {
        width: "100%"
    },  
    images: {
        width: screenWidth,
        height: screenWidth/2.5 
    },
    paginationContainer: {
        position: "absolute",
        bottom: 15,
        height: 10,
        width: "100%",
        display: "flex",
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: 'center'
    },
    pagination: {
        width: Metrics.sm,
        height: Metrics.sm,
        borderRadius: Metrics.sm / 2,
        margin: 5,
        backgroundColor: '#fff'
    }
})