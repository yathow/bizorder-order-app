import { Button, Modal, Text, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import React from 'react';
import Style from './ConfirmDialogStyle';
import { withTheme } from 'react-native-elements';

class ConfirmDialog extends BaseComponent {
  actionDismiss = () => {
    const { onCancel } = this.props;
    if (onCancel) {
      onCancel();
    }
  };

  actionConfirm = () => {
    const { onConfirm } = this.props;
    if (onConfirm) {
      onConfirm();
    }
  };

  render() {
    const { isVisible } = this.props;
    return (
      <Modal
        isVisible={isVisible}
        animationIn="fadeIn"
        animationOut="fadeOut"
        useNativeDriver={true}
        backdropTransitionOutTiming={0}
        hideModalContentWhileAnimating={true}
        onBackdropPress={this.actionDismiss}
        onBackButtonPress={this.actionDismiss}
        containerStyle={Style.container}
      >
        <View style={Style.content}>
          {this.renderTitle()}
          {this.renderContent()}
          {this.renderView()}
          {this.renderButtons()}
        </View>
      </Modal>
    );
  }

  renderTitle() {
    const { title, theme } = this.props;
    if (title) {
      return (
        <View style={Style.titleContainer(theme)}>
          <Text style={Style.titleLbl(theme)}>{title}</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  renderContent() {
    const { message, component } = this.props;
    if (typeof message == 'string') {
      return (
        <View style={Style.messageContainer}>
          <Text style={Style.messageLbl}>{message}</Text>
        </View>
      );
    } else if(component) {
      return component;
    } else {
      return null;
    }
  }

  renderView() {
    const { children } = this.props;
    if (children) {
      return children;
    } else {
      return null;
    }
  }

  renderButtons() {
    const { cancelText, confirmText, isLoading } = this.props;
    if (isLoading) {
      return null;
    } else {
      return (
        <View style={Style.buttonsRow}>
          <Button
            containerStyle={Style.btnContainer}
            onPress={this.actionDismiss}
            title={cancelText}
          />
          <Button
            status="primary"
            containerStyle={Style.btnContainer}
            onPress={this.actionConfirm}
            title={confirmText}
          />
        </View>
      );
    }
  }
}

ConfirmDialog.defaultProps = {
  isLoading: false
};

export default withTheme(ConfirmDialog);
