import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    padding: 0,
    borderRadius: Metrics.borderRadius
  },
  content: {},
  titleLbl: theme => ({
    padding: Metrics.md,
    margin: Metrics.md,
    color: theme.colors.primary,
    fontWeight: 'bold'
  }),
  titleContainer: theme => ({
    borderBottomColor: theme.colors.inputBorder,
    borderBottomWidth: 0.75
  }),
  messageLbl: {
    padding: Metrics.md,
    margin: Metrics.md
  },
  messageContainer: {
    paddingBottom: Metrics.md
  },
  buttonsRow: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    margin: Metrics.md,
    marginBottom: Metrics.lg,
    marginTop: Metrics.lg
  },
  btnContainer: {
    flex: 1,
    marginLeft: Metrics.sm,
    marginRight: Metrics.sm
  }
});
