import { Text, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import React from 'react';
import Style from './EmptyComponentStyle';

class EmptyComponent extends BaseComponent {
  render() {
    if (this.props.isLoading) {
      return null;
    } else {
      return (
        <View style={Style.content}>
          <Text>{this.props.title}</Text>
        </View>
      );
    }
  }
}

export default EmptyComponent;
