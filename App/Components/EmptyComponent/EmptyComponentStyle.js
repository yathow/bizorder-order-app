import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  content: {
    padding: Metrics.lg,
    flexDirection: 'column',
    alignItems: 'center'
  }
});
