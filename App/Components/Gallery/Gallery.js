import { Badge, FlatList, Image, Text } from '@merchstack/react-native-ui';
import React, { useState } from 'react';

import { Images } from '../../Theme';
import Style from './GalleryStyle';
import { View } from 'react-native';
import { withTheme } from 'react-native-elements';

const Gallery = props => {
  const [selected, setSelected] = useState(0);

  const renderDesc = desc => {
    if (desc) {
      return <Text style={Style.descLbl(props.theme)}>{desc}</Text>;
    } else {
      return null;
    }
  };

  const renderImage = ({ item, index }) => {
    return (
      <View style={Style.imagePanel}>
        <GalleryImage source={{ uri: item.source }} />
        {renderDesc(item.desc)}
      </View>
    );
  };

  const onViewRef = React.useRef(viewRef => {
    if (viewRef.viewableItems.length > 0) {
      setSelected(viewRef.viewableItems[0].index);
    } else {
      setSelected(0);
    }
  });

  return (
    <View style={Style.container}>
      <FlatList
        horizontal={true}
        pagingEnabled={true}
        keyExtractor={(data, index) => data.id.toString()}
        data={props.photos}
        renderItem={renderImage}
        showsHorizontalScrollIndicator={false}
        onViewableItemsChanged={onViewRef.current}
        viewabilityConfig={{
          itemVisiblePercentThreshold: 50
        }}
        ListEmptyComponent={<GalleryImage />}
      />
      <GalleryScroll photos={props.photos} theme={props.theme} selected={selected} />
    </View>
  );
};

const GalleryImage = props => {
  const source = props.source ? props.source : Images.noPhotoIcon;
  return <Image source={source} style={Style.image} resizeMode="contain" />;
};

const GalleryScroll = props => {
  if (props.photos && props.photos.length > 1) {
    return (
      <View style={Style.scrollPanel}>
        {props.photos.map((photo, index) => {
          let badgeStyle =
            props.selected === index
              ? Style.selectedIcon(props.theme)
              : Style.unselectedIcon(props.theme);
          return <Badge key={index} containerStyle={Style.scrollIcon} badgeStyle={badgeStyle} />;
        })}
      </View>
    );
  } else {
    return null;
  }
};

export default withTheme(Gallery);
