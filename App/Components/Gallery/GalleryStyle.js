import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  scrollPanel: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: Metrics.md
  },
  scrollIcon: {
    paddingLeft: Metrics.xs,
    paddingRight: Metrics.xs
  },
  descLbl: theme => ({
    position: 'absolute',
    top: 0,
    right: 0,
    borderRadius: Metrics.borderRadius,
    backgroundColor: theme.backgroundColors.info,
    color: theme.colors.info,
    fontWeight: 'bold',
    padding: Metrics.sm,
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md
  }),
  selectedIcon: theme => ({
    backgroundColor: theme.colors.primary
  }),
  unselectedIcon: theme => ({
    backgroundColor: theme.backgroundColors.panel
  }),
  imagePanel: {
    alignItems: 'center',
    width: Metrics.screenWidth() - 2 * Metrics.md
  },
  image: {
    width: 250,
    height: 250
  }
});
