import { Text, View } from '@merchstack/react-native-ui';

import BackButton from '../Button/BackButton';
import React from 'react';
import Style from './HeaderStyle';
import { withTheme } from 'react-native-elements';

const Header = props => {
  const showBackButton = () => {
    if (isNaN(props.showBackButton) || props.showBackButton) {
      return <BackButton style={Style.leftBtn} />;
    } else {
      return null;
    }
  };

  return (
    <View style={Style.container(props.theme)}>
      {showBackButton()}
      <Text h1 style={Style.title}>
        {props.title}
      </Text>
      {props.rightComponent}
    </View>
  );
};

export default withTheme(Header);
