import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: theme => ({
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: Metrics.md,
    height: Metrics.headerHeight
  }),
  leftBtn: {
    paddingRight: Metrics.md
  },
  title: {
    flex: 1
  }
});
