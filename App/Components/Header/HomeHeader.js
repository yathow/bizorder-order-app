import { Text, View } from '@merchstack/react-native-ui';

import BackButton from '../Button/BackButton';
import CartButton from '../Button/CartButton';
import MenuButton from '../Button/MenuButton';
import React from 'react';
import SearchButton from '../Button/SearchButton';
import Style from './HomeHeaderStyle';
import { withTheme } from 'react-native-elements';
import FilterButton from '../Button/FilterButton';

const HomeHeader = props => {
  const showBackButton = () => {
    if (isNaN(props.showBackButton) || props.showBackButton) {
      return <BackButton style={Style.leftBtn} />;
    } else {
      return null;
    }
  };

  const showMenuButton = () => {
    if (isNaN(props.showMenuButton) || props.showMenuButton) {
      return <MenuButton style={Style.leftBtn} />;
    } else {
      return null;
    }
  };

  const showSearchButton = () => {
    if (isNaN(props.showSearchButton) || props.showSearchButton) {
      return <SearchButton style={Style.rightBtn} resetFilters={props.resetFilters} />;
    } else {
      return null;
    }
  };

  const showCartButton = () => {
    if (isNaN(props.showCartButton) || props.showCartButton) {
      return <CartButton style={Style.rightBtn} />;
    } else {
      return null;
    }
  };

  const showFilterButton = () => {
    if (isNaN(props.showFilterButton) || props.showFilterButton) {
      return <FilterButton style={Style.rightBtn} />;
    } else {
      return null;
    }
  };

  return (
    <View style={Style.container(props.theme)}>
      {showBackButton()}
      {showMenuButton()}
      <Text h3 h3Style={Style.title(props.theme)}>
        {props.title}
      </Text>
      {showSearchButton()}
      {showCartButton()}
      {showFilterButton()}
    </View>
  );
};

export default withTheme(HomeHeader);
