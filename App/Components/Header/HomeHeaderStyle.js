import { Fonts, Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: theme => ({
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: Metrics.md,
    height: Metrics.headerHeight,
    backgroundColor: theme.backgroundColors.primary
  }),
  leftBtn: {
    paddingRight: Metrics.md
  },
  rightBtn: {
    paddingRight: Metrics.md,
    paddingLeft: Metrics.md
  },
  title: theme => ({
    flex: 1,
    color: theme.colors.inverse,
    paddingLeft: Metrics.md
  })
});
