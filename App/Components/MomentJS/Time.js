import React, { PureComponent } from 'react'
import { Text } from '@merchstack/react-native-ui'
import moment from 'moment'


class Time extends PureComponent {
    render() {
        const time = this.props.config == 'now' ? moment(this.props.time || moment.now()).fromNow()
                                                : moment(this.props.time || moment.now()).format('Do MMMM YYYY')
        return (
            <Text style={this.props.style || ''}>{time}</Text>
        )
    }
}

export default Time