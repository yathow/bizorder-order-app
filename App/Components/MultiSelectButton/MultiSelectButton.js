import React from "react";
import { View } from "react-native";
import treeChanges from 'tree-changes';

import PropTypes from 'prop-types';
import SelectButton from "./SelectButton";
import Style from './MultiSelectButtonStyle';

class MultiSelectButton extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            isShowAll: false,
            selectedValue: [],
            labelHash: {}
        };

        this.useOnSingleTap = this.useOnSingleTap.bind(this);

        this.isSelected = this.isSelected.bind(this);
    }
    
    componentDidMount() {
        //populate selectedValue
        let newSelectedValue = []; 
        if (this.props.selectedValue !== undefined) {
            this.props.selectedValue.map(value => {
                if(newSelectedValue.length > 1) {
                    if (this.props.multiple) {
                        newSelectedValue.push(value);
                    }
                } else {
                    newSelectedValue.push(value);
                }
                
            });
        }
        this.setState({
            selectedValue: newSelectedValue
        });

        //populate labelHash
        let newLabelHash = {};
        if(this.props.items !== undefined) {
            this.props.items.map(item => {
                newLabelHash[item.value] = item.label === undefined ? item.value : item.label
            });
        }
        this.setState({
            labelHash: newLabelHash
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const { changed } = treeChanges(prevProps, this.props);
        if (changed('selectedValue')) {
            let newSelectedValue = []; 
            if (this.props.selectedValue !== undefined) {
                this.props.selectedValue.map(value => {
                    if(newSelectedValue.length > 1) {
                        if (this.props.multiple) {
                            newSelectedValue.push(value);
                        }
                    } else {
                        newSelectedValue.push(value);
                    }
                    
                });
            }
            this.setState({
                selectedValue: newSelectedValue
            });
        }

        if(changed('items')) {
            let newLabelHash = {};
            if(this.props.items !== undefined) {
                this.props.items.map(item => {
                    newLabelHash[item.value] = item.label === undefined ? item.value : item.label
                });
            }
            this.setState({
                labelHash: newLabelHash
            });
        }
    }

    componentWillUnmount() {
        
    }
    
    useOnSingleTap(valueTap) {
        let newSelectedValue = [];
        if (this.props.multiple) {
            if (this.state.selectedValue.includes(valueTap)) {
                newSelectedValue = this.state.selectedValue.filter((curValue, curIndex) => {
                    return curValue !== valueTap
                });                
            } else {
                newSelectedValue = [
                    ...this.state.selectedValue,
                    valueTap
                ];
            }
        } else {
            newSelectedValue = [valueTap];
        }

        this.props.onValueChange(newSelectedValue);

        this.setState({
            selectedValue: newSelectedValue
        });

        this.props.singleTap(valueTap);
    }
    
    isSelected(value) {
        return this.state.selectedValue.includes(value);
    }
    
    render() {
        return (
            <View style={[Style.container, this.props.containerStyle]}>
                {this.props.items.map((ele, index) => (
                    <SelectButton
                        key={ele.value}
                        buttonStyle={this.props.buttonStyle}
                        textStyle={this.props.textStyle}
                        highlightStyle={this.props.highlightStyle}
                        multiple={this.props.multiple}
                        value={ele.value}
                        label={ele.label}
                        selected={this.isSelected(ele.value)}
                        singleTap={this.useOnSingleTap}
                    />
                ))}
            </View>
        );
    }
}

MultiSelectButton.propTypes = {
    multiple: PropTypes.bool,
    selectedValue: PropTypes.arrayOf(
        PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    ),

    items: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
                .isRequired,
            label: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        })
    ).isRequired,

    highlightStyle: PropTypes.shape({
        borderColor: PropTypes.string.isRequired,
        backgroundColor: PropTypes.string.isRequired,
        textColor: PropTypes.string.isRequired,
        borderTintColor: PropTypes.string.isRequired,
        backgroundTintColor: PropTypes.string.isRequired,
        textTintColor: PropTypes.string.isRequired
    }),
    containerStyle: PropTypes.object,
    buttonStyle: PropTypes.object,
    textStyle: PropTypes.object,

    singleTap: PropTypes.func,
    onValueChange: PropTypes.func
}

MultiSelectButton.defaultProps = {
    multiple: true,
    singleTap: valueTap => {},
    onValueChange: selectedValues => {}
};

export default MultiSelectButton;