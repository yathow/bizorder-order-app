import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flexWrap: "wrap",
        flexDirection: "row"
    }
});