import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback
} from 'react-native';
import PropTypes from 'prop-types';

import Style from './SelectButtonStyle';

const ios_blue = '#007AFF';

class SelectButton extends React.PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            selected: false,
        }
    }

    componentDidMount() {
        this.setState({
            selected: this.props.selected,
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.selected !== prevProps.selected) {
            this.setState({
              selected: this.props.selected
            });
        }
    }

    render() {
        return (
            <TouchableWithoutFeedback
                onPress={() => {
                    this.props.singleTap(this.props.value)
                }
            }>
                <View
                style={
                [
                    Style.button,
                    this.props.buttonStyle,
                    {
                        borderColor: this.state.selected ? this.props.highlightStyle.borderTintColor : this.props.highlightStyle.borderColor,
                        backgroundColor: this.state.selected ? this.props.highlightStyle.backgroundTintColor : this.props.highlightStyle.backgroundColor,
                    }
                ]
                }>
                    <Text style={
                    [
                        Style.text,
                        this.props.textStyle,
                        { color: this.state.selected ? this.props.highlightStyle.textTintColor : this.props.highlightStyle.textColor }
                    ]
                    }>
                    {this.props.label === undefined ? this.props.value : this.props.label}
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

SelectButton.propTypes = {
    selected: PropTypes.bool,

    value: PropTypes.oneOfType(
        [
            PropTypes.string,
            PropTypes.number
        ]
    ).isRequired,
    label: PropTypes.oneOfType(
        [
            PropTypes.string,
            PropTypes.number
        ]
    ),

    highlightStyle: PropTypes.shape({
        borderColor: PropTypes.string.isRequired,
        backgroundColor: PropTypes.string.isRequired,
        textColor: PropTypes.string.isRequired,
        borderTintColor: PropTypes.string.isRequired,
        backgroundTintColor: PropTypes.string.isRequired,
        textTintColor: PropTypes.string.isRequired,
    }),

    buttonStyle: PropTypes.object,
    textStyle: PropTypes.object,
    singleTap: PropTypes.func,
}

SelectButton.defaultProps = {
    selected: false,
    highlightStyle: {
        borderColor: 'gray',
        backgroundColor: 'transparent',
        textColor: 'gray',
        borderTintColor: ios_blue,
        backgroundTintColor: 'transparent',
        textTintColor: ios_blue,
    },

    singleTap: (valueTap) => { },
};

export default SelectButton;