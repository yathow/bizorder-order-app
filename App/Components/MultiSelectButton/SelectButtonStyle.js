import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    button: {
        margin: 5,
        borderRadius: 3,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1
    },
    text: {
        textAlign: 'center',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
    }
});