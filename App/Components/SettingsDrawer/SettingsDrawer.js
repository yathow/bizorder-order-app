import { Avatar, ListItem, SafeAreaView, Text, View } from '@merchstack/react-native-ui';

import AppActions from '../../Stores/App/Actions';
import { BaseComponent } from '..';
import DebtorActions from '../../Stores/Debtor/Actions';
import LocalesPicker from '../../Containers/Locales/LocalesPicker';
import PropTypes from 'prop-types';
import React from 'react';
import { ScrollView } from 'react-native';
import Style from './SettingsDrawerStyle';
import ThemeActions from '../../Stores/Theme/Actions';
import { ThemeConsumer } from 'react-native-elements';
import { Themes } from '../../Theme';
import { connect } from 'react-redux';
import { translate } from '../../Translations';

const SettingLinkItem = props => {
  return <ListItem {...props} bottomDivider />;
};

class SettingsDrawer extends BaseComponent {
  componentDidMount() {
    const { user, fetchDebtor } = this.props;
    if (user.debtorId > 0) {
      fetchDebtor(user.debtorId);
    }
  }

  componentDidUpdate(prevProps) {
    const { user, fetchDebtor } = this.props;
    if (user.debtorId > 0 && prevProps.user.debtorId !== user.debtorId) {
      fetchDebtor(user.debtorId);
    }
  }

  render() {
    const { user, debtor } = this.props;
    return (
      <ThemeConsumer>
        {({ theme }) => (
          <ScrollView>
            <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
              <View style={Style.profilePanel(theme)}>
                <Avatar
                  size="medium"
                  rounded
                  icon={{ name: 'person', color: theme.colors.primary }}
                  overlayContainerStyle={Style.avatar(theme)}
                />
                <View style={Style.userInfoPanel}>
                  <Text h2 h2Style={Style.username(theme)}>
                    {user.username}
                  </Text>
                  <Text style={Style.userInfo(theme)}>
                    {user.firstname} {user.lastname}
                  </Text>
                  <Text style={Style.userInfo(theme)}>{user.email}</Text>

                  {this.renderDebtor(theme, debtor)}
                </View>
              </View>
              <SettingLinkItem
                title={translate('my_orders')}
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.props.navigation.navigate('OrdersScreen');
                }}
              />
              <SettingLinkItem
                title={translate('my_notifications')}
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.props.navigation.navigate('NotificationsScreen');
                }}
              />
              <SettingLinkItem
                title={translate('manage_address')}
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.props.navigation.navigate('AddressesScreen');
                }}
              />
              <SettingLinkItem
                title={translate('change_password')}
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.props.navigation.navigate('ChangePasswordScreen');
                }}
              />
              <SettingLinkItem
                title={translate('logout')}
                onPress={() => {
                  this.props.logout();
                }}
              />
              <LocalesPicker />
            </SafeAreaView>
          </ScrollView>
        )}
      </ThemeConsumer>
    );
  }

  renderDebtor(theme, debtor) {
    if (debtor !== null) {
      return <Text style={Style.debtorname(theme)}>{debtor.companyName1}</Text>;
    }
  }
}

const mapStateToProps = state => ({
  user: state.app.user,
  debtor: state.debtor.debtor,
  local: state.app.locale,
  errorMessages: state.app.errorMessages,
  successMessages: state.app.successMessages
});

const mapDispatchToProps = dispatch => ({
  fetchDebtor: debtorId => dispatch(DebtorActions.fetchDebtor(debtorId)),
  logout: () => dispatch(AppActions.logout()),
  lightTheme: () => dispatch(ThemeActions.setTheme(Themes.light)),
  darkTheme: () => dispatch(ThemeActions.setTheme(Themes.dark))
});

SettingsDrawer.propTypes = {
  locale: PropTypes.string,
  user: PropTypes.object,
  debtor: PropTypes.object,
  username: PropTypes.string,
  firstname: PropTypes.string,
  lastname: PropTypes.string,
  email: PropTypes.string,

  errorMessages: PropTypes.array,
  successMessages: PropTypes.array,

  fetchDebtor: PropTypes.func,
  logout: PropTypes.func
};

SettingsDrawer.defaultProps = {
  locale: null,
  user: null,
  debtor: null,
  username: null,
  firstname: null,
  lastname: null,
  email: null,

  errorMessages: [],
  successMessages: [],

  fetchDebtor: () => {},
  logout: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsDrawer);
