import { Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  page: {
    ...Styles.page
  },
  container: {
    ...Styles.container
  },
  profilePanel: theme => {
    return {
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: theme.backgroundColors.primary,
      padding: Metrics.md
    };
  },
  userInfoPanel: {
    paddingLeft: Metrics.lg
  },
  userInfo: theme => {
    return {
      flex: 1,
      color: theme.colors.inverse
    };
  },
  username: theme => {
    return {
      flex: 1,
      color: theme.colors.inverse,
      paddingBottom: Metrics.sm
    };
  },
  debtorname: theme => {
    return {
      flex: 1,
      color: theme.colors.inverse,
      paddingTop: Metrics.lg,
      paddingBottom: Metrics.sm
    };
  },
  avatar: theme => {
    return {
      backgroundColor: theme.colors.inverse
    };
  },
  scroll: {
    flex: 1
  }
});
