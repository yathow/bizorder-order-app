import React, { Fragment } from 'react';
import { Text, View } from '@merchstack/react-native-ui';

import Style from './AddressStyle';
import { translate } from '../../Translations';

const Address = props => {
  const address = props.data;

  const renderAddressLine = lines => {
    let cLines = [];
    lines.map(line => {
      if (line && line.trim().length > 0) {
        cLines.push(line);
      }
    });
    if (cLines.length > 0) {
      return <Text>{cLines.join(', ')}</Text>;
    } else {
      return null;
    }
  };

  const renderAttention = () => {
    if (address.attention && address.attention !== '' && address.attention.trim().length > 0) {
      return (
        <View style={Style.attentionRow}>
          <Text>{translate('attention')}</Text>
          <Text style={Style.separator}>:</Text>
          <Text>{address.attention}</Text>
        </View>
      );
    } else {
      return null;
    }
  };

  if (address) {
    return (
      <Fragment>
        {renderAddressLine([address.unitNo, address.streetName])}
        {renderAddressLine([address.buildingName])}
        {renderAddressLine([address.postcode, address.areaName])}
        {renderAddressLine([address.stateName])}
        {renderAttention()}
      </Fragment>
    );
  } else {
    return <Text>-</Text>;
  }
};

export default Address;
