import { LoadingIndicator, Picker, Text, View } from '@merchstack/react-native-ui';

import ActionSheetIOS from '../../Components/ActionSheetIOS/ActionSheetIOS';
import AddressActions from '../../Stores/Address/Actions';
import { BaseComponent } from '../../Components';
import { Platform } from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './AddressAreasPickerStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class AddressAreasPicker extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionSetAddressArea = this.actionSetAddressArea.bind(this);
  }

  componentDidMount() {
    const { stateId, fetchAddressAreas } = this.props;
    fetchAddressAreas(stateId);
  }

  componentDidUpdate(prevProps) {
    const { address, fetchAddressAreas } = this.props;
    if (prevProps.address.stateId !== address.stateId) {
      fetchAddressAreas(address.stateId);
    }
  }

  renderStates() {
    const { areas } = this.props;

    let elements = [];
    elements.push(<Picker.Item key={0} label={translate('select_area')} value={0} />);
    if (areas && areas.length > 0) {
      areas.map(area => {
        elements.push(<Picker.Item key={area.id} label={area.code} value={area.id} />);
      });
    }
    return elements;
  }

  render() {
    const { address, isLoading, areas } = this.props;
    const areaId = address !== null && address.areaId !== null ? address.areaId : 0;

    if (isLoading) {
      return <LoadingIndicator size="large" />;
    } else {
      return (
        <View style={Style.content}>
          {Platform.OS === 'ios' ? (
            <ActionSheetIOS
              list={areas}
              filter={'code'}
              referenceHandler={this.actionSetAddressArea}
              placeholder={'select_area'}
              selectedValue={areaId}
            />
          ) : (
            <Picker
              style={Style.divisionPicker}
              selectedValue={areaId}
              onValueChange={itemValue => this.actionSetAddressArea(itemValue)}
            >
              {this.renderStates()}
            </Picker>
          )}

          {this.renderErrorMessage()}
        </View>
      );
    }
  }

  renderErrorMessage() {
    const { errorMessage, theme } = this.props;
    if (errorMessage) {
      return <Text style={Style.error(theme)}>{errorMessage}</Text>;
    } else {
      return null;
    }
  }

  actionSetAddressArea(areaId) {
    const { setAddressArea } = this.props;

    setAddressArea(areaId);
  }
}

const mapStateToProps = state => ({
  address: state.address.address,
  areas: state.address.areas,
  stateId: state.address.address.stateId,
  areaId: state.address.address.areaId,
  isLoading: state.address.areasIsLoading
});

const mapDispatchToProps = dispatch => ({
  fetchAddressAreas: stateId => dispatch(AddressActions.fetchAddressAreas(stateId)),
  setAddressArea: areaId => dispatch(AddressActions.setAddressArea(areaId))
});

AddressAreasPicker.propTypes = {
  address: PropTypes.object,
  areas: PropTypes.array,
  isLoading: PropTypes.bool,
  stateId: PropTypes.number,
  areaId: PropTypes.number,

  fetchAddressStates: PropTypes.func,
  setAddressState: PropTypes.func
};

AddressAreasPicker.defaultProps = {
  address: {},
  areas: [],
  isLoading: false,
  stateId: 0,
  areaId: 0,

  fetchAddressAreas: () => {},
  setAddressArea: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(AddressAreasPicker));
