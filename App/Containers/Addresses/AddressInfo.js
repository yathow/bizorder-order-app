import { Icon, Text, View } from '@merchstack/react-native-ui';
import React, { Fragment } from 'react';

import Address from './Address';
import Style from './AddressInfoStyle';

const AddressInfo = props => {
  const address = props.data;

  const renderContact = () => {
    if (address && address.phoneNo1) {
      return (
        <Fragment>
          <Icon name="phone" size={14} />
          <Text style={Style.phone}>{address.phoneNo1}</Text>
        </Fragment>
      );
    } else {
      return null;
    }
  };

  return (
    <View style={Style.row}>
      <View style={Style.addressPanel}>
        <Address data={address} />
      </View>
      <View style={Style.contactPanel}>{renderContact()}</View>
    </View>
  );
};

export default AddressInfo;
