import { BaseComponent, EmptyComponent } from '../../Components';
import { Button, FlatList, View, Text } from '@merchstack/react-native-ui';

import AddressActions from '../../Stores/Address/Actions';
import AddressInfo from './AddressInfo';
import AddressesActions from '../../Stores/Addresses/Actions';
import DeleteAddressDialog from './DeleteAddressDialog';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './AddressListStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withNavigation } from 'react-navigation';
import { withTheme } from 'react-native-elements';

class AddressesList extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionEditAddress = this.actionEditAddress.bind(this);
    this.actionConfirmDelete = this.actionConfirmDelete.bind(this);
  }

  componentDidMount() {
    this.actionFetchAddress();
  }

  componentDidUpdate() {
    const { refreshAddresses } = this.props;
    if (refreshAddresses) {
      this.actionFetchAddress();
    }
  }

  render() {
    const { addresses, isLoading } = this.props;
    return (
      <>
        <DeleteAddressDialog />
        <FlatList
          data={addresses}
          refreshing={false}
          isLoading={isLoading}
          onRefresh={this.fetchAddresses}
          keyExtractor={delAddr => delAddr.id.toString()}
          renderItem={this.renderDeliveryAddress}
          ListEmptyComponent={
            <EmptyComponent title={translate('no_results_found')} isLoading={isLoading} />
          }
        />
      </>
    );
  }

  renderDeliveryAddress = data => {
    const { theme, fcmToken } = this.props;

    const rowStyle =
      (data.index + 1) % 2 === 0 ? Style.delAddrRowEven(theme) : Style.delAddrRowOdd(theme);

    const delAddr = data.item;
    return (
      <View style={rowStyle}>
        <AddressInfo data={delAddr} />
        <View style={Style.btnPanel}>
          <Button
            status="info"
            buttonStyle={Style.btn}
            containerStyle={Style.btnContainer}
            onPress={() => this.actionEditAddress(delAddr.id)}
            title={translate('edit')}
          />
          <Button
            status="danger"
            buttonStyle={Style.btn}
            containerStyle={Style.btnContainer}
            onPress={() => this.actionConfirmDelete(delAddr.id)}
            title={translate('delete')}
          />
        </View>
      </View>
    );
  };

  actionEditAddress = addressId => {
    const { navigation, showAddress } = this.props;
    showAddress(addressId);
    navigation.navigate('AddressScreen');
  };

  actionConfirmDelete = addressId => {
    const { setModalDeleteVisible } = this.props;
    setModalDeleteVisible(true, addressId);
  };

  actionFetchAddress = () => {
    const { fetchAddresses } = this.props;
    fetchAddresses();
  };
}

const mapStateToProps = state => ({
  addresses: state.addresses.addresses,
  isLoading: state.addresses.addressesIsLoading,
  refreshAddresses: state.addresses.refreshAddresses,
  modalDeleteVisible: state.address.modalDeleteVisible,
  fcmToken: state.app.fcmToken
});

const mapDispatchToProps = dispatch => ({
  fetchAddresses: () => dispatch(AddressesActions.fetchAddresses()),
  showAddress: addressId => dispatch(AddressActions.showAddress(addressId)),
  deleteAddress: addressId => dispatch(AddressActions.deleteAddress(addressId)),
  setModalDeleteVisible: (modalDeleteVisible, addressId) =>
    dispatch(AddressActions.setModalDeleteVisible(modalDeleteVisible, addressId))
});

AddressesList.propTypes = {
  addresses: PropTypes.array,
  isLoading: PropTypes.bool,
  refreshAddresses: PropTypes.bool,

  fetchAddresses: PropTypes.func,
  setModalDeleteVisible: PropTypes.func
};

AddressesList.defaultProps = {
  addresses: [],
  isLoading: false,
  refreshAddresses: false,

  fetchAddresses: () => {},
  setModalDeleteVisible: () => {}
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withNavigation(withTheme(AddressesList)));
