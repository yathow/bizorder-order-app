import { Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

const row = {
  padding: Metrics.md
};

export default StyleSheet.create({
  delAddrRowOdd: theme => ({
    backgroundColor: theme.backgroundColors.odd,
    ...row
  }),
  delAddrRowEven: theme => ({
    backgroundColor: theme.backgroundColors.even,
    ...row
  }),
  state: {},
  phone: {
    marginLeft: Metrics.sm
  },
  addressPanel: {
    flex: 1
  },
  contactPanel: {
    width: 120,
    flexDirection: 'row',
    alignItems: 'center'
  },
  btnPanel: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingTop: Metrics.xl
  },
  btnContainer: {
    marginRight: Metrics.md
  },
  btn: {
    ...Styles.smallBtn
  }
});
