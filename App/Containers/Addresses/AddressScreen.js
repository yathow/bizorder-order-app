import * as Yup from 'yup';

import { BaseComponent, BaseView } from '../../Components';
import { Button, Divider, Input, LoadingIndicator, View } from '@merchstack/react-native-ui';

import AddressActions from '../../Stores/Address/Actions';
import AddressAreasPicker from './AddressAreasPicker';
import AddressStatesPicker from './AddressStatesPicker';
import { Formik } from 'formik';
import PropTypes from 'prop-types';
import React from 'react';
import { ScrollView } from 'react-native';
import Style from './AddressScreenStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class AddressScreen extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionSetAddress = this.actionSetAddress.bind(this);
    this.actionAddAddress = this.actionAddAddress.bind(this);
    this.actionUpdateAddress = this.actionUpdateAddress.bind(this);
  }

  componentDidMount() {
    const { addressId, fetchAddress } = this.props;
    if (addressId > 0) {
      fetchAddress(addressId);
    }
  }

  componentDidUpdate(prevProps) {
    const { addressId, fetchAddress } = this.props;
    if (prevProps.addressId !== addressId && addressId > 0) {
      fetchAddress(addressId);
    }
  }

  render() {
    const { address } = this.props;
    return (
      <BaseView
        title={address.id > 0 ? translate('edit_address') : translate('add_address')}
        showMenuButton={false}
        showSearchButton={false}
        showCartButton={false}
        errorMessage={this.props.errorMessages}
      >
        {this.renderAddress()}
      </BaseView>
    );
  }

  renderAddress() {
    const { address, isLoading } = this.props;
    if (isLoading) {
      return (
        <View style={Style.content}>
          <LoadingIndicator size="large" />
        </View>
      );
    } else {
    }

    return (
      <Formik
        enableReinitialize={true}
        initialValues={address}
        onSubmit={(values, formikBag) => {
          let data = {
            id: values.id,
            unit_no: values.unitNo,
            street_name: values.addressLine1,
            building_name: values.addressLine2,
            postcode: values.postcode,
            attention: values.attention,
            phone_01: values.phoneNo1,
            state_id: values.stateId,
            area_id: values.areaId
          };
          if (address.id > 0) {
            this.actionUpdateAddress(formikBag, data);
          } else {
            this.actionAddAddress(formikBag, data);
          }
        }}
        validationSchema={Yup.object().shape({
          unitNo: Yup.string().required(translate('unit_no_is_required')),
          addressLine1: Yup.string().required(translate('street_name_is_required')),
          postcode: Yup.string().required(translate('postcode_is_required')),
          phoneNo1: Yup.string().required(translate('phone_no_is_required')),
          stateId: Yup.number().min(1, translate('state_is_required')),
          areaId: Yup.number().min(1, translate('area_is_required'))
        })}
      >
        {({
          values,
          handleChange,
          errors,
          setFieldTouched,
          dirty,
          touched,
          isSubmitting,
          isValid,
          handleSubmit
        }) => {
          return (
            <View style={Style.content}>
              <ScrollView>
                <View style={Style.checkoutPanel}>
                  <Input
                    containerStyle={Style.unitNo}
                    placeholder={translate('unit_no')}
                    value={values.unitNo}
                    onChangeText={text => {
                      this.actionSetAddress({ ...values, unitNo: text });
                    }}
                    maxLength={4}
                    errorMessage={errors['unitNo']}
                  />
                  <Input
                    placeholder={translate('street_name')}
                    value={values.addressLine1}
                    onChangeText={text => {
                      this.actionSetAddress({ ...values, addressLine1: text });
                    }}
                    errorMessage={errors['addressLine1']}
                  />
                  <Input
                    placeholder={translate('building_name')}
                    value={values.addressLine2}
                    onChangeText={text => {
                      this.actionSetAddress({ ...values, addressLine2: text });
                    }}
                    errorMessage={errors['addressLine2']}
                  />
                  <Divider />
                  <AddressStatesPicker errorMessage={errors['stateId']} />
                  <AddressAreasPicker errorMessage={errors['areaId']} />
                  <Divider />
                  <Input
                    placeholder={translate('postcode')}
                    value={values.postcode}
                    onChangeText={text => {
                      this.actionSetAddress({ ...values, postcode: text });
                    }}
                    errorMessage={errors['postcode']}
                  />
                  <Input
                    placeholder={translate('attention')}
                    value={values.attention}
                    onChangeText={text => {
                      this.actionSetAddress({ ...values, attention: text });
                    }}
                    errorMessage={errors['attention']}
                  />
                  <Input
                    placeholder={translate('phoneNo')}
                    value={values.phoneNo1}
                    onChangeText={text => {
                      this.actionSetAddress({ ...values, phoneNo1: text });
                    }}
                    errorMessage={errors['phoneNo1']}
                  />
                </View>
              </ScrollView>
              <View style={Style.btnPanel}>
                <Button
                  status="primary"
                  title={address.id > 0 ? translate('update') : translate('save')}
                  onPress={handleSubmit}
                  loading={isSubmitting}
                />
              </View>
            </View>
          );
        }}
      </Formik>
    );
  }

  actionSetAddress = values => {
    const { setAddress } = this.props;
    setAddress(values);
  };

  actionAddAddress = (formikBag, values) => {
    const { addAddress } = this.props;
    addAddress(formikBag, values);
  };

  actionUpdateAddress = (formikBag, values) => {
    const { updateAddress } = this.props;
    updateAddress(formikBag, values);
  };
}

const mapStateToProps = state => ({
  errorMessages: state.messages.errorMessages,
  addressId: state.address.addressId,
  address: state.address.address,
  isLoading: state.address.addressIsLoading
});

const mapDispatchToProps = dispatch => ({
  fetchAddress: addressId => dispatch(AddressActions.fetchAddress(addressId)),
  setAddress: values => dispatch(AddressActions.setAddress(values)),
  addAddress: (formikBag, values) => dispatch(AddressActions.addAddress(formikBag, values)),
  updateAddress: (formikBag, values) => dispatch(AddressActions.updateAddress(formikBag, values))
});

AddressScreen.propTypes = {
  errorMessages: PropTypes.array,

  fetchAddressStates: PropTypes.func,
  setAddress: PropTypes.func,
  addAddress: PropTypes.func,
  updateAddress: PropTypes.func
};

AddressScreen.defaultProps = {
  errorMessages: [],

  fetchAddressStates: () => {},
  setAddress: () => {},
  addAddress: () => {},
  updateAddress: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(AddressScreen));
