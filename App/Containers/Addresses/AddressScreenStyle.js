import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  content: {
    flex: 1
  },
  checkoutPanel: {
    flex: 1
  },
  btnPanel: {
    padding: Metrics.md
  },
  unitNo: {
    width: 120
  }
});
