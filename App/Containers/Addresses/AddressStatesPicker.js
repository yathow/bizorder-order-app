import { LoadingIndicator, Picker, Text, View } from '@merchstack/react-native-ui';

import ActionSheetIOS from '../../Components/ActionSheetIOS/ActionSheetIOS';
import AddressActions from '../../Stores/Address/Actions';
import { BaseComponent } from '../../Components';
import { Platform } from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './AddressStatesPickerStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class AddressStatesPicker extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionSetAddressState = this.actionSetAddressState.bind(this);
  }

  componentDidMount() {
    const { fetchAddressStates } = this.props;
    fetchAddressStates();
  }

  renderStates() {
    const { states } = this.props;

    let elements = [];
    elements.push(<Picker.Item key={0} label={translate('select_state')} value={0} />);
    if (states && states.length > 0) {
      states.map(cState => {
        elements.push(<Picker.Item key={cState.id} label={cState.desc1} value={cState.id} />);
      });
    }

    return elements;
  }

  render() {
    const { address, isLoading, states } = this.props;
    const stateId = address !== null && address.stateId !== null ? address.stateId : 0;
    
    if (isLoading) {
      return <LoadingIndicator size="large" />;
    } else {
      return (
        <View style={Style.content}>
          {Platform.OS === 'ios' ? (
            <ActionSheetIOS
              list={states}
              filter={'desc1'}
              referenceHandler={this.actionSetAddressState}
              placeholder={'select_state'}
              selectedValue={stateId}
            />
          ) : (
            <Picker
              style={Style.divisionPicker}
              selectedValue={stateId}
              onValueChange={itemValue => this.actionSetAddressState(itemValue)}
            >
              {this.renderStates()}
            </Picker>
          )}

          {this.renderErrorMessage()}
        </View>
      );
    }
  }

  renderErrorMessage() {
    const { errorMessage, theme } = this.props;
    if (errorMessage) {
      return <Text style={Style.error(theme)}>{errorMessage}</Text>;
    } else {
      return null;
    }
  }

  actionSetAddressState(stateId) {
    const { setAddressState } = this.props;
    setAddressState(stateId);
  }
}

const mapStateToProps = state => ({
  address: state.address.address,
  states: state.address.states,
  isLoading: state.address.statesIsLoading
});

const mapDispatchToProps = dispatch => ({
  fetchAddressStates: () => dispatch(AddressActions.fetchAddressStates()),
  setAddressState: stateId => dispatch(AddressActions.setAddressState(stateId))
});

AddressStatesPicker.propTypes = {
  address: PropTypes.object,
  states: PropTypes.array,
  isLoading: PropTypes.bool,

  fetchAddressStates: PropTypes.func,
  setAddressState: PropTypes.func
};

AddressStatesPicker.defaultProps = {
  address: {},
  states: [],
  isLoading: false,

  fetchAddressStates: () => {},
  setAddressState: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(AddressStatesPicker));
