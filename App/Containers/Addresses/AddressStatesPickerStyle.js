import { Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  content: {
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md
  },
  divisionLbl: {
    paddingRight: Metrics.md
  },
  divisionPicker: {
    flex: 1
  },
  separator: {
    ...Styles.separator
  },
  error: theme => ({
    color: theme.colors.error,
    fontSize: theme.fonts.inputError,
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md
  })
});
