import { Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  delAddrRowOdd: theme => ({
    backgroundColor: theme.backgroundColors.odd,
    ...row
  }),
  delAddrRowEven: theme => ({
    backgroundColor: theme.backgroundColors.even,
    ...row
  }),
  phone: {
    marginLeft: Metrics.sm
  },
  addressPanel: {
    flex: 1
  },
  contactPanel: {
    width: 120,
    flexDirection: 'row',
    alignItems: 'center'
  },
  btnPanel: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingTop: Metrics.md
  },
  btnContainer: {
    marginRight: Metrics.md
  },
  btn: {
    ...Styles.smallBtn
  },
  attentionRow: {
    flexDirection: 'row',
    paddingTop: Metrics.md
  },
  separator: {
    paddingRight: Metrics.md
  }
});
