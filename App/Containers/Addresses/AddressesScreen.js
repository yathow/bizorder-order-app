import { BaseComponent, BaseView } from '../../Components';

import AddressActions from '../../Stores/Address/Actions';
import AddressList from './AddressList';
import { Icon } from '@merchstack/react-native-ui';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './AddressesStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class AddressesScreen extends BaseComponent {
  constructor(props) {
    super(props);
    this.actionAddAddress = this.actionAddAddress.bind(this);
  }

  render() {
    return (
      <BaseView
        title={translate('manage_address')}
        showMenuButton={false}
        showSearchButton={false}
        showCartButton={false}
        showFilterButton={false}
        errorMessage={this.props.errorMessages}
      >
        <AddressList />
        <Icon
          containerStyle={Style.addBtn}
          name="add"
          reverse
          color={this.props.theme.colors.primary}
          onPress={this.actionAddAddress}
        />
      </BaseView>
    );
  }

  actionAddAddress = () => {
    const { navigation, showAddress } = this.props;
    showAddress(0);
    navigation.navigate('AddressScreen');
  };
}

const mapStateToProps = state => ({
  errorMessages: state.messages.errorMessages
});

const mapDispatchToProps = dispatch => ({
  showAddress: addressId => dispatch(AddressActions.showAddress(addressId))
});

AddressesScreen.propTypes = {
  errorMessages: PropTypes.array
};

AddressesScreen.defaultProps = {
  errorMessages: []
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(AddressesScreen));
