import Metrics from '../../Theme/Metrics';
import { StyleSheet } from 'react-native';
import Styles from '../../Theme/Styles';

export default StyleSheet.create({
  page: {
    ...Styles.page,
    flex: 1
  },
  container: {
    ...Styles.container,
    flex: 1
  },
  content: {
    ...Styles.content,
    flex: 1,
    paddingTop: 0
  },
  addBtn: {
    position: 'absolute',
    bottom: Metrics.sm,
    right: Metrics.sm
  }
});
