import AddressActions from '../../Stores/Address/Actions';
import { BaseComponent } from '../../Components';
import ConfirmDialog from '../../Components/Dialog/ConfirmDialog';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class DeleteAddressDialog extends BaseComponent {
  constructor(props) {
    super(props);
    this.actionConfirm = this.actionConfirm.bind(this);
    this.actionDismiss = this.actionDismiss.bind(this);
  }

  actionDismiss = () => {
    const { setModalDeleteVisible } = this.props;
    setModalDeleteVisible(false);
  };

  actionConfirm = () => {
    const { addressId, deleteAddress, setModalDeleteVisible } = this.props;
    deleteAddress(addressId);
    setModalDeleteVisible(false);
  };

  render() {
    const { modalDeleteVisible } = this.props;
    return (
      <ConfirmDialog
        isVisible={modalDeleteVisible}
        title={translate('confirm_del')}
        message={translate('confirm_del_address')}
        confirmText={translate('confirm')}
        onConfirm={this.actionConfirm}
        cancelText={translate('cancel')}
        onCancel={this.actionDismiss}
      />
    );
  }
}

DeleteAddressDialog.propTypes = {
  addressId: PropTypes.number,
  modalDeleteVisible: PropTypes.bool,

  deleteAddress: PropTypes.func,
  setModalDeleteVisible: PropTypes.func
};

DeleteAddressDialog.defaultProps = {
  addressId: 0,
  modalDeleteVisible: false,
  deleteAddress: () => {},
  setModalDeleteVisible: () => {}
};

const mapStateToProps = state => ({
  addressId: state.address.addressId,
  modalDeleteVisible: state.address.modalDeleteVisible
});

const mapDispatchToProps = dispatch => ({
  deleteAddress: addressId => dispatch(AddressActions.deleteAddress(addressId)),
  setModalDeleteVisible: (modalDeleteVisible, addressId) =>
    dispatch(AddressActions.setModalDeleteVisible(modalDeleteVisible, addressId))
});

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(DeleteAddressDialog));
