import { Text, View } from '@merchstack/react-native-ui'
import PropTypes from 'prop-types'
import React from 'react'
import { Dimensions } from 'react-native'
import { connect } from 'react-redux'
import PromotionsActions from '../../Stores/Promotions/Actions'
import { BaseComponent } from '../../Components'
import Carousel from '../../Components/Carousel/Carousel'
import Style from './AdvertisementsStyle'

const screen = Dimensions.get('screen')

class Advertisements extends BaseComponent {
    componentDidMount() {
        const { fetchPromotions } = this.props
        fetchPromotions()
    }

    componentDidUpdate(prevProps) {
        const { refreshPromotions, setRefreshPromotions, fetchPromotions, divisionId } = this.props

        if((refreshPromotions || prevProps.divisionId !== divisionId) && divisionId > 0) {
            setRefreshPromotions(false)
            fetchPromotions()
        }
    }

    render() {
        const { divisionId, promotions } = this.props
        if (divisionId > 0) {
            return (
                <View style={Style.fullWidth}>
                    <Carousel promotions={promotions} />
                </View>
            )
        } else {
            return null
        }
    }
}

const mapStateToProps = state => ({
    locale: state.app.locale,
    divisionId: state.app.divisionId,
    promotions: state.promotions.promotions,
    refreshPromotions: state.promotions.refreshPromotions
})

const mapDispatchToProps = dispatch => ({
    fetchPromotions: () => dispatch(PromotionsActions.fetchPromotions()),
    setRefreshPromotions: boolean => dispatch(PromotionsActions.setRefreshPromotions(boolean))
})

Advertisements.propTypes = {
    locale: PropTypes.string,
    divisionId: PropTypes.number
}

Advertisements.defaultProps = {
    locale: null,
    divisionId: 0
}

export default connect(mapStateToProps, mapDispatchToProps)(Advertisements)
