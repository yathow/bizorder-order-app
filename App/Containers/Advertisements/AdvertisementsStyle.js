import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    fullWidth: {
        width: "100%"
    }
})