/* eslint-disable no-console */
import React from 'react';
import { PropTypes } from 'prop-types';
import { View, TouchableOpacity } from 'react-native';
import { Button, Text } from 'react-native-elements';
import { connect } from 'react-redux'
// eslint-disable-next-line import/no-unresolved
import { RNCamera } from 'react-native-camera';
import Style from './BarcodeScannerStyle';
import BarcodeScannerActions from '../../Stores/BarcodeScanner/Actions';
import { translate, setI18nConfig, addLocaleChangedListener, removeLocaleChangedListener } from '../../Translations';

const flashModeOrder = {
  off: 'torch',
  torch: 'off',
};

class BarcodeScannerScreen extends React.PureComponent {
  constructor() {
    super();

    setI18nConfig(); // set initial config

    this.state = {
      height: 640,
      width: 360,
      flash: 'off',
      zoom: 0,
      autoFocus: 'on',
      depth: 0,
      type: 'back',
      whiteBalance: 'auto',
      ratio: '16:9',
      canDetectBarcode: true,
      barcodes: {},
      selectedBarcode: ''
    };

    this.useZoomIn = this.useZoomIn.bind(this);
    this.useZoomOut = this.useZoomOut.bind(this);
    this.useToggleFlash = this.useToggleFlash.bind(this);
    this.useConfirmBarcode = this.useConfirmBarcode.bind(this);
    this.useOnBarcodeRecognized = this.useOnBarcodeRecognized.bind(this);
    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
  }

  useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
  };

  componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);
  }

  componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);
  }

  useToggleFlash() {
    this.setState({
      flash: flashModeOrder[this.state.flash],
    });
  }

  useZoomOut() {
    this.setState({
      zoom: this.state.zoom - 0.1 < 0 ? 0 : this.state.zoom - 0.1,
    });
  }

  useZoomIn() {
    this.setState({
      zoom: this.state.zoom + 0.1 > 1 ? 1 : this.state.zoom + 0.1,
    });
  }

  useOnBarcodeRecognized({ barcodes }) {
    let oldBarcodes = this.state.barcodes;
    let processedBarcodes = {};
    let curTimestamp = Date.now();

    //loop the barcodes first, barcodes is an array, not object
    for (let a = 0; a < barcodes.length; a++) {
      let tmpBarcode = barcodes[a];

      let redLineX = this.state.width / 2;
      let redLineY = this.state.height / 2;
      //check if this barcode bound touch the red line
      if(redLineY < tmpBarcode.bounds.origin.y 
        || redLineY > (tmpBarcode.bounds.origin.y + tmpBarcode.bounds.size.height)) {
        //skip this barcode if not touch red line
        continue;
      }

      //calculate the distance to center point
      let centerX = tmpBarcode.bounds.origin.x + (tmpBarcode.bounds.size.width / 2);
      let centerY = tmpBarcode.bounds.origin.y + (tmpBarcode.bounds.size.height / 2);
      let distX = centerX - redLineX;
      let distY = centerY - redLineY;
      let distance = Math.sqrt( distX*distX + distY*distY );
      
      processedBarcodes[key] = {
        ...tmpBarcode,
        timestamp: curTimestamp,
        isSelected: false,
        distance: distance
      };

      //check with oldBarcodes
      let key = tmpBarcode.data;
      if(oldBarcodes.hasOwnProperty(key)) {
        delete oldBarcodes[key]; 
      }
    }

    //loop the remained oldBarcodes, oldBarcodes is an key/value object (associated array)
    for (let key in oldBarcodes) {
      let tmpBarcode = oldBarcodes[key];

      //verify the timestamp, if older than 1 second, then just remove it
      //the timestamp is on miliseconds
      let seconds = curTimestamp - tmpBarcode.timestamp;
      if(seconds > 500) {
        continue;
      }

      //else add to processedBarcodes
      processedBarcodes[key] = {
        ...tmpBarcode
      };
    }

    //loop processedBarcodes, check smallest distance
    let smallestDistance = Number.MAX_VALUE;
    for (let key in processedBarcodes) {
      let tmpBarcode = processedBarcodes[key];

      if(tmpBarcode.distance < smallestDistance) {
        smallestDistance = tmpBarcode.distance;
      }
    }
    //set the smallest distance to isSelected
    this.setState({
      selectedBarcode: ''
    });
    for (let key in processedBarcodes) {
      let tmpBarcode = processedBarcodes[key];

      if(tmpBarcode.distance === smallestDistance) {
        tmpBarcode.isSelected = true;
        this.setState({
          selectedBarcode: tmpBarcode.data
        });
        break;
      }      
    }

    this.setState({ 
      barcodes: processedBarcodes
    });
  }

  useConfirmBarcode() {
    this.props.scanSuccess([this.state.selectedBarcode]);
  }

  renderBarcodes = () => (
    <View style={Style.barcodesContainer}>
      {Object.values(this.state.barcodes).map(({ bounds, data, isSelected }, index) => {
        return (
          <View
            key={index}
            style={[
              isSelected === true ? Style.selectedText : Style.text,
              {
                ...bounds.size,
                left: bounds.origin.x,
                top: bounds.origin.y,
              },
            ]}
          >
          </View>
        )
      })}
    </View>
  );

  renderCamera() {
    const { canDetectBarcode } = this.state;
    return (
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={{
          flex: 1,
        }}
        type={this.state.type}
        flashMode={this.state.flash}
        autoFocus={this.state.autoFocus}
        zoom={this.state.zoom}
        whiteBalance={this.state.whiteBalance}
        ratio={this.state.ratio}
        focusDepth={this.state.depth}
        trackingEnabled
        androidCameraPermissionOptions={{
          title: translate('permission_to_use_camera'),
          message: translate('We_need_your_permission_to_use_your_camera'),
          buttonPositive: translate('ok'),
          buttonNegative: translate('cancel'),
        }}
        faceDetectionLandmarks={undefined}
        faceDetectionClassifications={undefined}
        onFacesDetected={null}
        onTextRecognized={null}
        onGoogleVisionBarcodesDetected={canDetectBarcode ? this.useOnBarcodeRecognized : null}
        googleVisionBarcodeType={RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.ALL}
      >
        <View
          style={{
            flex: 0.5,
          }}
        >
          <View
            style={{
              backgroundColor: 'transparent',
              flexDirection: 'row',
              justifyContent: 'center',
            }}
          >
            <TouchableOpacity style={Style.flipButton} onPress={this.useToggleFlash}>
              <Text style={Style.flipText}> {translate('flash')}: {this.state.flash} </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[Style.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
              onPress={this.useZoomIn}
            >
              <Text style={Style.flipText}> + </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[Style.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
              onPress={this.useZoomOut}
            >
              <Text style={Style.flipText}> - </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            flex: 0.5,
            borderTopColor: 'red',
            borderTopWidth: 1,
          }}
        >
        </View>    
        {this.state.zoom !== 0 && (
          <Text style={[Style.flipText, Style.zoomText]}>Zoom: {this.state.zoom}</Text>
        )}
        {!!canDetectBarcode && this.renderBarcodes()}
      </RNCamera>
    );
  }

  render() {
    return (
      <View style={Style.container}>
        <View
        style={[
          Style.cameraContainer,
          {
            height: this.state.height,
            width: this.state.width,
          },
        ]}
        >
          {this.renderCamera()}
        </View>
        <View style={Style.footerContainer}>
          {this.state.selectedBarcode.length === 0 ? null :
          <Button 
            style={{}}
            disabled={this.state.selectedBarcode.length === 0}
            onPress={this.useConfirmBarcode} 
            title={this.state.selectedBarcode} 
          />
          }
        </View>
      </View>
    );
  }
}

BarcodeScannerScreen.propTypes = {
  scanSuccess: PropTypes.func,
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = (dispatch) => ({
  scanSuccess: (barcodes) => dispatch(BarcodeScannerActions.scanSuccess(barcodes)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BarcodeScannerScreen)