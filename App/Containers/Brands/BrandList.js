import { Avatar, FlatList, Image, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import BrandsActions from '../../Stores/Brands/Actions';
import ItemsActions from '../../Stores/Items/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './BrandListStyle';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { withTheme } from 'react-native-elements';

class BrandList extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionSelectBrand = this.actionSelectBrand.bind(this);
  }

  componentDidMount() {
    const { divisionId, fetchBrands } = this.props;
    if (divisionId > 0) {
      fetchBrands(divisionId);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { divisionId, refreshBrands, fetchBrands } = this.props;
    if ((prevProps.divisionId !== divisionId && divisionId > 0) || refreshBrands) {
      fetchBrands(divisionId);
    }
  }

  render() {
    const { brands, isLoading } = this.props;
    return (
      <View style={Style.brands}>
          <FlatList
            horizontal={true}
            data={brands}
            keyExtractor={item => item.id.toString()}
            refreshing={false}
            isLoading={isLoading}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }) => {
              return this.renderBrand(item);
            }}
          />
      </View>
    );
  }

  renderBrand(brand) {
    return (
      <TouchableWithoutFeedback
        style={Style.brand}
        onPress={() => this.actionSelectBrand(brand.id)}
      >
        {this.renderImage(brand)}
      </TouchableWithoutFeedback>
    );
  }

  renderImage(brand) {
    const { theme } = this.props;
    if (brand.photo && brand.photo.length > 0) {
      return (
        <Image
          style={Style.image}
          resizeMode="contain"
          source={{
            uri: brand.photo
          }}
        />
      );
    } else {
      return <Avatar size={100} titleStyle={Style.imageLbl(theme)} rounded title={brand.name} />;
    }
  }

  actionSelectBrand(brandId) {
    const { selectBrand, categoryIds } = this.props;

    selectBrand(brandId, categoryIds);
  }
}

BrandList.propTypes = {
  brands: PropTypes.array,
  isLoading: PropTypes.bool,
  divisionId: PropTypes.number,
  refreshBrands: PropTypes.bool,

  resetBrands: PropTypes.func,
  fetchBrands: PropTypes.func,
  selectBrandWithCategory: PropTypes.func,
  selectBrand: PropTypes.func
};

BrandList.defaultProps = {
  brands: [],
  isLoading: false,
  divisionId: 0,
  refreshBrands: false,

  resetBrands: () => {},
  fetchBrands: () => {},
  selectBrandWithCategory: () => {},
  selectBrand: () => {}
};

const mapStateToProps = state => {
  return {
    brands: state.brands.brands,
    isLoading: state.brands.brandsIsLoading,
    divisionId: state.app.divisionId,
    refreshBrands: state.brands.refreshBrands,
    categoryIds: state.items.filters.categoryIds
  };
};

const mapDispatchToProps = dispatch => ({
  resetBrands: () => dispatch(BrandsActions.resetBrands()),
  fetchBrands: divisionId => dispatch(BrandsActions.fetchBrands(divisionId)),
  selectBrand: (brandId, categoryIds) => dispatch(ItemsActions.selectBrand(brandId, categoryIds))
});

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(BrandList));
