import { Text, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import BrandList from './BrandList';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './BrandsStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';

class Brands extends BaseComponent {
  render() {
    const { divisionId } = this.props;
    if (divisionId > 0) {
      return (
        <View>
          <Text h2 style={Style.brandsLbl}>
            {translate('brands')}
          </Text>
          <BrandList />
        </View>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => ({
  locale: state.app.locale,
  divisionId: state.app.divisionId
});

const mapDispatchToProps = dispatch => ({});

Brands.propTypes = {
  locale: PropTypes.string,
  divisionId: PropTypes.number
};

Brands.defaultProps = {
  locale: null,
  divisionId: 0
};

export default connect(mapStateToProps, mapDispatchToProps)(Brands);
