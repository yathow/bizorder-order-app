import React, { Component } from 'react'
import { BaseComponent, BaseView } from '../../Components'
import { View } from '@merchstack/react-native-ui'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Brands from './Brands'
import Style from './BrandsScreenStyle'
import BrandsActions from '../../Stores/Brands/Actions'

class BrandsScreen extends BaseComponent {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps) { 
    }

    componentWillUnmount() {
        const { fetchBrands, divisionId } = this.props
        fetchBrands(divisionId)
    }

    render() {
        const { keyword, errorMessages } = this.props
        return (
            <BaseView title={keyword} showMenuButton={false} showFilterButton={false} errorMessage={errorMessages}>
                <View style={Style.brands}>
                    <Brands horizontal={false} />
                </View>
            </BaseView>
        )
    }
}

const mapStateToProps = state => ({
    errorMessages: state.messages.errorMessages,
    keyword: state.items.filters.keyword
})

const mapDispatchToProps = dispatch => ({
    fetchBrands: divisionId => dispatch(BrandsActions.fetchBrands(divisionId))
});

BrandsScreen.propTypes = {
    errorMessages: PropTypes.array,
    keyword: PropTypes.string
}

BrandsScreen.defaultProps = {
    errorMessages: [],
    keyword: ''
}

export default connect(mapStateToProps, mapDispatchToProps)(BrandsScreen)
