import { Colors, Fonts, Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'center'
  },
  brandsLbl: {
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md,
    paddingBottom: Metrics.sm,
    paddingTop: Metrics.md
  }
});
