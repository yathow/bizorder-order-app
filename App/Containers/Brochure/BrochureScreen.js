import React from 'react';
import { View, SafeAreaView, FlatList, ActivityIndicator } from 'react-native';
import { ListItem, Image, Text, Button, Avatar, Icon, Badge } from 'react-native-elements';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import BrochureActions from '../../Stores/Brochure/Actions';
import ItemDetailsActions from '../../Stores/ItemDetails/Actions';
import BarcodeScannerActions from '../../Stores/BarcodeScanner/Actions';
import Style from './BrochureStyle';
import FiltersDialog from './FiltersDialog';
import treeChanges from 'tree-changes';
import { Images } from 'App/Theme';
import {
  translate,
  setI18nConfig,
  addLocaleChangedListener,
  removeLocaleChangedListener
} from '../../Translations';

class BrochureScreen extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      isFilterVisible: false,
      scannerToken: Date.now()
    };

    setI18nConfig(); // set initial config

    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
    this.useOnEndReached = this.useOnEndReached.bind(this);
    this.useOnRefresh = this.useOnRefresh.bind(this);
    this.useShowFilters = this.useShowFilters.bind(this);
    this.useOnScanBarcode = this.useOnScanBarcode.bind(this);

    this._renderItem = this._renderItem.bind(this);

    this.selectItem = this.selectItem.bind(this);
  }

  useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
  }

  componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);

    //fetch item first page
    this.props.resetItems();
    this.props.fetchItems(1, this.props.sorts, this.props.filters, this.props.pageSize);
  }

  componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);
  }

  componentDidUpdate(prevProps, prevState) {
    const { changedFrom, changed } = treeChanges(prevProps, this.props);
    if (
      this.state.scannerToken === this.props.scannerToken &&
      changedFrom('scannerIsSuccess', false, true)
    ) {
      this.processBarcode(this.props.barcodes);
    }

    if (changed('filters')) {
      this.props.resetItems();
      this.props.fetchItems(1, this.props.sorts, this.props.filters, this.props.pageSize);
    }
  }

  selectItem(item, index) {
    this.props.selectItem(item.id, index);
  }

  processBarcode(barcodes) {
    if (barcodes.length > 0) {
      let barcode = barcodes[0];

      //this.props.resetFilters();
      this.props.setFilters({
        barcode: barcode
      });
    } else {
      this.props.showErrorMessage(translate('no_barcode_detected'));
    }
  }

  _renderItem({ item, index, separators }) {
    return (
      <ListItem
        key={item.id}
        title={item.code}
        subtitle={item.desc_01}
        leftElement={
          <Image
            source={item.feat_icon_url ? { uri: item.feat_icon_url } : Images.noPhotoIcon}
            style={Style.thumbnail}
            resizeMode="contain"
            PlaceholderContent={<ActivityIndicator />}
          />
        }
        onPress={() => this.selectItem(item, index)}
        onLongPress={() => this.selectItem(item, index)}
        bottomDivider
        chevron
      />
    );
  }

  useOnEndReached() {
    //increase the page
    if (this.props.itemsIsLoading === false) {
      let currentPage = this.props.currentPage;
      this.props.fetchItems(
        currentPage + 1,
        this.props.sorts,
        this.props.filters,
        this.props.pageSize
      );
    }
  }

  useOnRefresh() {
    //increase the page
    if (this.props.itemsIsLoading === false) {
      //fetch item first page
      this.props.resetItems();
      this.props.fetchItems(1, this.props.sorts, this.props.filters, this.props.pageSize);
    }
  }

  useOnScanBarcode() {
    //scanner token is to identify the scan result is belong to who
    this.props.startScanner(this.state.scannerToken);
  }

  useShowFilters() {
    this.setState({ isFilterVisible: true });
  }

  render() {
    let filterCount = 0;
    for (var field in this.props.filters) {
      if (Array.isArray(this.props.filters[field])) {
        if (this.props.filters[field].length > 0) {
          filterCount++;
        }
      } else {
        if (this.props.filters[field]) {
          filterCount++;
        }
      }
    }

    return (
      <SafeAreaView style={Style.container}>
        {this.props.successMessage !== '' && (
          <View style={Style.successContainer}>
            <Text style={Style.successText}>{this.props.successMessage}</Text>
          </View>
        )}
        {this.props.errorMessage !== '' && (
          <View style={Style.errorContainer}>
            <Text style={Style.errorText}>{this.props.errorMessage}</Text>
          </View>
        )}
        <FiltersDialog
          isVisible={this.state.isFilterVisible}
          onBackdropPress={() => this.setState({ isFilterVisible: false })}
        />
        <View style={Style.topBarContainer}>
          <View>
            <Avatar
              size="medium"
              rounded
              icon={{
                name: 'filter',
                type: 'material-community',
                size: 25
              }}
              onPress={this.useShowFilters}
            />
            {filterCount > 0 && (
              <Badge
                status="primary"
                containerStyle={{ position: 'absolute', top: -5, right: -5 }}
                value={filterCount}
              />
            )}
          </View>
          <View>
            <Button
              icon={<Icon name="barcode-scan" type="material-community" size={25} />}
              onPress={this.useOnScanBarcode}
            />
          </View>
        </View>
        <View style={Style.listContainer}>
          <FlatList
            keyExtractor={(item, index) => item.id.toString()}
            data={this.props.items}
            onEndReached={this.useOnEndReached}
            onEndReachedThreshold={0.5}
            renderItem={this._renderItem}
            refreshing={this.props.itemsIsLoading}
            onRefresh={this.useOnRefresh}
          />
        </View>
      </SafeAreaView>
    );
  }
}

BrochureScreen.propTypes = {
  items: PropTypes.array,
  itemsIsLoading: PropTypes.bool,
  currentPage: PropTypes.number,
  pageSize: PropTypes.string,
  successMessage: PropTypes.string,
  errorMessage: PropTypes.string,
  resetItems: PropTypes.func,
  fetchItems: PropTypes.func,
  selectItem: PropTypes.func
};

const mapStateToProps = state => ({
  items: state.brochure.items,
  itemsIsLoading: state.brochure.itemsIsLoading,
  sorts: state.brochure.sorts,
  filters: state.brochure.filters,
  currentPage: state.brochure.currentPage,
  pageSize: state.brochure.pageSize,
  successMessage: state.brochure.successMessage,
  errorMessage: state.brochure.errorMessage,

  scannerToken: state.barcodeScanner.token,
  barcodes: state.barcodeScanner.barcodes,
  scannerIsSuccess: state.barcodeScanner.scannerIsSuccess
});

const mapDispatchToProps = dispatch => ({
  resetItems: () => dispatch(BrochureActions.resetItems()),
  fetchItems: (page, sorts, filters, pageSize) =>
    dispatch(BrochureActions.fetchItems(page, sorts, filters, pageSize)),
  setFilters: filters => dispatch(BrochureActions.setFilters(filters)),
  resetFilters: () => dispatch(BrochureActions.resetFilters()),

  selectItem: (itemId, index) => dispatch(ItemDetailsActions.selectItem(itemId, index)),

  startScanner: token => dispatch(BarcodeScannerActions.startScanner(token)),
  showErrorMessage: errorMessage => dispatch(BarcodeScannerActions.showErrorMessage(errorMessage))
});

export default connect(mapStateToProps, mapDispatchToProps)(BrochureScreen);
