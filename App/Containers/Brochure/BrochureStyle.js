import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    topBarContainer: {
        flex: 0.1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10
    },
    listContainer: {
        flex: 0.9,
        flexDirection: 'column',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10
    },
    thumbnail: {
        width: 100, 
        height: 100
    },
    formContainer: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10
    },
    formElement: {
        marginTop: 10,
    },
    filterButtonsContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    errorContainer: {
        flex: 0.1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFB6C1'
    },
    errorText: {
        marginLeft: 10,
        color: 'red'
    },
    successContainer: {
        flex: 0.1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#98FB98'
    },
    successText: {
        marginLeft: 10,
        color: 'green'
    }
})
