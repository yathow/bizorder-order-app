import React from 'react';
import { ScrollView, View } from 'react-native';
import { Button, Overlay, Input, Divider, Text } from 'react-native-elements';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import Style from './FiltersStyle';
import { translate, setI18nConfig, addLocaleChangedListener, removeLocaleChangedListener } from '../../Translations';
import { MultiSelectButton } from '../../Components/MultiSelectButton';
import BrochureActions from '../../Stores/Brochure/Actions';
import treeChanges from 'tree-changes';
import * as Yup from 'yup';
import { Formik } from 'formik';

class FiltersDialog extends React.PureComponent {
    constructor() {
        super();

        setI18nConfig(); // set initial config

        this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
    }

    useOnLocalizationChange() {
        setI18nConfig();
        this.forceUpdate();
    }

    componentDidMount() {
        addLocaleChangedListener(this.handleLocalizationChange);

        this.props.resetItemGroup01s();
        this.props.fetchAllItemGroup01s();
    }

    componentWillUnmount() {
        removeLocaleChangedListener(this.handleLocalizationChange);
    }

    componentDidUpdate(prevProps, prevState) {

    }

    render() {
        const itemGroup01SelectItems = this.props.itemGroup01s.map(itemGroup01 => {
            return { 
                value: itemGroup01.code, 
                label: itemGroup01.desc_01 
            };
        });

        return (
            <Overlay
                fullScreen={true}
                isVisible={this.props.isVisible}
                onBackdropPress={this.props.onBackdropPress}
            >
                <View style={Style.container}>
                    <Formik
                        enableReinitialize={true}
                        initialValues={this.props.filters}
                        onReset={(values, formikBag) => {
                            this.props.resetFilters();
                        }}
                        onSubmit={(values, formikBag) => {
                            this.props.setFilters(values);
                            this.props.onBackdropPress();
                        }}
                        validationSchema={Yup.object().shape({
                            
                        })}
                    >
                        {({ values, handleChange, errors, setFieldValue, setFieldTouched, dirty, touched, isSubmitting, isValid, handleSubmit, handleReset }) => (
                        <ScrollView>
                            <Text h4 style={Style.title}>{translate('details')}</Text>
                            <Input
                                containerStyle={Style.formElement}
                                value={values.barcode}
                                onChangeText={handleChange('barcode')}
                                placeholder={translate('barcode')}
                                errorMessage={errors.barcode}
                                disabled={isSubmitting}
                            />
                            <Input
                                containerStyle={Style.formElement}
                                value={values.code}
                                onChangeText={handleChange('code')}
                                placeholder={translate('code')}
                                errorMessage={errors.code}
                                disabled={isSubmitting}
                            />
                            <Input
                                containerStyle={Style.formElement}
                                value={values.desc}
                                onChangeText={handleChange('desc')}
                                placeholder={translate('description')}
                                errorMessage={errors.desc}
                                disabled={isSubmitting}
                            />
                            <Divider style={Style.divider} />
                            <Text h4 style={Style.title}>{translate('brand')}</Text>
                            <MultiSelectButton
                                containerStyle={Style.selectButtonContainer}
                                selectedValue={values.item_group01_code_in}
                                onValueChange={handleChange('item_group01_code_in')}
                                items={itemGroup01SelectItems}
                                errorMessage={errors.item_group01_code_in}
                                disabled={isSubmitting}
                            />
                            <Divider style={Style.divider} />
                            <View style={Style.filterButtonsContainer}>
                                <Button 
                                    containerStyle={{...Style.formElement, flex: 0.5}}
                                    loading={isSubmitting}
                                    onPress={handleReset}
                                    title={translate('reset')}
                                />
                                <Button 
                                    containerStyle={{...Style.formElement, flex: 0.5}}
                                    disabled={!isValid}
                                    loading={isSubmitting}
                                    onPress={handleSubmit}
                                    title={translate('done')}
                                />
                            </View>
                        </ScrollView>
                        )}
                    </Formik>
                </View>
            </Overlay>
        );
    }
}

FiltersDialog.propTypes = {
    isVisible: PropTypes.bool,
    itemGroup01s: PropTypes.array,

    resetItemGroup01s: PropTypes.func,
    fetchAllItemGroup01s: PropTypes.func,
}

FiltersDialog.defaultProps = {
    isVisible: false,
    onBackdropPress: () => {}
};


const mapStateToProps = (state) => ({
    filters: state.brochure.filters,
    itemGroup01s: state.brochure.itemGroup01s
})
  
const mapDispatchToProps = (dispatch) => ({
    resetItemGroup01s: () => dispatch(BrochureActions.resetItemGroup01s()),
    fetchAllItemGroup01s: () => dispatch(BrochureActions.fetchAllItemGroup01s()),
    setFilters: (filters) => dispatch(BrochureActions.setFilters(filters)),
    resetFilters: () => dispatch(BrochureActions.resetFilters()),
})
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FiltersDialog)
