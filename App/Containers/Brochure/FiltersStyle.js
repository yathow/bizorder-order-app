import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10
    },
    filterButtonsContainer: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 30
    },
    title: {
        marginTop: 10
    },
    divider: {
        marginTop: 30
    },
    selectButtonContainer: {
        marginTop: 10
    },
})
