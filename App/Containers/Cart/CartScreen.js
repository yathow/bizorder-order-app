import { Button, FlatList, Text, View, Card } from '@merchstack/react-native-ui';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Style from './CartScreenStyle';
import { connect } from 'react-redux';
import { formatCurrency } from '../../Formatter/NumberFormat';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';
import { BaseView } from '../../Components';

import CartActions from '../../Stores/Cart/Actions';
import ItemDetailsActions from '../../Stores/ItemDetails/Actions';
import ItemQtyActions from '../../Stores/ItemQty/Actions';
import PromotionsActions from '../../Stores/Promotions/Actions'

import CartSuggestions from './CartSuggestions';
import ItemImage from '../ItemDetails/ItemImage';
import ItemInfo from '../ItemDetails/ItemInfo';
import ItemQty from '../ItemQty/ItemQty';
import ItemPromotionTags from '../ItemDetails/ItemPromotionTags';
import NoCartItems from './NoCartItems';

class CartScreen extends Component {
  constructor(props) {
    super(props);

    this.actionCheckout = this.actionCheckout.bind(this);
    this.actionRefreshCart = this.actionRefreshCart.bind(this);
    this.actionShowItemDetails = this.actionShowItemDetails.bind(this);
    this.actionShowItemQty = this.actionShowItemQty.bind(this);
    this.actionSetItemQtyInCart = this.actionSetItemQtyInCart.bind(this);
  }

  componentDidMount() {
    const { divisionId, refreshCartPromotions } = this.props;
    refreshCartPromotions()
  }

  componentDidUpdate(prevProps) {
    const { divisionId, refreshCart, setRefreshCart, refreshCartPromotions } = this.props;
    if (refreshCart || prevProps.divisionId !== divisionId) {
      refreshCartPromotions()
      setRefreshCart(false)
    }
  }

  render() {
    const { errorMessages, cart, isLoading } = this.props;
    return (
      <BaseView
        title={translate('shopping_cart')}
        showMenuButton={false}
        showSearchButton={false}
        showFilterButton={false}
        errorMessage={errorMessages}
      >
        <ItemQty />
        <FlatList
          data={cart.details}
          refreshing={false}
          isLoading={isLoading}
          onRefresh={this.actionRefreshCart}
          ListEmptyComponent={this.renderEmptyList()}
          keyExtractor={detail => detail.id.toString()}
          renderItem={this.renderDetail}
        />
        {this.renderFooter()}
      </BaseView>
    );
  }

  renderEmptyList = () => {
    const { isLoading } = this.props;
    return isLoading ? null : <NoCartItems />;
  };

  renderDetail = data => {
    const { theme, route } = this.props;

    const even = (data.index + 1) % 2 === 0;

    const detail = data.item;
    const items = detail.item;
    const uom = detail.uom;
    const uoms = [uom];
    
    return (
      <View style={even ? Style.itemRowEven(theme) : Style.itemRowOdd(theme)}>
        {detail.status === 4
        && (
          <View style={Style.itemRowDisabled}>
            <Card 
              containerStyle={Style.itemCardContainer(theme)}
              title="Warning"
              titleStyle={Style.itemCardTitle(theme)}
            >
              <Text style={{textAlign: 'center'}}>There are some changes in the promotion for this item.</Text>
              <View style={Style.itemCardFooter}>
                <View style={Style.btnMargin}>
                <Button
                  status="primary"
                  buttonStyle={Style.deleteBtn}
                  title={translate('view_changes')}
                  onPress={() => {
                    this.actionShowItemDetails(detail)
                  }}
                />
                </View>
                <View style={Style.btnMargin}>
                <Button
                  status="secondary"
                  buttonStyle={Style.deleteBtn}
                  title={translate('confirm')}
                  onPress={() => {
                    this.actionConfirm(detail)
                  }}
                />
                </View>
              </View>
            </Card>
          </View>
        )}
        <CartSuggestions suggestions={detail.suggestions} />
        <View style={Style.itemContent}>
          <ItemImage
            style={Style.itemImage}
            photos={items.photos}
            onPress={() => this.actionShowItemDetails(items.id)}
          />
          <View style={Style.itemInfo}>
            <ItemInfo item={items} uoms={uoms} onPress={() => this.actionShowItemDetails(items.id)} />
            {/* {this.renderPromotion(detail)} */}
            <ItemPromotionTags item={detail} />
          </View>
        </View>
        {this.renderRowQty(detail, items)}
      </View>
    );
  };

  renderRowQty = (detail, items) => {
    const { theme } = this.props;
    
    if (detail.isFoc) {
      return (
        <View style={Style.subtotalRow}>
          <Text style={Style.qtyLbl}>{translate('foc_qty')}</Text>
          <Text style={Style.separator}>: </Text>
          <Text style={Style.qtyLink(theme)}>{detail.qty}</Text>
          <Text style={Style.subtotal(theme)}>{formatCurrency(detail.netAmt)}</Text>
        </View>
      );
    } else {
      return (
        <View style={Style.subtotalRow}>
          <Text style={Style.qtyLbl}>{translate('order_qty')}</Text>
          <Text style={Style.separator}>: </Text>
          <Text style={Style.qtyLink(theme)} onPress={() => this.actionShowItemQty(items.id)}>
            {detail.qty}
          </Text>
          <Button
            status="danger"
            disabled={detail.status == 4 ? true : false}
            buttonStyle={Style.deleteBtn}
            title={translate('delete')}
            onPress={() => this.actionSetItemQtyInCart(detail.itemId, detail.uomId, 0)}
          />
          <Text style={Style.subtotal(theme)}>{formatCurrency(detail.netAmt)}</Text>
        </View>
      );
    }
  };

  renderPromotion = detail => {
    const { theme } = this.props;

    const hasPromotion = detail.promotion;
    if (hasPromotion) {
      return (
        <View style={Style.detailPromoRow}>
          <Text style={Style.promo(theme)}>*{detail.promotion.desc1}</Text>
        </View>
      );
    } else {
      return null;
    }
  };

  renderCheckout = () => {
    const { theme, cart } = this.props;

    let details = cart.details
    let disabled = false

    if(details.length > 0) {
      disabled = details.some(detail => {
        return detail.status === 4
      })
    }

    return (
      <Button
        status="primary"
        title={translate("checkout")}
        onPress={this.actionCheckout}
        disabled={disabled}
      />
    );
  };

  renderFooter = () => {
    const { cart, theme } = this.props;
    if (cart.details.length > 0) {
      return (
        <View style={Style.footerPanel(theme)}>
          <View style={Style.totalPanel(theme)}>
            <Text style={Style.totalAmtLbl(theme)}>{translate('total')}</Text>
            <Text style={Style.totalAmt(theme)}>{formatCurrency(cart.totalAmt)}</Text>
          </View>
          <View style={Style.buttonPanel(theme)}>{this.renderCheckout()}</View>
        </View>
      );
    } else {
      return null;
    }
  };

  actionCheckout = () => {
    const { goToDelivery } = this.props;

    goToDelivery()
  };

  actionRefreshCart = () => {
    const { refreshCartPromotions } = this.props;
    refreshCartPromotions();
  };

  actionShowItemDetails = detail => {
    const { showItemDetails, resolveConflict } = this.props;
    resolveConflict(detail.id)
    showItemDetails(detail.itemId)
  };

  actionConfirm = detail => {
    const { resolveConflict } = this.props
    resolveConflict(detail.id)
  }

  actionShowItemQty = itemId => {
    const { showItemQty } = this.props;
    showItemQty(itemId);
  };

  actionSetItemQtyInCart = (itemId, uomId, qty) => {
    const { setItemQtyInCart } = this.props;
    setItemQtyInCart(itemId, uomId, qty);
  };
}

const mapStateToProps = state => ({
  divisionId: state.app.divisionId,
  cart: state.cart.cart,
  refreshCart: state.cart.refreshCart,
  isLoading: state.cart.cartIsLoading,
  errorMessages: state.messages.errorMessages,
  route: state.app.route
});

const mapDispatchToProps = dispatch => ({
  fetchCart: divisionId => dispatch(CartActions.fetchCart(divisionId)),
  setItemQtyInCart: (itemId, uomId, qty) =>
    dispatch(CartActions.setItemQtyInCart(itemId, uomId, qty)),
  showItemQty: itemId => dispatch(ItemQtyActions.showItemQty(itemId)),
  showItemDetails: itemId => dispatch(ItemDetailsActions.showItemDetails(itemId)),
  goToDelivery: () => dispatch(CartActions.goToDelivery()),
  setRefreshCart: status => dispatch(CartActions.setRefreshCart(status)),
  refreshCartPromotions: () =>dispatch(CartActions.refreshCartPromotions()),
  resolveConflict: (id) => dispatch(CartActions.resolveConflict(id))
});

CartScreen.propTypes = {
  divisionId: PropTypes.number,
  cart: PropTypes.object,
  refreshCart: PropTypes.bool,
  isLoading: PropTypes.bool,
  errorMessages: PropTypes.array,

  fetchCart: PropTypes.func,
  clearMessages: PropTypes.func,
  setItemQtyInCart: PropTypes.func,
  showItemQty: PropTypes.func,
  showItemDetails: PropTypes.func,
  goToDelivery: PropTypes.func
};

CartScreen.defaultProps = {
  divisionId: 0,
  cart: {},
  refreshCart: false,
  isLoading: false,
  errorMessages: [],

  fetchCart: () => {},
  clearMessages: () => {},
  setItemQtyInCart: () => {},
  showItemQty: () => {},
  showItemDetails: () => {},
  goToDelivery: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(CartScreen));
