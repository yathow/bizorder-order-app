import { Fonts, Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  page: {
    flex: 1
  },
  container: {
    ...Styles.container
  },
  content: {
    ...Styles.content
  },
  headerLbl: {
    ...Styles.headerLbl,
    flex: 1
  },
  buttonPanel: theme => ({
    padding: Metrics.md
  }),
  totalPanel: theme => ({
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: Metrics.sm,
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md,
    paddingTop: Metrics.md
  }),
  footerPanel: theme => ({}),
  checkoutBtn: theme => ({
    flex: 1
  }),
  totalAmtLbl: theme => ({
    flex: 1,
    textAlign: 'right'
  }),
  totalAmt: theme => ({
    ...Fonts.h3,
    flex: 1,
    color: theme.colors.highlight,
    textAlign: 'right',
    fontWeight: 'bold'
  }),
  itemRowOdd: theme => ({
    backgroundColor: theme.backgroundColors.odd
  }),
  itemRowEven: theme => ({
    backgroundColor: theme.backgroundColors.even
  }),
  itemRowDisabled: {
    backgroundColor: `rgba(0,0,0,0.5)`,
    zIndex: 10,
    width: `100%`,
    height: `100%`,
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  itemCardContainer: theme => ({
    width: `75%`,
    backgroundColor: theme.backgroundColors.warning,
    borderWidth: 2,
    borderColor: theme.colors.secondary
  }),
  itemCardTitle: theme => ({
    color: theme.colors.secondary,
    textAlign: 'center'
  }),
  itemCardFooter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnMargin: {
    marginTop: Metrics.md,
    marginHorizontal: Metrics.sm
  },
  itemContent: {
    flexDirection: 'row',
    padding: Metrics.md
  },
  itemImage: {
    width: 100,
    height: 100
  },
  itemCode: theme => ({}),
  itemDesc: theme => ({
    color: theme.colors.primary,
    fontWeight: 'bold'
  }),
  itemInfo: {
    flex: 1,
    marginLeft: Metrics.md,
    marginRight: Metrics.md
  },
  detailPromoRow: {
    paddingTop: Metrics.md
  },
  promo: theme => ({
    color: theme.colors.secondary
  }),
  subtotalRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: Metrics.md
  },
  subtotal: theme => ({
    ...Fonts.h4,
    flex: 1,
    textAlign: 'right',
    color: theme.colors.secondary
  }),
  separator: {
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md
  },
  qtyLbl: {},
  qtyLink: theme => ({
    width: 75,
    backgroundColor: theme.backgroundColors.main,
    padding: Metrics.sm,
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md,
    marginRight: Metrics.md,
    borderWidth: 1,
    borderColor: theme.colors.input
  }),
  deleteBtn: {
    ...Styles.smallBtn
  }
});
