import { BaseComponent } from '../../Components';
import React from 'react';
import Style from './CartSuggestionStyle';
import { Text } from '@merchstack/react-native-ui';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class CartSuggestion extends BaseComponent {
  render() {
    const { theme, suggestion } = this.props;
    return <Text style={Style.content(theme)}>{this.getMessage(suggestion)}</Text>;
  }

  getMessage(suggestion) {
    const { promoDesc1, reqQty, exceedQty, uomQty } = suggestion;
    const params = { desc: promoDesc1, qty: uomQty };
    if (reqQty > 0) {
      return translate('add_n_promo', params);
    } else if (exceedQty > 0) {
      return translate('exceed_n_promo', params);
    }
  }
}

export default withTheme(CartSuggestion);
