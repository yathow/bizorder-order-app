import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  content: theme => ({
    padding: Metrics.md,
    backgroundColor: theme.backgroundColors.suggestion,
    marginBottom: 1
  })
});
