import { BaseComponent } from '../../Components';
import CartSuggestion from './CartSuggestion';
import React from 'react';
import { View } from '@merchstack/react-native-ui';

class CartSuggestions extends BaseComponent {
  render() {
    const { suggestions } = this.props;
    if (suggestions) {
      return (
        <View>
          {suggestions.map(suggestion => (
            <CartSuggestion suggestion={suggestion} />
          ))}
        </View>
      );
    } else {
      return null;
    }
  }
}

export default CartSuggestions;
