import { Text, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import React from 'react';
import Style from './NoCartItemsStyle';
import { translate } from '../../Translations';

class NoCartItems extends BaseComponent {
  render() {
    return (
      <View style={Style.content}>
        <Text>{translate('empty_cart')}</Text>
      </View>
    );
  }
}

export default NoCartItems;
