import { Text, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import CategoryList from './CategoryList';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './CategoriesStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';

class Categories extends BaseComponent {
  render() {
    const { divisionId } = this.props;
    if (divisionId > 0) {
      return (
        <View>
          <Text h2 style={Style.brandsLbl}>
            {translate('categories')}
          </Text>
          <CategoryList />
        </View>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => ({
  locale: state.app.locale,
  divisionId: state.app.divisionId
});

const mapDispatchToProps = dispatch => ({});

Categories.propTypes = {
  locale: PropTypes.string,
  divisionId: PropTypes.number
};

Categories.defaultProps = {
  locale: null,
  divisionId: 0
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
