import React, { Component } from 'react'
import { BaseComponent, BaseView } from '../../Components'
import { View } from '@merchstack/react-native-ui'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Categories from './Categories'
import Style from './CategoriesScreenStyle'
import CategoriesActions from '../../Stores/Categories/Actions'
import { fetchCategories } from '../../Stores/Categories/Reducers'

class CategoriesScreen extends BaseComponent {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps) { 
    }

    componentWillUnmount() {
        const { fetchCategories, divisionId } = this.props
        fetchCategories(divisionId)
    }

    render() {
        const { keyword, errorMessages } = this.props
        return (
            <BaseView title={keyword} showMenuButton={false} showFilterButton={false} errorMessage={errorMessages}>
                <View style={Style.categories}>
                    <Categories horizontal={false}/>
                </View>
            </BaseView>
        )
    }
}

const mapStateToProps = state => ({
    errorMessages: state.messages.errorMessages,
    keyword: state.items.filters.keyword,
    divisionId: state.app.divisionId,
})

const mapDispatchToProps = dispatch => ({
    fetchCategories: divisionId => dispatch(CategoriesActions.fetchCategories(divisionId))
});

CategoriesScreen.propTypes = {
    errorMessages: PropTypes.array,
    keyword: PropTypes.string,
    divisionId: PropTypes.number,
}

CategoriesScreen.defaultProps = {
    errorMessages: [],
    keyword: '',
    divisionId: 0,
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesScreen)
