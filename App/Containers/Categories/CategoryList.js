import { Avatar, FlatList, Image, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import CategoriesActions from '../../Stores/Categories/Actions';
import ItemsActions from '../../Stores/Items/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './CategoryListStyle';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { withTheme } from 'react-native-elements';

class CategoryList extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionSelectCategory = this.actionSelectCategory.bind(this);
  }

  componentDidMount() {
    const { divisionId, fetchCategories } = this.props;
    if (divisionId > 0) {
      fetchCategories(divisionId);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { divisionId, refreshCategories, fetchCategories } = this.props;
    if ((prevProps.divisionId !== divisionId && divisionId > 0) || refreshCategories) {
      fetchCategories(divisionId);
    }
  }

  render() {
    const { categories, isLoading } = this.props;
    
    return (
      <View style={Style.brands}>
          <FlatList
            horizontal={true}
            data={categories}
            keyExtractor={item => item.id.toString()}
            refreshing={false}
            isLoading={isLoading}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }) => {
              return this.renderCategory(item);
            }}
          />
      </View>
    );
  }

  renderCategory(category) {
    return (
      <TouchableWithoutFeedback
        style={Style.brand}
        onPress={() => this.actionSelectCategory(category.id)}
      >
        {this.renderImage(category)}
      </TouchableWithoutFeedback>
    );
  }

  renderImage(brand) {
    const { theme } = this.props;
    if (brand.photo && brand.photo.length > 0) {
      return (
        <Image
          style={Style.image}
          resizeMode="contain"
          source={{
            uri: brand.photo
          }}
        />
      );
    } else {
      return <Avatar size={100} titleStyle={Style.imageLbl(theme)} rounded title={brand.name} />;
    }
  }

  actionSelectCategory(categoryId) {
    const { selectCategory, brandIds } = this.props;

      selectCategory(categoryId, brandIds);
  }
}

CategoryList.propTypes = {
  categories: PropTypes.array,
  isLoading: PropTypes.bool,
  divisionId: PropTypes.number,
  refreshCategories: PropTypes.bool,

  resetCategories: PropTypes.func,
  fetchCategories: PropTypes.func,
  fetchCategoriesByBrand: PropTypes.func,
  selectCategoryWithBrand: PropTypes.func,
  selectCategory: PropTypes.func
};

CategoryList.defaultProps = {
  categories: [],
  refreshCategories: false,
  isLoading: false,
  divisionId: 0,

  resetCategories: () => {},
  fetchCategories: () => {},
  selectCategoryWithBrand: () => {},
  selectCategory: () => {}
};

const mapStateToProps = state => ({
  categories: state.categories.categories,
  isLoading: state.categories.categoriesIsLoading,
  divisionId: state.app.divisionId,
  refreshCategories: state.categories.refreshCategories,
  brandIds: state.items.filters.brandIds
});

const mapDispatchToProps = dispatch => ({
  resetCategories: () => dispatch(CategoriesActions.resetCategories()),
  fetchCategories: divisionId => dispatch(CategoriesActions.fetchCategories(divisionId)),
  selectCategory: (categoryId, brandIds) => dispatch(ItemsActions.selectCategory(categoryId, brandIds))
});

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(CategoryList));
