import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  brands: {
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md
  },
  brand: {
    paddingRight: Metrics.md
  },
  image: {
    width: Metrics.list.category.width,
    height: Metrics.list.category.height
  },
  imageLbl: theme => ({
    fontSize: theme.fonts.small
  }),
  seperator: {
    height: Metrics.md
  }
});
