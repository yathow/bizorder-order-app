import * as Yup from 'yup';

import { BaseComponent, BaseView } from '../../Components';
import { Button, Input, View } from '@merchstack/react-native-ui';
import React, { Fragment } from 'react';

import AppActions from '../../Stores/App/Actions';
import { Formik } from 'formik';
import MessagesActions from '../../Stores/Messages/Actions';
import { PropTypes } from 'prop-types';
import Style from './ChangePasswordStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';

class ChangePasswordScreen extends BaseComponent {
  constructor() {
    super();
    this.state = {
      currentPassword: '',
      newPassword: '',
      retypeNewPassword: ''
    };
  }

  componentDidMount() {
    const { clearMessages } = this.props;

    clearMessages();
  }

  render() {
    let initialValues = {
      currentPassword: this.state.currentPassword,
      newPassword: this.state.newPassword,
      retypeNewPassword: this.state.retypeNewPassword
    };

    return (
      <BaseView
        title={translate('change_password')}
        showMenuButton={false}
        showSearchButton={false}
        showCartButton={false}
        showFilterButton={false}
        errorMessage={this.props.errorMessages}
        successMessage={this.props.successMessages}
      >
        <View style={Style.content}>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            onSubmit={(values, formikBag) => {
              let currentPassword = values.currentPassword;
              let newPassword = values.newPassword;
              let retypeNewPassword = values.retypeNewPassword;
              //save to state
              this.setState({
                currentPassword: currentPassword,
                newPassword: newPassword,
                retypeNewPassword: retypeNewPassword
              });
              //dispatch the action
              this.props.changePassword(formikBag, currentPassword, newPassword);
            }}
            validationSchema={Yup.object().shape({
              currentPassword: Yup.string().required(translate('current_password_is_required')),
              newPassword: Yup.string().required(translate('new_password_is_required')),
              retypeNewPassword: Yup.string().test(
                'passwords-match',
                translate('passwords_do_not_match'),
                function(value) {
                  return this.parent.newPassword === value;
                }
              )
            })}
          >
            {({
              values,
              handleChange,
              errors,
              setFieldTouched,
              dirty,
              touched,
              isSubmitting,
              isValid,
              handleSubmit
            }) => (
              <Fragment>
                <Input
                  containerStyle={Style.formElement}
                  value={values.currentPassword}
                  onChangeText={handleChange('currentPassword')}
                  placeholder={translate('current_password')}
                  secureTextEntry={true}
                  errorMessage={errors.currentPassword}
                  disabled={isSubmitting}
                />
                <Input
                  containerStyle={Style.formElement}
                  value={values.newPassword}
                  onChangeText={handleChange('newPassword')}
                  placeholder={translate('new_password')}
                  secureTextEntry={true}
                  errorMessage={errors.newPassword}
                  disabled={isSubmitting}
                />
                <Input
                  containerStyle={Style.formElement}
                  value={values.retypeNewPassword}
                  onChangeText={handleChange('retypeNewPassword')}
                  placeholder={translate('retype_new_password')}
                  secureTextEntry={true}
                  errorMessage={errors.retypeNewPassword}
                  disabled={isSubmitting}
                />
                <Button
                  status="primary"
                  containerStyle={Style.confirmBtn}
                  disabled={!isValid}
                  loading={isSubmitting}
                  onPress={handleSubmit}
                  title={translate('change_password')}
                />
              </Fragment>
            )}
          </Formik>
        </View>
      </BaseView>
    );
  }
}

const mapStateToProps = state => ({
  successMessages: state.messages.successMessages,
  errorMessages: state.messages.errorMessages
});

const mapDispatchToProps = dispatch => ({
  changePassword: (formikBag, currentPassword, newPassword) =>
    dispatch(AppActions.changePassword(formikBag, currentPassword, newPassword)),
  clearMessages: () => dispatch(MessagesActions.clearMessages())
});

ChangePasswordScreen.propTypes = {
  successMessages: PropTypes.array,
  errorMessages: PropTypes.array,

  changePassword: PropTypes.func,
  clearMessages: PropTypes.func
};

ChangePasswordScreen.defaultProps = {
  successMessages: [],
  errorMessages: [],

  changePassword: () => {},
  clearMessages: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangePasswordScreen);
