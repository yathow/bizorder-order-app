import Metrics from '../../Theme/Metrics';
import { StyleSheet } from 'react-native';
import Styles from '../../Theme/Styles';

export default StyleSheet.create({
  container: {
    ...Styles.container
  },
  content: {
    ...Styles.content
  },
  formElement: {},
  confirmBtn: {
    marginTop: Metrics.md
  }
});
