import { BaseComponent, BaseView } from '../../Components';
import { Button, View } from '@merchstack/react-native-ui';

import CartActions from '../../Stores/Cart/Actions';
import MessagesActions from '../../Stores/Messages/Actions';
import Order from '../Orders/Order';
import React from 'react';
import Style from './CheckoutScreenStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class CheckoutScreen extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionPlaceOrder = this.actionPlaceOrder.bind(this);
  }

  componentDidMount() {
    const { clearMessages, fetchCart, divisionId } = this.props;
    clearMessages();
    fetchCart(divisionId);
  }

  render() {
    const { errorMessages, cart, isLoading } = this.props;
    return (
      <BaseView
        title={translate('checkout_summary')}
        showMenuButton={false}
        showSearchButton={false}
        showCartButton={false}
        showFilterButton={false}
        errorMessage={errorMessages}
      >
        <Order order={cart} isLoading={isLoading} />
        {this.renderButtonPanel()}
      </BaseView>
    );
  }

  renderButtonPanel() {
    const { isLoading } = this.props;
    if (isLoading) {
      return null;
    } else {
      return (
        <View style={Style.btnPanel}>
          <Button
            status="primary"
            title={translate('place_order')}
            onPress={this.actionPlaceOrder}
          />
        </View>
      );
    }
  }

  actionPlaceOrder = () => {
    const { clearMessages, placeOrder, cart } = this.props;
    clearMessages();
    placeOrder(cart.id);
  };
}

const mapStateToProps = state => ({
  divisionId: state.app.divisionId,
  cart: state.cart.cart,
  orderId: state.cart.orderId,
  isLoading: state.cart.cartIsLoading,
  successMessages: state.messages.successMessages,
  errorMessages: state.messages.errorMessages
});

const mapDispatchToProps = dispatch => ({
  fetchCart: divisionId => dispatch(CartActions.fetchCart(divisionId)),
  clearMessages: () => dispatch(MessagesActions.clearMessages()),
  placeOrder: cartId => dispatch(CartActions.placeOrder(cartId))
});

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(CheckoutScreen));
