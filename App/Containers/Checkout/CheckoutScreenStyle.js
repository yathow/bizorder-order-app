import { Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  page: {
    ...Styles.page,
    flex: 1
  },
  container: {
    ...Styles.container,
    flex: 1
  },
  btnPanel: {
    padding: Metrics.md
  }
});
