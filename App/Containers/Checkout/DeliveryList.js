import { Button, FlatList, LoadingIndicator, View } from '@merchstack/react-native-ui';

import AddressInfo from '../Addresses/AddressInfo';
import AddressesActions from '../../Stores/Addresses/Actions';
import { BaseComponent } from '../../Components';
import CartActions from '../../Stores/Cart/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './DeliveryListStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class DeliveryList extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionFetchAddresses = this.actionFetchAddresses.bind(this);
    this.actionSetCheckoutAddress = this.actionSetCheckoutAddress.bind(this);
  }

  componentDidMount() {
    const { fetchAddresses } = this.props;
    fetchAddresses();
  }

  render() {
    const { isLoading, addresses } = this.props;
    if (isLoading) {
      return <LoadingIndicator size="large" />;
    } else {
      return (
        <FlatList
          data={addresses}
          refreshing={isLoading}
          onRefresh={() => this.actionFectchAddress}
          keyExtractor={delAddr => delAddr.id.toString()}
          renderItem={this.renderDeliveryAddress}
        />
      );
    }
  }

  renderDeliveryAddress = data => {
    const rowStyle =
      (data.index + 1) % 2 === 0
        ? Style.delAddrRowEven(this.props.theme)
        : Style.delAddrRowOdd(this.props.theme);

    const address = data.item;
    return (
      <View style={rowStyle}>
        <AddressInfo data={address} />
        <View style={Style.btnPanel}>
          <Button
            status="primary"
            buttonStyle={Style.btn}
            containerStyle={Style.btnContainer}
            title={translate('use_this_addresss')}
            onPress={() => this.actionSetCheckoutAddress(address.id)}
          />
        </View>
      </View>
    );
  };

  actionFetchAddresses() {
    const { fetchAddresses } = this.props;
    fetchAddresses();
  }

  actionSetCheckoutAddress(addressId) {
    const { setCheckoutAddress } = this.props;
    setCheckoutAddress(addressId);
  }
}

const mapStateToProps = state => ({
  addresses: state.addresses.addresses,
  isLoading: state.addresses.addressesIsLoading
});

const mapDispatchToProps = dispatch => ({
  fetchAddresses: () => dispatch(AddressesActions.fetchAddresses()),
  setCheckoutAddress: addressId => dispatch(CartActions.setCheckoutAddress(addressId))
});

DeliveryList.propTypes = {
  addresses: PropTypes.array,
  isLoading: PropTypes.bool,

  fetchAddresses: PropTypes.func,
  setCheckoutAddress: PropTypes.func
};

DeliveryList.defaultProps = {
  addresses: [],
  isLoading: false,

  fetchAddresses: () => {},
  setCheckoutAddress: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(DeliveryList));
