import { BaseComponent, BaseView } from '../../Components';
import { ListItem, View } from '@merchstack/react-native-ui';

import DeliveryList from './DeliveryList';
import React from 'react';
import Style from './DeliveryScreenStyle';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class DeliveryScreen extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionManageAddress = this.actionManageAddress.bind(this);
  }

  render() {
    const { theme } = this.props;
    return (
      <BaseView
        title={translate('deliver_to')}
        showMenuButton={false}
        showSearchButton={false}
        showCartButton={false}
        showFilterButton={false}
      >
        <View style={Style.list}>
          <DeliveryList />
        </View>

        <ListItem
          leftIcon={{ name: 'settings' }}
          title={translate('manage_address')}
          containerStyle={Style.btnPanel(theme)}
          onPress={this.actionManageAddress}
        />
      </BaseView>
    );
  }

  actionManageAddress = () => {
    const { navigation } = this.props;
    navigation.navigate('AddressesScreen');
  };
}

export default withTheme(DeliveryScreen);
