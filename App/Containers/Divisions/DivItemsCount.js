import { BaseComponent } from '../../Components';
import DivItemsCountActions from '../../Stores/DivItemsCount/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './DivItemsCountStyle';
import { Text } from '@merchstack/react-native-ui';
import { connect } from 'react-redux';
import { translate } from '../../Translations';

class DivItemsCount extends BaseComponent {
  componentDidMount() {
    const { divisionId, fetchDivItemsCount } = this.props;

    if (divisionId > 0) {
      fetchDivItemsCount(divisionId);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { divisionId, refreshDivItemsCount, fetchDivItemsCount } = this.props;
    if ((prevProps.divisionId !== divisionId && divisionId > 0) || refreshDivItemsCount) {
      fetchDivItemsCount(divisionId);
    }
  }

  render() {
    const { divisionId, isLoading, divItemsCount } = this.props;
    if (divisionId > 0) {
      if (isLoading) {
        return <Text style={Style.itemCountLbl} />;
      } else {
        return (
          <Text style={Style.itemCountLbl}>
            {divItemsCount} {translate('items')}
          </Text>
        );
      }
    } else {
      return null;
    }
  }
}

DivItemsCount.propTypes = {
  locale: PropTypes.string,
  divisionId: PropTypes.number,
  divItemsCount: PropTypes.number,
  isLoading: PropTypes.bool,
  refreshDivItemsCount: PropTypes.bool
};

DivItemsCount.defaultProps = {
  locale: null,
  divisionId: 0,
  divItemsCount: 0,
  isLoading: true,
  refreshDivItemsCount: false
};

const mapStateToProps = state => ({
  locale: state.app.locale,
  divisionId: state.app.divisionId,
  divItemsCount: state.divItemsCount.divItemsCount,
  isLoading: state.divItemsCount.divItemsCountIsLoading,
  refreshDivItemsCount: state.divItemsCount.refreshDivItemsCount
});

const mapDispatchToProps = dispatch => ({
  fetchDivItemsCount: divisionId => dispatch(DivItemsCountActions.fetchDivItemsCount(divisionId))
});

export default connect(mapStateToProps, mapDispatchToProps)(DivItemsCount);
