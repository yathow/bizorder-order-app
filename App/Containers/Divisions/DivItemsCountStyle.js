import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  itemCountLbl: {
    paddingTop: Metrics.md
  }
});
