import { Card, Icon, LoadingIndicator, Text, View } from '@merchstack/react-native-ui';

import AppActions from '../../Stores/App/Actions';
import { BaseComponent } from '../../Components';
import DivItemsCount from './DivItemsCount';
import DivisionsActions from '../../Stores/Divisions/Actions';
import MessagesActions from '../../Stores/Messages/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './DivisionPickerStyle';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withNavigation } from 'react-navigation';

class DivisionPicker extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionSelectDivision = this.actionSelectDivision.bind(this);
  }

  componentDidMount() {
    const { clearMessages, setDivision, fetchDivisions } = this.props;

    clearMessages();
    setDivision(0);
    fetchDivisions();
  }

  render() {
    const { isLoading } = this.props;
    if (isLoading) {
      return <LoadingIndicator size="large" />;
    } else {
      return (
        <TouchableWithoutFeedback style={Style.content} onPress={this.actionSelectDivision}>
          <Card>
            <View style={Style.divPanel}>
              <View style={Style.infoPanel}>
                <Text style={Style.titleLbl}>{translate('division')}</Text>
                <Text h3 h3Style={Style.divisionLbl}>
                  {this.props.divisionId > 0
                    ? this.props.divisionName
                    : translate('no_division_assign')}
                </Text>
                <DivItemsCount />
              </View>
              <View style={Style.btnPanel}>
                <Icon name="arrow-drop-down" />
              </View>
            </View>
          </Card>
        </TouchableWithoutFeedback>
      );
    }
  }

  actionSelectDivision = () => {
    const { navigation } = this.props;
    navigation.navigate('DivisionsScreen');
  };
}

const mapStateToProps = state => ({
  locale: state.app.locale,
  divisionId: state.app.divisionId,
  divisionName: state.app.divisionName,
  divisions: state.divisions.divisions,
  isLoading: state.divisions.divisionsIsLoading
});

const mapDispatchToProps = dispatch => ({
  fetchDivisions: () => dispatch(DivisionsActions.fetchDivisions()),
  setDivision: divisionId => dispatch(AppActions.setDivision(divisionId)),
  clearMessages: () => dispatch(MessagesActions.clearMessages())
});

DivisionPicker.propTypes = {
  locale: PropTypes.string,
  divisionId: PropTypes.number,
  divisionName: PropTypes.string,
  divisions: PropTypes.array,
  isLoading: PropTypes.bool,

  fetchDivisions: PropTypes.func,
  setDivision: PropTypes.func,
  clearMessages: PropTypes.func
};

DivisionPicker.defaultProps = {
  locale: null,
  divisionId: 0,
  divisionName: null,
  divisions: [],
  isLoading: false,

  fetchDivisions: () => {},
  setDivision: () => {},
  clearMessages: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(DivisionPicker));
