import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  content: {
    paddingBottom: Metrics.md,
    paddingTop: Metrics.md,
    padding: Metrics.sm
  },
  divPanel: {
    flexDirection: 'row'
  },
  infoPanel: {
    flex: 1
  },
  btnPanel: {},
  titleLbl: {
    fontWeight: 'bold'
  },
  divisionLbl: {
    paddingTop: Metrics.sm
  },
  itemInfoLbl: {
    paddingTop: Metrics.md
  },
  divisionPicker: {
    flex: 1
  }
});
