import { BaseComponent, BaseView, EmptyComponent } from '../../Components';
import { FlatList, Text } from '@merchstack/react-native-ui';

import AppActions from '../../Stores/App/Actions';
import DivisionsActions from '../../Stores/Divisions/Actions';
import MessagesActions from '../../Stores/Messages/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './DivisionsScreenStyle';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withNavigation } from 'react-navigation';
import { withTheme } from 'react-native-elements';

class DivisionsScreen extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionFetchDivisions = this.actionFetchDivisions.bind(this);
  }

  componentDidMount() {
    const { clearMessages, fetchDivisions } = this.props;
    clearMessages();
    fetchDivisions();
  }

  render() {
    const { divisions, isLoading, errorMessages } = this.props;
    return (
      <BaseView
        title={translate('divisions')}
        showMenuButton={false}
        showCartButton={false}
        showSearchButton={false}
        showFilterButton={false}
        errorMessage={errorMessages}
      >
        <FlatList
          refreshing={false}
          isLoading={isLoading}
          onRefresh={this.actionFetchDivisions}
          data={divisions}
          keyExtractor={item => item.id.toString()}
          renderItem={this.renderDivision}
          ListEmptyComponent={this.renderEmptyList()}
        />
      </BaseView>
    );
  }

  renderDivision = data => {
    const { theme } = this.props;

    const division = data.item;
    const rowStyle =
      (data.index + 1) % 2 === 0 ? Style.itemRowEven(theme) : Style.itemRowOdd(theme);

    return (
      <TouchableWithoutFeedback style={rowStyle} onPress={() => this.actionSetDivision(division)}>
        <Text h3>{division.name}</Text>
      </TouchableWithoutFeedback>
    );
  };

  renderEmptyList = () => {
    const { isLoading } = this.props;
    return <EmptyComponent isLoading={isLoading} title={translate('no_divisions')} />;
  };

  actionFetchDivisions = () => {
    const { fetchDivisions } = this.props;
    fetchDivisions();
  };

  actionSetDivision = division => {
    const { setDivision, navigation } = this.props;
    setDivision(division);
    navigation.goBack();
  };
}

DivisionsScreen.propTypes = {
  divisions: PropTypes.array,
  isLoading: PropTypes.bool,
  successMessages: PropTypes.array,
  errorMessages: PropTypes.array,

  clearMessages: PropTypes.func
};

DivisionsScreen.defaultProps = {
  divisions: [],
  isLoading: false,
  successMessages: [],
  errorMessages: [],

  clearMessages: () => {}
};

const mapStateToProps = state => ({
  divisions: state.divisions.divisions,
  isLoading: state.divisions.divisionsIsLoading,
  successMessages: state.messages.successMessages,
  errorMessages: state.messages.errorMessages
});

const mapDispatchToProps = dispatch => ({
  fetchDivisions: () => dispatch(DivisionsActions.fetchDivisions()),
  setDivision: division => dispatch(AppActions.setDivision(division.id, division.name)),
  clearMessages: () => dispatch(MessagesActions.clearMessages())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withNavigation(withTheme(DivisionsScreen)));
