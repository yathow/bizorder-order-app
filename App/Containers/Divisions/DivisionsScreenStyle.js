import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  itemRowOdd: theme => ({
    padding: Metrics.md,
    paddingTop: Metrics.lg,
    paddingBottom: Metrics.lg,
    backgroundColor: theme.backgroundColors.odd
  }),
  itemRowEven: theme => ({
    padding: Metrics.md,
    paddingTop: Metrics.lg,
    paddingBottom: Metrics.lg,
    backgroundColor: theme.backgroundColors.even
  })
});
