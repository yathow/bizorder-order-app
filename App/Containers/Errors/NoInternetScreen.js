import { BaseComponent, BaseView } from '../../Components';
import { Button, Icon, Text, View } from '@merchstack/react-native-ui';

import React from 'react';
import Style from './NoInternetScreenStyle';
import { translate } from '../../Translations';

class NoInternetScreen extends BaseComponent {
  constructor(props) {
    super(props);
    this.actionTryAgain = this.actionTryAgain.bind(this);
  }

  actionTryAgain() {
    const { navigation } = this.props;
    navigation.navigate('HomeScreen');
  }

  render() {
    return (
      <BaseView
        title={'Bizorder'}
        showMenuButton={false}
        showSearchButton={false}
        showCartButton={false}
        showBackButton={false}
        showFilterButton={false}
      >
        <View style={Style.container}>
          <View style={Style.topPanel}>
            <Icon name="signal-wifi-off" size={65} containerStyle={Style.icon} />
            <Text style={Style.warningLbl}>{translate('no_internet')}</Text>
          </View>
          <View style={Style.bottomPanel}>
            <Button
              status="primary"
              buttonStyle={Style.btn}
              title={translate('try_again')}
              onPress={this.actionTryAgain}
            />
          </View>
        </View>
      </BaseView>
    );
  }
}

export default NoInternetScreen;
