import { Metrics, Styles, Fonts } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    ...Styles.container
  },
  topPanel: {
    flex: 1,
    justifyContent: 'flex-end',
    padding: Metrics.lg
  },
  icon: {
    padding: Metrics.lg
  },
  warningLbl: {
    ...Fonts.h3,
    padding: Metrics.md,
    textAlign: 'center'
  },
  bottomPanel: {
    flex: 1,
    justifyContent: 'flex-start',
    padding: Metrics.lg
  },
  btn: {
    padding: Metrics.md
  }
});
