import PropTypes from 'prop-types';
import React from 'react';
import { RefreshControl } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { View } from '@merchstack/react-native-ui';
import { connect } from 'react-redux';

//Components
import { BaseComponent, BaseView } from '../../Components';

//Containers
import Brands from '../Brands/Brands';
import Categories from '../Categories/Categories';
import DivisionPicker from '../Divisions/DivisionPicker';
import Advertisements from '../Advertisements/Advertisements';

//Stores
import AppActions from '../../Stores/App/Actions'
import BrandsActions from '../../Stores/Brands/Actions';
import CartActions from '../../Stores/Cart/Actions';
import CarouselActions from '../../Stores/Carousel/Actions';
import CategoriesActions from '../../Stores/Categories/Actions';
import DivItemsCountActions from '../../Stores/DivItemsCount/Actions';
import ItemQtyActions from '../../Stores/ItemQty/Actions'
import MessagesActions from '../../Stores/Messages/Actions';
import PromotionsActions from '../../Stores/Promotions/Actions';

import { translate } from '../../Translations';

export class HomeScreen extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionOnRefresh = this.actionOnRefresh.bind(this);
  }

  componentDidMount() {
    const { clearMessages, clearRoutes, resetItemQty } = this.props;
    clearMessages();
    clearRoutes()
    resetItemQty()

  }

  componentDidUpdate(prevProps) {
    const { refreshCart, divisionId, fetchCart } = this.props;
    if (refreshCart || prevProps.divisionId !== divisionId) {
      fetchCart(divisionId);
    }
  }

  render() {
    const { errorMessages, successMessages } = this.props;
    return (
      <BaseView
        title={translate('app_name')}
        showBackButton={false}
        showFilterButton={false}
        errorMessage={errorMessages}
        successMessage={successMessages}
        resetFilters={true}
      >
        <ScrollView
          refreshControl={<RefreshControl refreshing={false} onRefresh={this.actionOnRefresh} />}
        >
          <View>
            <DivisionPicker />
            <Advertisements />
            <Brands/>
            <Categories/>
          </View>
        </ScrollView>
      </BaseView>
    );
  }

  actionOnRefresh() {
    const {
      setRefreshBrands,
      setRefreshCategories,
      setRefreshDivItemsCount,
      setRefreshCart,
      setRefreshPromotions,
      setCarouselIndex
    } = this.props;
    setRefreshBrands(true);
    setRefreshCategories(true);
    setRefreshDivItemsCount(true);
    setRefreshCart(true);
    setRefreshPromotions(true);
    setCarouselIndex(0);
  }
}

const mapStateToProps = state => {
  return {
    locale: state.app.locale,
    successMessages: state.messages.successMessages,
    errorMessages: state.messages.errorMessages,
    divisionId: state.app.divisionId,
    refreshCart: state.cart.refreshCart
  };
};

const mapDispatchToProps = dispatch => ({
  clearMessages: () => dispatch(MessagesActions.clearMessages()),
  fetchCart: divisionId => dispatch(CartActions.fetchCart(divisionId)),

  setRefreshBrands: status => dispatch(BrandsActions.setRefreshBrands(status)),
  setRefreshCategories: status => dispatch(CategoriesActions.setRefreshCategories(status)),
  setRefreshDivItemsCount: status => dispatch(DivItemsCountActions.setRefreshDivItemsCount(status)),
  setRefreshCart: status => dispatch(CartActions.setRefreshCart(status)),
  setRefreshPromotions: status => dispatch(PromotionsActions.setRefreshPromotions(status)),
  setCarouselIndex: index => dispatch(CarouselActions.setCarouselIndex(index)),
  clearRoutes: () => dispatch(AppActions.clearRoutes()),
  resetItemQty: () => dispatch(ItemQtyActions.resetItemQty())
});

HomeScreen.propTypes = {
  locale: PropTypes.string,
  successMessages: PropTypes.array,
  errorMessages: PropTypes.array,
  divisionId: PropTypes.number,
  refreshCart: PropTypes.bool,

  clearMessages: PropTypes.func,
  fetchCart: PropTypes.func,
  setRefreshBrands: PropTypes.func,
  setRefreshCategories: PropTypes.func,
  setRefreshDivItemsCount: PropTypes.func,
  setRefreshCart: PropTypes.func,
  setRefreshPromotions: PropTypes.func,
  setCarouselIndex: PropTypes.func,
  clearRoutes: PropTypes.func,
};

HomeScreen.defaultProps = {
  locale: null,
  successMessages: [],
  errorMessages: [],
  divisionId: 0,
  refreshCart: false,

  clearMessages: () => {},
  fetchCart: () => {},

  setRefreshBrands: () => {},
  setRefreshCategories: () => {},
  setRefreshDivItemsCount: () => {},
  setRefreshCart: () => {},
  setRefreshPromotions: () => {},
  setCarouselIndex: () => {},
  clearRoutes: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
