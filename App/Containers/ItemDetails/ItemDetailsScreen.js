import { BaseComponent, BaseView } from '../../Components';
import { Button, Divider, LoadingIndicator, Text, View } from '@merchstack/react-native-ui';

import CartActions from '../../Stores/Cart/Actions';
import ItemDetailsActions from '../../Stores/ItemDetails/Actions';
import ItemGallery from './ItemGallery';
import ItemPromotionTags from './ItemPromotionTags';
import ItemQty from '../ItemQty/ItemQty';
import ItemQtyActions from '../../Stores/ItemQty/Actions';
import ItemUomPricings from './ItemUomPricings';
import MessagesActions from '../../Stores/Messages/Actions';
import AppActions from '../../Stores/App/Actions'
import PropTypes from 'prop-types';
import React from 'react';
import { ScrollView } from 'react-native';
import Style from './ItemDetailsStyle';
import { connect } from 'react-redux';
import { withTheme } from 'react-native-elements';
import { translate } from '../../Translations';

class ItemDetailsScreen extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionShowItemQty = this.actionShowItemQty.bind(this);
  }

  componentDidMount() {
    const { clearMessages, resetItemQty, itemId, fetchItemDetails } = this.props;
    
    clearMessages();
    resetItemQty();
    fetchItemDetails(itemId);
  }

  componentDidUpdate(prevProps) {
    const { refreshCart, divisionId, fetchCart } = this.props;
    if (refreshCart || prevProps.divisionId !== divisionId) {
      fetchCart(divisionId);
    }
  }

  render() {
    const { errorMessages, itemIsLoading, item, theme } = this.props;
    
    return (
      <BaseView showMenuButton={false} showSearchButton={false} showFilterButton={false} errorMessage={errorMessages}>
        <ScrollView>
          <View style={Style.content}>
            <ItemQty />
            {this.renderItem(itemIsLoading, item, theme)}
          </View>
        </ScrollView>
      </BaseView>
    );
  }

  renderItem(itemIsLoading, item, theme) {
    if (itemIsLoading) {
      return <LoadingIndicator size="large" />;
    } else if (item) {
      return (
        <View>
          <View style={Style.itemInfoPanel}>
            <Text h2>{item.code}</Text>
            <Text h1>{item.desc1}</Text>
            <Text h1>{item.desc2}</Text>
          </View>
          <ItemGallery item={item} />
          <Text h3 h3Style={Style.descTitle}>
            {translate('product_desc')}
          </Text>
          <ItemUomPricings uoms={item.uoms} />
          <View style={Style.miscInfo}>
            {this.renderGroupInfo(translate('brand'), item.brandCode)}
            {this.renderGroupInfo(translate('category'), item.categoryCode)}
            {this.renderGroupInfo(translate('manufacturer'), item.manufacturerCode)}
            {this.renderDesc()}
          </View>
          <Button
            status="secondary"
            title={translate('add_to_cart')}
            onPress={() => this.actionShowItemQty()}
          />
          {this.renderPromotions(item, theme)}
        </View>
      );
    } else {
      return null;
    }
  }

  renderGroupInfo(label, value) {
    return (
      <View style={Style.groupRow}>
        <Text style={Style.groupLbl}>{label}</Text>
        <Text style={Style.separator}>:</Text>
        <Text style={Style.groupTxt}>{value ? value : translate('-')}</Text>
      </View>
    );
  }

  renderDesc() {
    const { item } = this.props;
    return (
      <View>
        <Text>{item.desc2}</Text>
      </View>
    );
  }

  renderPromotions(item, theme) {
    if (item && item.promotions.length > 0) {
      return (
        <View>
          <Text h3 h3Style={Style.promoTitle}>
            {translate('promotions')}
          </Text>
          <ItemPromotionTags item={item} theme={theme} expandPromotion={true}/>
        </View>
      );
    } else {
      return null;
    }
  }

  actionShowItemQty() {
    const { showItemQty } = this.props;
    showItemQty(this.props.item.id);
  }
}

const mapStateToProps = state => ({
  itemId: state.itemDetails.itemId,
  item: state.itemDetails.item,
  itemIsLoading: state.itemDetails.itemIsLoading,
  divisionId: state.app.divisionId,
  refreshCart: state.cart.refreshCart,
  successMessages: state.messages.successMessages,
  errorMessages: state.messages.errorMessages,
  route: state.app.route,
});

const mapDispatchToProps = dispatch => ({
  selectPhotos: photos => dispatch(ItemDetailsActions.selectPhotos(photos)),
  resetItemQty: () => dispatch(ItemQtyActions.resetItemQty()),
  fetchItemDetails: itemId => dispatch(ItemDetailsActions.fetchItemDetails(itemId)),
  fetchCart: divisionId => dispatch(CartActions.fetchCart(divisionId)),
  clearMessages: () => dispatch(MessagesActions.clearMessages()),
  showItemQty: (itemId, item, uoms) => dispatch(ItemQtyActions.showItemQty(itemId, item, uoms)),
  updateRoute: route => dispatch(AppActions.updateRoute(route)),
  setExpandPromotion: boolean => dispatch()
});

ItemDetailsScreen.propTypes = {
  itemId: PropTypes.number,
  successMessage: PropTypes.string,
  errorMessage: PropTypes.string,

  selectPhotos: PropTypes.func,
  resetItemQty: PropTypes.func,
  fetchItemDetails: PropTypes.func,
  fetchCart: PropTypes.func,
  clearMessages: PropTypes.func,
  showItemQty: PropTypes.func,
  updateRoute: PropTypes.func
};

ItemDetailsScreen.defaultProps = {
  itemId: 0,
  successMessage: '',
  errorMessages: '',

  selectPhotos: () => {},
  resetItemQty: () => {},
  fetchItemDetails: () => {},
  fetchCart: () => {},
  clearMessages: () => {},
  showItemQty: () => {},
  updateRoute: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ItemDetailsScreen));
