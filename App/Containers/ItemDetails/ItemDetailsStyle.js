import { Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  page: {
    ...Styles.container
  },
  container: {
    ...Styles.container
  },
  scrollView: {},
  content: {
    ...Styles.content
  },
  itemInfoPanel: {
    paddingTop: Metrics.sm,
    paddingBottom: Metrics.sm
  },
  miscInfo: {
    paddingTop: Metrics.md,
    paddingBottom: Metrics.md
  },
  groupRow: {
    flexDirection: 'row',
    paddingBottom: Metrics.sm
  },
  groupLbl: {
    width: 100
  },
  separator: {
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md
  },
  groupTxt: {
    flex: 1
  },
  descTitle: {
    paddingTop: Metrics.md,
    paddingBottom: Metrics.md
  },
  promoTitle: {
    paddingTop: Metrics.lg,
    paddingBottom: Metrics.md
  }
});
