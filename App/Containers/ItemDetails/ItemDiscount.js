import {} from '../../';

import React from 'react';
import Style from './ItemDiscountStyle';
import { Text } from '@merchstack/react-native-ui';
import { formatPercent } from '../../Formatter/NumberFormat';
import { translate } from '../../Translations';

const ItemDiscount = props => {
  return (
    <>
      {props.discount && props.discount.price > 0
        &&  <Text h4 h4Style={Style.label}>
              {translate('special_price')}
            </Text>
      }
      {props.discount && props.discount.rate > 0
        &&  <Text h4 h4Style={Style.label}>
              {formatPercent(props.discount.rate)} OFF
            </Text>
      }
    </>    
  ) 
};

export default ItemDiscount;
