import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  label: {
    padding: Metrics.sm,
    textAlign: 'center'
  }
});
