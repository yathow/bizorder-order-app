import Gallery from '../../Components/Gallery/Gallery';
import React from 'react';

const ItemGallery = props => {
  const { item } = props;

  let photos = [];

  item.photos.map(photo => {
    let uomCode;
    for (let i = 0; i < item.uoms.length; i++) {
      let uom = item.uoms[i];
      if (uom.id === photo.uomId) {
        uomCode = uom.uomCode;
      }
    }

    photos.push({
      id: photo.id,
      source: photo.path,
      desc: uomCode
    });
  });

  return <Gallery photos={photos} />;
};

export default ItemGallery;
