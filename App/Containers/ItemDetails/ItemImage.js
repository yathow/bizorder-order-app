import { Image, LoadingIndicator } from '@merchstack/react-native-ui';

import { Images } from '../../Theme';
import React from 'react';

const ItemImage = props => {
  const { photos, onPress, style } = props;

  const photo = photos && photos.length > 0 ? photos[0] : null;
  const source = photo ? { uri: photo.path } : Images.noPhotoIcon;
  
  return (
    <Image
      source={source}
      style={style}
      resizeMode="contain"
      onPress={onPress}
      PlaceholderContent={<LoadingIndicator />}
    />
  );
};

export default ItemImage;
