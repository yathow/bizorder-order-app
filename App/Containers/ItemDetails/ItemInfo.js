import React, { Component, Fragment } from 'react';
import { Text, TouchableWithoutFeedback } from 'react-native';

import ItemUoms from './ItemUoms';
import Style from './ItemInfoStyle';
import { View } from '@merchstack/react-native-ui';
import { withTheme } from 'react-native-elements';

class ItemInfo extends Component {
  render() {
    const onPress = this.props.onPress;
    if (onPress) {
      return (
        <TouchableWithoutFeedback onPress={onPress}>
          <View>{this.renderInfo()}</View>
        </TouchableWithoutFeedback>
      );
    } else {
      return <Fragment>{this.renderInfo()}</Fragment>;
    }
  }

  renderInfo() {
    const { item, uoms, theme } = this.props;
    if (item) {
      return (
        <View>
          <Text style={Style.itemDesc(theme)}>{item.desc1}</Text>
          <Text style={Style.itemDesc(theme)}>{item.desc2}</Text>
          <ItemUoms uoms={uoms} />
        </View>
      );
    } else {
      return null;
    }
  }
}

export default withTheme(ItemInfo);
