import { Colors, Fonts, Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  itemCode: theme => ({}),
  itemDesc: theme => ({
    color: theme.colors.primary,
    fontWeight: 'bold'
  })
});
