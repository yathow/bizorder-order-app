import { Text, View, TouchableOpacity } from 'react-native';
import React from 'react';
import Style from './ItemPromotionTagsStyle';
import PromotionsActions from '../../Stores/Promotions/Actions'
import { useDispatch } from 'react-redux'
import { withTheme } from 'react-native-elements';
import { connect } from 'react-redux';

const ItemPromotionTags = props => {
  const { item, theme, expandPromotion = false} = props
  const dispatch = useDispatch()

  countApplied = () => {
    const { item } = props
    let countApplied = 0
    
    item.promotions.forEach(promo => {
      if(promo.applied)
        countApplied++
    })

    return countApplied
  }

  actionGoToPromotion = promotionId => {
    dispatch(PromotionsActions.goToPromotion(promotionId))
  }

  renderDetails = (promotion) => {
    if(expandPromotion) {
      return (
        
          <View>
            <Text style={Style.promoItemText(theme, promotion, countApplied())}>{promotion.desc1}</Text>
            <View style={Style.promoItemDetails}>
              {
                !promotion.applied 
                  &&  <Text style={[
                        Style.promoItemDetailPadding, 
                        Style.promoItemText(theme, promotion, countApplied())
                      ]}>
                        The best promotion discount by type have been applied.
                      </Text>
              }
              {
                promotion.desc2 !== '' 
                  &&  <Text style={[
                        Style.promoItemDetailPadding, 
                        Style.promoItemText(theme, promotion, countApplied())
                      ]}>
                        {promotion.desc2}
                      </Text>
              }
              <Text style={[Style.promoItemDetailPadding, Style.promoItemText(theme, promotion, countApplied())]}>Code: {promotion.code}</Text>
            </View>
            <TouchableOpacity key={item.id + '-' + promotion.id} onPress={() => actionGoToPromotion(promotion.id)}>
              <Text style={Style.promoItemTextMore(props.theme)}>More Info</Text>
            </TouchableOpacity>
          </View>
      )
    } else {
      return(
        <View>
          <Text style={Style.promoItemText(theme, promotion, countApplied())}>{promotion.desc1}</Text>
        </View>
      )
    }
  }

    
  return (
    expandPromotion
    ? item.promotions.map((promotion) => {
        return (
            <View key={item.id + '-' + promotion.id} style={Style.promoRow}>
              <View style={[Style.promoItemContainer(theme, promotion, countApplied()), Style.promoItemContainerPaddingMd]}>
                {renderDetails(promotion, expandPromotion)}
              </View>
            </View>
        )
      })
    : item.promotions.map((promotion) => {
        if(promotion.applied) {
          return (
            <View key={item.id + '-' + promotion.id} style={Style.promoRow}>
              <View style={[Style.promoItemContainer(theme, promotion, countApplied()), Style.promoItemContainerPaddingXs]}>
                {renderDetails(promotion, expandPromotion)}
              </View>
            </View>
          )
        }
      })
  )
}

export default withTheme(ItemPromotionTags);
