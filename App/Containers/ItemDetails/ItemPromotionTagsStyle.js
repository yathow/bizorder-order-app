import { Metrics } from '../../Theme';
import { StyleSheet, Dimensions } from 'react-native';
import { useSelector } from 'react-redux'

const DEVICE_WIDTH = Dimensions.get('screen').width

export default StyleSheet.create({
  promoRow: {
    flexDirection: 'column',
    marginBottom: 5
  },
  promoItemContainer: (theme, promotion, count) => ({
    width: '100%',
    borderColor: count == 2 && promotion.applied
                  ? promotion.type == 1 
                    ? theme.colors.secondary
                    : theme.colors.success 
                  : count == 1 && promotion.applied
                    ? theme.colors.success
                    : theme.colors.muted,
    backgroundColor: count == 2 && promotion.applied
                      ? promotion.type == 1 
                        ? theme.backgroundColors.warning
                        : theme.backgroundColors.success 
                      : count == 1 && promotion.applied
                        ? theme.backgroundColors.success
                        : theme.backgroundColors.disabledLight,
    borderRadius: Metrics.borderRadius,
    borderWidth: 1, 
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md,
    margin: Metrics.xs
  }),
  promoItemContainerPaddingXs: {
    padding: Metrics.xs
  },
  promoItemContainerPaddingMd: {
    padding: Metrics.md
  },
  promoItemText: (theme, promotion, count) => ({
    color: count == 2 && promotion.applied
            ? promotion.type == 1 
              ? theme.colors.secondary
              : theme.colors.success 
            : count == 1 && promotion.applied
              ? theme.colors.success
              : theme.colors.muted
  }),
  promoItemTextMore: theme => ({
    color: theme.colors.primary
  }),
  promoItemDetails: {
    marginTop: Metrics.md
  },
  promoItemDetailPadding: {
    paddingVertical: Metrics.xs
  },
  promoItemDetailListPadding: {
    paddingVertical: Metrics.md
  },
  promoItemDetailListTitle: (theme, applied) => ({
    paddingBottom: Metrics.sm,
    color: applied ? theme.colors.primary : theme.backgroundColors.disabledDark,
    fontSize: 16
  }),
  promoItemImageContainer: {
    width: DEVICE_WIDTH * 0.2,
    margin: Metrics.sm,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  promoItemImage: {
    width: Math.floor(DEVICE_WIDTH * 0.2),
    height: Math.floor(DEVICE_WIDTH * 0.2),
    marginBottom: Metrics.sm,
    borderRadius: Math.floor(DEVICE_WIDTH * 0.2)/2
  },
  promoItemImageText: (theme, promotion, count) => ({
    color: count == 2 && promotion.applied
            ? promotion.type == 1 
              ? theme.colors.secondary
              : theme.colors.success 
            : count == 1 && promotion.applied
              ? theme.colors.success
              : theme.colors.muted,
    fontSize: 10
  }),
});
