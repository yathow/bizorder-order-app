import { Text, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import ItemQtyActions from '../../Stores/ItemQty/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './ItemUomPricingsStyle';
import { connect } from 'react-redux';
import { formatCurrency } from '../../Formatter/NumberFormat';
import { withTheme } from 'react-native-elements';

class ItemUomPricings extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionShowItemQty = this.actionShowItemQty.bind(this);
  }

  render() {
    const { item, uoms, theme } = this.props;
    return (
      <View style={Style.uomsPanel}>
        {uoms.map((uom, index) => {
          return (
            <ItemUom
              uom={uom}
              key={uom.id}
              theme={theme}
              addToCart={this.actionShowItemQty}
            />
          );
        })}
      </View>
    );
  }

  actionShowItemQty() {
    const { showItemQty } = this.props;
    showItemQty(this.props.item.id);
  }
}

class ItemUom extends BaseComponent {
  render() {
    const { uom, theme } = this.props;
    const price = uom.price;
    const oriPrice = uom.oriPrice;
    
    return (
      <View style={Style.itemUomPanel}>
        <Text style={Style.itemUomName(theme)}>{uom.code}</Text>
        <Text style={Style.separator}>/</Text>
        {price.length > 0 
          ? (<>
              {price.slice(0).reverse().map((p, index) => (
                  <Text key={index} style={index == 0 ? Style.itemUomPrice1(theme) : Style.itemUomPrice2(theme)}>
                    { p > 0 ? formatCurrency(p) : translate('foc')}
                  </Text>
                )
              )}
              <Text style={Style.oriPrice(theme)}>{formatCurrency(oriPrice)}</Text>
            </>
          ) : (
            <Text style={Style.itemUomPrice1(theme)}>{formatCurrency(oriPrice)}</Text>
          )
        }
      </View>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  showItemQty: (itemId, item, uoms) => dispatch(ItemQtyActions.showItemQty(itemId, item, uoms))
});

ItemUomPricings.propTypes = {
  showItemQty: PropTypes.func
};

ItemUomPricings.defaultProps = {
  showItemQty: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ItemUomPricings));
