import { Fonts, Metrics } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  uomsPanel: {
    flexDirection: 'column',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    paddingTop: Metrics.sm,
    paddingBottom: Metrics.sm
  },
  itemUomPanel: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: Metrics.sm
  },
  itemUomName: theme => ({
    textAlign: 'center'
  }),
  separator: {
    paddingLeft: Metrics.sm,
    paddingRight: Metrics.sm
  },
  itemUomPrice1: theme => ({
    paddingLeft: Metrics.sm,
    color: theme.colors.success
  }),
  itemUomPrice2: theme => ({
    paddingLeft: Metrics.sm,
    textDecorationLine: 'line-through',
    color: theme.colors.secondary
  }),
  oriPrice: theme => ({
    color: theme.colors.muted,
    paddingLeft: Metrics.sm,
    textDecorationLine: 'line-through',
    flex: 1
  })
});
