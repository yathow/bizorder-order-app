import React, { useContext } from 'react';
import { Text, View } from '@merchstack/react-native-ui';

import Style from './ItemUomsStyle';
import { ThemeContext } from 'react-native-elements';
import { formatCurrency } from '../../Formatter/NumberFormat';
import { translate } from '../../Translations';

const ItemUoms = props => {
  if (props.uoms && props.uoms.length > 0) {
    return (
      <View>
        {props.uoms.map((uom, index) => {
          return <ItemUom uom={uom} key={index} discount={props.discount} />;
        })}
      </View>
    );
  } else {
    return null;
  }
};

const ItemUom = props => {
  const { theme } = useContext(ThemeContext);

  const price = props.uom.price;
  const oriPrice = props.uom.oriPrice;
  
  return (
    <View style={Style.itemUomPanel}>
      <Text style={Style.itemUomName}>{props.uom.code}</Text>
      <Text style={Style.divider}>/</Text>
      {price.length > 0 
        ? (<>
            {price.slice(0).reverse().map((p, index) => (
                <Text key={index} style={index == 0 ? Style.itemUomPrice1(theme) : Style.itemUomPrice2(theme)}>
                  { p > 0 ? formatCurrency(p) : translate('foc')}
                </Text>
              )
            )}
            <Text style={Style.oriPrice(theme)}>{formatCurrency(oriPrice)}</Text>
          </>
        ) : (
          <Text style={Style.itemUomPrice1(theme)}>{formatCurrency(oriPrice)}</Text>
        )
      }
    </View>
  );
};

export default ItemUoms;
