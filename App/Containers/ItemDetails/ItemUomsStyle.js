import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  itemUomPanel: {
    flexDirection: 'row'
  },
  itemUomName: {},
  divider: {
    paddingRight: Metrics.sm,
    paddingLeft: Metrics.sm
  },
  oriPrice: theme => ({
    color: theme.colors.muted,
    paddingLeft: Metrics.sm,
    textDecorationLine: 'line-through',
    flex: 1
  }),
  itemUomPrice1: theme => ({
    paddingLeft: Metrics.sm,
    color: theme.colors.success
  }),
  itemUomPrice2: theme => ({
    paddingLeft: Metrics.sm,
    textDecorationLine: 'line-through',
    color: theme.colors.secondary
  })
});
