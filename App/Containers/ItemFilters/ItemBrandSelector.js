import { Button, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import BrandsActions from '../../Stores/Brands/Actions';
import ItemsActions from '../../Stores/Items/Actions';
import CategoriesActions from '../../Stores/Categories/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './SelectorStyle';
import { connect } from 'react-redux';
import { withTheme } from 'react-native-elements';

class ItemBrandSelector extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionToggleBrandFilters = this.actionToggleBrandFilters.bind(this);
  }

  componentDidMount() {
    const { divisionId, fetchBrands } = this.props;
    if (divisionId > 0) {
      //fetchBrands(divisionId);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { divisionId, fetchBrands } = this.props;
    if (prevProps.divisionId !== divisionId && divisionId > 0) {
      //fetchBrands(divisionId);
    }
  }

  render() {
    const { theme } = this.props;
    return (
      <View style={Style.filterPanel}>
        {this.props.brands.map((brand, index) => {
          let isSelected = false;

          this.props.brandIds.map(brandId => {
            if (brandId === brand.id) {
              isSelected = true;
            }
          });

          let buttonStyle = isSelected ? Style.selectedBtn(theme) : Style.btn(theme);
          return (
            <View key={brand.id} style={Style.containerBtn}>
              <Button
                buttonStyle={buttonStyle}
                title={brand.name}
                inputStyle={Style.inputStyle(theme)}
                onPress={() => this.actionToggleBrandFilters(brand.id)}
              />
            </View>
          );
        })}
      </View>
    );
  }

  actionToggleBrandFilters = brandId => {
    const { toggleBrandFilters, fetchCategoriesByBrand, fetchItems, route } = this.props;
    toggleBrandFilters(brandId);
    
    if(route == 'Brands' || route == 'Categories')
      fetchItems();
    else if(route == 'ItemFilters')
      fetchCategoriesByBrand();
  };
}

const mapStateToProps = state => {
  return {
    brands: state.brands.brands,
    isLoading: state.brands.brandsIsLoading,
    divisionId: state.app.divisionId,
    brandIds: state.items.newFilters.brandIds,
    route: state.app.route
  };
};

const mapDispatchToProps = dispatch => ({
  resetBrands: () => dispatch(BrandsActions.resetBrands()),
  fetchBrands: divisionId => dispatch(BrandsActions.fetchBrands(divisionId)),
  toggleBrandFilters: brandId => dispatch(ItemsActions.toggleBrandFilters(brandId)),
  fetchCategoriesByBrand: () => dispatch(CategoriesActions.fetchCategoriesByBrand()),
  fetchItems: () => dispatch(ItemsActions.fetchItems())
});

ItemBrandSelector.propTypes = {
  brands: PropTypes.array,
  isLoading: PropTypes.bool,
  divisionId: PropTypes.number,
  brandIds: PropTypes.array,

  resetBrands: PropTypes.func,
  fetchBrands: PropTypes.func,
  toggleBrandFilters: PropTypes.func,
  fetchCategoriesByBrand: PropTypes.func,
  fetchItems: PropTypes.func
};

ItemBrandSelector.defaultProps = {
  brands: [],
  isLoading: false,
  divisionId: 0,
  brandIds: [],

  applyFilters: () => {},
  fetchBrands: () => {},
  toggleBrandFilters: () => {},
  fetchCategoriesByBrand: () => {},
  fetchItems: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ItemBrandSelector));
