import { Button, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import CategoriesActions from '../../Stores/Categories/Actions';
import ItemsActions from '../../Stores/Items/Actions';
import BrandsActions from '../../Stores/Brands/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './SelectorStyle';
import { connect } from 'react-redux';
import { withTheme } from 'react-native-elements';
import { selectCategory, fetchItems } from '../../Sagas/ItemsSaga';
import { toggleCategoryFilters } from '../../Stores/Items/Reducers';

class ItemBrandSelector extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionToggleCategoryFilters = this.actionToggleCategoryFilters.bind(this);
  }

  componentDidMount() {
    const { divisionId, fetchCategories } = this.props;
    if (divisionId > 0) {
      //fetchCategories(divisionId);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { divisionId, fetchCategories } = this.props;
    if (prevProps.divisionId !== divisionId && divisionId > 0) {
      //fetchCategories(divisionId);
    }
  }

  render() {
    const { categories, categoryIds } = this.props;
    return (
      <View style={Style.filterPanel}>
        {categories.map((category, index) => {
          let isSelected = false;

          categoryIds.map(categoryId => {
            if (categoryId === category.id) {
              isSelected = true;
            }
          });

          let buttonStyle = isSelected
            ? Style.selectedBtn(this.props.theme)
            : Style.btn(this.props.theme);
          return (
            <View key={category.id} style={Style.containerBtn}>
              <Button
                buttonStyle={buttonStyle}
                title={category.name}
                onPress={() => this.actionToggleCategoryFilters(category.id)}
              />
            </View>
          );
        })}
      </View>
    );
  }

  actionToggleCategoryFilters = categoryId => {
    const { toggleCategoryFilters, fetchBrandsByCategory, fetchItems, route } = this.props;
    toggleCategoryFilters(categoryId);
    
    if(route == 'Brands' || route == 'Categories')
      fetchItems();
    else if(route == 'ItemFilters')
      fetchBrandsByCategory();

  };
}

const mapStateToProps = state => ({
  categories: state.categories.categories,
  isLoading: state.brands.brandsIsLoading,
  divisionId: state.app.divisionId,
  categoryIds: state.items.newFilters.categoryIds,
  brands: state.brands.brands,
  route: state.app.route
});

const mapDispatchToProps = dispatch => ({
  fetchCategories: divisionId => dispatch(CategoriesActions.fetchCategories(divisionId)),
  fetchBrandsByCategory: () => dispatch(BrandsActions.fetchBrandsByCategory()),
  toggleCategoryFilters: categoryId => dispatch(ItemsActions.toggleCategoryFilters(categoryId)),
  fetchItems: () => dispatch(ItemsActions.fetchItems())
});

ItemBrandSelector.propTypes = {
  categories: PropTypes.array,
  isLoading: PropTypes.bool,
  divisionId: PropTypes.number,
  categoryIds: PropTypes.array,

  fetchCategories: PropTypes.func,
  fetchBrandsByCategory: PropTypes.func,
  toggleCategoryFilters: PropTypes.func,
  fetchItems: PropTypes.func
};

ItemBrandSelector.defaultProps = {
  brands: [],
  isLoading: false,
  divisionId: 0,
  categoryIds: [],

  applyFilters: () => {},
  fetchCategories: () => {},
  fetchBrandsByCategory: () => {},
  toggleCategoryFilters: () => {},
  fetchItems: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ItemBrandSelector));
