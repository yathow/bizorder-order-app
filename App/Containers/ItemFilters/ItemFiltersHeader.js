import { BackButton, BaseComponent } from '../../Components';
import { Button, Icon, View } from '@merchstack/react-native-ui';
import { Input, withTheme } from 'react-native-elements';

import React from 'react';
import SearchButton from '../../Components/Button/SearchButton';
import Style from './ItemFiltersHeaderStyle';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

class ItemFiltersHeader extends BaseComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      value,
      autoFocus,
      placeholder,
      onChangeText,
      onSearch,
      onSubmitEditing,
      returnKeyType,
      theme
    } = this.props;
    return (
      <View style={Style.container}>
        <BackButton style={Style.backBtn} />
        <Input
          value={value}
          inputStyle={Style.searchTxt}
          placeholder={placeholder}
          autoFocus={autoFocus}
          returnKeyType={returnKeyType}
          onChangeText={onChangeText}
          containerStyle={Style.searchPanel}
          onSubmitEditing={onSubmitEditing}
        />
        {this.renderClearButton()}
        <SearchButton style={Style.searchBtn(theme)} onPress={onSearch} />
      </View>
    );
  }

  renderClearButton() {
    const { value, onClear, theme } = this.props;
    if (value !== undefined && value.length > 0) {
      return (
        <TouchableWithoutFeedback onPress={onClear}>
          <Icon
            name="clear"
            size={Style.clearIconSize(theme)}
            iconStyle={Style.clearIcon(theme)}
            containerStyle={Style.clearPanel(theme)}
          />
        </TouchableWithoutFeedback>
      );
    } else {
      return null;
    }
  }
}

export default withTheme(ItemFiltersHeader);
