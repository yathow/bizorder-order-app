import { Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    padding: Metrics.md,
    paddingLeft: Metrics.sm,
    paddingBottom: Metrics.sm,
    flexDirection: 'row',
    alignItems: 'center'
  },
  searchPanel: {
    flex: 1
  },
  backBtn: {
    padding: Metrics.sm,
    color: 'black'
  },
  searchBtn: theme => ({
    ...Styles.smallBtn,
    marginRight: Metrics.sm,
    color: theme.colors.inverse,
    borderRadius: Metrics.borderRadius,
    backgroundColor: theme.backgroundColors.primary
  }),
  clearIconSize: theme => theme.fonts.h1.fontSize,
  clearIcon: theme => ({
    color: theme.colors.error
  }),
  clearPanel: theme => ({
    marginRight: Metrics.lg
  }),
  searchTxt: {
    padding: 0,
    borderColor: 'transparent'
  }
});
