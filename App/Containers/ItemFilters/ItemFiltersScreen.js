import { BaseComponent, BaseView } from '../../Components';
import { Button, Divider, Text, View } from '@merchstack/react-native-ui';

import ItemBrandSelector from './ItemBrandSelector';
import ItemCategorySelector from './ItemCategorySelector';
import ItemFiltersHeader from './ItemFiltersHeader';
import ItemSortSelector from './ItemSortSelector';
import AppActions from '../../Stores/App/Actions'
import ItemsActions from '../../Stores/Items/Actions';
import BrandsActions from '../../Stores/Brands/Actions';
import CategoriesActions from '../../Stores/Categories/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import Style from './ItemFiltersScreenStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class ItemFiltersScreen extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionUpdateKeyword = this.actionUpdateKeyword.bind(this);
    this.actionClearKeyword = this.actionClearKeyword.bind(this);
    this.actionApplyFilters = this.actionApplyFilters.bind(this);
    this.actionClearNewFilters = this.actionClearNewFilters.bind(this);
  }

  componentDidUpdate(prevProps) {}

  componentWillUnmount() {
    const { updateRoute } = this.props

    updateRoute('')
  }

  render() {
    const { keyword } = this.props;
    return (
      <BaseView showHeader={false} showMenuButton={false} showSearchButton={false} showFilterButton={false}>
        <ScrollView>
          <View style={Style.container}>
            <ItemFiltersHeader
              placeholder={translate('looking_for')}
              value={keyword}
              autoFocus={true}
              onChangeText={this.actionUpdateKeyword}
              onSearch={this.actionApplyFilters}
              onSubmitEditing={this.actionApplyFilters}
              onClear={this.actionClearKeyword}
              returnKeyType="search"
            />
            <Divider style={Style.divider} />
            <View style={Style.panel}>
              <Text h2>{translate('brands')}</Text>
              <ItemBrandSelector />
            </View>
            <Divider style={Style.divider} />
            <View style={Style.panel}>
              <Text h2>{translate('categories')}</Text>
              <ItemCategorySelector />
            </View>
            <Divider style={Style.divider} />
            <View style={Style.panel}>
              <Text h2>{translate('sort_by')}</Text>
              <ItemSortSelector />
            </View>
            <View style={Style.bottomBtnPanel}>
              <Button
                status="secondary"
                containerStyle={Style.btn}
                title={translate('clear_filters')}
                onPress={this.actionClearNewFilters}
              />
              <Button
                status="primary"
                containerStyle={Style.btn}
                title={translate('search')}
                onPress={this.actionApplyFilters}
              />
            </View>
          </View>
        </ScrollView>
      </BaseView>
    );
  }

  actionUpdateKeyword = keyword => {
    const { updateKeyword } = this.props;
    updateKeyword(keyword);
  };

  actionClearKeyword = keyword => {
    const { updateKeyword } = this.props;
    updateKeyword('');
  };

  actionApplyFilters = () => {
    const { applyFilters } = this.props;
    applyFilters();
  };

  actionClearNewFilters = () => {
    const { divisionId, clearNewFilters, fetchBrands, fetchCategories } = this.props;
    clearNewFilters();

    fetchBrands(divisionId)
    fetchCategories(divisionId)
  };
}

const mapStateToProps = state => {
  return {
    keyword: state.items.newFilters.keyword,
    brandIds: state.items.newFilters.brandIds,
    categoryIds: state.items.newFilters.categoryIds,
    divisionId: state.app.divisionId,
    route: state.app.route,
  };
};

const mapDispatchToProps = dispatch => ({
  applyFilters: () => dispatch(ItemsActions.applyFilters()),
  clearNewFilters: () => dispatch(ItemsActions.clearNewFilters()),
  updateKeyword: keyword => dispatch(ItemsActions.updateKeyword(keyword)),
  fetchCategories: divisionId => dispatch(CategoriesActions.fetchCategories(divisionId)),
  fetchBrands: divisionId => dispatch(BrandsActions.fetchBrands(divisionId)),
  updateRoute: route => dispatch(AppActions.updateRoute(route))
});

ItemFiltersScreen.propTypes = {
  keyword: PropTypes.string,
  route: PropTypes.string,

  applyFilters: PropTypes.func,
  clearFilters: PropTypes.func,
  updateKeyword: PropTypes.func,
  fetchCategories: PropTypes.func,
  fetchBrands: PropTypes.func,
  clearNewFilters: PropTypes.func,
  updateRoute: PropTypes.func,
};

ItemFiltersScreen.defaultProps = {
  keyword: '',
  route: PropTypes.string,

  applyFilters: () => {},
  clearFilters: () => {},
  updateKeyword: () => {},
  fetchCategories: () => {},
  fetchBrands: () => {},
  clearNewFilters: () => {},
  updateRoute: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ItemFiltersScreen));
