import { Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  page: {
    flex: 1
  },
  container: {
    flex: 1
  },
  content: {
    ...Styles.content
  },
  divider: {
    marginRight: Metrics.md,
    marginLeft: Metrics.md
  },
  panel: {
    padding: Metrics.md
  },
  bottomBtnPanel: {
    padding: Metrics.md
  },
  btn: {
    marginTop: Metrics.md
  }
});
