import { Button, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import ItemsActions from '../../Stores/Items/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './SelectorStyle';
import { connect } from 'react-redux';
import { withTheme } from 'react-native-elements';

class ItemSortSelector extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionSelectSortType = this.actionSelectSortType.bind(this);
  }

  componentDidMount() {
    const { fetchSortTypes } = this.props;
    fetchSortTypes();
  }

  render() {
    const { sortType } = this.props;
    return (
      <View style={Style.filterPanel}>
        {this.props.sortTypes.map((cSortType, index) => {
          let isSelected = cSortType.id === sortType;

          let buttonStyle = isSelected
            ? Style.selectedBtn(this.props.theme)
            : Style.btn(this.props.theme);

          return (
            <View key={index} style={Style.containerBtn}>
              <Button
                title={cSortType.name}
                buttonStyle={buttonStyle}
                icon={{
                  name: cSortType.order === 'asc' ? 'arrow-drop-up' : 'arrow-drop-down'
                }}
                iconRight={true}
                onPress={() => this.actionSelectSortType(cSortType.id)}
              />
            </View>
          );
        })}
      </View>
    );
  }

  actionSelectSortType = id => {
    const { selectSortType } = this.props;
    selectSortType(id);
  };
}

const mapStateToProps = state => ({
  sortTypes: state.items.sortTypes,
  sortType: state.items.newFilters.sortType
});

const mapDispatchToProps = dispatch => ({
  fetchSortTypes: () => dispatch(ItemsActions.fetchSortTypes()),
  selectSortType: sortType => dispatch(ItemsActions.selectSortType(sortType))
});

ItemSortSelector.propTypes = {
  sortTypes: PropTypes.array,
  sortType: PropTypes.number,

  fetchSortTypes: PropTypes.func,
  selectSortType: PropTypes.func
};

ItemSortSelector.defaultProps = {
  sortTypes: '',
  sortType: 0,

  fetchSortTypes: () => {},
  selectSortType: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ItemSortSelector));
