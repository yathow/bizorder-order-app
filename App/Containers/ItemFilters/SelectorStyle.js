import { Colors, Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  brands: {
    flex: 1
  },
  filterPanel: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    paddingTop: Metrics.md,
    paddingBottom: Metrics.md
  },
  containerBtn: {
    padding: Metrics.sm
  },
  btn: theme => ({
    ...Styles.smallBtn,
    borderRadius: Metrics.borderRadius,
    backgroundColor: Colors.transparent
  }),
  selectedBtn: theme => ({
    ...Styles.smallBtn,
    borderRadius: Metrics.borderRadius,
    backgroundColor: theme.backgroundColors.secondary
  }),
  inputStyle: theme => ({
    fontSize: theme.fonts.regular
  })
});
