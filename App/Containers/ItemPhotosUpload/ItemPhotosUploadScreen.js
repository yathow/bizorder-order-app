import React from 'react';
import { View, SafeAreaView, FlatList, ActivityIndicator } from 'react-native';
import { Input, Image, Button, Text } from 'react-native-elements';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import ItemDetailsActions from '../../Stores/ItemDetails/Actions';
import Style from './ItemPhotosUploadStyle';
import * as Yup from 'yup';
import { Formik } from 'formik';
import ImagePicker from 'react-native-image-crop-picker';
import { translate, setI18nConfig, addLocaleChangedListener, removeLocaleChangedListener } from '../../Translations';

class ItemPhotosUploadScreen extends React.PureComponent {
  constructor() {
    super();

    setI18nConfig(); // set initial config

    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
    this.useStopUploadItemPhoto = this.useStopUploadItemPhoto.bind(this);
    this._renderItem = this._renderItem.bind(this);
    this._renderSeparator = this._renderSeparator.bind(this);
  }

  useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
  }

  componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);
  }

  componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);
  }

  componentDidUpdate(prevProps, prevState) {
  }

  componentWillUnmount() {
    /*
    ImagePicker.clean().then(() => {
      
    }).catch(e => {
      
    });
    */
  }

  useStopUploadItemPhoto() {
    this.props.stopUploadItemPhoto();
  }

  _renderItem({item, index, separators}) {
    let initialValues = {
      itemId: this.props.itemId,
      desc01: '',
      desc02: '',
    }
    
    return (
        <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            onSubmit={(values, formikBag) => {
              let itemId =values.itemId;
              let isFeatured = false;
              let uomId = 35;
              let desc01 = values.desc01;
              let desc02 = values.desc02;
              //dispatch the action
              this.props.uploadItemPhoto(formikBag, index, itemId, isFeatured, uomId, desc01, desc02, item);
            }}
            validationSchema={Yup.object().shape({

            })}
        >
        {({ values, handleChange, errors, setFieldTouched, dirty, touched, isSubmitting, isValid, handleSubmit }) => (
            <View style={Style.formContainer}>
              <Image 
                style={Style.mediumImage}
                source={{
                  uri: item.path, 
                  width: item.width, 
                  height: item.height, 
                  mime: item.mime
                }}
                resizeMode='contain'
                PlaceholderContent={<ActivityIndicator />}
              />
              <Input
                  containerStyle={Style.formElement}
                  value={values.desc01}
                  onChangeText={handleChange('desc01')}
                  placeholder={translate('desc01')}
                  errorMessage={errors.desc_01}
              />
              <Input
                  containerStyle={Style.formElement}
                  value={values.desc02}
                  onChangeText={handleChange('desc02')}
                  placeholder={translate('desc02')}
                  secureTextEntry={true}
                  errorMessage={errors.desc_02}
              />
              <Button 
                  containerStyle={Style.formElement}
                  disabled={!isValid}
                  loading={isSubmitting}
                  onPress={handleSubmit} 
                  title={translate('upload')}
              />
            </View>
        )}
        </Formik>
    );
  }

  _renderSeparator() {
    return (
      <View
        style={{
          borderBottomColor: 'black',
          borderBottomWidth: 1,
        }}
      />
    );
  }

  render() {
    return (
      <SafeAreaView style={Style.container}>
        {this.props.successMessage !== '' &&
          <Text style={Style.successText}>{this.props.successMessage}</Text>
        }
        {this.props.errorMessage !== '' &&
          <Text style={Style.errorText}>{this.props.errorMessage}</Text>
        }
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={this.props.selectedPhotos}
          renderItem={this._renderItem}
          ItemSeparatorComponent={this._renderSeparator}
          ListEmptyComponent={
            <View style={Style.noPhotoContainer}>
              <Text style={Style.noPhotoText}>{translate('no_photo_to_upload')}</Text>
              <Button 
                style={{}}
                onPress={this.useStopUploadItemPhoto} 
                title={translate('back')} 
              />
            </View>
          }
        />
      </SafeAreaView>
    )
  }
}

ItemPhotosUploadScreen.propTypes = {
  itemId: PropTypes.number,
  selectedPhotos: PropTypes.array,
  successMessage: PropTypes.string,
  errorMessage: PropTypes.string,
  uploadItemPhoto: PropTypes.func,
}

const mapStateToProps = (state) => ({
  itemId: state.itemDetails.itemId,
  selectedPhotos: state.itemDetails.selectedPhotos,
  successMessage: state.itemDetails.successMessage,
  errorMessage: state.itemDetails.errorMessage,
})

const mapDispatchToProps = (dispatch) => ({
  uploadItemPhoto: (formikBag, itemPhotoIndex, itemId, isFeatured, uomId, desc01, desc02, photo) => dispatch(ItemDetailsActions.uploadItemPhoto(formikBag, itemPhotoIndex, itemId, isFeatured, uomId, desc01, desc02, photo)),
  stopUploadItemPhoto: () => dispatch(ItemDetailsActions.stopUploadItemPhoto())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemPhotosUploadScreen)
