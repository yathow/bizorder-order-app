import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    mediumImage: {
        width: 300, 
        height: 300,
    },
    formContainer: {
        flexDirection: 'column',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10
    },
    formElement: {
        marginBottom: 10,
    },
    noPhotoContainer: {
        flexDirection: 'column',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10
    },
    noPhotoText: {
        alignSelf: 'center'
    },
    errorContainer: {
        flex: 0.1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFB6C1'
    },
    errorText: {
        marginLeft: 10,
        color: 'red'
    },
    successContainer: {
        flex: 0.1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#98FB98'
    },
    successText: {
        marginLeft: 10,
        color: 'green'
    }
})
