import { FlatList, LoadingIndicator, Modal, NumberPicker } from '@merchstack/react-native-ui';
import { Text, View } from 'react-native';

import { BaseComponent } from '../../Components';
import CartActions from '../../Stores/Cart/Actions';
import CartSuggestions from '../Cart/CartSuggestions';
import ConfirmDialog from '../../Components/Dialog/ConfirmDialog';
import ItemImage from '../ItemDetails/ItemImage';
import ItemInfo from '../ItemDetails/ItemInfo';
import ItemQtyActions from '../../Stores/ItemQty/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './ItemQtyStyles';
import { connect } from 'react-redux';
import { formatCurrency } from '../../Formatter/NumberFormat';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';
import { fetchItemQty } from '../../Sagas/ItemQtySaga';

class ItemQty extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionConfirm = this.actionConfirm.bind(this);
    this.actionDecrement = this.actionDecrement.bind(this);
    this.actionDismiss = this.actionDismiss.bind(this);
    this.actionIncrement = this.actionIncrement.bind(this);
    this.actionQtyChange = this.actionQtyChange.bind(this);
    this.actionUpdateQty = this.actionUpdateQty.bind(this);
  }

  componentDidMount() {
    const { resetItemQty } = this.props;
    resetItemQty();
  }

  componentDidUpdate(prevProps) {
    const { showItemQty, itemId, fetchItemQty, resetItemQty, uoms } = this.props;
    if (showItemQty) {
      // need fetch
      if (itemId !== prevProps.itemId) {
        fetchItemQty(itemId);
      }
    } else {
      // reset
      resetItemQty();
    }
  }

  render() {
    const { item, uoms, itemIsLoading, showItemQty } = this.props;
    
    return (
      <ConfirmDialog
        isVisible={showItemQty}
        confirmText={translate('confirm')}
        onConfirm={this.actionConfirm}
        cancelText={translate('cancel')}
        onCancel={this.actionDismiss}
        isLoading={itemIsLoading}
      >
        {this.renderItemQty(item, uoms)}
      </ConfirmDialog>
    );
  }

  renderItemQty(item, uoms) {
    const { itemIsLoading } = this.props;
    if (itemIsLoading) {
      return (
        <View style={Style.container(0)}>
          <LoadingIndicator size="large" />
        </View>
      );
    } else if (!item) {
      return <View style={Style.container(0)} />;
    } else {
      return (
        <View style={Style.container(uoms.length + item.suggestions.length)}>
          <View style={Style.itemInfo}>
            <ItemInfo item={item} />
          </View>
          <View style={Style.imagePanel}>
            <ItemImage style={Style.itemImage} photos={item.photos} />
          </View>
          <CartSuggestions suggestions={item.suggestions} />
          {this.renderUomQtys(uoms)}
        </View>
      );
    }
  }

  renderUomQtys(uoms) {
    if (uoms) {
      return (
        <FlatList
          data={uoms}
          keyExtractor={uom => uom.id.toString()}
          renderItem={this.renderUomQty}
        />
      );
    } else {
      return null;
    }
  }

  renderUomQty = data => {
    const { theme } = this.props;

    const rowStyle = (data.index + 1) % 2 === 0 ? Style.qtyRowEven(theme) : Style.qtyRowOdd(theme);
    const uom = data.item;
    if (uom) {
      const price = uom.price;
      const oriPrice = uom.oriPrice;
      return (
        <View style={rowStyle}>
          <View style={Style.ratePanel}>
            <Text style={Style.uomLbl}>{uom.uomCode}</Text>
            <View style={Style.pricePanel}>
            {price.length > 0 
              ? ( <>
                    <Text style={Style.oriPrice(theme)}>{formatCurrency(oriPrice)}</Text>
                    {price.map((p, index) => (
                        <Text key={index} style={index == price.length-1 ? Style.itemUomPrice1(theme) : Style.itemUomPrice2(theme)}>
                          { p > 0 ? formatCurrency(p) : translate('foc')}
                        </Text>
                      )
                    )}
                  </>
              ) : (
                <Text style={Style.itemUomPrice1(theme)}>{formatCurrency(oriPrice)}</Text>
              )
            }
            </View>
          </View>
          <View style={Style.qtyPanel}>
            <NumberPicker
              style={Style.qty}
              value={uom.qty.toString()}
              onChangeText={text => this.actionQtyChange(uom.id, text)}
              increment={() => this.actionIncrement(uom.id, uom.qty)}
              decrement={() => this.actionDecrement(uom.id, uom.qty)}
            />
          </View>
        </View>
      );
    }
  };

  actionIncrement = (uomId, qty) => {
    this.actionUpdateQty(uomId, qty + 1);
  };

  actionDecrement = (uomId, qty) => {
    this.actionUpdateQty(uomId, qty > 0 ? qty - 1 : 0);
  };

  actionQtyChange = (uomId, text) => {
    let qty = parseInt(text, 10);
    if (!qty || qty < 0) {
      qty = 0;
    }
    this.actionUpdateQty(uomId, qty);
  };

  actionUpdateQty = (uomId, qty) => {
    const { updateItemQty, itemId } = this.props;
    updateItemQty(uomId, qty);
  };

  actionDismiss = () => {
    const { setShowItemQty } = this.props;
    setShowItemQty(false);
  };

  actionConfirm = () => {
    const { saveItemQty } = this.props;
    saveItemQty();
  };
}

const mapStateToProps = state => ({
  itemId: state.itemQty.itemId,
  item: state.itemQty.itemQty.item,
  uoms: state.itemQty.itemQty.uoms,
  uom: state.itemQty.itemQty.uom,
  showItemQty: state.itemQty.showItemQty,
  itemIsLoading: state.itemQty.itemIsLoading,
  uomsIsLoading: state.itemQty.uomsIsLoading
});

const mapDispatchToProps = dispatch => ({
  resetItemQty: boolean => dispatch(ItemQtyActions.resetItemQty()),
  fetchItemQty: itemId => dispatch(ItemQtyActions.fetchItemQty(itemId)),
  setItemQty: (item, uoms) => dispatch(ItemQtyActions.setItemQty(item, uoms)),
  setShowItemQty: boolean => dispatch(ItemQtyActions.setShowItemQty(boolean)),
  updateItemQty: (uomId, qty) => dispatch(ItemQtyActions.updateItemQty(uomId, qty)),
  setItemQtyInCart: (itemId, uomId, qty) =>
    dispatch(CartActions.setItemQtyInCart(itemId, uomId, qty)),
  saveItemQty: () => dispatch(ItemQtyActions.saveItemQty())
});

ItemQty.propTypes = {
  itemId: PropTypes.number,
  item: PropTypes.object,
  uoms: PropTypes.array,
  uom: PropTypes.object,
  showItemQty: PropTypes.bool,
  itemIsLoading: PropTypes.bool,
  uomsIsLoading: PropTypes.bool,

  resetItemQty: PropTypes.func,
  fetchItemQty: PropTypes.func,
  setItemQty: PropTypes.func,
  setShowItemQty: PropTypes.func,
  updateItemQty: PropTypes.func,
  setItemQtyInCart: PropTypes.func,
  saveItemQty: PropTypes.func
};

ItemQty.defaultProps = {
  itemId: 0,
  item: {},
  uoms: [],
  uom: {},
  showItemQty: false,
  itemIsLoading: false,
  uomsIsLoading: false,

  resetItemQty: () => {},
  fetchItemQty: () => {},
  setItemQty: () => {},
  setShowItemQty: () => {},
  updateItemQty: () => {},
  setItemQtyInCart: () => {},
  saveItemQty: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ItemQty));
