import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: length => ({
    padding: Metrics.md,
    minHeight: 50
    // height: 380 + length * 35
  }),
  imagePanel: {
    alignItems: 'center',
    padding: Metrics.md
  },
  itemImage: {
    width: 200,
    height: 200
  },
  itemInfo: {
    padding: Metrics.md
  },
  itemCode: theme => ({}),
  itemDesc: theme => ({
    color: theme.colors.primary,
    fontWeight: 'bold'
  }),
  qtyRowEven: theme => ({
    backgroundColor: theme.backgroundColors.even,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: Metrics.sm
  }),
  qtyRowOdd: theme => ({
    backgroundColor: theme.backgroundColors.odd,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: Metrics.sm
  }),
  ratePanel: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md
  },
  pricePanel: {
    flexDirection: 'column'
  },
  qtyPanel: {
    flex: 1
  },
  uomLbl: {},
  separator: {
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md
  },
  priceLbl: theme => ({
    color: theme.colors.secondary
  }),
  oriPrice: theme => ({
    color: theme.colors.muted,
    paddingLeft: Metrics.sm,
    textDecorationLine: 'line-through'
  }),
  qty: {
    width: 125
  },
  buttonsRow: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    padding: 0,
    marginTop: Metrics.xl
  },
  btn: theme => ({
    flex: 1,
    textAlign: 'center',
    color: theme.colors.link,
    fontWeight: 'bold',
    padding: Metrics.md,
    marginLeft: Metrics.sm,
    marginRight: Metrics.sm
  }),
  itemUomPrice1: theme => ({
    paddingLeft: Metrics.sm,
    color: theme.colors.success
  }),
  itemUomPrice2: theme => ({
    paddingLeft: Metrics.sm,
    textDecorationLine: 'line-through',
    color: theme.colors.secondary
  }),
});
