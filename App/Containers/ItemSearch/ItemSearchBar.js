import { TextInput, TouchableOpacity } from 'react-native';

import { BaseComponent } from '../../Components';
import { Icon } from '@merchstack/react-native-ui';
import ItemsActions from '../../Stores/Items/Actions';
import React from 'react';
import Style from './ItemSearchBarStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class ItemSearchBar extends BaseComponent {
  render() {
    return (
      <TouchableOpacity
        style={Style.container(this.props.theme)}
        onPress={() => {
          this.props.fetchFilters();
        }}
      >
        <Icon containerStyle={Style.icon} name="search" type="material-icons" />
        <TextInput
          editable={false}
          style={Style.searchInput(this.props.theme)}
          value={this.props.keyword}
          placeholder={translate('looking_for')}
        />
        <Icon containerStyle={Style.icon} name="filter" type="material-community" />
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = state => {
  return {
    keyword: state.items.filters.keyword
  };
};

const mapDispatchToProps = dispatch => ({
  resetDivisions: () => dispatch(ItemsActions.resetDivisions()),
  fetchDivisions: () => dispatch(ItemsActions.fetchDivisions()),
  fetchFilters: () => dispatch(ItemsActions.fetchFilters())
});

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ItemSearchBar));
