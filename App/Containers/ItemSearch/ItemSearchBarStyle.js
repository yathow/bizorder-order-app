import { Fonts, Metrics } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: theme => {
    return {
      flexDirection: 'row',
      alignItems: 'center',
      padding: Metrics.sm,
      borderColor: theme.backgroundColors.primary,
      borderWidth: 3,
      borderRadius: Metrics.borderRadius,
      margin: Metrics.md
    };
  },
  searchInput: theme => ({
    flex: 1,
    padding: Metrics.sm,
    fontSize: Fonts.input,
    color: theme.colors.input
  }),
  icon: {
    padding: Metrics.sm
  }
});
