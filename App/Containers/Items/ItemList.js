import { DataProvider, LayoutProvider } from 'recyclerlistview';

import { BaseComponent } from '../../Components';
import ItemDetailsActions from '../../Stores/ItemDetails/Actions';
import ItemListItem from './ItemListItem';
import ItemsActions from '../../Stores/Items/Actions';
import { Metrics } from '../../Theme';
import NoItems from './NoItems';
import PropTypes from 'prop-types';
import React from 'react';
import { RecyclerView } from '@merchstack/react-native-ui';
import { connect } from 'react-redux';
import { withTheme } from 'react-native-elements';

class ItemList extends BaseComponent {
  constructor() {
    super();

    this.dataProvider = new DataProvider((r1, r2) => {
      return r1 !== r2;
    });

    this.state = {
      dataProvider: this.dataProvider.cloneWithRows([])
    };

    this.layoutProvider = new LayoutProvider(
      index => {
        let item = this.props.items[index];

        let promoLines = 0;
        item.promotions.filter(promotion => {
          if(promotion.applied){
            let lines = parseInt(promotion.desc1.length / 50, 10) + 1;
            promoLines += lines;
          }
        });

        return {
          uoms: item.uoms.length,
          promos: promoLines,
          discount: item.discount
        };
      },
      (type, dim) => {
        const { uoms, promos, discount } = type;
        const leftColWidth =
          Metrics.catalog.items.height + 15 + (discount.rate > 0 || discount.price > 0 ? 28 : 0);
        const rightColWidth =
          100 +
          uoms * Metrics.catalog.uom.height +
          (promos > 0 ? Metrics.md : 0) +
          promos * Metrics.catalog.promo.height;

        dim.width = Metrics.screenWidth() - 0.001;
        dim.height = Math.max(leftColWidth, rightColWidth);
      }
    );

    this.actionFetchMoreItems = this.actionFetchMoreItems.bind(this);
  }

  componentDidMount() {
    const { fetchItems } = this.props;
    fetchItems();
  }

  componentDidUpdate(prevProps) {
    const { refreshItems, fetchItems } = this.props;
    if (refreshItems) {
      fetchItems();
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      dataProvider: this.dataProvider.cloneWithRows(nextProps.items)
    });
  }

  render() {
    const { isLoading, items } = this.props;
    return (
        <RecyclerView
          dataProvider={this.state.dataProvider}
          layoutProvider={this.layoutProvider}
          isLoading={isLoading}
          data={items}
          rowRenderer={this.rowRenderer}
          onEndReachedThreshold={5}
          onEndReached={this.actionFetchMoreItems}
          renderEmptyList={isLoading ? null : <NoItems />}
        />
    );
  }

  rowRenderer = (type, data, index) => {
    return <ItemListItem item={data} index={index} />;
  };

  actionFetchMoreItems() {
    const { isLoading, currentPage, lastPage, fetchMoreItems } = this.props;
    if (!isLoading && currentPage < lastPage) {
      fetchMoreItems();
    }
  }
}

const mapStateToProps = state => ({
  items: state.items.items,
  filters: state.items.filters,
  currentPage: state.items.currentPage,
  lastPage: state.items.lastPage,
  total: state.items.total,
  isLoading: state.items.isLoading,
  refreshItems: state.items.refreshItems
});

const mapDispatchToProps = dispatch => ({
  resetItems: () => dispatch(ItemsActions.resetItems()),
  fetchItems: () => dispatch(ItemsActions.fetchItems()),
  fetchMoreItems: () => dispatch(ItemsActions.fetchMoreItems()),
});

ItemList.propTypes = {
  items: PropTypes.array,
  filters: PropTypes.object,
  currentPage: PropTypes.number,
  lastPage: PropTypes.number,
  total: PropTypes.number,
  isLoading: PropTypes.bool,
  refreshItems: PropTypes.bool,

  resetItems: PropTypes.func,
  fetchItems: PropTypes.func,
  fetchMoreItems: PropTypes.func,
  showItemDetails: PropTypes.func,
};

ItemList.defaultProps = {
  items: [],
  filters: {},
  currentPage: 1,
  lastPage: 1,
  total: 0,
  isLoading: false,
  refreshItems: false,

  resetItems: () => {},
  fetchItems: () => {},
  fetchMoreItems: () => {},
  showItemDetails: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ItemList));
