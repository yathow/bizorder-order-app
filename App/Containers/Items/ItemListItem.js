import React, { Component } from 'react';

import ItemDetailsActions from '../../Stores/ItemDetails/Actions';
import ItemDiscount from '../ItemDetails/ItemDiscount';
import ItemImage from '../ItemDetails/ItemImage';
import ItemInfo from '../ItemDetails/ItemInfo';
import ItemPromotionTags from '../ItemDetails/ItemPromotionTags';
import PropTypes from 'prop-types';
import Style from './ItemListItemStyle';
import { TouchableWithoutFeedback } from 'react-native';
import { View } from '@merchstack/react-native-ui';
import { connect } from 'react-redux';
import { withTheme } from 'react-native-elements';

class ItemListItem extends Component {
  constructor(props) {
    super(props);

    this.actionShowItemDetails = this.actionShowItemDetails.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.item.id !== nextProps.item.id) {
      return true;
    }
    return false;
  }

  render() {
    const { theme, item, index } = this.props;
    const rowStyle = (index + 1) % 2 === 0 ? Style.evenItemContent : Style.oddItemContent;
    return (
      <TouchableWithoutFeedback onPress={() => this.actionShowItemDetails(item.id, item)}>
        <View style={rowStyle(theme)}>
          <View>
            <ItemImage style={Style.itemImage} photos={item.photos} />
            <ItemDiscount discount={item.discount} />
          </View>
          <View style={Style.itemInfo}>
            <ItemInfo item={item} uoms={item.uoms} />
            <View style={Style.itemPromos}>
              <ItemPromotionTags item={item} theme={theme} expandPromotion={false}/>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  actionShowItemDetails(itemId, item) {
    const { showItemDetails } = this.props;
    showItemDetails(itemId, item);
  }
}

const mapStateToProps = state => ({
  items: state.items.items,
  filters: state.items.filters,
  currentPage: state.items.currentPage,
  lastPage: state.items.lastPage,
  total: state.items.total,
  isLoading: state.items.isLoading,
});

const mapDispatchToProps = dispatch => ({
  showItemDetails: (itemId, item) => dispatch(ItemDetailsActions.showItemDetails(itemId, item))
});

ItemListItem.propTypes = {
  items: PropTypes.array,
  filters: PropTypes.object,
  currentPage: PropTypes.number,
  lastPage: PropTypes.number,
  total: PropTypes.number,
  isLoading: PropTypes.bool,

  showItemDetails: PropTypes.func
};

ItemListItem.defaultProps = {
  items: [],
  filters: {},
  currentPage: 1,
  lastPage: 1,
  total: 0,
  isLoading: false,

  showItemDetails: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ItemListItem));
