import { Colors, Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  page: {
    flex: 1
  },
  container: {
    ...Styles.container,
    flex: 1
  },
  content: {
    ...Styles.content,
    flex: 1
  },
  evenItemContent: theme => ({
    flex: 1,
    flexDirection: 'row',
    padding: Metrics.md,
    backgroundColor: theme.backgroundColors.even
  }),
  oddItemContent: theme => ({
    flex: 1,
    flexDirection: 'row',
    padding: Metrics.md,
    backgroundColor: theme.backgroundColors.odd
  }),
  itemImage: {
    width: Metrics.catalog.items.width,
    height: Metrics.catalog.items.height
  },
  itemInfo: {
    flex: 1,
    marginLeft: Metrics.md
  },
  itemPromos: {
    marginTop: Metrics.lg
  },
  actionPanel: {
    flexDirection: 'row',
    paddingTop: Metrics.sm,
    paddingBottom: Metrics.sm,
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  searchBar: {
    flex: 1
  },
  filterBtn: {
    backgroundColor: Colors.transparent
  }
});
