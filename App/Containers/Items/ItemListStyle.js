import { Colors, Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  page: {
    flex: 1
  },
  container: {
    ...Styles.container,
    flex: 1
  },
  content: {
    ...Styles.content,
    flex: 1
  },
  itemContainer: {},
  itemContent: {
    width: '50%',
    flexDirection: 'column',
    padding: Metrics.md
  },
  itemImage: {
    width: '100%',
    height: 175
  },
  itemInfo: {
    flex: 1,
    marginTop: Metrics.md
  },
  actionPanel: {
    flexDirection: 'row',
    paddingTop: Metrics.sm,
    paddingBottom: Metrics.sm,
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  searchBar: {
    flex: 1
  },
  filterBtn: {
    backgroundColor: Colors.transparent
  }
});
