import { BaseComponent, BaseView } from '../../Components';

import { View, Text, Divider } from '@merchstack/react-native-ui';
import ItemList from './ItemList';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import CategoriesActions from '../../Stores/Categories/Actions';
import BrandsActions from '../../Stores/Brands/Actions';
import ItemsActions from '../../Stores/Items/Actions';
import AppActions from '../../Stores/App/Actions';

import Style from '../ItemFilters/ItemFiltersScreenStyle';

import ItemCategorySelector from '../ItemFilters/ItemCategorySelector';
import ItemBrandSelector from '../ItemFilters/ItemBrandSelector';

class ItemScreens extends BaseComponent {
  componentDidMount() {
    const { brandIds, categoryIds, fetchBrandsByCategory, fetchCategoriesByBrand } = this.props

    if(categoryIds.length > 0 && brandIds.length == 0){
      fetchBrandsByCategory()
    } 
    if(brandIds.length > 0 && categoryIds.length == 0){
      fetchCategoriesByBrand()
    }
  }

  componentDidUpdate(prevProps) {
    const { route } = this.props

    if(prevProps.route !== route) {
      this.forceUpdate()
    }
  }

  componentWillUnmount() {
    const { divisionId, clearNewFilters, fetchBrands, fetchCategories } = this.props

    clearNewFilters()
    fetchBrands(divisionId)
    fetchCategories(divisionId)
  }

  render() {
    const { keyword, errorMessages, route, categories, brands, brandIds, categoryIds } = this.props;

    const brand = brands[brands.findIndex((b) => b.id == brandIds[0])] || null
    const category = categories[categories.findIndex((c) => c.id == categoryIds[0])] || null

    return (
      <BaseView title={
        (route[route.length-1] == 'Brands') 
        ? (brand ? brand.name : '')
        : (route[route.length-1] == 'Categories') 
        ? (category ? category.name : '')
        : (keyword)
        } 
        showMenuButton={false}
        showCartButton={false}
        showFilterButton={false}
        errorMessage={errorMessages}
      >
        { 
          route[route.length-1] == 'Brands' 
          ? (categories.length > 1 
            ? (
              <>
                <View style={Style.panel}>
                  <Text h2>{translate('categories')}</Text>
                  <ItemCategorySelector />
                </View>
                <Divider style={Style.divider} />
              </>
            ) : (<></>)) 
            : route[route.length-1] == 'Categories' 
              ? (brands.length > 1 
                ? (
                  <>
                    <View style={Style.panel}>
                      <Text h2>{translate('brands')}</Text>
                      <ItemBrandSelector />
                    </View>
                    <Divider style={Style.divider} />
                  </>
                ) : (<></>)) : (<></>)
        }
        <ItemList />
      </BaseView>
    );
  }
}

const mapStateToProps = state => ({
  errorMessages: state.messages.errorMessages,
  keyword: state.items.filters.keyword,
  brandIds: state.items.filters.brandIds,
  categoryIds: state.items.filters.categoryIds,
  route: state.app.route,
  categories: state.categories.categories,
  brands: state.brands.brands
});

const mapDispatchToProps = dispatch => ({
  fetchCategoriesByBrand: () => dispatch(CategoriesActions.fetchCategoriesByBrand()),
  fetchBrandsByCategory: () => dispatch(BrandsActions.fetchBrandsByCategory()),
  fetchCategories: divisionId => dispatch(CategoriesActions.fetchCategories(divisionId)),
  fetchBrands: divisionId => dispatch(BrandsActions.fetchBrands(divisionId)),
  clearNewFilters: () => dispatch(ItemsActions.clearNewFilters()),
  updateRoute: route => dispatch(AppActions.updateRoute(route))
});

ItemScreens.propTypes = {
  errorMessages: PropTypes.array,
  keyword: PropTypes.string,
  brandIds: PropTypes.array,
  categoryIds: PropTypes.array,
  route: PropTypes.array,
  categories: PropTypes.array,
  brands: PropTypes.array,

  fetchCategories: PropTypes.func,
  fetchCategoriesByBrand: PropTypes.func,
  fetchBrands: PropTypes.func,
  fetchBrandsByCategory: PropTypes.func,
  clearNewFilters: PropTypes.func
};

ItemScreens.defaultProps = {
  errorMessages: [],
  keyword: '',
  brandIds: [],
  categoryId: [],
  route: [],
  categories: [],
  brands: [],

  fetchCategories: () => {},
  fetchCategoriesByBrand: () => {},
  fetchBrands: () => {}, 
  fetchBrandsByCategory: () => {},
  clearNewFilters: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemScreens);
