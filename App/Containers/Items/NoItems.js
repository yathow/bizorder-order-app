import { Text, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import React from 'react';
import Style from './NoItemsStyle';
import { translate } from '../../Translations';

class NoItems extends BaseComponent {
  render() {
    return (
      <View style={Style.content}>
        <Text>{translate('no_items')}</Text>
      </View>
    );
  }
}

export default NoItems;
