import { Text, View } from '@merchstack/react-native-ui';

import AppActions from '../../Stores/App/Actions';
import { BaseComponent } from '../../Components';
import { PropTypes } from 'prop-types';
import React from 'react';
import Style from './LocalesPickerStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class LocalesPicker extends BaseComponent {
  render() {
    const { theme, setLocale } = this.props;
    return (
      <View style={Style.container}>
        <Text style={Style.link(theme)} onPress={() => setLocale('en')}>
          {translate('english')}
        </Text>
        <Text style={Style.separator}>{translate('|')}</Text>
        <Text style={Style.link(theme)} onPress={() => setLocale('bm')}>
          {translate('bahasa_malaysia')}
        </Text>
      </View>
    );
  }
}

LocalesPicker.propTypes = {
  setLocale: PropTypes.func
};

const mapStateToProps = state => ({
  locale: state.app.locale
});

const mapDispatchToProps = dispatch => ({
  setLocale: locale => dispatch(AppActions.setLocale(locale))
});

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(LocalesPicker));
