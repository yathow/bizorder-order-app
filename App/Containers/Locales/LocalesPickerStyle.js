import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: Metrics.md,
    paddingTop: Metrics.lg
  },
  link: theme => ({
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md,
    color: theme.colors.link
  }),
  separator: {}
});
