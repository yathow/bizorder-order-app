import * as Yup from 'yup';

import {
  Button,
  ContentView,
  Image,
  Input,
  SafeAreaView,
  Text,
  View
} from '@merchstack/react-native-ui';
import React, { Fragment } from 'react';

import AppActions from '../../Stores/App/Actions';
import OptionsActions from '../../Stores/Options/Actions';
import { BaseComponent } from '../../Components';
import { Formik } from 'formik';
import { Images } from 'App/Theme';
import LocalesPicker from '../Locales/LocalesPicker';
import { PropTypes } from 'prop-types';
import { ScrollView } from 'react-native-gesture-handler';
import Style from './LoginStyle';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class LoginScreen extends BaseComponent {
  constructor() {
    super();

    this.state = {
      username: '',
      password: '',
      scannerToken: Date.now()
    };
  }

  componentDidMount() {
    const { resetUsername, fetchUsername } = this.props;

    resetUsername();
    fetchUsername();
  }

  componentDidUpdate(prevProps) {
    const { username } = this.props;

    if (username && username !== prevProps.username) {
      this.setState({
        ...this.state,
        username: username
      });
    }
  }

  render() {
    const { theme, showOptions, successMessage, errorMessage } = this.props;

    let initialValues = {
      username: this.state.username,
      password: this.state.password,
      apiUrl: this.props.apiUrl
    };

    return (
      <SafeAreaView style={Style.page}>
        <ContentView
          style={Style.container(theme)}
          successMessage={successMessage}
          errorMessage={errorMessage}
        >
          <ScrollView style={Style.content}>
            <View style={Style.loginPanel}>
              <View style={Style.logoContainer}>
                <Image source={Images.logo} style={Style.logoImage} resizeMode="contain" />
              </View>
              <View style={Style.formContainer}>
                <Formik
                  enableReinitialize={true}
                  initialValues={initialValues}
                  onSubmit={(values, formikBag) => {
                    let username = values.username;
                    let password = values.password;
                    //save to state
                    this.setState({
                      username: username,
                      password: password
                    });
                    //dispatch the action
                    this.props.authenticate(formikBag, username, password);
                  }}
                  validationSchema={Yup.object().shape({
                    username: Yup.string().required(translate('username_is_required')),
                    password: Yup.string().required(translate('password_is_required')),
                    apiUrl: Yup.string().required(translate('url_is_required'))
                  })}
                >
                  {({
                    values,
                    handleChange,
                    errors,
                    setFieldTouched,
                    dirty,
                    touched,
                    isSubmitting,
                    isValid,
                    handleSubmit
                  }) => (
                    <Fragment>
                      <Input
                        value={values.username}
                        onChangeText={handleChange('username')}
                        placeholder={translate('username')}
                        errorMessage={errors.username}
                        disabled={isSubmitting}
                      />
                      <Input
                        value={values.password}
                        onChangeText={handleChange('password')}
                        placeholder={translate('password')}
                        secureTextEntry={true}
                        errorMessage={errors.password}
                        disabled={isSubmitting}
                      />
                      <View style={Style.btnPanel}>
                        <Button
                          status="primary"
                          disabled={!isValid}
                          loading={isSubmitting}
                          onPress={handleSubmit}
                          title={translate('login')}
                        />
                      </View>
                    </Fragment>
                  )}
                </Formik>
              </View>
              <View style={Style.optionPanel}>
                <Text style={Style.options(theme)} onPress={() => showOptions()}>
                  {translate('more_options')}
                </Text>
                {this.renderDemo()}
              </View>
              <LocalesPicker />
            </View>
          </ScrollView>
        </ContentView>
      </SafeAreaView>
    );
  }

  renderDemo() {
    const { theme, versionType } = this.props;
    if (versionType === 1) {
      return <Text style={Style.demo(theme)}>[ {translate('demo_version')} ]</Text>;
    } else {
      return null;
    }
  }
}

LoginScreen.propTypes = {
  versionType: PropTypes.number,
  apiUrl: PropTypes.string,
  username: PropTypes.string,
  locale: PropTypes.string,
  successMessage: PropTypes.array,
  errorMessage: PropTypes.array,

  resetUsername: PropTypes.func,
  fetchUsername: PropTypes.func,
  authenticate: PropTypes.func,
  showErrorMessage: PropTypes.func,
  updateApiUrl: PropTypes.func,
  setLocale: PropTypes.func
};

LoginScreen.defaultProps = {
  versionType: 0,
  apiUrl: '',
  username: '',
  locale: '',
  successMessage: [],
  errorMessage: [],

  resetUsername: () => {},
  fetchUsername: () => {},
  authenticate: () => {},
  showErrorMessage: () => {},
  updateApiUrl: () => {},
  setLocale: () => {},
  showOptions: () => {}
};

const mapStateToProps = state => ({
  versionType: state.app.versionType,
  apiUrl: state.app.apiUrl,
  username: state.app.username,
  locale: state.app.locale,
  successMessage: state.messages.successMessages,
  errorMessage: state.messages.errorMessages
});

const mapDispatchToProps = dispatch => ({
  resetUsername: () => dispatch(AppActions.resetUsername()),
  fetchUsername: () => dispatch(AppActions.fetchUsername()),
  authenticate: (formikBag, username, password) =>
    dispatch(AppActions.authenticate(formikBag, username, password)),
  showErrorMessage: errorMessage => dispatch(AppActions.showErrorMessage(errorMessage)),
  updateApiUrl: apiUrl => dispatch(AppActions.updateApiUrl(apiUrl)),
  setLocale: locale => dispatch(AppActions.setLocale(locale)),
  showOptions: () => dispatch(OptionsActions.showOptions())
});

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(LoginScreen));
