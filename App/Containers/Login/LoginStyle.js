import { Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  page: {
    flex: 1
  },
  container: theme => ({
    ...Styles.container,
    backgroundColor: theme.backgroundColors.primary
  }),
  content: {
    flex: 1,
    paddingTop: Metrics.xl,
    paddingBottom: Metrics.xl,
    marginTop: Metrics.xl
  },
  loginPanel: {
    flex: 1,
    padding: Metrics.lg,
    backgroundColor: 'white'
  },
  logoContainer: {
    alignItems: 'center',
    padding: Metrics.md
  },
  logoImage: {
    width: 275,
    height: 75
  },
  formContainer: {
    flex: 1,
    flexDirection: 'column',
    padding: Metrics.md
  },
  btnPanel: {
    paddingTop: Metrics.md
  },
  options: theme => ({
    color: theme.colors.link,
    padding: Metrics.md,
    flex: 1
  }),
  localeLink: theme => ({
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md,
    color: theme.colors.link
  }),
  demo: theme => ({
    padding: Metrics.md,
    color: theme.colors.error,
    fontWeight: 'bold'
  }),
  optionPanel: {
    flexDirection: 'row'
  }
});
