import React, { PureComponent } from 'react';
import { View } from '@merchstack/react-native-ui'
import { Images } from '../../Theme';
import { Avatar } from 'react-native-elements'

class NotificationImage extends PureComponent {
    render() {
        const navigation = this.props.payload.navigation ? this.props.payload.navigation.replace('Screen', '') : ''
        let source = Images.noPhotoIcon

        if(navigation == 'Promotions') {
            source = Images.promotionsNotification
        }
        
        return (
            <View style={this.props.style ? this.props.style : ''}>
                <Avatar
                    source={ source }
                />
            </View>
        );
    }
}

export default NotificationImage;