import React, { PureComponent } from 'react'
import { Text } from '@merchstack/react-native-ui'

class NotificationType extends PureComponent {
    render() {
        const navigation = this.props.payload.navigation
        
        if(navigation) {
            return (
                <Text style={this.props.style || ''}>
                    {
                        navigation == 'OrdersScreen' 
                        ? '[Sales Order]'
                        : navigation == 'PromotionsScreen'
                            ? '[PromotionsScreen]'
                            : ''
                    }
                </Text>
            )
        } else { return null }
    }
}

export default NotificationType