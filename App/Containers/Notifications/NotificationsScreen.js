import React from 'react';

import { BaseComponent, BaseView, EmptyComponent } from '../../Components';
import { Text, View, LoadingIndicator } from '@merchstack/react-native-ui'
import { TouchableHighlight, TouchableOpacity } from 'react-native'
import { withTheme } from 'react-native-elements'
import { SwipeListView } from 'react-native-swipe-list-view'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { translate } from '../../Translations';

import NotificationsActions from '../../Stores/Notifications/Actions'
import PromotionsActions from '../../Stores/Promotions/Actions'
import OrderActions from '../../Stores/Order/Actions'

import Time from '../../Components/MomentJS/Time'

import Style from './NotificationsStyle'
import NotificationType from './NotificationType'
import NotificationImage from './NotificationImage'
import ConfirmDialog from '../../Components/Dialog/ConfirmDialog';

class NotificationsScreen extends BaseComponent {
    constructor(props) {
        super(props); 
    }

    componentDidMount() {
        const { fetchNotifications } = this.props

        fetchNotifications()
    }
    componentDidUpdate(prevProps) {
        
    }

    render() {
        const { errorMessages, isLoading, moreIsLoading, notifications, modalConfirmVisible, 
                modalConfirmType, confirmTitle, confirmMessage } = this.props;
        
        return (
        <BaseView
            title={translate('notifications')}
            showMenuButton={false}
            showSearchButton={false}
            showCartButton={false}
            showFilterButton={false}
            errorMessage={errorMessages}
        >
            <ConfirmDialog
                isVisible={modalConfirmVisible}
                title={translate(confirmTitle)}
                message={translate(confirmMessage)}
                confirmText={translate('confirm')}
                onConfirm={modalConfirmType == 'clear' 
                    ? this.actionClearNotifications 
                    : modalConfirmType == 'mark'
                        ? this.actionMarkAllAsSeen
                        : this.actionDeleteNotification
                }
                cancelText={translate('cancel')}
                onCancel={this.actionDismiss}
            />
            <View style={Style.container}>
                <SwipeListView
                    data={notifications}
                    keyExtractor={this.getNotificationKey}
                    refreshing={false}
                    isLoading={isLoading}
                    onRefresh={() => {
                        this.props.fetchNotifications();
                    }}
                    renderItem={this.renderNotifications}
                    renderHiddenItem={this.renderHiddenItem}
                    leftOpenValue={75}
                    rightOpenValue={-75}
                    disableRightSwipe
                    onEndReached={this.actionFetchMoreNotifications}
                    onEndReachedThreshold={0.5}
                    ListEmptyComponent={
                        <EmptyComponent
                            title={translate('no_results_found')}
                            isLoading={isLoading}
                        />
                    }
                    ListFooterComponent={() => moreIsLoading && <LoadingIndicator style={Style.loading} size="large" />}
                /> 
                {this.renderFooter()}
            </View>
        </BaseView>
        );
    }

    getNotificationKey = ({ id }) => {
        return id.toString();
    };

    renderNotifications = ({ item }) => {
        return (
            <View style={[Style.rowFront, item.status == 1 ? Style.readRow(this.props.theme) : Style.unreadRow]}>
                <TouchableHighlight 
                    style={[Style.rowFrontVisible, item.status == 1 ? Style.readRow(this.props.theme) : Style.unreadRow]} 
                    onPress={() => this.actionOnPressNotification(item)}
                >
                    <View style={[Style.listContainer]}>
                        <NotificationImage style={Style.listImage} payload={item.payload}/>
                        <View style={Style.listContent}>
                            <Text style={[Style.title, item.status == 0 && Style.unreadText]} numberOfLines={1}>{item.title}</Text>
                            <Text style={[Style.details, item.status == 0 && Style.unreadText]} numberOfLines={2}>{item.content}</Text>
                            <View style={Style.contentFooter}>
                                <NotificationType style={Style.contentDate(this.props.theme)} payload={item.payload}/>
                                <Time style={Style.contentDate(this.props.theme)} time={item.createdAt} config="now"/>
                            </View>
                        </View>
                    </View>
                </TouchableHighlight>
            </View>
        )
    }

    renderHiddenItem = (data, rowMap) => {
        return (
            <View style={Style.rowBack}>
                <Text>Left</Text>
                <TouchableOpacity 
                    style={[Style.backRightBtn, Style.backRightBtnRight(this.props.theme)]}
                    onPress={() => {
                        this.deleteRow(data.item.id)
                        this.actionSetConfirmDialog('delete')
                    }} 
                >
                    <View style={Style.trash}>
                        <MaterialCommunityIcons 
                            name="trash-can-outline" 
                            size={20} 
                            color="#fff"
                        />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderFooter = () => {
        return (
            <>
                <View style={Style.footerSeperator}/>
                <View style={Style.footerContainer}>
                    <TouchableHighlight onPress={() => this.actionSetConfirmDialog('mark')}>
                        <View style={Style.markAsSeenContainer(this.props.theme)}>
                            <Text style={Style.markAsSeenLabel(this.props.theme)}>Mark All As Seen</Text>
                        </View>
                    </TouchableHighlight>
                    
                    <TouchableHighlight onPress={() => this.actionSetConfirmDialog('clear')}>
                        <View style={Style.clearNotificationsContainer(this.props.theme)}>
                            <Text style={Style.clearNotificationsLabel(this.props.theme)}>Clear All</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            </>
        )
    }

    deleteRow = (data) => {
        const { setDeleteNotification } = this.props
        setDeleteNotification(data)
    }

    actionDeleteNotification = () => {
        const { deleteNotification, setConfirmDialogVisible } = this.props

        //notification id to delete will be in the saga function
        deleteNotification()
        setConfirmDialogVisible(false)
    }

    actionFetchMoreNotifications = () => {
        const { fetchMoreNotifications } = this.props

        fetchMoreNotifications()
    }

    actionSetConfirmDialog = (confirmType) => {
        const { setConfirmDialog } = this.props
        let title, message = ''
        
        if(confirmType == 'clear') {
            title = 'clear_notifications'
            message = 'confirm_clear_notifications'
        } else if(confirmType == 'mark') {
            title = 'mark_seen'
            message = 'confirm_mark_seen'
        } else if(confirmType == 'delete') {
            title = 'delete_notification'
            message = 'confirm_delete_notification'
        }
        setConfirmDialog(title, message, confirmType, true)
    }

    actionDismiss = () => {
        const { setConfirmDialogVisible, setDeleteNotification } = this.props
        setConfirmDialogVisible(false)
        setDeleteNotification(0)
    }

    actionMarkAllAsSeen = () => {
        const { markAllAsSeen, setConfirmDialogVisible } = this.props

        markAllAsSeen()
        setConfirmDialogVisible(false)
    }

    actionClearNotifications = () => {
        const { clearNotifications, setConfirmDialogVisible } = this.props

        clearNotifications()
        setConfirmDialogVisible(false)
    }

    actionOnPressNotification(item) {
        const { readNotification, goToPromotion, goToOrder } = this.props

        const payloadId = item.payload.id
        const payloadNavigation = item.payload.navigation

        if(item.status == 0)
            readNotification(item.id)

        if(payloadNavigation == 'PromotionsScreen') 
            goToPromotion(payloadId)
        else if(payloadNavigation == 'OrderScreen') {
            goToOrder(payloadId)
        }
        
    }
}

const mapStateToProps = state => ({
    isLoading: state.notifications.notificationsIsLoading,
    moreIsLoading: state.notifications.moreNotificationsIsLoading,
    notifications: state.notifications.notifications,
    errorMessages: state.messages.errorMessages,
    modalConfirmVisible: state.notifications.modalConfirmVisible,
    modalConfirmType: state.notifications.modalConfirmType,
    confirmTitle: state.notifications.confirmTitle,
    confirmMessage: state.notifications.confirmMessage,
});

const mapDispatchToProps = dispatch => ({
    clearNotifications: () => dispatch(NotificationsActions.clearNotifications()),
    deleteNotification: () => dispatch(NotificationsActions.deleteNotification()),
    fetchNotifications: () => dispatch(NotificationsActions.fetchNotifications()),
    fetchMoreNotifications: () => dispatch(NotificationsActions.fetchMoreNotifications()),
    goToOrder: (id) => dispatch(OrderActions.viewOrder(id)),
    goToPromotion: (id) => dispatch(PromotionsActions.goToPromotion(id)),
    markAllAsSeen: () => dispatch(NotificationsActions.markAllAsSeen()),
    readNotification: (id) => dispatch(NotificationsActions.readNotification(id)),
    setConfirmDialog: (title, message, confirmType, boolean) => dispatch(NotificationsActions.setConfirmDialog(title, message, confirmType, boolean)),
    setConfirmDialogVisible: (boolean) => dispatch(NotificationsActions.setConfirmDialogVisible(boolean)),
    setDeleteNotification: (id) => dispatch(NotificationsActions.setDeleteNotification(id))
});

NotificationsScreen.propTypes = {
    isLoading: PropTypes.bool,
    notifications: PropTypes.array,
    errorMessages: PropTypes.array,
    moreIsLoading: PropTypes.bool,
    modalConfirmVisible: PropTypes.bool,
    modalConfirmType: PropTypes.string,
    confirmTitle: PropTypes.string,
    confirmMessage: PropTypes.string,

    fetchNotifications: PropTypes.func,
    fetchMoreNotifications: PropTypes.func,
    readNotification: PropTypes.func,
    setConfirmDialog: PropTypes.func,
    setConfirmDialogVisible: PropTypes.func,
    markAllAsSeen: PropTypes.func,
    clearNotifications: PropTypes.func,
    goToPromotion: PropTypes.func,
    goToOrder: PropTypes.func
};

NotificationsScreen.defaultProps = {
    isLoading: false,
    notifications: [],
    errorMessages: [],
    moreIsLoading: false,
    modalConfirmVisible: false,
    modalConfirmType: '',
    confirmTitle: '',
    confirmMessage: '',

    fetchNotifications: ()=>{},
    fetchMoreNotifications: ()=>{},
    readNotification: ()=>{},
    setConfirmDialog: ()=>{},
    setConfirmDialogVisible: ()=>{},
    markAllAsSeen: ()=>{},
    clearNotifications: ()=>{},
    goToPromotion: ()=>{},
    goToOrder: ()=>{}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(NotificationsScreen));
