import { Colors, Metrics, Styles } from '../../Theme';

import { StyleSheet, Dimensions } from 'react-native';


const DEVICE_WIDTH = Dimensions.get('screen').width
const DEVICE_HEIGHT = Dimensions.get('screen').height

export default StyleSheet.create({ 
    container: {
        backgroundColor: '#f4f4f4',
        flex: 1
    },
    backTextWhite: {
        color: '#fff'
    },
    unreadRow: {
        backgroundColor: '#fff',
    },
    readRow: theme => ({
        backgroundColor: theme.colors.grey
    }),
    rowFront: {
        borderRadius: 5,
        height: 92,
        margin: 5,
        marginBottom: 5,
        shadowColor: '#999',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    rowFrontVisible: {
        backgroundColor: '#fff',
        borderRadius: 5,
        height: 92,
        padding: 10,
        marginBottom: 5
    },
    listContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        width: DEVICE_WIDTH,
    },
    listImage: {
        marginRight: Metrics.md,
        width: DEVICE_WIDTH * 0.1
    },
    listContent: {
        width: DEVICE_WIDTH * 0.8
    },
    rowBack: {
        alignItems: 'center',
        backgroundColor: '#ddd',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 15,
        margin: 5,
        marginBottom: 5,
        borderRadius: 5,
    },
    backRightBtn: {
        alignItems: 'flex-end',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        width: 75,
        paddingRight: 17
    },
    backRightBtnRight: theme => ({
        backgroundColor: theme.colors.danger,
        right: 0,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    }),
    trash: {
        height: 25,
        width: 25,
        marginRight: 7
    },
    unreadText: {
        fontWeight: 'bold'
    },
    title: {
        fontSize: 14,
        marginBottom: 5,
        color: '#666'
    },
    details: {
        fontSize: 12,
        color: '#999'
    },
    footerSeperator: {
        height: 1,
        width: "100%",
        backgroundColor: "#CED0CE"
    },
    footerContainer: {
        flexDirection: 'row',
        width: DEVICE_WIDTH
    },
    markAsSeenContainer: theme => ({ 
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: theme.colors.secondary,
        padding: Metrics.lg,
        width: DEVICE_WIDTH * 0.5
    }),
    markAsSeenLabel: theme => ({
        color: theme.colors.inverse,
        fontWeight: 'bold'
    }),
    clearNotificationsContainer: theme => ({ 
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: theme.colors.danger,
        padding: Metrics.lg,
        width: DEVICE_WIDTH * 0.5
    }),
    clearNotificationsLabel: theme => ({
        color: theme.colors.inverse,
        fontWeight: 'bold'
    }),
    contentFooter: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    contentDate: theme => ({
        fontSize: 11,
        color: theme.colors.darkGrey1
    }),
})