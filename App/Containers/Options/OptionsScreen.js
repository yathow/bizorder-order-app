import { BaseComponent, BaseView } from '../../Components';
import { Text, View } from '@merchstack/react-native-ui';
import React from 'react';
import { translate } from '../../Translations';
import Style from './OptionsScreenStyle';

import VersionTypeSelector from './VersionTypeSelector';

class OptionsScreen extends BaseComponent {
  componentDidMount() {}

  render() {
    return (
      <BaseView
        title={translate('options')}
        showBackButton={true}
        showSearchButton={false}
        showMenuButton={false}
        showCartButton={false}
        showFilterButton={false}
      >
        <View style={Style.container}>
          <View style={Style.panel}>
            <Text h2>{translate('version')}</Text>
            <VersionTypeSelector />
          </View>
        </View>
      </BaseView>
    );
  }
}
export default OptionsScreen;
