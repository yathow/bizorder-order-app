import { Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  page: {
    flex: 1
  },
  content: {
    flex: 1
  },
  panel: {
    padding: Metrics.md
  }
});
