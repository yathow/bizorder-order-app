import { Button, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import AppActions from '../../Stores/App/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './SelectorStyle';
import { connect } from 'react-redux';
import { withTheme } from 'react-native-elements';
import { translate } from '../../Translations';

class VersionTypeSelector extends BaseComponent {
  constructor(props) {
    super(props);

    this.actionSelectVersionType = this.actionSelectVersionType.bind(this);
  }

  componentDidMount() {
    const { fetchVersionTypes } = this.props;
    fetchVersionTypes();
  }

  render() {
    const { versionType, versionTypes } = this.props;
    return (
      <View style={Style.filterPanel}>
        {versionTypes.map((cSortType, index) => {
          let isSelected = cSortType.id === versionType;

          let buttonStyle = isSelected
            ? Style.selectedBtn(this.props.theme)
            : Style.btn(this.props.theme);

          return (
            <View key={index} style={Style.containerBtn}>
              <Button
                title={translate(cSortType.name)}
                buttonStyle={buttonStyle}
                iconRight={true}
                onPress={() => this.actionSelectVersionType(cSortType.id)}
              />
            </View>
          );
        })}
      </View>
    );
  }

  actionSelectVersionType = id => {
    const { selectVersionType } = this.props;
    selectVersionType(id);
  };
}

const mapStateToProps = state => ({
  versionTypes: state.app.versionTypes,
  versionType: state.app.versionType
});

const mapDispatchToProps = dispatch => ({
  fetchVersionTypes: () => dispatch(AppActions.fetchVersionTypes()),
  selectVersionType: versionType => dispatch(AppActions.selectVersionType(versionType))
});

VersionTypeSelector.propTypes = {
  sortTypes: PropTypes.array,
  sortType: PropTypes.number,

  fetchVersionTypes: PropTypes.func,
  selectVersionType: PropTypes.func
};

VersionTypeSelector.defaultProps = {
  versionTypes: '',
  versionType: 0,

  fetchVersionTypes: () => {},
  selectVersionType: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(VersionTypeSelector));
