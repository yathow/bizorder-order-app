import React from 'react'
import { View, Button, Text } from '@merchstack/react-native-ui'

import OrdersActions from '../../Stores/Orders/Actions'
import { BaseComponent } from '../../Components'
import ConfirmDialog from '../../Components/Dialog/ConfirmDialog'
import ActionSheetIOS from '../../Components/ActionSheetIOS/ActionSheetIOS'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { translate } from '../../Translations'
import { withTheme } from 'react-native-elements'
import Style from './FilterOrdersDialogStyle'
import { getDocStatus } from '../../Services/OrderService';
import { isObject } from 'formik'

class FilterOrdersDialog extends BaseComponent {
    constructor(props) {
        super(props)
        // this.actionConfirm = this.actionConfirm.bind(this)
        this.actionDismiss = this.actionDismiss.bind(this)
    }

    componentDidUpdate(prevProps) {
        const { setRefreshOrders, fetchOrders, refreshOrders } = this.props
        
        if(prevProps.refreshOrders !== refreshOrders) {
            setRefreshOrders(false)
            fetchOrders()
        }
    }

    actionDismiss = () => {
        const { setDialogVisible } = this.props

        setDialogVisible(false)
    }

    renderStatusPicker = () =>{
        const status = [2,3,50,100]
    
        let elements = []
        if (status && status.length > 0) {
            status.map((cStatus, index) => {
                elements.push({value:cStatus, label:this.renderStatusText(cStatus)})
            })
        }
    
        return elements
    }

    renderStatusText = statusId => {
        const nStatus = getDocStatus(statusId);
        return translate(nStatus.label);
    };

    actionToggleFilterOrderStatus = (status) => {
        const { toggleFilterOrderStatus } = this.props
        toggleFilterOrderStatus(status)
    }

    actionConfirm = () => {
        const { setDialogVisible, applyFilters } = this.props
        
        setDialogVisible(false)
        applyFilters()
    }

    render() {
        const { dialogVisibility, newFilterStatus } = this.props
        return (
            <ConfirmDialog
                isVisible={dialogVisibility}
                title={translate('filter_orders')}
                component={(
                    <View style={Style.messageContainer}>
                        <Text style={Style.title}>Status</Text>
                        {/* <Picker
                            selectedValue={newFilterStatus}
                            onValueChange={status => this.actionSetFilterOrderStatus(status)}
                        >
                            {this.renderStatusPicker()}
                        </Picker> */}
                        <View style={Style.filterPanel}>
                            {this.renderStatusPicker().map((status, index) => {
                                let isSelected = false;

                                for(i=0; i<newFilterStatus.length; i++){
                                    if (newFilterStatus[i] === status.value) {
                                        isSelected = true;
                                    }
                                }

                                let buttonStyle = isSelected
                                    ? Style.selectedBtn(this.props.theme)
                                    : Style.btn(this.props.theme);
                                return (
                                    <View key={status.value} style={Style.containerBtn}>
                                    <Button
                                        buttonStyle={buttonStyle}
                                        title={status.label}
                                        onPress={() => this.actionToggleFilterOrderStatus(status.value)}
                                    />
                                    </View>
                                );
                            })}
                        </View>
                    </View>
                )}
                confirmText={translate('filter')}
                onConfirm={this.actionConfirm}
                cancelText={translate('cancel')}
                onCancel={this.actionDismiss}
            />
        )
    }
}

FilterOrdersDialog.propTypes = {
    dialogVisibility: PropTypes.bool,
    newFilterStatus: PropTypes.array,
    refreshOrders: PropTypes.bool,

    setDialogVisible: PropTypes.func,
    toggleStatusFilterOrder: PropTypes.func,
    applyFilters: PropTypes.func,
    fetchOrders: PropTypes.func,
    setRefreshOrders: PropTypes.func
}

FilterOrdersDialog.defaultProps = {
    dialogVisibility: false,
    newFilterStatus: [],
    refreshOrders: false,

    setDialogVisible: () => {},
    toggleStatusFilterOrder: () => {},
    applyFilters: () => {},
    fetchOrders: () => {},
    setRefreshOrders: () => {}
}

const mapStateToProps = state => ({
    dialogVisibility: state.orders.filterOrderDialogVisible,
    filterStatus: state.orders.filterStatus,
    newFilterStatus: state.orders.newFilterStatus,
    refreshOrders: state.orders.refreshOrders
})

const mapDispatchToProps = dispatch => ({
    setDialogVisible: dialogVisibility =>
        dispatch(OrdersActions.setFilterOrderDialogVisible(dialogVisibility)),
    toggleFilterOrderStatus: status => dispatch(OrdersActions.toggleFilterOrderStatus(status)),
    applyFilters: () => dispatch(OrdersActions.applyFilterOrder()),
    fetchOrders: () => dispatch(OrdersActions.fetchOrders()),
    setRefreshOrders: (boolean) => dispatch(OrdersActions.setRefreshOrders(boolean))
})

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(FilterOrdersDialog))
