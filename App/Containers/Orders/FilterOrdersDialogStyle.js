import { Colors, Metrics, Styles } from '../../Theme';
import { StyleSheet, Dimensions } from 'react-native';

const windowWidth = Dimensions.get('window').width;

export default StyleSheet.create({
  messageContainer: {
    padding: Metrics.lg
  },
  filterPanel: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    paddingTop: Metrics.md,
    paddingBottom: Metrics.md
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  containerBtn: {
    width: windowWidth * 0.4,
    padding: Metrics.sm
  },
  btn: theme => ({
    ...Styles.smallBtn,
    borderRadius: Metrics.borderRadius,
    backgroundColor: Colors.transparent
  }),
  selectedBtn: theme => ({
    ...Styles.smallBtn,
    borderRadius: Metrics.borderRadius,
    backgroundColor: theme.backgroundColors.secondary
  }),
});
