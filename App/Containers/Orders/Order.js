import { Card, FlatList, LoadingIndicator, Text, View } from '@merchstack/react-native-ui';
import React, { Fragment } from 'react';
import { getDocStatus, isDocVoided } from '../../Services/OrderService';

import AddressInfo from '../Addresses/AddressInfo';
import { BaseComponent } from '../../Components';
import ItemImage from '../ItemDetails/ItemImage';
import ItemPromotionTags from '../ItemDetails/ItemPromotionTags';
import Style from './OrderStyle';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { formatCurrency } from '../../Formatter';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

class Order extends BaseComponent {
  render() {
    const { isLoading, order } = this.props;
    if (isLoading) {
      return <LoadingIndicator style={Style.loading} size="large" />;
    } else {
      return (
        <FlatList
          ListHeaderComponent={this.renderHeader}
          data={order.details}
          keyExtractor={this.getDetailKey}
          renderItem={this.renderDetail}
        />
      );
    }
  }

  renderHeader = () => {
    const { theme, order } = this.props;
    return (
      <Fragment>
        {this.renderTitle()}
        <Card containerStyle={Style.card(theme)}>
          <Text h4 style={Style.deliveryLbl(theme)}>
            {translate('delivery_address')}
          </Text>
          <AddressInfo data={order.address} />
        </Card>
        <Card containerStyle={Style.card(theme)}>
          <View style={Style.totalRow(theme)}>
            <Text style={Style.totalLbl(theme)}>{translate('subtotal')}</Text>
            <Text style={Style.total(theme)}>{formatCurrency(order.subtotalAmt)}</Text>
          </View>
          <View style={Style.totalRow(theme)}>
            <Text style={Style.totalLbl(theme)}>{translate('tax')}</Text>
            <Text style={Style.total(theme)}>{formatCurrency(order.taxAmt)}</Text>
          </View>
          <View style={Style.totalRow(theme)}>
            <Text style={Style.totalLbl(theme)}>{translate('discount')}</Text>
            <Text style={Style.total(theme)}>{formatCurrency(order.discAmt)}</Text>
          </View>
          <View style={Style.totalRow(theme)}>
            <Text h4 style={Style.totalLbl(theme)}>
              {translate('order_total')}
            </Text>
            <Text h4 style={Style.total(theme)}>
              {formatCurrency(order.totalAmt)}
            </Text>
          </View>
        </Card>
        {this.renderVoidInfo()}
        <Text h4 style={Style.detailsLbl(theme)}>
          {translate('order_items')}
        </Text>
      </Fragment>
    );
  };

  renderTitle = () => {
    const { title, theme } = this.props;
    if (title) {
      return (
        <View style={Style.titlePanel(theme)}>
          <Text h2 h2Style={Style.titleLbl(theme)}>
            {title}
          </Text>
          {this.renderStatus()}
        </View>
      );
    } else {
      return null;
    }
  };

  getDetailKey = item => item.id.toString();

  renderDetail = data => {
    const { theme, onItemPress } = this.props;

    const rowStyle =
      (data.index + 1) % 2 === 0 ? Style.detailRowEven(theme) : Style.detailRowOdd(theme);

    const detail = data.item;
    const items = detail.item;
    const uom = detail.uom;

    return (
      <TouchableWithoutFeedback
        onPress={() => {
          if (onItemPress) {
            onItemPress(items.id);
          }
        }}
      >
        <View style={rowStyle}>
          <View style={Style.itemContent}>
            <ItemImage style={Style.itemImage} photos={items.photos} />
            <View style={Style.itemInfo}>
              <Text style={Style.itemCode(theme)}>{items.code}</Text>
              <Text style={Style.itemDesc(theme)}>{items.desc1}</Text>
              <Text>{items.desc2}</Text>
              <ItemPromotionTags item={detail} />
            </View>
          </View>
          <View style={Style.detailTotalRow}>
            {this.renderPrice(uom)}
            <Text style={Style.separator}>x</Text>
            <Text>
              {detail.qty} {uom.code}
            </Text>
            <Text style={Style.detailTotal(theme)}>{formatCurrency(detail.netAmt)}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  renderPrice = uom => {
    const { theme } = this.props;

    const price = uom.price;
    const oriPrice = uom.oriPrice;

    return(
      <View style={Style.priceInfo}>
        {price.length > 0 
          ? (<>
              <Text style={Style.oriPrice(theme)}>{formatCurrency(oriPrice)}</Text>
              {price.map((p, index) => (
                  <Text key={index} style={index == price.length-1 ? Style.itemUomPrice1(theme) : Style.itemUomPrice2(theme)}>
                    { p > 0 ? formatCurrency(p) : translate('foc')}
                  </Text>
                )
              )}
            </>
          ) : (
            <Text style={Style.itemUomPrice1(theme)}>{formatCurrency(oriPrice)}</Text>
          )
        }
      </View>
    )
  };

  renderPromotion = detail => {
    const hasPromotion = detail.promotion;
    if (hasPromotion) {
      return (
        <View style={Style.detailPromoRow}>
          <Text style={Style.promo}>*{detail.promotion.desc1}</Text>
        </View>
      );
    } else {
      return null;
    }
  };

  renderVoidInfo = () => {
    const { theme, order } = this.props;

    if (isDocVoided(order.docStatus)) {
      return (
        <Card containerStyle={Style.voidCard(theme)}>
          <Text style={Style.voidTitleLbl(theme)}>{translate('void_reason')}</Text>
          <Text style={Style.voidReasonLbl(theme)}>
            {order.voidReason && order.voidReason.length > 0 ? order.voidReason : translate('-')}
          </Text>
        </Card>
      );
    } else {
      return null;
    }
  };

  renderStatus = () => {
    const { order, theme } = this.props;
    const nStatus = getDocStatus(order.docStatus);
    return <Text style={Style.statusLbl(theme, nStatus.status)}>{translate(nStatus.label)}</Text>;
  };
}

export default withTheme(Order);
