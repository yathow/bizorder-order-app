import { BaseComponent, BaseView } from '../../Components';

import ItemDetailsActions from '../../Stores/ItemDetails/Actions';
import Order from './Order';
import OrderActions from '../../Stores/Order/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { translate } from '../../Translations';

class OrderScreen extends BaseComponent {
  constructor(props) {
    super(props);
    this.actionShowItemDetails = this.actionShowItemDetails.bind(this);
  }

  componentDidMount() {
    const { orderId, fetchOrder } = this.props;
    fetchOrder(orderId);
  }

  render() {
    const { order, errorMessages, isLoading } = this.props;
    
    const title = order && order.docCode ? order.docCode : '';
    return (
      <BaseView
        title={translate('order_#') + title}
        showMenuButton={false}
        showSearchButton={false}
        showCartButton={false}
        showFilterButton={false}
        errorMessage={errorMessages}
      >
        <Order
          title={title}
          isLoading={isLoading}
          order={order}
          onItemPress={this.actionShowItemDetails}
        />
      </BaseView>
    );
  }

  actionShowItemDetails = itemId => {
    const { showItemDetails } = this.props;
    showItemDetails(itemId);
  };
}

const mapStateToProps = state => ({
  orderId: state.order.orderId,
  order: state.order.order,
  isLoading: state.order.orderIsLoading,
  errorMessages: state.messages.errorMessages
});

const mapDispatchToProps = dispatch => ({
  fetchOrder: orderId => dispatch(OrderActions.fetchOrder(orderId)),
  showItemDetails: itemId => dispatch(ItemDetailsActions.showItemDetails(itemId))
});

OrderScreen.propTypes = {
  orderId: PropTypes.number,
  order: PropTypes.object,
  isLoading: PropTypes.bool,
  errorMessages: PropTypes.array,

  fetchOrders: PropTypes.func,
  showItemDetails: PropTypes.func
};

OrderScreen.defaultProps = {
  orders: [],
  isLoading: false,
  errorMessages: [],

  fetchOrders: () => {},
  showItemDetails: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderScreen);
