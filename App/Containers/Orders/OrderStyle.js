import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  titlePanel: theme => ({
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: Metrics.md
  }),
  titleLbl: theme => ({
    flex: 1,
    paddingRight: Metrics.lg,
    paddingLeft: Metrics.lg
  }),
  statusLbl: (theme, status) => ({
    color: theme.colors[status],
    backgroundColor: theme.backgroundColors[status],
    fontWeight: 'bold',
    padding: Metrics.md,
    paddingRight: Metrics.lg,
    paddingLeft: Metrics.lg,
    marginRight: Metrics.md
  }),
  card: theme => ({
    ...theme.Card.containerStyle,
    marginTop: Metrics.md,
    margin: Metrics.md
  }),
  voidCard: theme => ({
    ...theme.Card.containerStyle,
    borderColor: theme.colors.error,
    backgroundColor: theme.backgroundColors.error,
    marginTop: Metrics.md,
    margin: Metrics.md
  }),
  voidTitleLbl: theme => ({
    color: theme.colors.error,
    fontWeight: 'bold'
  }),
  voidReasonLbl: theme => ({
    paddingTop: Metrics.md,
    paddingBottom: Metrics.md
  }),
  deliveryLbl: theme => ({
    paddingBottom: Metrics.md
  }),
  totalRow: theme => ({
    flexDirection: 'row',
    alignItems: 'center'
  }),
  totalLbl: theme => ({
    flex: 1,
    textAlign: 'right'
  }),
  total: theme => ({
    flex: 1,
    color: theme.colors.highlight,
    textAlign: 'right'
  }),
  detailsLbl: theme => ({
    padding: Metrics.md
  }),

  detailRowEven: theme => ({
    padding: Metrics.md,
    backgroundColor: theme.backgroundColors.even
  }),
  detailRowOdd: theme => ({
    padding: Metrics.md,
    backgroundColor: theme.backgroundColors.odd
  }),
  itemContent: {
    flexDirection: 'row'
  },
  itemInfo: {
    flex: 1,
    marginLeft: Metrics.md,
    marginRight: Metrics.md
  },
  itemCode: theme => ({}),
  itemDesc: theme => ({
    color: theme.colors.primary,
    fontWeight: 'bold'
  }),
  itemImage: {
    width: 100,
    height: 100
  },
  detailTotalRow: {
    flexDirection: 'row',
    paddingTop: Metrics.sm
  },
  priceInfo: {
    flexDirection: 'row'
  },
  price: theme => ({}),
  separator: {
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md
  },
  oriPrice: theme => ({
    color: theme.colors.muted,
    paddingRight: Metrics.sm,
    textDecorationLine: 'line-through'
  }),
  detailTotal: theme => ({
    flex: 1,
    textAlign: 'right',
    color: theme.colors.success
  }),
  detailPromoRow: {
    flexDirection: 'row',
    paddingTop: Metrics.sm
  },
  promo: {
    flex: 1
  },
  discount: {
    textAlign: 'right',
    paddingLeft: Metrics.md
  },

  loading: {
    padding: Metrics.md
  },
  itemUomPrice1: theme => ({
    paddingLeft: Metrics.sm,
    color: theme.colors.success
  }),
  itemUomPrice2: theme => ({
    paddingLeft: Metrics.sm,
    textDecorationLine: 'line-through',
    color: theme.colors.secondary
  })
});
