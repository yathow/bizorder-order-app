import { BaseComponent, BaseView, EmptyComponent } from '../../Components';
import { FlatList, StatusText, Text, View } from '@merchstack/react-native-ui';
import { formatCurrency, formatDate } from '../../Formatter';

import OrderActions from '../../Stores/Order/Actions';
import OrdersActions from '../../Stores/Orders/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import Style from './OrdersStyle';
import { TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { getDocStatus } from '../../Services/OrderService';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';
import FilterOrdersDialog from './FilterOrdersDialog';

class OrdersScreen extends BaseComponent {
  componentDidMount() {
    this.props.fetchOrders();
  }

  componentWillUnmount() {
    const { resetFilters } = this.props

    resetFilters()
  }

  render() {
    const { orders, isLoading } = this.props
    
    return (
      <>
        <BaseView
          title={translate('my_orders')}
          showMenuButton={false}
          showSearchButton={false}
          showCartButton={false}
          showFilterButton={false}
          errorMessage={this.props.errorMessages}
        >
          <View style={Style.content}>
            {this.renderHeader()}
            <FlatList
              data={orders}
              keyExtractor={this.getOrderKey}
              refreshing={false}
              isLoading={isLoading}
              onRefresh={() => {
                this.props.fetchOrders();
              }}
              renderItem={this.renderOrder}
              onEndReached={this.actionFetchMoreOrders}
              onEndReachedThreshold={0.5}
              ListEmptyComponent={
                <EmptyComponent
                  title={translate('no_results_found')}
                  isLoading={this.props.isLoading}
                />
              }
            /> 
          </View>
        </BaseView>
      </>
    );
  }

  renderHeader = () => {
    return (
      <View>
        <View style={Style.rowHeader(this.props.theme)}>
          <Text style={Style.orderNoHeader(this.props.theme)}>{translate('order_#')}</Text>
          <Text style={Style.totalHeader(this.props.theme)}>{translate('total')}</Text>
          <Text style={Style.statusHeader(this.props.theme)}>{translate('status')}</Text>
        </View>
      </View>
    );
  };

  getOrderKey = ({ id }) => {
    return id.toString();
  };

  renderOrder = ({ index, item }) => {
    const rowStyle =
      (index + 1) % 2 === 0 ? Style.rowEven(this.props.theme) : Style.rowOdd(this.props.theme);
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          this.actionViewOrder(item.id);
        }}
      >
        <View style={rowStyle}>
          <View style={Style.orderInfo(this.props.theme)}>
            <Text style={Style.orderNo(this.props.theme)}>{item.docNo}</Text>
            <Text style={Style.date(this.props.theme)}>{formatDate(item.docDate)}</Text>
          </View>
          <Text style={Style.total}>{formatCurrency(item.totalAmt)}</Text>
          {this.renderStatus(item.docStatus)}
        </View>
      </TouchableWithoutFeedback>
    );
  };

  renderStatus = statusId => {
    const nStatus = getDocStatus(statusId);
    return (
      <StatusText status={nStatus.status} style={Style.status}>
        {translate(nStatus.label)}
      </StatusText>
    );
  };

  actionViewOrder = orderId => {
    this.props.viewOrder(orderId);
  };

  actionFetchMoreOrders = () => {
    const { fetchMoreOrders } = this.props;
    fetchMoreOrders();
  };
}

const mapStateToProps = state => ({
  orders: state.orders.orders,
  isLoading: state.orders.ordersIsLoading,
  errorMessages: state.messages.errorMessages
});

const mapDispatchToProps = dispatch => ({
  fetchOrders: () => dispatch(OrdersActions.fetchOrders()),
  fetchMoreOrders: () => dispatch(OrdersActions.fetchMoreOrders()),
  viewOrder: orderId => dispatch(OrderActions.viewOrder(orderId)),
  resetFilters: () => dispatch(OrdersActions.resetFilters())
});

OrdersScreen.propTypes = {
  orders: PropTypes.array,
  isLoading: PropTypes.bool,
  errorMessages: PropTypes.array,

  fetchOrders: PropTypes.func,
  viewOrder: PropTypes.func,
  resetFilters: PropTypes.func
};

OrdersScreen.defaultProps = {
  orders: [],
  isLoading: false,
  errorMessages: [],

  fetchOrders: () => {},
  viewOrder: () => {},
  resetFilters: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(OrdersScreen));
