import { Colors, Metrics, Styles } from '../../Theme';

import { StyleSheet } from 'react-native';

const rowHeaderStyle = theme => ({
  color: theme.colors.inverse,
  fontWeight: 'bold'
});

const orderNoStyle = {
  flex: 1
};

const actionStyle = {
  width: 50,
  textAlign: 'center'
};

const statusStyle = {
  width: 90,
  textAlign: 'center',
  marginLeft: Metrics.md
};

const totalStyle = {
  flex: 1,
  textAlign: 'right'
};

export default StyleSheet.create({
  page: {
    ...Styles.page,
    flex: 1
  },
  container: {
    ...Styles.container,
    flex: 1
  },
  content: {
    ...Styles.content,
    padding: 0
  },
  headerLbl: {
    padding: Metrics.md
  },
  headerPanel: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: Metrics.md
  },
  rowHeader: theme => ({
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: theme.colors.secondary,
    padding: Metrics.md
  }),
  rowEven: theme => ({
    flexDirection: 'row',

    backgroundColor: theme.backgroundColors.even,
    padding: Metrics.md
  }),
  rowOdd: theme => ({
    flexDirection: 'row',
    backgroundColor: theme.backgroundColors.odd,
    padding: Metrics.md
  }),
  orderNoHeader: theme => ({
    ...rowHeaderStyle(theme),
    ...orderNoStyle
  }),
  actionHeader: theme => ({
    ...rowHeaderStyle(theme),
    ...actionStyle
  }),
  statusHeader: theme => ({
    ...rowHeaderStyle(theme),
    ...statusStyle
  }),
  totalHeader: theme => ({
    ...rowHeaderStyle(theme),
    ...totalStyle
  }),
  orderInfo: theme => ({
    ...orderNoStyle,
    color: theme.colors.primary,
    fontWeight: 'bold'
  }),
  orderNo: theme => ({
    color: theme.colors.primary,
    fontWeight: 'bold'
  }),
  date: theme => ({
    marginTop: Metrics.sm
  }),
  total: totalStyle,
  action: actionStyle,
  status: {
    ...statusStyle,
    fontWeight: 'bold',
    backgroundColor: Colors.transparent
  }
});
