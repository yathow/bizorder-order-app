import React, { Fragment } from 'react'
import { View, Text, Image, Card } from '@merchstack/react-native-ui'
import Style from './PromotionsStyle'
import { withTheme } from 'react-native-elements'
import { translate } from '../../Translations'
import { BaseComponent } from '../../Components'
import Time from '../../Components/MomentJS/Time'

class Promotion extends BaseComponent {
    render() {
        const { promotion, theme } = this.props

        return (
            typeof(promotion) !== 'undefined'
            ? this.renderHeader(promotion, theme)
            : null
        )
    }

    renderHeader = (promotion, theme) => {
        return (
            <Fragment>
                <Card containerStyle={Style.card(theme)}>
                    <Text h4 style={Style.promotionLbl(theme)}>
                        {translate('promotion_details')}
                    </Text>
                    <Image style={Style.images} source={{uri: promotion && promotion.asset_url}} />
                    {this.renderHeaderDetails(promotion)}
                </Card>
            </Fragment>
        )
    }
    
    renderHeaderDetails = (promotion) => {
        if(promotion && promotion.valid_from && promotion.valid_to) {
            return (
                <View style={Style.col}>
                    <View style={Style.row}>
                        <View style={Style.parent}>
                            <Text>Code:</Text>
                        </View>
                        <View style={Style.child}>
                            <Text>{promotion.code}</Text>
                        </View>
                    </View>
                    <View style={Style.row}>
                        <View style={Style.parent}>
                            <Text>Desc 1:</Text>
                        </View>
                        <View style={Style.child}>
                            <Text>{promotion.desc_01}</Text>
                        </View>
                    </View>
                    {
                        promotion.desc_02 
                        ?  (<View style={Style.row}>
                                <View style={Style.parent}>
                                    <Text>Desc 2:</Text>
                                </View>
                                <View style={Style.child}>
                                    <Text>{promotion.desc_02}</Text>
                                </View>
                            </View>)
                        : (null)
                    }
                    <View style={Style.row}>
                        <View style={Style.parent}>
                            <Text>Valid From:</Text>
                        </View>
                        <Time style={Style.time} time={promotion.valid_from}/>
                    </View>
                    <View style={Style.row}>
                        <View style={Style.parent}>
                            <Text>Valid To:</Text>
                        </View>
                        <Time style={Style.time} time={promotion.valid_to}/>
                    </View>
                </View>
            )
        } else { 
            return null
        }
    }
}

export default withTheme(Promotion);
