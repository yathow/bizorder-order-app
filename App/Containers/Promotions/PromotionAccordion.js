import React, { Component } from 'react'
import { TouchableOpacity, LayoutAnimation, Platform, UIManager, FlatList, TouchableWithoutFeedback } from 'react-native'
import { View, Text, Icon} from "@merchstack/react-native-ui"
import Style from './PromotionAccordionStyle'
import { withTheme } from 'react-native-elements'
import { connect } from 'react-redux'
import ItemImage from '../ItemDetails/ItemImage'
import ItemInfo from '../ItemDetails/ItemInfo'
import PromotionsActions from '../../Stores/Promotions/Actions'
import ItemDetailsActions from '../../Stores/ItemDetails/Actions'

class PromotionAccordion extends Component{
    constructor(props) {
        super(props);

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    render() {
        const { theme, title, item } = this.props
        
        return (
            <View>
                <TouchableOpacity style={Style.row(theme)} onPress={() => this.toggleExpand(title)}>
                    <Text style={Style.title(theme)}>{title}</Text>
                    <Icon name={item.expanded ? "keyboard-arrow-up" : "keyboard-arrow-down"} size={30} />
                </TouchableOpacity>
                <View style={Style.parentHr(theme)}/>
                {
                    item.expanded &&
                    <View style={Style.child(theme)}>
                        <FlatList 
                            data={item.items}
                            numColumns={1}
                            scrollEnabled={false}
                            renderItem={({item, index}) => 
                                this.renderItem(item, index)
                            }
                        />
                    </View>
                }
            </View> 
        )
    }

    renderItem = (item, index) => {
        const { theme } = this.props
        const rowStyle = (index + 1) % 2 === 0 ? Style.evenItemContent : Style.oddItemContent;

        return (
            <TouchableWithoutFeedback onPress={() => this.actionShowItemDetails(item.id, item)}>
                <View style={rowStyle(theme)}>
                    <View>
                        <ItemImage style={Style.itemImage} photos={item.photos} />
                    </View>
                    <View style={Style.itemInfo}>
                        <ItemInfo item={item} uoms={item.uoms} />
                        <Text style={Style.warningText(theme)}>
                            Stated prices are before discounts. Click for more details.
                            </Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    actionShowItemDetails = (itemId, item) => {
        const { showItemDetails } = this.props;
        showItemDetails(itemId, item);
    }

    toggleExpand = (title) => {
        const { togglePromotionItemExpand, setForceUpdate } = this.props
        
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
        togglePromotionItemExpand(title)
        setForceUpdate(true)
    }

}

mapStateToProps = state => ({
    
})

mapDispatchToProps = dispatch => ({
    togglePromotionItemExpand: (title) => dispatch(PromotionsActions.togglePromotionItemExpand(title)),
    setForceUpdate: (boolean) => dispatch(PromotionsActions.setForceUpdate(boolean)),
    showItemDetails: (itemId, item) => dispatch(ItemDetailsActions.showItemDetails(itemId, item))
})


export default connect(mapStateToProps, mapDispatchToProps)(withTheme(PromotionAccordion))