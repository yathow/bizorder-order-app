import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    title: theme => ({
        fontSize: 14,
        fontWeight:'bold',
        color: theme.colors.darkgrey,
    }),
    row: theme => ({
        flexDirection: 'row',
        justifyContent:'space-between',
        height:56,
        paddingLeft:25,
        paddingRight:18,
        alignItems:'center',
        backgroundColor: theme.backgroundColors.lightGray,
    }),
    parentHr: theme => ({
        height:1,
        // color: Colors.WHITE,
        width:'100%'
    }),
    child: theme => ({
        // backgroundColor: Colors.LIGHTGRAY,
        padding:16,
    }),
    evenItemContent: theme => ({
        flex: 1,
        flexDirection: 'row',
        padding: Metrics.md,
        backgroundColor: theme.backgroundColors.even
    }),
    oddItemContent: theme => ({
        flex: 1,
        flexDirection: 'row',
        padding: Metrics.md,
        backgroundColor: theme.backgroundColors.odd
    }),
    itemImage: {
        width: Metrics.catalog.items.width,
        height: Metrics.catalog.items.height
    },
    itemInfo: {
        flex: 1,
        marginLeft: Metrics.md
    },
    warningText: theme => ({
        color: theme.colors.secondary
    })
});