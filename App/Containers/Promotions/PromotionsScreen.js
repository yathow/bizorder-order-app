import React from 'react'
import { BaseComponent, BaseView } from '../../Components'
import { Text, FlatList, LoadingIndicator } from '@merchstack/react-native-ui'

import { translate } from '../../Translations'
import { withTheme } from 'react-native-elements'
import { connect } from 'react-redux'

import Promotion from './Promotion'
import PromotionAccordion from './PromotionAccordion'
import PromotionsActions from '../../Stores/Promotions/Actions'
import Style from './PromotionsStyle'

class PromotionsScreen extends BaseComponent {
    componentDidMount() {
        const { fetchPromotion, fetchPromotionItems, promotionId } = this.props
        
        fetchPromotion(promotionId)
        fetchPromotionItems(promotionId)
    }

    componentDidUpdate(prevProps) { 
        const { fetchPromotion, fetchPromotionItems, promotionId, forceUpdate, setForceUpdate } = this.props

        if(prevProps.promotionId !== promotionId) {
            fetchPromotion(promotionId)
            fetchPromotionItems(promotionId)
        }

        if(forceUpdate) {
            setForceUpdate(false)
        }
    }

    render() {
        const { errorMessages, items } = this.props

        return (
            <BaseView
                title={translate('promotion')}
                showMenuButton={false}
                showSearchButton={false}
                showCartButton={false}
                showFilterButton={false}
                errorMessage={errorMessages}
            >
                <FlatList
                    ListHeaderComponent={this.renderHeader}
                    data={items}
                    renderItem={(data) => this.renderDetail(data)}
                    keyExtractor={(item, index) => index.toString()}
                />
            </BaseView>
        )
    }

    renderHeader = () => {
        const { promotion, promotionsIsLoading, theme } = this.props

        if(typeof(promotion) !== 'undefined' || promotion) {
            if(promotionsIsLoading) {
                return <LoadingIndicator style={Style.loading} size="large" />
            } else {
                return (
                    <>
                        <Promotion 
                            promotion={promotion} 
                            theme={theme}
                        />
                        <Text h4 style={Style.detailsLbl(theme)}>
                            {translate('related_items')}
                        </Text>
                    </>
                )
            }
        } else {
            return null
        }
    }

    renderDetail = (data) => {
        const { itemsIsLoading, theme } = this.props
        
        if(itemsIsLoading) {
            return <LoadingIndicator styles={Style.loading} size="large" />
        } else {
            return (
                <PromotionAccordion
                    title={data.item.title}
                    theme={theme}
                    item={data.item}
                />
            ) 
        }
    }
}

const mapStateToProps = state => ({
    items: state.promotions.items,
    promotionsIsLoading: state.promotions.promotionsIsLoading,
    itemsIsLoading: state.promotions.itemsIsLoading,
    errorMessages: state.messages.errorMessages,
    promotionId: state.promotions.promotionId,
    promotion: state.promotions.promotion,
    forceUpdate: state.promotions.forceUpdate
});

const mapDispatchToProps = dispatch => ({
    fetchPromotion: (promotionId) => dispatch(PromotionsActions.fetchPromotion(promotionId)),
    fetchPromotionItems: (promotionId) => dispatch(PromotionsActions.fetchPromotionItems(promotionId)),
    setForceUpdate: (boolean) => dispatch(PromotionsActions.setForceUpdate(boolean))
});

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(PromotionsScreen));