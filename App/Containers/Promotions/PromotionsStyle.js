import { Metrics } from '../../Theme';
import { StyleSheet, Dimensions } from 'react-native';

const screenWidth = Dimensions.get('screen').width * 0.9
const screenHeight = Dimensions.get('screen').height

export default StyleSheet.create({
    card: theme => ({
        ...theme.Card.containerStyle,
        marginTop: Metrics.md,
        margin: Metrics.md
    }),
    child: {
        width: '80%'
    },
    col: {
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    detailsLbl: theme => ({
        padding: Metrics.md
    }),
    images: {
        width: screenWidth,
        height: screenWidth/2.5,
        marginBottom: Metrics.md
    },
    loading: {
        padding: Metrics.md
    },
    parent: {
        width: '20%'
    },
    promotionLbl: theme => ({
        paddingBottom: Metrics.md
    }),
    row: {
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    halfPanel: {
        width: screenWidth/2,
    },
    fullPanel: {
        width: screenWidth
    },
    time: {
        fontSize: 12
    }
});
