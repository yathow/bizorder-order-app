import React, { Component } from 'react';

import AppNavigator from 'App/Navigators/AppNavigator';
import NotificationService from 'App/Services/NotificationService';
import NavigationService from 'App/Services/NavigationService';
import { PropTypes } from 'prop-types';
import StartupActions from 'App/Stores/Startup/Actions';
import { ThemeProvider } from 'react-native-elements';
import { Themes } from '../../Theme';
import { connect } from 'react-redux';
import { loadLocale } from '../../Translations';

class RootScreen extends Component {
  componentDidMount() {
    // Run the startup saga when the application is starting
    this.props.startup();
    loadLocale(this.props.locale);
  }

  componentDidUpdate() {
    loadLocale(this.props.locale);
  }

  componentWillUnmount() {
    // Run the shutdown saga when the application is terminating
  }

  render() {
    const { token } = this.props
    
    return (
      <ThemeProvider theme={Themes.light}>
        {/* Hide the line below and in index.js to turn off notification handling */}
        { token ? <NotificationService /> : null} 
        
        <AppNavigator
          // Initialize the NavigationService (see https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html)
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </ThemeProvider>
    );
  }
}

RootScreen.propTypes = {
  locale: PropTypes.string,
  theme: PropTypes.object,

  startup: PropTypes.func
};

RootScreen.defaultProps = {
  locale: null,
  theme: null,

  startup: () => {}
};

const mapStateToProps = state => ({
  theme: state.theme.theme,
  token: state.app.token,
  locale: state.app.locale
});

const mapDispatchToProps = dispatch => ({
  startup: () => dispatch(StartupActions.startup())
});

export default connect(mapStateToProps, mapDispatchToProps)(RootScreen);
