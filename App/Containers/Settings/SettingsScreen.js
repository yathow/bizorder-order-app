import { Avatar, ListItem, SafeAreaView, Text, View } from '@merchstack/react-native-ui';

import AppActions from '../../Stores/App/Actions';
import { BaseComponent } from '../../Components';
import DebtorActions from '../../Debtor/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import { ScrollView } from 'react-native';
import Style from './SettingsScreenStyle';
import ThemeActions from '../../Stores/Theme/Actions';
import { Themes } from '../../Theme';
import { connect } from 'react-redux';
import { translate } from '../../Translations';
import { withTheme } from 'react-native-elements';

const SettingLinkItem = props => {
  return <ListItem {...props} bottomDivider />;
};

class SettingsScreen extends BaseComponent {
  componentDidUpdate(prevProps) {
    const { debtorId, fetchDebtor } = this.props;

    console.log(debtorId + ' vs ' + prevProps.debtorId);

    if (prevProps.debtorId !== debtorId) {
      fetchDebtor(debtorId);
    }
  }

  render() {
    const { theme, username, firstname, lastname, email, debtor } = this.props;
    return (
      <SafeAreaView style={Style.container}>
        <View style={Style.profilePanel(theme)}>
          <Avatar
            size="large"
            rounded
            icon={{ name: 'person', color: theme.colors.primary }}
            overlayContainerStyle={Style.avatar(theme)}
          />
          <View style={Style.userInfoPanel}>
            <Text h2 h2Style={Style.userInfo(theme)}>
              {username}
            </Text>
            <Text style={Style.userInfo(theme)}>
              {firstname} {lastname}
            </Text>
            <Text style={Style.userInfo(theme)}>{debtor.name}</Text>
            <Text style={Style.userInfo(theme)}>{email}</Text>
          </View>
        </View>
        <ScrollView style={Style.scroll}>
          <SettingLinkItem
            title={translate('order_history')}
            onPress={() => {
              this.props.navigation.navigate('OrdersScreen');
            }}
          />
          <SettingLinkItem
            title={translate('manage_address')}
            onPress={() => {
              this.props.navigation.navigate('AddressesScreen');
            }}
          />
          <SettingLinkItem
            title={translate('change_password')}
            onPress={() => {
              this.props.navigation.navigate('ChangePasswordScreen');
            }}
          />
          <SettingLinkItem
            title={translate('logout')}
            onPress={() => {
              this.props.logout();
            }}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  const { errorMessage, successMessage } = state.app;
  const { username, firstname, lastname, email, debtorId } = state.app.user;
  const { debtor } = state.app.debtor;
  return {
    username: username,
    firstname: firstname,
    lastname: lastname,
    email: email,
    debtorId: debtorId,
    debtor: debtor,
    errorMessage: errorMessage,
    successMessage: successMessage
  };
};

const mapDispatchToProps = dispatch => ({
  fetchDebtor: debtorId => dispatch(DebtorActions.fetchDebtor(debtorId)),
  logout: () => dispatch(AppActions.logout()),
  lightTheme: () => dispatch(ThemeActions.setTheme(Themes.light)),
  darkTheme: () => dispatch(ThemeActions.setTheme(Themes.dark))
});

SettingsScreen.propTypes = {
  fetchDebtor: PropTypes.func,
  logout: PropTypes.func
};

SettingsScreen.defaultProps = {
  fetchDebtor: () => {},
  logout: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(SettingsScreen));
