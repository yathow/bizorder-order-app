import { Image, Text, View } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import { Images } from 'App/Theme';
import React from 'react';
import Style from './SplashScreenStyle';
import { translate } from '../../Translations';

export default class SplashScreen extends BaseComponent {
  render() {
    return (
      <View style={Style.container}>
        <View style={Style.bottomSpace} />
        <View style={Style.logoContainer}>
          <Image source={Images.logo} style={Style.logoImage} resizeMode="contain" />
        </View>
        <View style={Style.appInfoContainer}>
          <Text>{translate('powered_by')}</Text>
          <Text h1>Merchstack</Text>
        </View>
        <View style={Style.bottomSpace} />
      </View>
    );
  }
}
