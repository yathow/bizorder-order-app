import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logoImage: {
    width: 275,
    height: 75
  },
  appInfoContainer: {
    paddingBottom: Metrics.spacer,
    alignItems: 'center'
  },
  bottomSpace: {
    height: 100
  }
});
