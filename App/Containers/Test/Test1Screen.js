import { BaseComponent } from '../../Components';
import { Button } from '@merchstack/react-native-ui';
import React from 'react';

class Test1Screen extends BaseComponent {
  componentDidMount() {}

  render() {
    return (
      <Button
        onPress={() => {
          this.props.navigation.openDrawer();
        }}
        title="test1"
      />
    );
  }
}

export default Test1Screen;
