import { BaseComponent } from '../../Components';
import { Button } from '@merchstack/react-native-ui';
import React from 'react';
import { withNavigation } from 'react-navigation';

class Test2Screen extends BaseComponent {
  componentDidMount() {}

  render() {
    return (
      <Button
        onPress={() => {
          this.props.navigation.openDrawer();
        }}
        title="test2"
      />
    );
  }
}

export default withNavigation(Test2Screen);
