import { Button, Text } from '@merchstack/react-native-ui';

import { BaseComponent } from '../../Components';
import React from 'react';

class Test3Screen extends BaseComponent {
  componentDidMount() {}

  render() {
    return (
      <Button
        onPress={() => {
          this.props.navigation.openDrawer();
        }}
        title="test3"
      />
    );
  }
}

export default Test3Screen;
