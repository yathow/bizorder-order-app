import * as Yup from 'yup';

import { Button, Icon, Image, Input, Text } from 'react-native-elements';
import React, { Fragment } from 'react';
import { SafeAreaView, View } from 'react-native';
import {
  addLocaleChangedListener,
  removeLocaleChangedListener,
  setI18nConfig,
  translate
} from '../../Translations';

import AppActions from '../../Stores/App/Actions';
import { Formik } from 'formik';
import { PropTypes } from 'prop-types';
import Style from './ChangePasswordStyle';
import { connect } from 'react-redux';

class UserProfileScreen extends React.PureComponent {
  constructor() {
    super();

    setI18nConfig(); // set initial config

    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
  }

  useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
  }

  componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);
  }

  componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);
  }

  componentDidUpdate(prevProps, prevState) {}

  render() {
    let initialValues = {
      username: this.state.username,
      email: this.state.email,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      timezone: this.state.timezone,
      last_login: this.state.last_login,
      password_changed_at: this.state.password_changed_at
    };

    return (
      <SafeAreaView style={Style.container}>
        {this.props.successMessage !== '' && (
          <View style={Style.successContainer}>
            <Text style={Style.successText}>{this.props.successMessage}</Text>
          </View>
        )}
        {this.props.errorMessage !== '' && (
          <View style={Style.errorContainer}>
            <Text style={Style.errorText}>{this.props.errorMessage}</Text>
          </View>
        )}
        <View style={Style.formContainer}>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            onSubmit={(values, formikBag) => {
              let currentPassword = values.currentPassword;
              let newPassword = values.newPassword;
              let retypeNewPassword = values.retypeNewPassword;
              //save to state
              this.setState({
                currentPassword: currentPassword,
                newPassword: newPassword,
                retypeNewPassword: retypeNewPassword
              });
              //dispatch the action
              this.props.changePassword(formikBag, currentPassword, newPassword);
            }}
            validationSchema={Yup.object().shape({
              currentPassword: Yup.string().required(translate('current_password_is_required')),
              newPassword: Yup.string().required(translate('new_password_is_required')),
              retypeNewPassword: Yup.string().test(
                'passwords-match',
                translate('passwords_do_not_match'),
                function(value) {
                  return this.parent.newPassword === value;
                }
              )
            })}
          >
            {({
              values,
              handleChange,
              errors,
              setFieldTouched,
              dirty,
              touched,
              isSubmitting,
              isValid,
              handleSubmit
            }) => (
              <Fragment>
                <Input
                  containerStyle={Style.formElement}
                  value={values.currentPassword}
                  onChangeText={handleChange('currentPassword')}
                  placeholder={translate('currentPassword')}
                  secureTextEntry={true}
                  errorMessage={errors.currentPassword}
                  disabled={isSubmitting}
                />
                <Input
                  containerStyle={Style.formElement}
                  value={values.newPassword}
                  onChangeText={handleChange('newPassword')}
                  placeholder={translate('newPassword')}
                  secureTextEntry={true}
                  errorMessage={errors.newPassword}
                  disabled={isSubmitting}
                />
                <Input
                  containerStyle={Style.formElement}
                  value={values.retypeNewPassword}
                  onChangeText={handleChange('retypeNewPassword')}
                  placeholder={translate('retypeNewPassword')}
                  secureTextEntry={true}
                  errorMessage={errors.retypeNewPassword}
                  disabled={isSubmitting}
                />
                <Button
                  containerStyle={Style.formElement}
                  disabled={!isValid}
                  loading={isSubmitting}
                  onPress={handleSubmit}
                  title={translate('changePassword')}
                />
              </Fragment>
            )}
          </Formik>
        </View>
      </SafeAreaView>
    );
  }
}

UserProfileScreen.propTypes = {
  changePassword: PropTypes.func
};

const mapStateToProps = state => ({
  successMessage: state.app.successMessage,
  errorMessage: state.app.errorMessage
});

const mapDispatchToProps = dispatch => ({
  changePassword: (formikBag, currentPassword, newPassword) =>
    dispatch(AppActions.changePassword(formikBag, currentPassword, newPassword))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileScreen);
