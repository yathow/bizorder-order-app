import moment from 'moment';

const formatDate = text => {
  return moment(text).format('DD/MM/YYYY');
};

export { formatDate };
