import i18n from 'i18n-js';

const formatCurrency = number => {
  return i18n.toCurrency(number ? number : 0, { unit: 'RM', format: '%u %n' });
};

const formatPercent = number => {
  return i18n.toPercentage(number ? number : 0, { precision: 2, strip_insignificant_zeros: true });
};

export { formatCurrency, formatPercent };
