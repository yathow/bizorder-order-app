import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import LoggedInNavigator from './LoggedInNavigator';
import NoInternetNavigator from './NoInternetNavigator';
import NotLoggedInNavigator from './NotLoggedInNavigator';
import SplashScreen from '../Containers/SplashScreen/SplashScreen';

export default createAppContainer(
  createSwitchNavigator(
    {
      SplashScreen: SplashScreen,
      LoggedInNavigator: LoggedInNavigator,
      NotLoggedInNavigator: NotLoggedInNavigator,
      NoInternetNavigator: NoInternetNavigator
    },
    {
      initialRouteName: 'SplashScreen'
    }
  )
);
