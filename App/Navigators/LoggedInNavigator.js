import AddressScreen from '../Containers/Addresses/AddressScreen';
import AddressesScreen from '../Containers/Addresses/AddressesScreen';
import BrandsScreen from '../Containers/Brands/BrandsScreen';
import CartScreen from '../Containers/Cart/CartScreen';
import CategoriesScreen from '../Containers/Categories/CategoriesScreen';
import ChangePasswordScreen from '../Containers/ChangePassword/ChangePasswordScreen';
import CheckoutScreen from '../Containers/Checkout/CheckoutScreen';
import DeliveryScreen from '../Containers/Checkout/DeliveryScreen';
import DivisionsScreen from '../Containers/Divisions/DivisionsScreen';
import HomeScreen from '../Containers/Home/HomeScreen';
import ItemDetailsScreen from '../Containers/ItemDetails/ItemDetailsScreen';
import ItemFiltersScreen from '../Containers/ItemFilters/ItemFiltersScreen';
import ItemsScreen from '../Containers/Items/ItemsScreen';
import { Metrics } from '../Theme';
import NotificationsScreen from '../Containers/Notifications/NotificationsScreen'
import OrderScreen from '../Containers/Orders/OrderScreen';
import OrdersScreen from '../Containers/Orders/OrdersScreen';
import PromotionsScreen from '../Containers/Promotions/PromotionsScreen';
import SettingsDrawer from '../Components/SettingsDrawer/SettingsDrawer';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import { setI18nConfig } from '../Translations';

setI18nConfig();

const DrawerNavigator = createDrawerNavigator(
  {
    HomeScreen: {
      screen: HomeScreen,
      navigationOptions: {
        drawerLabel: () => null
      }
    }
  },
  {
    initialRouteName: 'HomeScreen',
    contentComponent: SettingsDrawer,
    drawerWidth: Metrics.screenWidth() * 0.85
  }
);

const LoggedInNavigator = createStackNavigator(
  {
    AddressScreen: AddressScreen,
    AddressesScreen: AddressesScreen,
    BrandsScreen: BrandsScreen,
    CartScreen: CartScreen,
    CategoriesScreen: CategoriesScreen,
    ChangePasswordScreen: ChangePasswordScreen,
    CheckoutScreen: CheckoutScreen,
    DeliveryScreen: DeliveryScreen,
    DivisionsScreen: DivisionsScreen,
    DrawerNavigator: DrawerNavigator,
    ItemDetailsScreen: ItemDetailsScreen,
    ItemFiltersScreen: ItemFiltersScreen,
    ItemsScreen: ItemsScreen,
    NotificationsScreen: NotificationsScreen,
    OrdersScreen: OrdersScreen,
    OrderScreen: OrderScreen,
    PromotionsScreen: PromotionsScreen,
  },
  {
    initialRouteName: 'DrawerNavigator',
    headerMode: 'none'
  }
);

export default LoggedInNavigator;
