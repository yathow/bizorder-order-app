import NoInternetScreen from '../Containers/Errors/NoInternetScreen';
import { createStackNavigator } from 'react-navigation-stack';

const NoInternetNavigator = createStackNavigator(
  {
    NoInternetScreen: NoInternetScreen
  },
  {
    initialRouteName: 'NoInternetScreen',
    headerMode: 'none'
  }
);

export default NoInternetNavigator;
