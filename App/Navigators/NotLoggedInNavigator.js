import { createStackNavigator } from 'react-navigation-stack';

import LoginScreen from '../Containers/Login/LoginScreen';
import OptionsScreen from '../Containers/Options/OptionsScreen';
import BarcodeScannerScreen from '../Containers/BarcodeScanner/BarcodeScannerScreen';

const NotLoggedInNavigator = createStackNavigator(
  {
    LoginScreen: LoginScreen,
    BarcodeScannerScreen: BarcodeScannerScreen,
    OptionsScreen: OptionsScreen
  },
  {
    initialRouteName: 'LoginScreen',
    headerMode: 'none',
  }
)

export default NotLoggedInNavigator;
