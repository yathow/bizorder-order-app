import { call, put, select } from 'redux-saga/effects';

import AddressActions from '../Stores/Address/Actions';
import AddressesActions from '../Stores/Addresses/Actions';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import MessagesActions from '../Stores/Messages/Actions';
import NavigationService from '../Services/NavigationService';

const getAppStore = state => state.app;

export function* fetchAddress({ addressId }) {
  yield put(MessagesActions.clearMessages());

  const appStore = yield select(getAppStore);

  const getData = {};
  try {
    const result = yield call(
      ApiService.getApi, //function
      appStore.apiUrl,
      `deliveryPoint/showModel/${addressId}`,
      appStore.token,
      getData //params
    );

    if (result.isSuccess) {
      let address = {
        id: result.data.id,
        unitNo: result.data.unit_no,
        addressLine1: result.data.street_name,
        addressLine2: result.data.building_name,
        postcode: result.data.postcode,
        stateId: result.data.state_id,
        state: result.data.state_name,
        areaId: result.data.area_id,
        city: result.data.area_code,
        phoneNo1: result.data.phone_01,
        attention: result.data.attention
      };
      yield put(AddressActions.fetchAddressSuccess(address));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(AddressActions.fetchAddressLoading(false));
  }
}

export function* addAddress({ formikBag, values }) {
  formikBag.setSubmitting(true);

  yield put(MessagesActions.clearMessages());

  const appStore = yield select(getAppStore);

  const debtorId = appStore.user.debtorId;

  const getData = { data: { ...values, debtor_id: debtorId, code: '' } };
  try {
    const result = yield call(
      ApiService.postApi, //function
      appStore.apiUrl,
      `deliveryPoint/createModel`,
      appStore.token,
      getData //params
    );

    if (result.isSuccess) {
      yield put(AddressesActions.setRefreshAddresses(true));
      NavigationService.goBack();
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(AddressActions.fetchAddressLoading(false));
  }
  formikBag.setSubmitting(false);
}

export function* updateAddress({ formikBag, values }) {
  formikBag.setSubmitting(true);

  yield put(MessagesActions.clearMessages());

  const appStore = yield select(getAppStore);

  const getData = { data: values };
  try {
    const result = yield call(
      ApiService.putApi, //function
      appStore.apiUrl,
      `deliveryPoint/updateModel`,
      appStore.token,
      getData //params
    );

    if (result.isSuccess) {
      yield put(AddressesActions.setRefreshAddresses(true));
      NavigationService.goBack();
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(AddressActions.fetchAddressLoading(false));
  }
  formikBag.setSubmitting(false);
}

export function* deleteAddress({addressId}) {
  yield put(MessagesActions.clearMessages());

  const appStore = yield select(getAppStore);

  const getData = {};
  try {
    const result = yield call(
      ApiService.deleteApi, //function
      appStore.apiUrl,
      `deliveryPoint/deleteModel/${addressId}`,
      appStore.token,
      getData //params
    );

    if (result.isSuccess) {
      yield put(AddressesActions.setRefreshAddresses(true));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(AddressActions.fetchAddressLoading(false));
  }
}

export function* fetchAddressStates() {
  yield put(MessagesActions.clearMessages());

  try {
    const appStore = yield select(getAppStore);

    const getData = {
      search: '',
      filters: {}
    };

    const result = yield call(
      ApiService.postApi, //function
      appStore.apiUrl,
      `state/select2`,
      appStore.token,
      getData
    );

    if (result.isSuccess === true) {
      let states = [];
      result.data.data.map(cState => {
        let tState = {
          id: cState.id,
          desc1: cState.desc_01
        };
        states.push(tState);
      });
      yield put(AddressActions.fetchAddressStatesSuccess(states));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(AddressActions.fetchAddressStatesLoading(false));
  }
}

export function* fetchAddressAreas({ stateId }) {
  yield put(MessagesActions.clearMessages());

  try {
    const appStore = yield select(getAppStore);

    const getData = {
      search: '',
      filters: {
        state_id: stateId
      },
      pageSize: 100
    };

    const result = yield call(
      ApiService.postApi, //function
      appStore.apiUrl,
      `area/select2`,
      appStore.token,
      getData
    );

    if (result.isSuccess === true) {
      let areas = [];
      result.data.data.map(cArea => {
        let tArea = {
          id: cArea.id,
          code: cArea.code
        };
        areas.push(tArea);
      });
      yield put(AddressActions.fetchAddressAreasSuccess(areas));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(AddressActions.fetchAddressAreasLoading(false));
  }
}
