import { call, put, select } from 'redux-saga/effects';

import AddressesActions from '../Stores/Addresses/Actions';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import MessagesActions from '../Stores/Messages/Actions';

const getAppStore = state => state.app;

export function* fetchAddresses() {
  yield put(MessagesActions.clearMessages());

  const appStore = yield select(getAppStore);

  const debtorId = appStore.user.debtorId;

  let filters = [];
  let sorts = ['id:asc'];

  filters.push('item_group_01_id:' + appStore.divisionId);

  const getData = {};
  try {
    const result = yield call(
      ApiService.getApi, //function
      appStore.apiUrl,
      `debtor/getDeliveryPoint/${debtorId}`,
      appStore.token,
      getData //params
    );

    let addresses = [];
    if (result.isSuccess) {
      result.data.map(address => {
        const cAddress = {
          id: address.id,
          unitNo: address.unit_no,
          streetName: address.street_name,
          buildingName: address.building_name,
          postcode: address.postcode,
          phoneNo1: address.phone_01,
          attention: address.attention,
          stateId: address.state_id,
          areaId: address.area_id,
          stateName: address.state ? address.state.desc_01 : null,
          areaName: address.area ? address.area.code : null
        };
        addresses.push(cAddress);
      });

      yield put(AddressesActions.fetchAddressesSuccess(addresses));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(AddressesActions.fetchAddressesLoading(false));
  }
}
