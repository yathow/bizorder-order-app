import { call, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import AsyncStorage from '@react-native-community/async-storage';
import MessagesActions from '../Stores/Messages/Actions';
import NavigationService from '../Services/NavigationService';

const getAppStore = state => state.app;

export function* authenticate({ formikBag, username, password }) {
  yield put(MessagesActions.clearMessages());

  formikBag.setSubmitting(true);
  try {
    const app = yield select(getAppStore);
    const postData = {
      username: username,
      password: password
    };

    const result = yield call(
      ApiService.postApi, //function
      app.apiUrl,
      'login/authenticate',
      app.token,
      postData //params
    );

    if (result.isSuccess === true) {
      const user = {
        username: result.data.username,
        email: result.data.email,
        debtorId: result.data.debtor_id,
        debtorname: result.data.debtor_name,
        firstname: result.data.first_name,
        lastname: result.data.last_name,
        timezone: result.data.timezone,
        last_login: result.data.last_login,
        password_changed_at: result.data.password_changed_at
      };

      AsyncStorage.setItem('@last_username', username);

      yield put(AppActions.authenticateSuccess(result.data.token, user, result.message));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    formikBag.setSubmitting(false);
  }
}

export function* authenticateSuccess({ token, successMessage }) {
  NavigationService.navigate('LoggedInNavigator');
}

export function* tokenExpired() {
  NavigationService.navigate('NotLoggedInNavigator');
}

export function* passwordExpired() {
  NavigationService.navigate('ChangePasswordScreen');
}

export function* changePassword({ formikBag, currentPassword, newPassword }) {
  yield put(MessagesActions.clearMessages());

  formikBag.setSubmitting(true);
  try {
    const app = yield select(getAppStore);
    const postData = {
      currentPassword: currentPassword,
      newPassword: newPassword
    };

    const result = yield call(
      ApiService.postApi, //function
      app.apiUrl,
      `auth/changePassword`,
      app.token,
      postData //params
    );
    if (result.isSuccess === true) {
      NavigationService.navigate('HomeScreen');
      yield put(AppActions.changePasswordSuccess(result.message));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    formikBag.setSubmitting(false);
  }
}

export function* changePasswordSuccess({ successMessage }) {
  yield put(MessagesActions.addSuccessMessage(successMessage));
}

export function* logout() {
  try {
    const app = yield select(getAppStore);

    const postData = {
      fcmToken: app.fcmToken
    };

    const result = yield call(
      ApiService.postApi, //function
      app.apiUrl,
      `login/logout`,
      app.token,
      postData
    );
    if (result.isSuccess === true) {
      yield put(AppActions.unregisterFCMSuccess());
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(AppActions.showErrorMessage(error.message));
  }

  yield put(AppActions.logoutSuccess());
}

export function* logoutSuccess() {
  NavigationService.navigate('NotLoggedInNavigator');
}

export function* setDivision({ divisionId }) {
  // Todo reload brands
  // Todo reload cart
}

export function* fetchUsername() {
  const username = yield call(getUsername);
  yield put(AppActions.setUsername(username));
}

async function getUsername() {
  const username = await AsyncStorage.getItem('@last_username');
  return username;
}

export function* setHttpError({ result }) {
  if (result.isTokenExpired === true) {
    yield put(AppActions.tokenExpired(result.message));
  } else if (result.isPasswordExpired === true) {
    yield put(AppActions.passwordExpired(result.message));
  } else if (result.status !== 200) {
    if (result.status === 0) {
      NavigationService.navigate('NoInternetNavigator');
    } else {
      yield put(MessagesActions.addErrorMessage('HTTP Code ' + result.status + ': ' + result.message));
    }
  } else {
    yield put(MessagesActions.addErrorMessage(result.message));
  }
}

export function* registerFCM({ fcmToken }) {
  yield put(MessagesActions.clearMessages());

  try {
    const app = yield select(getAppStore);
    const postData = {
      fcmToken: fcmToken
    };

    const result = yield call(
      ApiService.postApi, //function
      app.apiUrl,
      'notifications/register',
      app.token,
      postData //params
    );
    if (result.isSuccess === true) {
      yield put(AppActions.registerFCMSuccess(result.data.fcm_token));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
  }
}

// export function* unregisterFCM({ fcmToken }) {
//   yield put(MessagesActions.clearMessages());

//   try {
//     const app = yield select(getAppStore);
//     const postData = {
//       fcmToken: fcmToken
//     };

//     const result = yield call(
//       ApiService.postApi, //function
//       app.apiUrl,
//       'notifications/unregister',
//       app.token,
//       postData //params
//     );
//     console.log(result)
//     if (result.isSuccess === true) {
//       yield put(AppActions.unregisterFCMSuccess())
//     } else {
//       yield put(AppActions.setHttpError(result));
//     }
//   } catch (error) {
//     yield put(MessagesActions.addErrorMessage(error.message));
//   } finally {
//   }
// }

export function* selectVersionType({ versionType }) {
  const app = yield select(getAppStore);

  let apiUrl = null;
  app.versionTypes.map(cVersionType => {
    if (cVersionType.id === versionType) {
      apiUrl = cVersionType.url;
    }
  });

  if (apiUrl !== null) {
    yield put(AppActions.updateApiUrl(apiUrl));
  }
}
