import NavigationService from '../Services/NavigationService';

export function* startScanner() {
    NavigationService.navigate('BarcodeScannerScreen');
}

export function* scanSuccess({barcodes}) {
    NavigationService.goBack();
}