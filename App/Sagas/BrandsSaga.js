import { call, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import BrandsActions from '../Stores/Brands/Actions';
import MessagesActions from '../Stores/Messages/Actions';

const getAppStore = state => state.app;
const getItemsStore = state => state.items;

export function* fetchBrands({ divisionId }) {
  yield put(MessagesActions.clearMessages());

  try {
    const appStore = yield select(getAppStore);

    let filters = [];
    filters.push('division_id:' + appStore.divisionId);

    let sorts = [];
    sorts.push('sequence:' + 'ASC');

    const getData = {
      search: '',
      filters: filters,
      sorts: sorts
    };

    const result = yield call(
      ApiService.postApi, //function
      appStore.apiUrl,
      `item/itemGroup01`,
      appStore.token,
      getData //params
    );

    let brands = [];

    if (result.isSuccess === true) {
      result.data.data.map(brand => {
        let cBrand = {
          id: brand.id,
          name: brand.code,
          photo: brand.image_path
        };
        brands.push(cBrand);
      });

      yield put(BrandsActions.fetchBrandsSuccess(brands));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  }
}

export function* fetchBrandsByCategory() {
  yield put(MessagesActions.clearMessages());

  try {
    const appStore = yield select(getAppStore);
    const itemsStore = yield select(getItemsStore);
    const itemsFilter = itemsStore.filters;

    let filters = [];
    filters.push('division_id:' + appStore.divisionId);

    if (itemsFilter.categoryIds.length > 0) {
      filters.push('item_group_02_id:' + itemsFilter.categoryIds[0]);
    }

    const getData = {
      search: '',
      filters: filters
    };

    const result = yield call(
      ApiService.postApi, //function
      appStore.apiUrl,
      `item/itemGroup01`,
      appStore.token,
      getData //params
    );

    let brands = [];

    if (result.isSuccess === true) {
      result.data.data.map(brand => {
        let cBrand = {
          id: brand.id,
          name: brand.code,
          photo: brand.image_path
        };
        brands.push(cBrand);
      });

      yield put(BrandsActions.fetchBrandsSuccess(brands));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(BrandsActions.fetchBrandsLoading(false));
  }
}
