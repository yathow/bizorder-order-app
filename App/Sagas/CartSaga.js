import { call, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import CartActions from '../Stores/Cart/Actions';
import MessagesActions from '../Stores/Messages/Actions';
import NavigationService from '../Services/NavigationService';
import OrderActions from '../Stores/Order/Actions';

const getAppStore = state => state.app;
const getCartStore = state => state.cart;

export function* addItemToCart({ itemId, uomId }) {
  const appStore = yield select(getAppStore);
  const cartStore = yield select(getCartStore);

  const cartId = cartStore.cart.id;
  const getData = {
    data: [
      {
        uom_id: uomId,
        item_id: itemId,
        qty: 1
      }
    ]
  };

  try {
    const result = yield call(
      ApiService.postApi,
      appStore.apiUrl,
      `cart/updateCartItem/${cartId}`,
      appStore.token,
      getData //params
    );

    if (result.isSuccess === true) {
      yield put(CartActions.fetchCart());
      NavigationService.navigate('CartScreen');
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  }
}

export function* addQtyItemInCart({ detailId }) {
  const appStore = yield select(getAppStore);
  const cartStore = yield select(getCartStore);
  const details = cartStore.cart.details;

  for (let i = 0; i < details.length; i++) {
    const detail = details[i];
    if (detail.id === detailId) {
      const cartId = cartStore.cart.id;
      const getData = {
        data: [
          {
            uom_id: detail.uomId,
            item_id: detail.itemId,
            qty: detail.qty + 1
          }
        ]
      };

      try {
        const result = yield call(
          ApiService.postApi,
          appStore.apiUrl,
          `cart/updateCartItem/${cartId}`,
          appStore.token,
          getData //params
        );

        if (result.isSuccess === true) {
          yield put(CartActions.fetchCart());
        } else {
          yield put(AppActions.setHttpError(result));
        }
      } catch (error) {
        yield put(MessagesActions.addErrorMessage(error.message));
      }
    }
  }
}

export function* fetchCart({ divisionId }) {
  console.log("Fetch Cart")
  yield put(MessagesActions.clearMessages());

  const appStore = yield select(getAppStore);

  const getData = {
    data: {
      division_id: divisionId
    }
  };
  try {
    const result = yield call(
      ApiService.postApi, //function
      appStore.apiUrl,
      `cart/findOrCreateDraftHeader`,
      appStore.token,
      getData //params
    );
    
    if (result.isSuccess === true) {
      let cartHdr = result.data.cartHdr;

      let cart = {
        id: cartHdr.id,
        details: [],
        subtotalAmt: 0,
        taxAmt: cartHdr.tax_amt,
        discAmt: 0,
        totalAmt: cartHdr.net_amt
      };

      cart.address = {
        unitNo: cartHdr.delivery_point_unit_no,
        streetName: cartHdr.delivery_point_street_name,
        buildingName: cartHdr.delivery_point_building_name,
        postcode: cartHdr.delivery_point_postcode,

        areaName: cartHdr.delivery_point_area_name,
        stateName: cartHdr.delivery_point_state_name,

        attention: cartHdr.delivery_point_attention,
        phoneNo1: cartHdr.delivery_point_phone_01,
        phoneNo2: cartHdr.delivery_point_phone_02
      };

      if (cartHdr.cart_dtls) { 
        cartHdr.cart_dtls.map(cartDtl => {
          let dtl = {
            id: cartDtl.id,
            itemId: cartDtl.item_id,
            uomId: cartDtl.uom_id,
            promoUuid: cartDtl.promo_uuid,
            oriPrice: cartDtl.sale_price,
            grossPrice: cartDtl.sale_price * parseInt(cartDtl.qty, 10),
            discAmt: (cartDtl.sale_price * parseInt(cartDtl.qty, 10)) - cartDtl.net_amt,
            netAmt: cartDtl.net_amt,
            disc1: cartDtl.dtl_disc_perc_01,
            qty: parseInt(cartDtl.qty, 10),
            isFoc: !(cartDtl.net_amt > 0),
            status: cartDtl.status
          };

          let price = []
          let fixed = 0
          let perc = 0;
          cartDtl.promotions.forEach(promo => {
            if(promo.price_disc > 0 && promo.disc_perc_01 == 0) {
              fixed = promo.price_disc
            }
          })

          cartDtl.promotions.forEach(promo => {
            if(promo.disc_perc_01 > 0 && promo.price_disc == 0) {
              perc = promo.disc_perc_01
            }
          })

          let newPrice = cartDtl.sale_price
          if(fixed > 0) {
            newPrice -= fixed
            price.push(parseFloat(newPrice))
          }
          if(perc > 0) {
            newPrice *= ((100-perc)/100)
            price.push(parseFloat(newPrice))
          }
          
          dtl.item = {
            id: cartDtl.item.id,
            code: cartDtl.item.code,
            desc1: cartDtl.item.desc_01,
            desc2: cartDtl.item.desc_02,
            photos: []
          };

          dtl.uom = {
            id: cartDtl.uom.id,
            code: cartDtl.uom.code,
            price: price,
            oriPrice: parseFloat(cartDtl.sale_price)
          };

          let promotions = []
          if (cartDtl.promotions) {
            cartDtl.promotions.reverse().map(promotion =>
            promotions.push({
              id: promotion.promotion_id,
              desc1: promotion.desc_01,
              type: promotion.disc_perc_01 > 0 && promotion.price_disc == 0
                      ? 0
                      : promotion.price_disc > 0 && promotion.disc_perc_01 == 0
                        && 1,
              applied: true
              
            }))
          }
          dtl.promotions = promotions

          if (cartDtl.item.item_photos) {
            cartDtl.item.item_photos.map(photo => {
              dtl.item.photos.push({
                id: photo.id,
                path: photo.path
              });
            });
          }

          let suggestions = [];
          cartDtl.suggestions.map(suggestion => {
            suggestions.push({
              id: dtl.id,
              promoDesc1: suggestion.promo_desc_01,
              type: suggestion.type,
              reqQty: suggestion.type === 'min_qty' ? suggestion.req_qty : 0,
              exceedQty: suggestion.type === 'max_qty' ? suggestion.req_qty : 0,
              uomQty: suggestion.uom_qty
            });
          });
          dtl.suggestions = suggestions;

          cart.details.push(dtl);
        });
      }
      
      let subtotalAmt = 0
      let discAmt = 0
      cart.details.forEach(dtl => {
        return subtotalAmt = subtotalAmt === 0 ? parseFloat(dtl.grossPrice) : subtotalAmt + parseFloat(dtl.grossPrice)
      })
      cart.details.forEach((dtl, index) => {
        return discAmt = discAmt === 0 ? parseFloat(dtl.discAmt) : discAmt + parseFloat(dtl.discAmt)
      })

      cart.subtotalAmt = subtotalAmt
      cart.discAmt = discAmt
      yield put(CartActions.fetchCartSuccess(cart));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  }

  yield put(CartActions.fetchCartLoading(false));
}

//Remove item from cart
export function* refreshCartPromotions({}) {
  console.log("Refresh Cart")
  yield put(MessagesActions.clearMessages())

  const appStore = yield select(getAppStore)
  const cartStore = yield select(getCartStore)
  
  // If cart has items, reapply promotions
  if(cartStore.cart.details.length > 0) {    
    let item_ids = []
    let item_filters = []
    let item_sorts = []
  
    //Push cart items to array
    cartStore.cart.details.map(cartDtl => {
      if(!cartDtl.isFoc) {
        item_ids.push(cartDtl.itemId)
      }
    })
    item_filters.push('id:' + item_ids)
    item_filters.push('division_id:' + appStore.divisionId);
    
    const getData = {
      page: 1,
      sorts: item_sorts,
      filters: item_filters
    };

    if(cartStore.cart.details.length > 0) {
      try {
        const fetch_item_result = yield call(
          ApiService.getApi, 
          appStore.apiUrl,
          `item/indexProcess/ITEM_LIST_02`,
          appStore.token,
          getData
        )

        if(fetch_item_result.isSuccess === true) {
          let conflicted_dtls = []
          cartStore.cart.details.forEach(dtl => {
            let conflict = false

            if(dtl.promotions.length > 0) {            
              conflict = dtl.promotions.some(promo => {
                if(promo.type == 1) {
                  return fetch_item_result.data.data.some(item => {
                    if(item.id == dtl.itemId && item.promotion.applied.price !== promo.id) {
                      return true
                    } else {
                      return false
                    }
                  })
                } else if(promo.type == 0) {
                  return fetch_item_result.data.data.map(item => {
                    if(item.id == dtl.itemId && item.promotion.applied.rate !== promo.id) {
                      return true
                    } else {
                      return false
                    }
                  })
                }
              })
            } else {
              conflict = fetch_item_result.data.data.some(item => {
                if(item.id == dtl.itemId && (item.promotion.applied.price > 0 || item.promotion.applied.rate > 0)) {
                  return true
                } else {
                  return false
                }
              })
            }

            if(conflict) {
              conflicted_dtls.push(dtl.id)
            }
          })

          const postData = {
            data: conflicted_dtls
          }

          try {
            const apply_conflict_result = yield call(
              ApiService.postApi,
              appStore.apiUrl,
              `cart/applyConflict`,
              appStore.token,
              postData
            )
      
            if (apply_conflict_result.isSuccess === true) {
              yield put(CartActions.fetchCart(appStore.divisionId));
            } else {
              yield put(MessagesActions.addErrorMessage("Apply Conflict:" + apply_conflict_result.message));
            }
          } catch (error) {
            yield put(MessagesActions.addErrorMessage(error.message));
          }
        } else {
          yield put(MessagesActions.addErrorMessage("Fetch Items:" + fetch_item_result.message));
        }
      } catch (error) {
        yield put(MessagesActions.addErrorMessage(error.message));
      }
    }
  }
}

export function* resolveConflict({ id }) {
  console.log("Resolve Conflict")
  yield put(MessagesActions.clearMessages())

  const appStore = yield select(getAppStore)
  const cartStore = yield select(getCartStore)
  const cartId = cartStore.cart.id

  let cart_dtl = {}

  cartStore.cart.details.forEach(dtl => {
    if(id === dtl.id) {
      cart_dtl = {
        item_id: dtl.itemId,
        uom_id: dtl.uomId,
        qty: dtl.qty
      }
    }
  })

  const removeCartItem = {
    data: [{
      item_id: cart_dtl.item_id,
      uom_id: cart_dtl.uom_id,
      qty: 0
    }]
  };

  try {
    const remove_result = yield call(
      ApiService.postApi,
      appStore.apiUrl,
      `cart/updateCartItem/${cartId}`,
      appStore.token,
      removeCartItem //params
    );

    if (remove_result.isSuccess === true) {
      const addCartItem = {
        data: [{
          item_id: cart_dtl.item_id,
          uom_id: cart_dtl.uom_id,
          qty: cart_dtl.qty
        }]
      }

      try {
        const add_result = yield call(
          ApiService.postApi,
          appStore.apiUrl,
          `cart/updateCartItem/${cartId}`,
          appStore.token,
          addCartItem //params
        );
    
        if (add_result.isSuccess === true) {
          yield put(CartActions.setRefreshCart(true));
        } else {
          yield put(MessagesActions.addErrorMessage(add_result.message));
        }
      } catch (error) {
        yield put(MessagesActions.addErrorMessage(error.message));
      }
    } else {
      yield put(MessagesActions.addErrorMessage(remove_result.message));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  }
}

export function* goToDelivery({ cart }) {
  const appStore = yield select(getAppStore);
  const getData = {
    data: {
      division_id: appStore.divisionId
    }
  };
  try {
    const result = yield call(
      ApiService.postApi, //function
      appStore.apiUrl,
      `cart/findOrCreateDraftHeader`,
      appStore.token,
      getData //params
    );

    if(result.isSuccess == true) {
      let cartDtls = result.data.cartHdr.cart_dtls

      conflict = cartDtls.some(dtl => dtl.status === 4)

      if(!conflict) {
        NavigationService.push('DeliveryScreen')
      } else {
        yield put(CartActions.setRefreshCart(true))
        NavigationService.replace('CartScreen')
      }
    } else {
      yield put(AppActions.setHttpError(result))
    }
  } catch(error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  }
}

export function* placeOrder({ cartId }) {
  yield put(MessagesActions.clearMessages());

  const appStore = yield select(getAppStore);

  const getData = {
    data: []
  };

  try {
    const result = yield call(
      ApiService.postApi,
      appStore.apiUrl,
      `cart/placeOrder/${cartId}`,
      appStore.token,
      getData //params
    );

    if (result.isSuccess === true) {
      const orderId = result.data.sls_ord_header.id;
      yield put(CartActions.placeOrderSuccess(orderId));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  }
}

export function* placeOrderSuccess({ orderId }) {
  const appStore = yield select(getAppStore);

  // Ensure can't back to checkout pages
  NavigationService.goBack(3);

  // Refetch cart
  yield put(CartActions.fetchCart(appStore.divisionId));
  yield put(OrderActions.viewOrder(orderId));
}

export function* setCheckoutAddress({ addressId }) {
  const appStore = yield select(getAppStore);
  const cartStore = yield select(getCartStore);

  const cartId = cartStore.cart.id;
  const getData = {
    hdrId: cartId,
    deliveryPointId: addressId
  };

  try {
    const result = yield call(
      ApiService.postApi,
      appStore.apiUrl,
      `cart/updateDeliveryPoint`,
      appStore.token,
      getData //params
    );

    if (result.isSuccess === true) {
      NavigationService.navigate('CheckoutScreen');
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  }
}

export function* setItemQtyInCart({ itemId, uomId, qty }) {
  const appStore = yield select(getAppStore);
  const cartStore = yield select(getCartStore);

  const cartId = cartStore.cart.id;
  const getData = {
    data: [
      {
        uom_id: uomId,
        item_id: itemId,
        qty: qty
      }
    ]
  };

  try {
    const result = yield call(
      ApiService.postApi,
      appStore.apiUrl,
      `cart/updateCartItem/${cartId}`,
      appStore.token,
      getData //params
    );

    if (result.isSuccess === true) {
      yield put(CartActions.setRefreshCart(true));
    } else {
      yield put(MessagesActions.addErrorMessage(result.message));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  }
}
