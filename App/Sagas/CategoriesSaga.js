import { call, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import CategoriesActions from '../Stores/Categories/Actions';
import MessagesActions from '../Stores/Messages/Actions';

const getAppStore = state => state.app;
const getItemsStore = state => state.items;

export function* fetchCategories({ divisionId }) {
  yield put(MessagesActions.clearMessages());

  try {
    const appStore = yield select(getAppStore);

    let filters = [];
    filters.push('division_id:' + appStore.divisionId);

    const getData = {
      search: '',
      filters: filters
    };

    const result = yield call(
      ApiService.postApi, //function
      appStore.apiUrl,
      `item/itemGroup02`,
      appStore.token,
      getData //params
    );

    let categories = [];

    if (result.isSuccess === true) {
      result.data.data.map(category => {
        let cCategory = {
          id: category.id,
          name: category.code,
          photo: category.image_path
        };
        categories.push(cCategory);
      });

      yield put(CategoriesActions.fetchCategoriesSuccess(categories));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(CategoriesActions.fetchCategoriesLoading(false));
  }
}

export function* fetchCategoriesByBrand() {
  yield put(MessagesActions.clearMessages());

  try {
    const appStore = yield select(getAppStore);
    const itemsStore = yield select(getItemsStore);
    const itemsFilter = itemsStore.filters;

    let filters = [];
    filters.push('division_id:' + appStore.divisionId);

    if (itemsFilter.brandIds.length > 0) {
      filters.push('item_group_01_id:' + itemsFilter.brandIds[0]);
    }

    const getData = {
      search: '',
      filters: filters
    };
    
    const result = yield call(
      ApiService.postApi, //function
      appStore.apiUrl,
      `item/itemGroup02`,
      appStore.token,
      getData //params
    );

    let categories = [];

    if (result.isSuccess === true) {
      result.data.data.map(category => {
        let cCategory = {
          id: category.id,
          name: category.code,
          photo: category.image_path
        };
        categories.push(cCategory);
      });

      yield put(CategoriesActions.fetchCategoriesSuccess(categories));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(CategoriesActions.fetchCategoriesLoading(false));
  }
}
