import { all, call, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import DebtorActions from '../Stores/Debtor/Actions';
import MessagesActions from '../Stores/Messages/Actions';

const getAppStore = state => state.app;

export function* fetchDebtor({ debtorId }) {
  yield put(MessagesActions.clearMessages());

  try {
    const appStore = yield select(getAppStore);

    const getData = {};

    const result = yield call(
      ApiService.getApi, //function
      appStore.apiUrl,
      `debtor/showModel/${debtorId}`,
      appStore.token,
      getData
    );

    if (result.isSuccess === true) {
      const debtor = {
        code: result.data.code,
        companyName1: result.data.company_name_01
      };
      yield put(DebtorActions.fetchDebtorSuccess(debtor));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(DebtorActions.fetchDebtorLoading(false));
  }
}
