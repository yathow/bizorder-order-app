import { call, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import DivItemsCountActions from '../Stores/DivItemsCount/Actions';
import MessagesActions from '../Stores/Messages/Actions';

const getAppStore = state => state.app;

export function* fetchDivItemsCount({ divisionId }) {
  yield put(MessagesActions.clearMessages());

  let filters = [];
  let sorts = [];

  const getData = {
    page: 1,
    sorts: sorts,
    filters: filters
  };

  try {
    const appStore = yield select(getAppStore);

    let filters = [];
    filters.push('division_id:' + appStore.divisionId);

    const getData = {
      search: '',
      filters: filters
    };

    const result = yield call(
      ApiService.getApi, //function
      appStore.apiUrl,
      `item/indexProcess/ITEM_LIST_02`,
      appStore.token,
      getData //params
    );

    if (result.isSuccess === true) {
      yield put(DivItemsCountActions.fetchDivItemsCountSuccess(result.data.total));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(DivItemsCountActions.fetchDivItemsCountLoading(false));
  }
}
