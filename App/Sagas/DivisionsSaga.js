import { call, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import DivisionsActions from '../Stores/Divisions/Actions';
import MessagesActions from '../Stores/Messages/Actions';

const getAppStore = state => state.app;

export function* fetchDivisions() {
  yield put(MessagesActions.clearMessages());

  try {
    const appStore = yield select(getAppStore);
    const debtorId = appStore.user.debtorId;

    const getData = {};

    const result = yield call(
      ApiService.getApi, //function
      appStore.apiUrl,
      `debtor/showDivisions/${debtorId}`,
      appStore.token,
      getData
    );

    if (result.isSuccess === true) {
      const divisions = [];
      result.data.map(division => {
        divisions.push({
          id: division.division_id,
          name: division.division_name_01
        });
      });

      yield put(DivisionsActions.fetchDivisionsSuccess(divisions));

      // assign default division
      if (divisions.length === 0) {
        yield put(AppActions.setDivision(0, ''));
      } else {
        yield put(AppActions.setDivision(divisions[0].id, divisions[0].name));
      }
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(DivisionsActions.fetchDivisionsLoading(false));
  }
}
