import { call, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import ItemDetailsActions from '../Stores/ItemDetails/Actions';
import MessagesActions from '../Stores/Messages/Actions';
import NavigationService from '../Services/NavigationService';

const getAppStore = state => state.app;

export function* showItemDetails({ itemId }) {
  yield put(AppActions.updateRoute('ItemDetails'))
  NavigationService.push('ItemDetailsScreen');
}

export function* fetchItemDetails({ itemId }) {
  yield put(ItemDetailsActions.fetchItemDetailsLoading(true));
  yield put(MessagesActions.clearMessages());

  if (itemId === 0) {
    return;
  }

  const appStore = yield select(getAppStore);

  let filters = [];
  let sorts = ['code:asc'];

  filters.push('division_id:' + appStore.divisionId);
  filters.push('id:' + itemId);

  const getData = {
    page: 1,
    sorts: sorts,
    filters: filters
  };

  try {
    const result = yield call(
      ApiService.getApi, //function
      appStore.apiUrl,
      `item/indexProcess/ITEM_LIST_02`,
      appStore.token,
      getData //params
    );
    
    let cItem = null;
    let promotionIds = [];
    
    if (result.isSuccess === true) {
      result.data.data.map(item => {
        cItem = {
          id: item.id,
          code: item.code,
          desc1: item.desc_01,
          desc2: item.desc_02,
          brandId: item.item_group_01_id,
          brandCode: item.item_group_01_code,
          categoryId: item.item_group_02_id,
          categoryCode: item.item_group_02_code,
          manufacturerId: item.item_group_03_id,
          manufacturerCode: item.item_group_03_code,
          uoms: [],
          photos: [],
          promotions: [],
          applied: {
            price: 0,
            rate: 0
          }
        };
        
        if (item.details) {
          item.details.map(uom => {
            const salesPrice = uom.salePrice ? parseFloat(uom.salePrice.sale_price) : 0;
            cItem.uoms.push({
              id: uom.uom_id,
              code: uom.uom_code,
              uomRate: uom.uom_rate,
              price: new Array(),
              oriPrice: salesPrice
            });
          });
        }

        if (item.item_photos) {
          item.item_photos.map(photo => {
            cItem.photos.push({
              id: photo.id,
              path: photo.path,
              uomId: photo.uom_id
            });
          });
        }
        
        if (item.promotion) {
          cItem.discount = {
            rate: item.promotion.discount_rate,
            price: item.promotion.discount_price
          };
          
          cItem.uoms.map(uom => {
            let price = uom.oriPrice
            if (cItem.discount.price > 0) {
              price = uom.oriPrice - (cItem.discount.price * uom.uomRate);
              uom.price.push(price)
            }
            if (cItem.discount.rate > 0) {
              price *= ((100 - cItem.discount.rate) / 100);
              uom.price.push(price)
            } 
          });

          item.promotion.promotions.map(promotion => {
            promotionIds.push(promotion.id)

            cItem.promotions.push({
              id: promotion.id,
              code: promotion.code,
              desc1: promotion.desc_01,
              desc2: promotion.desc_02,
              type: promotion.type,
              applied: promotion.id == item.promotion.applied.rate || promotion.id == item.promotion.applied.price ? true : false,
              items: [],
              item_group_01s: [],
              item_group_02s: []
            });
          });
        }
      });

      //API call to get all related item based on promotion ids
      let filters = [];

      filters.push('division_id:' + appStore.divisionId);

      let getPromotionData = {
        page: 1,
        sorts: sorts,
        filters: filters,
        promotionIds: promotionIds
      };

      const promotion_result = yield call(
        ApiService.getApi, //function
        appStore.apiUrl,
        `promotion/findAllRelatedItems`,
        appStore.token,
        getPromotionData //params
      );

      promotion_result.data.forEach((result, index) => {
        cItem.promotions[index].items = result.items.filter(item => item.id !== itemId)
        cItem.promotions[index].item_group_01s = result.item_group_01s
        cItem.promotions[index].item_group_02s = result.item_group_02s
      })
      //Sort by applied promotions first
      // cItem.promotions.sort(function(a,b) { return (a.type === b.type)? 0 : a.type === 0 ? -1 : 1; })
      cItem.promotions.sort(function(a,b) { return (a.applied === b.applied)? 0 : a.applied? -1 : 1; })

      yield put(ItemDetailsActions.fetchItemDetailsSuccess(cItem));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(ItemDetailsActions.showErrorMessage(error.message));
  }

  yield put(ItemDetailsActions.fetchItemDetailsLoading(false));
}
