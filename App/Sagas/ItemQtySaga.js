import { all, call, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import CartActions from '../Stores/Cart/Actions';
import ItemQtyActions from '../Stores/ItemQty/Actions';
import MessagesActions from '../Stores/Messages/Actions';

const getAppStore = state => state.app;
const getItemQtyStore = state => state.itemQty;

export function* fetchItemQty({ itemId }) {
  const appStore = yield select(getAppStore);

  let filters = [];
  let sorts = ['code:asc'];

  filters.push('division_id:' + appStore.divisionId);
  filters.push('id:' + itemId);

  const getData1 = {
    page: 1,
    sorts: sorts,
    filters: filters
  };

  const getData2 = {
    data: {
      division_id: appStore.divisionId
    }
  };

  try {
    const [result1, result2] = yield all([
      call(
        ApiService.getApi, //function
        appStore.apiUrl,
        `item/indexProcess/ITEM_LIST_02`,
        appStore.token,
        getData1 //params
      ),
      call(
        ApiService.postApi, //function
        appStore.apiUrl,
        `cart/findOrCreateDraftHeader`,
        appStore.token,
        getData2 //params
      )
    ]);

    let cItem = null;
    let cartId = null;
    let totalQty = 0

    if (result1.isSuccess && result2.isSuccess) {
      const cartHdr = result2.data.cartHdr;
      cartId = cartHdr.id;
      const cartDtls = cartHdr.cart_dtls;

      result1.data.data.map(item => {
        cItem = {
          id: item.id,
          code: item.code,
          desc1: item.desc_01,
          desc2: item.desc_02,
          uoms: [],
          photos: [],
          suggestions: [],
          discount: {
            price: 0,
            rate: 0
          },
          applied: {
            price: [],
            rate: []
          },
          price: []
        };

        if (item.item_photos) {
          item.item_photos.map(photo => {
            cItem.photos.push({
              id: photo.id,
              path: photo.path
            });
          });
        }

        if (item.details) {
          item.details.map(uom => {
            let qty = 0;

            for (let i = 0; i < cartDtls.length; i++) {
              let cartDtl = cartDtls[i];

              let isFoc = cartDtl.promo_hdr_id && !(cartDtl.net_amt > 0);

              if (!isFoc && cartDtl.item_id === itemId) {
                if (cartDtl.uom_id === uom.uom_id) {
                  qty = parseInt(cartDtl.qty, 10);

                  cartDtl.suggestions.map(suggestion => {
                    cItem.suggestions.push({
                      id: cartDtl.id,
                      promoDesc1: suggestion.promo_desc_01,
                      reqQty: suggestion.type === 'min_qty' ? suggestion.req_qty : 0,
                      exceedQty: suggestion.type === 'max_qty' ? suggestion.req_qty : 0,
                      uomQty: suggestion.uom_qty
                    });
                  });
                }
              }
            }

            const salePrice = uom.salePrice ? parseFloat(uom.salePrice.sale_price) : 0;
            cItem.uoms.push({
              id: uom.uom_id,
              uomCode: uom.uom_code,
              uomRate: uom.uom_rate,
              price: new Array(),
              oriPrice: salePrice,
              qty: qty
            });
          });
        }

        if (item.promotion) {
          cItem.uoms.map(uom => totalQty += uom.qty * uom.uomRate)
          // console.log("Total Item Qty: ", totalQty)

          item.promotion.promotions.map(promotion => {
            if(promotion.id === item.promotion.applied.price) {
              promotion.variants.map(variant => {
                if(variant.item_id === item.id) {
                  cItem.applied.price = {
                    promotionType: promotion.type,
                    minQty: variant.min_qty,
                    maxQty: variant.max_qty
                  }
                }
              })
            } else if(promotion.id === item.promotion.applied.rate) {
              promotion.variants.map(variant => {
                switch(variant.variant_type) {
                  case 'item': if(variant.item_id === item.id) {
                    cItem.applied.rate = {
                      promotionType: promotion.type,
                      minQty: variant.min_qty,
                      maxQty: variant.max_qty
                    }
                  }
                  break
                  case 'item_group01': if(variant.item_group_01_id === item.item_group01.id) {
                    cItem.applied.rate = {
                      promotionType: promotion.type,
                      minQty: variant.min_qty,
                      maxQty: variant.max_qty
                    }
                  }
                  break
                  case 'item_group02': if(variant.item_group_02_id === item.item_group02.id) {
                    cItem.applied.rate = {
                      promotionType: promotion.type,
                      minQty: variant.min_qty,
                      maxQty: variant.max_qty
                    }
                  }
                  break
                }
              })
            }
          })
          cItem.discount = {
            rate: item.promotion.discount_rate,
            price: item.promotion.discount_price
          };
          
          cItem.uoms.map(uom => {
            let price = uom.oriPrice
            let uomPrice = []
            if (cItem.discount.price > 0) {
              price = price - (cItem.discount.price * uom.uomRate);
              uomPrice.push(price)
            }
            if (cItem.discount.rate > 0) {
              price *= ((100 - cItem.discount.rate) / 100);
              uomPrice.push(price)
            }

            cItem.price.push(uomPrice)
          });
          
          let [fixedPrice, ratePrice, singlePromo] = getAppliedPromos(cItem.applied, totalQty)
          cItem.uoms = applyPromoPrices(cItem.uoms, fixedPrice, ratePrice, singlePromo, cItem.price, cItem)
        }
      });
    } else {
      if (!result1.isSuccess) {
        yield put(AppActions.setHttpError(result1));
      } else if (!result2.isSuccess) {
        yield put(AppActions.setHttpError(result2));
      }
    }
    yield put(ItemQtyActions.setTotalQty(totalQty))
    yield put(ItemQtyActions.setItemQty(cartId, cItem, cItem.uoms));
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  }
}

getAppliedPromos = (applied, totalQty) => {
  let fixedPrice, ratePrice, singlePromo = false

  if(Object.keys(applied.price).length > 0 && Object.keys(applied.rate).length > 0) {
    singlePromo = false
    if(totalQty >= applied.price.minQty) {
      if(applied.price.maxQty > 0) {
        if(totalQty <= applied.price.maxQty) {
          fixedPrice = true    
        } else {
          fixedPrice = false
        }
      } else {
        fixedPrice = true
      }
    } else {
      fixedPrice = false
    }

    if(totalQty >= applied.rate.minQty) {
      if(applied.rate.maxQty > 0) {
        if(totalQty <= applied.rate.maxQty) {
          ratePrice = true
        } else {
          ratePrice = false
        }
      } else {
        ratePrice = true
      }
    } else {
      ratePrice = false
    }
  } else if(Object.keys(applied.price).length > 0) {
    singlePromo = true
    if(totalQty >= applied.price.minQty) {
      if(applied.price.maxQty > 0) {
        if(totalQty <= applied.price.maxQty) {
          fixedPrice = true    
        } else {
          fixedPrice = false
        }
      } else {
        fixedPrice = true
      }
    } else {
      fixedPrice = false
    }
  } else if(Object.keys(applied.rate).length > 0) {
    singlePromo = true
    if(totalQty >= applied.rate.minQty) {
      if(applied.rate.maxQty > 0) {
        if(totalQty <= applied.rate.maxQty) {
          ratePrice = true
        } else {
          ratePrice = false
        }
      } else {
        ratePrice = true
      }
    } else {
      ratePrice = false
    }
  } else {
    singlePromo = false
    fixedPrice = false
    ratePrice = false
  }

  return [fixedPrice, ratePrice, singlePromo]
}

applyPromoPrices = (nUoms, fixedPrice, ratePrice, singlePromo, priceUom, item) => {
  nUoms.map((uom,index) => {
    if(fixedPrice && ratePrice) {
      uom.price = priceUom[index]
    } else if(!fixedPrice && ratePrice) {
      if(singlePromo) {
        uom.price = priceUom[index]
      } else {
        uom.price = [uom.oriPrice * ((100-item.discount.rate)/100)]
      }
    } else if(fixedPrice && !ratePrice) {
      if(singlePromo) {
        uom.price = priceUom[index]
      } else {
        uom.price = [uom.oriPrice - (item.discount.price * uom.uomRate)]
      }
    } else {
      uom.price = []
    }
  })

  return nUoms
}

export function* updateItemQty({ uomId, qty }) {
  const itemQtyStore = yield select(getItemQtyStore);
  let nUoms = itemQtyStore.itemQty.uoms;
  const applied = itemQtyStore.itemQty.item.applied
  const item = itemQtyStore.itemQty.item

  let totalQty = parseInt(itemQtyStore.itemQty.totalQty);

  if (nUoms) {
    const priceUom = itemQtyStore.itemQty.item.price
    let rateQty = 0
    nUoms.map(uom => {
      if (uom.id === uomId) {
        rateQty = uom.uomRate * (qty - uom.qty)
        uom.qty = qty;
      }
    });
    
    totalQty += parseInt(rateQty)

    let [fixedPrice, ratePrice, singlePromo] = getAppliedPromos(applied, totalQty)

    nUoms = applyPromoPrices(nUoms, fixedPrice, ratePrice, singlePromo, priceUom, item)

    yield put(ItemQtyActions.setUoms(nUoms));
    yield put(ItemQtyActions.setTotalQty(totalQty));
  }
  yield put(ItemQtyActions.setShowItemQty(true));
}

export function* saveItemQty() {
  const appStore = yield select(getAppStore);

  const itemQtyStore = yield select(getItemQtyStore);
  const cartId = itemQtyStore.itemQty.cartId;
  const item = itemQtyStore.itemQty.item;
  const nUoms = itemQtyStore.itemQty.uoms;

  let data = [];
  nUoms.map(uom => {
    data.push({
      uom_id: uom.id,
      item_id: item.id,
      qty: uom.qty
    });
  });

  const getData = {
    data: data
  };

  try {
    const result = yield call(
      ApiService.postApi,
      appStore.apiUrl,
      `cart/updateCartItem/${cartId}`,
      appStore.token,
      getData //params
    );

    if (result.isSuccess === true) {
      yield put(CartActions.setRefreshCart(true));
    } else {
      yield put(MessagesActions.addErrorMessage(result.message));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  }

  yield put(ItemQtyActions.setShowItemQty(false));
}
