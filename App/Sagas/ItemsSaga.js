import { call, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import ItemService from '../Services/ItemService';
import ItemsActions from '../Stores/Items/Actions';
import MessagesActions from '../Stores/Messages/Actions';
import NavigationService from '../Services/NavigationService';

const getAppStore = state => state.app;
const getItemsStore = state => state.items;

export function* selectBrand() {
  const itemStore = yield select(getItemsStore)

  yield put(AppActions.updateRoute('Brands'));
  NavigationService.navigate('ItemsScreen');
  yield put(ItemsActions.toggleBrandFilters(itemStore.filters.brandIds[0]))
}

export function* selectCategory() {
  const itemStore = yield select(getItemsStore)

  yield put(AppActions.updateRoute('Categories'));
  NavigationService.navigate('ItemsScreen');
  yield put(ItemsActions.toggleCategoryFilters(itemStore.filters.categoryIds[0]))
}

export function* fetchFilters() {
  // NavigationService.goBack();
  NavigationService.navigate('ItemFiltersScreen');
  yield put(AppActions.updateRoute('ItemFilters'));
}

export function* applyFilters() {
  NavigationService.goBack();
  NavigationService.navigate('ItemsScreen');
}

export function* fetchItems() {
  yield put(MessagesActions.clearMessages());

  const appStore = yield select(getAppStore);
  const itemsStore = yield select(getItemsStore);

  const itemsFilter = itemsStore.filters;

  let sortBy = ItemService.getSortByType(itemsFilter.sortType);

  let filters = [];
  let sorts = [sortBy];

  filters.push('division_id:' + appStore.divisionId);

  if (itemsFilter.brandIds.length > 0) {
    filters.push('item_group_01_id:' + itemsFilter.brandIds[0]);
  }

  if (itemsFilter.categoryIds.length > 0) {
    filters.push('item_group_02_id:' + itemsFilter.categoryIds[0]);
  }

  if (itemsFilter.keyword.trim() !== '') {
    filters.push('keyword:' + itemsFilter.keyword);
  } else {
    filters.push('keyword:' + '');
  }

  const getData = {
    page: 1,
    sorts: sorts,
    filters: filters
  };
  
  try {
    const result = yield call(
      ApiService.getApi, //function
      appStore.apiUrl,
      `item/indexProcess/ITEM_LIST_02`,
      appStore.token,
      getData //params
    );
    
    let items = [];
    let currentPage = 1;
    let lastPage = 1;
    let total = 0;
    
    if (result.isSuccess === true) {
      currentPage = result.data.current_page;
      lastPage = result.data.last_page;
      total = result.data.total;

      result.data.data.map(item => {
        const cItem = {
          id: item.id,
          code: item.code,
          desc1: item.desc_01,
          desc2: item.desc_02,
          brandId: item.item_group_01_id,
          brandCode: item.item_group_01_code,
          categoryId: item.item_group_02_id,
          categoryCode: item.item_group_02_code,
          manufacturerId: item.item_group_03_id,
          manufacturerCode: item.item_group_03_code,
          uoms: [],
          photos: [],
          promotions: [],
          discount: 0
        };
        
        if (item.details) {
          item.details.map(uom => {
            const salesPrice = uom.salePrice ? parseFloat(uom.salePrice.sale_price) : 0;
            cItem.uoms.push({
              id: uom.uom_id,
              code: uom.uom_code,
              uomRate: uom.uom_rate,
              price: new Array(),
              oriPrice: salesPrice
            });
          });
        }

        if (item.item_photos) {
          item.item_photos.map(photo => {
            cItem.photos.push({
              id: photo.id,
              path: photo.path,
              uomId: photo.uom_id
            });
          });
        }

        if (item.promotion) {
          cItem.discount = {
            rate: item.promotion.discount_rate,
            price: item.promotion.discount_price
          };

          cItem.uoms.map(uom => {
            let price = uom.oriPrice
            if (cItem.discount.price > 0) {
              price = uom.oriPrice - (cItem.discount.price * uom.uomRate);
              uom.price.push(price)
            }
            if (cItem.discount.rate > 0) {
              price *= ((100 - cItem.discount.rate) / 100);
              uom.price.push(price)
            } 
          });
          
          item.promotion.promotions.forEach(promotion => {
            cItem.promotions.push({
              id: promotion.id,
              desc1: promotion.desc_01,
              desc2: promotion.desc_02,
              type: promotion.type,
              applied: promotion.id == item.promotion.applied.rate || promotion.id == item.promotion.applied.price ? true : false
            });
          });
          //Sort by applied promotions first
          // cItem.promotions.sort(function(a,b) { return (a.type === b.type)? 0 : a.type === 0 ? -1 : 1; })
          cItem.promotions.sort(function(a,b) { return (a.applied === b.applied)? 0 : a.applied? -1 : 1; })
        }

        items.push(cItem);
      });
      
      yield put(ItemsActions.fetchItemsSuccess(items, currentPage, lastPage, total));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(ItemsActions.fetchItemsLoading(false));
  }
}

export function* fetchMoreItems() {
  yield put(MessagesActions.clearMessages());

  const appStore = yield select(getAppStore);
  const itemsStore = yield select(getItemsStore);

  const itemsFilter = itemsStore.filters;

  let filters = [];
  let sorts = ['code:asc'];

  filters.push('division_id:' + appStore.divisionId);

  if (itemsFilter.brandIds.length > 0) {
    filters.push('item_group_01_id:' + itemsFilter.brandIds[0]);
  }

  if (itemsFilter.categoryIds.length > 0) {
    filters.push('item_group_02_id:' + itemsFilter.categoryIds[0]);
  }

  if (itemsFilter.keyword.trim() !== '') {
    filters.push('keyword:' + itemsFilter.keyword);
  } else {
    filters.push('keyword:' + '');
  }

  const getData = {
    page: itemsStore.currentPage + 1,
    sorts: sorts,
    filters: filters
  };

  try {
    const result = yield call(
      ApiService.getApi, //function
      appStore.apiUrl,
      `item/indexProcess/ITEM_LIST_02`,
      appStore.token,
      getData //params
    );

    let items = [];
    let currentPage = 1;
    let lastPage = 1;
    let total = 0;

    if (result.isSuccess === true) {
      currentPage = result.data.current_page;
      lastPage = result.data.last_page;
      total = result.data.total;

      result.data.data.map(item => {
        const cItem = {
          id: item.id,
          code: item.code,
          desc1: item.desc_01,
          desc2: item.desc_02,
          brandId: item.item_group_01_id,
          brandCode: item.item_group_01_code,
          categoryId: item.item_group_02_id,
          categoryCode: item.item_group_02_code,
          manufacturerId: item.item_group_03_id,
          manufacturerCode: item.item_group_03_code,
          uoms: [],
          promotions: [],
          photos: []
        };

        if (item.details) {
          item.details.map(uom => {
            const salesPrice = uom.salePrice ? parseFloat(uom.salePrice.sale_price) : 0;
            cItem.uoms.push({
              id: uom.uom_id,
              code: uom.uom_code,
              uomRate: uom.uom_rate,
              price: salesPrice,
              oriPrice: salesPrice
            });
          });
        }

        if (item.item_photos) {
          item.item_photos.map(photo => {
            cItem.photos.push({
              id: photo.id,
              path: photo.path,
              uomId: photo.uom_id
            });
          });
        }

        if (item.promotion) {
          cItem.discount = {
            rate: item.promotion.discount_rate,
            price: item.promotion.discount_price
          };

          cItem.uoms.map(uom => {
            if (cItem.discount.rate > 0) {
              uom.price = ((100 - cItem.discount.rate) / 100) * uom.oriPrice;
            } else if (cItem.discount.price > 0) {
              uom.price = cItem.discount.price * uom.uomRate;
            }
          });

          item.promotion.promotions.map(promotion => {
            cItem.promotions.push({
              id: promotion.id,
              desc1: promotion.desc_01,
              desc2: promotion.desc_02
            });
          });
        }

        items.push(cItem);
      });

      yield put(ItemsActions.fetchItemsSuccess(items, currentPage, lastPage, total));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message));
  } finally {
    yield put(ItemsActions.fetchItemsLoading(false));
  }
}
