import { delay, put } from 'redux-saga/effects';

import MessagesActions from '../Stores/Messages/Actions';

export function* delayClearMessages({ ms }) {
  yield delay(ms);
  yield put(MessagesActions.clearMessages());
}
