import { put, select, call } from 'redux-saga/effects'

import ApiService from '../Services/ApiService'

import AppActions from '../Stores/App/Actions'
import NotificationsActions from '../Stores/Notifications/Actions'
import MessagesActions from '../Stores/Messages/Actions'

const getAppStore = state => state.app
const getNotificationsStore = state => state.notifications

export function* fetchNotifications({  }) {
    yield put(MessagesActions.clearMessages())
    yield put(NotificationsActions.fetchNotificationsLoading(true))

    const appStore = yield select(getAppStore)
    
    let filters = []
    filters.push('division_id:' + appStore.divisionId)

    let sorts = []
    sorts.push('created_at:desc')

    const getData = {
        page: 1,
        filters,
        sorts
    }

    try {
        let messages = []
        let currentPage = 1
        let lastPage = 1
        let total = 0

        const result = yield call(
            ApiService.getApi, //function
            appStore.apiUrl,
            `message/index/${appStore.user.debtorId}`,
            appStore.token,
            getData //params
        )

        if(result.isSuccess) {
            currentPage = result.data.current_page
            lastPage = result.data.last_page
            total = result.data.total

            result.data.data.forEach(data => {
                let message = {
                    id: data.id,
                    userId: data.user_id,
                    divisionId: data.division_id,
                    companyId: data.company_id,
                    title: data.title,
                    content: data.content,
                    payload: JSON.parse(data.payload),
                    priority: data.priority,
                    status: data.status,
                    createdAt: data.created_at,
                    updatedAt: data.updated_at,
                }

                messages.push(message)
            })

            yield put(NotificationsActions.fetchNotificationsSuccess(messages, currentPage, lastPage, total))
        } else {
            yield put(AppActions.setHttpError(result))
        }
    } catch (error) {            
        yield put(MessagesActions.addErrorMessage(error.message))
    } finally {
        yield put(NotificationsActions.fetchNotificationsLoading(false))
    }
}

export function* fetchMoreNotifications() {
    yield put(NotificationsActions.fetchMoreNotificationsLoading(true))
    yield put(MessagesActions.clearMessages())

    const appStore = yield select(getAppStore)
    const notificationsStore = yield select(getNotificationsStore)

    let filters = []
    filters.push('division_id:' + appStore.divisionId)

    let sorts = []
    sorts.push('created_at:desc')

    const getData = {
        page: notificationsStore.currentPage + 1,
        filters: filters,
        sorts: sorts
    }

    try {
        let notifications = notificationsStore.notifications
        let currentPage = 1
        let lastPage = 1
        let total = 0
    
        const result = yield call(
            ApiService.getApi, //function
            appStore.apiUrl,
            `message/index/${appStore.user.debtorId}`,
            appStore.token,
            getData //params
        )

        if(result.isSuccess === true) {
            currentPage = result.data.current_page;
            lastPage = result.data.last_page;
            total = result.data.total;

            result.data.data.map(data => {
                let notification = {
                    id: data.id,
                    userId: data.user_id,
                    divisionId: data.division_id,
                    companyId: data.company_id,
                    title: data.title,
                    content: data.content,
                    payload: JSON.parse(data.payload),
                    priority: data.priority,
                    status: data.status,
                    createdAt: data.created_at,
                    updatedAt: data.updated_at,
                }
    
                notifications.push(notification);
            });
    
            yield put(NotificationsActions.fetchNotificationsSuccess(notifications, currentPage, lastPage, total));
        } else {
            yield put(AppActions.setHttpError(result));
        }
    } catch (error) {
        yield put(MessagesActions.addErrorMessage(error.message))
    } finally {
        yield put(NotificationsActions.fetchMoreNotificationsLoading(false))
    }
}

export function* readNotification({ id }) {
    const appStore = yield select(getAppStore)

    const postData = {
        data: {
            id,
            status: 1
        }
    }

    try {
        const result = yield call(
            ApiService.putApi, //function
            appStore.apiUrl,
            `message/updateModel`,
            appStore.token,
            postData //params
        )

        if(result.isSuccess) {
            let model = result.data.model
            
            let notification = {
                id: model.id,
                userId: model.user_id,
                divisionId: model.division_id,
                companyId: model.company_id,
                title: model.title,
                content: model.content,
                payload: JSON.parse(model.payload),
                priority: model.priority,
                status: model.status,
                createdAt: model.created_at,
                updatedAt: model.updated_at,
            }

            yield put(NotificationsActions.readNotificationSuccess(notification));
        } else {
            yield put(AppActions.setHttpError(result))
        }
    } catch (error) {
        yield put(MessagesActions.addErrorMessage(error.message))
    }
}

export function* markAllAsSeen() {
    const appStore = yield select(getAppStore)
    const notificationsStore = yield select(getNotificationsStore)

    let filters = []
    filters.push('division_id:' + appStore.divisionId)
    filters.push('limit:' + notificationsStore.currentPage)

    let sorts = []
    sorts.push('created_at:desc')

    const postData = {
        userId: appStore.user.debtorId,
        filters,
        sorts,
        currentPage: notificationsStore.currentPage,
        page: 1,
        data: {
            status: 1
        }
    }

    try {
        let currentPage = notificationsStore.currentPage
        let lastPage = notificationsStore.lastPage
        let total = notificationsStore.total

        const result = yield call(
            ApiService.putApi, //function
            appStore.apiUrl,
            `message/markAllAsSeen`,
            appStore.token,
            postData //params
        )

        if(result.isSuccess) {
            let notifications = result.data.map(data => {
                return {
                    id: data.id,
                    userId: data.user_id,
                    divisionId: data.division_id,
                    companyId: data.company_id,
                    title: data.title,
                    content: data.content,
                    payload: JSON.parse(data.payload),
                    priority: data.priority,
                    status: data.status,
                    createdAt: data.created_at,
                    updatedAt: data.updated_at,
                }
            })

            yield put(NotificationsActions.fetchNotificationsSuccess(notifications, currentPage, lastPage, total));
        } else {
            yield put(AppActions.setHttpError(result))
        }
    } catch (error) {
        yield put(MessagesActions.addErrorMessage(error.message))
    }
}

export function* clearNotifications() {
    const appStore = yield select(getAppStore)

    let filters = []
    filters.push('division_id:' + appStore.divisionId)

    let sorts = []
    sorts.push('created_at:desc')

    const postData = {
        userId: appStore.user.debtorId,
        filters,
        sorts,
        page: 1
    }

    try {
        let currentPage = 1
        let lastPage = 1
        let total = 0

        const result = yield call(
            ApiService.deleteApi, //function
            appStore.apiUrl,
            `message/clearNotifications`,
            appStore.token,
            postData //params
        )

        if(result.isSuccess) {
            yield put(NotificationsActions.fetchNotificationsSuccess([], currentPage, lastPage, total));
        } else {
            yield put(AppActions.setHttpError(result))
        }
    } catch (error) {
        yield put(MessagesActions.addErrorMessage(error.message))
    }
}

export function* deleteNotification() {
    const appStore = yield select(getAppStore)
    const notificationsStore = yield select(getNotificationsStore)
    const id = notificationsStore.deleteNotificationId
    
    try {
        const result = yield call(
            ApiService.deleteApi,
            appStore.apiUrl,
            `message/deleteModel/${id}`,
            appStore.token
        )
        
        if(result.isSuccess) {
            yield put(NotificationsActions.deleteNotificationSuccess(id))
        } else {
            yield put(AppActions.setHttpError(result))
        }
    } catch (error) {
        yield put(MessagesActions.addErrorMessage(error.message))
    }
}
