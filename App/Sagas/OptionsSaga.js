import NavigationService from '../Services/NavigationService';

export function* showOptions() {
  NavigationService.navigate('OptionsScreen');
}
