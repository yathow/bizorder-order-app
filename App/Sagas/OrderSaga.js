import { all, call, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import MessagesActions from '../Stores/Messages/Actions';
import NavigationService from '../Services/NavigationService';
import OrderActions from '../Stores/Order/Actions';

const getAppStore = state => state.app;

export function* viewOrder({ orderId }) {
  NavigationService.navigate('OrderScreen');
}

export function* fetchOrder({ orderId }) {
  const appStore = yield select(getAppStore);
  const getData = {};
  try {
    const [result1, result2] = yield all([
      call(
        ApiService.getApi, //function
        appStore.apiUrl,
        `slsOrd/showHeader/${orderId}`,
        appStore.token,
        getData //params
      ),
      call(
        ApiService.getApi, //function
        appStore.apiUrl,
        `slsOrd/showDetails/${orderId}`,
        appStore.token,
        getData //params
      )
    ]);

    if (result1.isSuccess === true && result2.isSuccess === true) {
      let ordHdr = result1.data;
      let ordDtls = result2.data;
      let details = [];
      let order = {
        id: 1,
        docCode: ordHdr.doc_code,
        details: details,
        subtotalAmt: 0,
        taxAmt: ordHdr.tax_amt,
        discAmt: 0,
        totalAmt: ordHdr.net_amt,
        docStatus: ordHdr.doc_status,
        voidReason: ordHdr.desc_01
      };

      order.address = {
        unitNo: ordHdr.delivery_point_unit_no,
        streetName: ordHdr.delivery_point_street_name,
        buildingName: ordHdr.delivery_point_building_name,
        postcode: ordHdr.delivery_point_postcode,
        areaName: ordHdr.delivery_point_area_name,
        stateName: ordHdr.delivery_point_state_name,
        phoneNo1: ordHdr.delivery_point_phone_01,
        phoneNo2: ordHdr.delivery_point_phone_02,
        attention: ordHdr.delivery_point_attention
      };

      ordDtls.map(ordDtl => {
        let detail = {
          id: ordDtl.id,
          qty: parseInt(ordDtl.qty, 10),
          oriPrice: parseFloat(ordDtl.sale_price),
          grossPrice: ordDtl.sale_price * parseInt(ordDtl.qty, 10),
          discAmt: (ordDtl.sale_price * parseInt(ordDtl.qty, 10)) - ordDtl.net_amt,
          netAmt: parseFloat(ordDtl.net_amt),
          isFoc: ordDtl.promotions > 0 && !(ordDtl.net_amt > 0)
        };

        let price = [], fixed = perc = 0;
        ordDtl.promotions.forEach(promo => {
          if(promo.price_disc > 0 && promo.disc_perc_01 == 0) {
            fixed = promo.price_disc
          }
        })

        ordDtl.promotions.forEach(promo => {
          if(promo.disc_perc_01 > 0 && promo.price_disc == 0) {
            perc = promo.disc_perc_01
          }
        })

        let newPrice = ordDtl.sale_price
        if(fixed > 0) {
          newPrice -= fixed
          price.push(parseFloat(newPrice))
        }
        if(perc > 0) {
          newPrice *= ((100-perc)/100)
          price.push(parseFloat(newPrice))
        }

        if (ordDtl.item) {
          detail.item = {
            id: ordDtl.item.id,
            code: ordDtl.item.code,
            desc1: ordDtl.item.desc_01,
            desc2: ordDtl.item.desc_02,
            photos: []
          };

          if (ordDtl.uom) {
            detail.uom = {
              id: ordDtl.uom.id,
              code: ordDtl.uom.code,
              price: price,
              oriPrice: parseFloat(ordDtl.sale_price)
            };
          }

          if (ordDtl.item.item_photos) {
            ordDtl.item.item_photos.map(photo => {
              detail.item.photos.push({
                id: photo.id,
                path: photo.path
              });
            });
          }
        }

        let promotions = []
        if (ordDtl.promotions) {
          ordDtl.promotions.reverse().map(promotion =>{

          promotions.push({
            id: promotion.promotion.id,
            desc1: promotion.promotion.desc_01,
            type: promotion.disc_perc_01 > 0 && promotion.price_disc == 0
                    ? 0
                    : promotion.price_disc > 0 && promotion.disc_perc_01 == 0
                      && 1,
            applied: true
            
          })})
        }
        detail.promotions = promotions

        details.push(detail);
      });

      let subtotalAmt = discAmt = 0
      order.details.forEach(dtl => {
        return subtotalAmt = subtotalAmt === 0 ? parseFloat(dtl.grossPrice) : subtotalAmt + parseFloat(dtl.grossPrice)
      })
      order.details.forEach((dtl, index) => {
        return discAmt = discAmt === 0 ? parseFloat(dtl.discAmt) : discAmt + parseFloat(dtl.discAmt)
      })

      order.subtotalAmt = subtotalAmt
      order.discAmt = discAmt

      yield put(OrderActions.fetchOrderSuccess(order));
    } else {
      yield put(MessagesActions.showErrorMessage(result1.message));
    }
  } catch (error) {
    yield put(MessagesActions.showErrorMessage(error.message));
  } finally {
    yield put(OrderActions.fetchOrderLoading(false));
  }
}
