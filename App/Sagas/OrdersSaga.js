import { call, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import MessagesActions from '../Stores/Messages/Actions';
import OrdersActions from '../Stores/Orders/Actions';

const getAppStore = state => state.app;
const getOrdersStore = state => state.orders;

export function* fetchOrders() {
  yield put(MessagesActions.clearMessages());
  yield put(OrdersActions.fetchOrdersLoading(true));

  const appStore = yield select(getAppStore);
  const ordersStore = yield select(getOrdersStore);

  const divisionId = appStore.divisionId;
  const debtorId = appStore.user.debtorId;
  
  let filters = [];
  filters.push('debtor_id:' + debtorId);
  filters.push('status:' + ordersStore.filterStatus);

  let sorts = [];
  sorts.push('doc_code:desc');

  const getData = {
    page: 1,
    filters: filters,
    sorts: sorts
  };

  try {
    let orders = [];
    let currentPage = 1;
    let lastPage = 1;
    let total = 0;

    const result = yield call(
      ApiService.getApi, //function
      appStore.apiUrl,
      `slsOrd/index/${divisionId}`,
      appStore.token,
      getData //params
    );

    if (result.isSuccess === true) {
      currentPage = result.data.current_page;
      lastPage = result.data.last_page;
      total = result.data.total;

      result.data.data.map(ordHdr => {
        let order = {
          id: ordHdr.id,
          docNo: ordHdr.doc_code,
          docDate: ordHdr.doc_date,
          docStatus: ordHdr.doc_status,
          totalAmt: ordHdr.net_amt
        };

        orders.push(order);
      });

      yield put(OrdersActions.fetchOrdersSuccess(orders, currentPage, lastPage, total));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message))
  } finally {
    yield put(OrdersActions.fetchOrdersLoading(false));
  }
}

export function* fetchMoreOrders() {
  yield put(MessagesActions.clearMessages());

  const appStore = yield select(getAppStore);
  const ordersStore = yield select(getOrdersStore);

  const divisionId = appStore.divisionId;
  const debtorId = appStore.user.debtorId;

  let filters = [];
  filters.push('debtor_id:' + debtorId);
  filters.push('status:' + ordersStore.filterStatus);

  let sorts = [];
  sorts.push('doc_code:desc');

  const getData = {
    page: ordersStore.currentPage + 1,
    filters: filters,
    sorts: sorts
  };

  try {
    let orders = [];
    let currentPage = 1;
    let lastPage = 1;
    let total = 0;

    const result = yield call(
      ApiService.getApi, //function
      appStore.apiUrl,
      `slsOrd/index/${divisionId}`,
      appStore.token,
      getData //params
    );

    if (result.isSuccess === true) {
      currentPage = result.data.current_page;
      lastPage = result.data.last_page;
      total = result.data.total;

      result.data.data.map(ordHdr => {
        let order = {
          id: ordHdr.id,
          docNo: ordHdr.doc_code,
          docDate: ordHdr.doc_date,
          docStatus: ordHdr.doc_status,
          totalAmt: ordHdr.net_amt
        };

        orders.push(order);
      });

      yield put(OrdersActions.fetchOrdersSuccess(orders, currentPage, lastPage, total));
    } else {
      yield put(AppActions.setHttpError(result));
    }
  } catch (error) {
    yield put(MessagesActions.addErrorMessage(error.message))
  }
}
