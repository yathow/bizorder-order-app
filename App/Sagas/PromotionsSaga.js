import { call, put, select } from 'redux-saga/effects'
import ItemDetailsActions from '../Stores/ItemDetails/Actions'
import NavigationService from '../Services/NavigationService';
import PromotionsActions from '../Stores/Promotions/Actions'
import MessagesActions from '../Stores/Messages/Actions'
import AppActions from '../Stores/App/Actions'
import ApiService from '../Services/ApiService'

const getAppStore = state => state.app

export function* fetchPromotions() {
    yield put(PromotionsActions.fetchPromotionsLoading(true))

    const appStore = yield select(getAppStore)

    let sorts = []
    let filters = []
    sorts.push('sequence:asc')
    filters.push('status:100')

    const getData = {
        filters: filters,
        sorts: sorts
    }

    try {
        const result = yield call(
            ApiService.getApi, //function
            appStore.apiUrl,
            `promotion/index/${appStore.divisionId}`,
            appStore.token,
            getData //params
        )

        if (result.isSuccess === true) {
            yield put(PromotionsActions.fetchPromotionsSuccess(result.data.data))
        } else {
            yield put(AppActions.setHttpError(result))
        }
    } catch (error) {
        yield put(MessagesActions.addErrorMessage(error.message))
    } finally {
        yield put(PromotionsActions.fetchPromotionsLoading(false))
    }
}

export function* fetchPromotionItems({ promotionId }) {
    yield put(PromotionsActions.fetchPromotionItemsLoading(true))

    const appStore = yield select(getAppStore)

    const getData = {
        promotionId: promotionId
    }

    try{
        const result = yield call(
            ApiService.getApi, //function
            appStore.apiUrl,
            `promotion/findPromotionItems`,
            appStore.token,
            getData //params
        )

        if(result.isSuccess === true) {
            let titles = []

            result.data.forEach(category => {
                const cCategory = {
                    title: category.title,
                    expanded: category.expanded,
                    items: []
                }

                category.items.forEach(item => {
                    const cItem = {
                        id: item.id,
                        code: item.code,
                        desc1: item.desc_01,
                        desc2: item.desc_02,
                        uoms: [],
                        photos: []
                    };

                    if(item.item_uoms) {
                        
                        item.item_uoms.forEach(uom => {
                            let prices = item.item_prices.filter(price => price.uom_id == uom.uom_id)
                            if(item.unit_uom_id === uom.uom_id) {
                                cItem.uoms.push({
                                    id: uom.uom_id,
                                    code: item.unit_uom.code,
                                    uomRate: uom.uom_rate,
                                    price: new Array(),
                                    oriPrice: prices[0].sale_price
                                });
                            } else if(item.case_uom_id === uom.uom_id) {
                                cItem.uoms.push({
                                    id: uom.uom_id,
                                    code: item.case_uom.code,
                                    uomRate: uom.uom_rate,
                                    price: new Array(),
                                    oriPrice: prices[0].sale_price
                                });
                            }
                        })

                    }

                    if (item.item_photos) {

                        item.item_photos.map(photo => {
                            cItem.photos.push({
                                id: photo.id,
                                path: photo.path,
                                uomId: photo.uom_id
                            });
                        });

                    }

                    cCategory.items.push(cItem)
                })

                titles.push(cCategory)
            })


            yield put(PromotionsActions.fetchPromotionItemsSuccess(titles))
        } else {
            yield put(MessagesActions.addErrorMessage(result.message))
        }
    } catch (error) {
        yield put(MessagesActions.addErrorMessage(error.message))
    } finally {
        yield put(PromotionsActions.fetchPromotionItemsLoading(false))     
    }
}

export function* fetchPromotion({ promotionId }) {
    yield put(PromotionsActions.fetchPromotionsLoading(true))

    const appStore = yield select(getAppStore)

    const getData = {
        promotionId: promotionId
    }

    try{
        const result = yield call(
            ApiService.getApi, //function
            appStore.apiUrl,
            `promotion/findPromotion`,
            appStore.token,
            getData //params
        )
        
        if(result.isSuccess === true) {
            yield put(PromotionsActions.fetchPromotionSuccess(result.data))
        }   
    } catch (error) {
        yield put(MessagesActions.addErrorMessage(error.message))
    } finally {
        yield put(PromotionsActions.fetchPromotionsLoading(false))     
    }
}

export function* goToPromotion({ promotionId }) {
    yield put(PromotionsActions.setPromotion(promotionId))
    yield put(AppActions.updateRoute('Promotion'))
    NavigationService.push('PromotionsScreen')
}