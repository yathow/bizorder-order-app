import { put, select } from 'redux-saga/effects';

import AppActions from '../Stores/App/Actions';
import MessagesActions from '../Stores/Messages/Actions';
import NavigationService from '../Services/NavigationService';

/**
 * The startup saga is the place to define behavior to execute when the application starts.
 */

const getAppStore = state => state.app;

export function* startup() {
  yield put(AppActions.initLocale());
  yield put(AppActions.initApiUrl(true));
  yield put(AppActions.setConnected(true));

  yield put(MessagesActions.clearMessages());

  const app = yield select(getAppStore);
  if (app.token === null) {
    NavigationService.navigate('NotLoggedInNavigator');
  } else {
    NavigationService.navigate('LoggedInNavigator');
  }
}
