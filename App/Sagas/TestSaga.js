import { all, call, delay, put, select } from 'redux-saga/effects';

import ApiService from '../Services/ApiService';
import TestActions from '../Stores/Test/Actions';

const getAppStore = state => state.app;

export function* reset() {
  console.log(' >> reset start');
  console.log(' >> reset end');
}

export function* fetch1({ id }) {
  console.log(' >> fetch1 start');

  const appStore = yield select(getAppStore);
  const getData = {
    data: {
      division_id: id
    }
  };

  try {
    const result = call(
      ApiService.postApi, //function
      appStore.apiUrl,
      `cart/findOrCreateDraftHeader`,
      appStore.token,
      getData //params
    );

    console.log(result);
  } catch (error) {
    console.log(error.message);
  }

  console.log(' >> fetch1 end');
}

export function* fetch2({ id }) {
  console.log(' >> fetch2 start');

  const appStore = yield select(getAppStore);
  const getData = {
    data: {
      division_id: id
    }
  };

  try {
    const result = call(
      ApiService.postApi, //function
      appStore.apiUrl,
      `cart/findOrCreateDraftHeader`,
      appStore.token,
      getData //params
    );

    console.log(result);
  } catch (error) {
    console.log(error.message);
  }

  console.log(' >> fetch2 end');
}

export function* setValue1({ value1 }) {
  console.log(' >> setValue1 start');
  console.log(' >> setValue1 end');
}

export function* setValue2({ value2 }) {
  console.log(' >> setValue2 start');
  console.log(' >> setValue2 end');
}

export function* process1() {
  console.log(' >> process1 start');

  yield* fetch1(1);
  yield* fetch2(1);

  console.log(' >> process1 end');
}

export function* process2() {
  console.log(' >> process2 start');
  console.log(' >> process2 end');
}
