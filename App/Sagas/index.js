import { all, takeLatest } from 'redux-saga/effects';
import {
  addAddress,
  fetchAddress,
  fetchAddressAreas,
  fetchAddressStates,
  updateAddress,
  deleteAddress
} from './AddressSaga';
import { 
  fetchAddresses 
} from './AddressesSaga';
import {
  authenticate,
  authenticateSuccess,
  changePassword,
  changePasswordSuccess,
  fetchUsername,
  logout,
  logoutSuccess,
  passwordExpired,
  setDivision,
  setHttpError,
  registerFCM,
  selectVersionType,
  tokenExpired
} from './AppSaga';
import { 
  scanSuccess, 
  startScanner 
} from './BarcodeScannerSaga';
import { 
  fetchBrands, 
  fetchBrandsByCategory 
} from './BrandsSaga';
import {
  addItemToCart,
  addQtyItemInCart,
  fetchCart,
  goToDelivery,
  placeOrder,
  placeOrderSuccess,
  setCheckoutAddress,
  setItemQtyInCart,
  refreshCartPromotions,
  resolveConflict
} from './CartSaga';
import { 
  fetchCategories, 
  fetchCategoriesByBrand 
} from './CategoriesSaga';
import { 
  fetchDebtor 
} from './DebtorSaga';
import { 
  fetchDivisions 
} from './DivisionsSaga';
import { 
  fetchDivItemsCount 
} from './DivItemsCountSaga';
import { 
  fetchItemDetails, 
  showItemDetails 
} from './ItemDetailsSaga';
import {
  applyFilters,
  fetchFilters,
  fetchItems,
  fetchMoreItems,
  selectBrand,
  selectCategory
} from './ItemsSaga';
import { 
  fetchItemQty, 
  saveItemQty, 
  updateItemQty 
} from './ItemQtySaga';
import { 
  delayClearMessages 
} from './MessagesSaga';
import {
  fetchNotifications,
  fetchMoreNotifications,
  readNotification,
  markAllAsSeen,
  clearNotifications,
  deleteNotification
} from './NotificationsSaga'
import { 
  showOptions 
} from './OptionsSaga';
import { 
  fetchOrder, 
  viewOrder 
} from './OrderSaga';
import { 
  fetchMoreOrders, 
  fetchOrders 
} from './OrdersSaga';
import { 
  fetchPromotions, 
  fetchPromotion, 
  fetchPromotionItems, 
  goToPromotion
} from './PromotionsSaga';
import { 
  startup 
} from './StartupSaga';

import { fetch1, fetch2, process1, process2, reset, setValue1, setValue2 } from './TestSaga';

import { AddressTypes } from '../Stores/Address/Actions';
import { AddressesTypes } from '../Stores/Addresses/Actions';
import { AppTypes } from '../Stores/App/Actions';
import { BarcodeScannerTypes } from '../Stores/BarcodeScanner/Actions';
import { BrandsTypes } from '../Stores/Brands/Actions';
import { CartTypes } from '../Stores/Cart/Actions';
import { CategoriesTypes } from '../Stores/Categories/Actions';
import { DebtorTypes } from '../Stores/Debtor/Actions';
import { DivItemsCountTypes } from '../Stores/DivItemsCount/Actions';
import { DivisionsTypes } from '../Stores/Divisions/Actions';
import { ItemDetailsTypes } from '../Stores/ItemDetails/Actions';
import { ItemQtyTypes } from '../Stores/ItemQty/Actions';
import { ItemsTypes } from '../Stores/Items/Actions';
import { MessagesTypes } from '../Stores/Messages/Actions';
import { NotificationsTypes } from '../Stores/Notifications/Actions'
import { OrderTypes } from '../Stores/Order/Actions';
import { OrdersTypes } from '../Stores/Orders/Actions';
import { OptionsTypes } from '../Stores/Options/Actions';
import { StartupTypes } from '../Stores/Startup/Actions';
import { PromotionsTypes } from '../Stores/Promotions/Actions';
import { TestTypes } from '../Stores/Test/Actions';

export default function* root() {
  yield all([
    /**
     * @see https://redux-saga.js.org/docs/basics/UsingSagaHelpers.html
     */
    takeLatest(StartupTypes.STARTUP, startup),


    takeLatest(AddressTypes.FETCH_ADDRESS, fetchAddress),
    takeLatest(AddressTypes.ADD_ADDRESS, addAddress),
    takeLatest(AddressTypes.DELETE_ADDRESS, deleteAddress),
    takeLatest(AddressTypes.UPDATE_ADDRESS, updateAddress),
    takeLatest(AddressTypes.FETCH_ADDRESS_STATES, fetchAddressStates),
    takeLatest(AddressTypes.FETCH_ADDRESS_AREAS, fetchAddressAreas),

    takeLatest(AddressesTypes.FETCH_ADDRESSES, fetchAddresses),
    
    takeLatest(AppTypes.AUTHENTICATE, authenticate),
    takeLatest(AppTypes.AUTHENTICATE_SUCCESS, authenticateSuccess),
    takeLatest(AppTypes.TOKEN_EXPIRED, tokenExpired),
    takeLatest(AppTypes.PASSWORD_EXPIRED, passwordExpired),
    takeLatest(AppTypes.CHANGE_PASSWORD, changePassword),
    takeLatest(AppTypes.CHANGE_PASSWORD_SUCCESS, changePasswordSuccess),
    takeLatest(AppTypes.LOGOUT, logout),
    takeLatest(AppTypes.LOGOUT_SUCCESS, logoutSuccess),
    takeLatest(AppTypes.FETCH_USERNAME, fetchUsername),
    takeLatest(AppTypes.SET_DIVISION, setDivision),
    takeLatest(AppTypes.SET_HTTP_ERROR, setHttpError),
    takeLatest(AppTypes.REGISTER_FCM, registerFCM),
    takeLatest(AppTypes.SELECT_VERSION_TYPE, selectVersionType),

    takeLatest(BarcodeScannerTypes.START_SCANNER, startScanner),
    takeLatest(BarcodeScannerTypes.SCAN_SUCCESS, scanSuccess),

    takeLatest(BrandsTypes.FETCH_BRANDS, fetchBrands),
    takeLatest(BrandsTypes.FETCH_BRANDS_BY_CATEGORY, fetchBrandsByCategory),

    takeLatest(CartTypes.ADD_ITEM_TO_CART, addItemToCart),
    takeLatest(CartTypes.ADD_QTY_ITEM_IN_CART, addQtyItemInCart),
    takeLatest(CartTypes.FETCH_CART, fetchCart),
    takeLatest(CartTypes.GO_TO_DELIVERY, goToDelivery),
    takeLatest(CartTypes.PLACE_ORDER, placeOrder),
    takeLatest(CartTypes.PLACE_ORDER_SUCCESS, placeOrderSuccess),
    takeLatest(CartTypes.SET_ITEM_QTY_IN_CART, setItemQtyInCart),
    takeLatest(CartTypes.SET_CHECKOUT_ADDRESS, setCheckoutAddress),
    takeLatest(CartTypes.REFRESH_CART_PROMOTIONS, refreshCartPromotions),
    takeLatest(CartTypes.RESOLVE_CONFLICT, resolveConflict),

    takeLatest(CategoriesTypes.FETCH_CATEGORIES, fetchCategories),
    takeLatest(CategoriesTypes.FETCH_CATEGORIES_BY_BRAND, fetchCategoriesByBrand),

    takeLatest(DebtorTypes.FETCH_DEBTOR, fetchDebtor),

    takeLatest(DivisionsTypes.FETCH_DIVISIONS, fetchDivisions),

    takeLatest(DivItemsCountTypes.FETCH_DIV_ITEMS_COUNT, fetchDivItemsCount),

    takeLatest(ItemDetailsTypes.SHOW_ITEM_DETAILS, showItemDetails),
    takeLatest(ItemDetailsTypes.FETCH_ITEM_DETAILS, fetchItemDetails),

    takeLatest(ItemsTypes.SELECT_BRAND, selectBrand),
    takeLatest(ItemsTypes.SELECT_CATEGORY, selectCategory),
    takeLatest(ItemsTypes.FETCH_FILTERS, fetchFilters),
    takeLatest(ItemsTypes.APPLY_FILTERS, applyFilters),
    takeLatest(ItemsTypes.FETCH_ITEMS, fetchItems),
    takeLatest(ItemsTypes.FETCH_MORE_ITEMS, fetchMoreItems),

    takeLatest(ItemQtyTypes.FETCH_ITEM_QTY, fetchItemQty),
    takeLatest(ItemQtyTypes.UPDATE_ITEM_QTY, updateItemQty),
    takeLatest(ItemQtyTypes.SAVE_ITEM_QTY, saveItemQty),

    takeLatest(MessagesTypes.DELAY_CLEAR_MESSAGES, delayClearMessages),

    takeLatest(NotificationsTypes.FETCH_NOTIFICATIONS, fetchNotifications),
    takeLatest(NotificationsTypes.FETCH_MORE_NOTIFICATIONS, fetchMoreNotifications),
    takeLatest(NotificationsTypes.READ_NOTIFICATION, readNotification),
    takeLatest(NotificationsTypes.MARK_ALL_AS_SEEN, markAllAsSeen),
    takeLatest(NotificationsTypes.CLEAR_NOTIFICATIONS, clearNotifications),
    takeLatest(NotificationsTypes.DELETE_NOTIFICATION, deleteNotification),

    takeLatest(OptionsTypes.SHOW_OPTIONS, showOptions),

    takeLatest(OrderTypes.VIEW_ORDER, viewOrder),
    takeLatest(OrderTypes.FETCH_ORDER, fetchOrder),

    takeLatest(OrdersTypes.FETCH_ORDERS, fetchOrders),
    takeLatest(OrdersTypes.FETCH_MORE_ORDERS, fetchMoreOrders),

    takeLatest(PromotionsTypes.FETCH_PROMOTIONS, fetchPromotions),
    takeLatest(PromotionsTypes.GO_TO_PROMOTION, goToPromotion),
    takeLatest(PromotionsTypes.FETCH_PROMOTION, fetchPromotion),
    takeLatest(PromotionsTypes.FETCH_PROMOTION_ITEMS, fetchPromotionItems),

    takeLatest(TestTypes.RESET, reset),
    takeLatest(TestTypes.FETCH1, fetch1),
    takeLatest(TestTypes.FETCH2, fetch2),
    takeLatest(TestTypes.PROCESS1, process1),
    takeLatest(TestTypes.PROCESS2, process2),
    takeLatest(TestTypes.SET_VALUE1, setValue1),
    takeLatest(TestTypes.SET_VALUE2, setValue2)
  ]);
}
