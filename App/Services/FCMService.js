import messaging from '@react-native-firebase/messaging'
import {Platform} from 'react-native'

class FCMService {
    register = (onRegister, onNotification,  onOpenNotification) => {
        this.checkPermission(onRegister)
        this.createNotificationListeners(onRegister, onNotification, onOpenNotification)
    }

    registerAppWithFCM = async() => {
        if(Platform.OS === 'ios') {
            await messaging().setAutoInitEnabled(false)
        }
    }

    checkPermission = (onRegister) => {
        messaging().hasPermission()
        .then(enabled => {
            if(enabled) {
                //User has permission
                this.getToken(onRegister)
            } else {
                //User does not have permission
                this.requestPermission(onRegister)
            }
        }).catch(error => {
            console.log("[FSCMService] Permission rejected ", error)
        })
    }

    getToken = (onRegister) => {
        messaging().getToken()
        .then(fcmToken => {
            if(fcmToken) {
                onRegister(fcmToken)
            } else {
                console.log("[FCMSservice] User does not have a device token")
            }
        }).catch(error => {
            console.log("[FCMService] getToken rejected ", error)
        })
    }

    requestPermission = (onRegister) => {
        messaging().requestPermission()
        .then(() => {
            this.getToken(onRegister)
        }).catch(error => {
            console.log("[FCMService] Request permission rejected ", error)
        })
    }

    deleteToken = () => {
        console.log("[FCMService] deleteToken ")
        messaging().deleteToken()
        .catch(error => {
            console.log("[FCMService] Delete token error ", error)
        })
    }

    createNotificationListeners = (onRegister, onNotification, onOpenNotification) => {
        // //When the app is running in the background
        // messaging()
        // .onNotificationOpenedApp(remoteMessage => {
        //     console.log("[FCMService] onNotificationOpenedApp Notification caused app to open when running in background")
        //     if(remoteMessage) {
        //         notification = remoteMessage.notification
        //         data = remoteMessage.data

        //         onOpenNotification(notification, data)
        //     }
        // })

        //When app is opened from a quit state
        messaging()
        .getInitialNotification()
        .then(remoteMessage => {
            console.log("[FCMService] getInitialNotification Notification caused app to open from quit state: ", remoteMessage)

            if(remoteMessage) {
                const notification = remoteMessage.notification
                const data = remoteMessage.data.data || remoteMessage.data

                onOpenNotification(notification, data)
            }
        })

        //Foreground state messages
        this.messageListener = messaging().onMessage(async remoteMessage => {
            console.log("[FCMService] A new FCM message has arrived!", remoteMessage)
            
            if(remoteMessage) {
                notification = remoteMessage.notification
                data = remoteMessage.data
                
                onNotification(notification, data)
            }
        })

        //Triggered when new token is created
        messaging().onTokenRefresh(fcmToken => {
            console.log("[FCMService] New token refresh: ", fcmToken)
        })
    }

    unRegister = () => {
        this.messageListener()
    }
}

export const fcmService = new FCMService()