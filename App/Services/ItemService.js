function getSortByType(type) {
  let sortBy;
  switch (type) {
    case 1:
      sortBy = 'desc_01:desc';
      break;
    default:
      sortBy = 'desc_01:asc';
      break;
  }
  return sortBy;
}

export default { getSortByType };
