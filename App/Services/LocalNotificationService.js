import PushNotification from 'react-native-push-notification'
import PushNotificationIOS from '@react-native-community/push-notification-ios'
import {Platform} from 'react-native'

class LocalNotificationService {
    configure = (onOpenNotification) => {
        PushNotification.configure({
            onRegister: function (token) {
                console.log("[LocalNotificationService] onRegister: ", token)
            },
            onNotification: function (notification) {
                console.log("[LocalNotificationService] onNotification: ", notification)
                if(!notification?.data) {
                    return
                }
                notification.body = notification.message
                notification.userInteraction = true
                data = notification.data
                onOpenNotification(notification, data)

                if(Platform.OS === 'ios') {
                    // (required) Called when a remote is received or opened, or local notification is opened
                    // notification.finish(PushNotificationIOS.FetchResult.NoData)
                }
            },

            //IOS ONLY (optional): default: all - Permissions to register.
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },

            //Should the initial notification be popped automatically
            //default: true
            popInitialNotification: true,

            /**
             * (optional) default: true
             * - Specified if permissions (ios) and token (android and ios) will request or not,
             * - If not, you must call PushNotificationsHandler.requestPermissions() later
             * - If you are not using remote notification or do not have a Firebase installed, use this:
             *      requestPermissions: Platform.OS === 'ios
             */
            requestPermissions: true,
        })
    }

    unregister = () => {
        PushNotification.unregister()
        PushNotification.abandonPermissions()
    }

    showNotification = (id, title, message, data = {}, options = {}) => {
        PushNotification.localNotification({
            /* Android Only Properties */
            ...this.buildAndroidNotification(id, title, message, data, options),
            /* iOS and Android properties */
            ...this.buildIOSNotification(id, title, message, data, options),

            title: title || '',
            message: message || '',
            sound: options.sound || true,
            soundName: options.soundName || 'default',
            userInteraction: false
        })
    }

    buildAndroidNotification = (id, title, message, data = {}, options = {}) => {
        return {
            id: id,
            autoCancel: true, 
            largeIcon: options.largeIcon || 'ic_launcher',
            smallIcon: options.smallIcon || 'ic_notification',
            bigText: message || '',
            subText: title || '',
            vibrate: options.vibrate || true,
            vibration: options.vibration || 300,
            priority: options.priority || 'high',
            importance: options.importance || 'high',
            data: data
        }
    }

    buildIOSNotification = (id, title, message, data = {}, options = {}) => {
        return {
            alertAction: options.alertAction || 'view',
            category: options.category || "",
            userInfo: {
                id: id,
                item: data,
                data: options
            }
        }
    }

    cancelAllLocalNotifications = () => {
        if(Platform.OS === 'ios') {
            PushNotificationIOS.removeAllDeliveredNotifications()
        } else {
            PushNotification.cancelAllLocalNotifications()
        }
    }

    removeDeliveredNotificationByID = (notificationId) => {
        console.log("[LocalNotificationService] this.removeDeliveredNotificationByID: ", notificationId)
        PushNotification.cancelLocalNotifications({id: `${notificationId}`})
    }
}

export const localNotificationService = new LocalNotificationService()