import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import AppActions from '../Stores/App/Actions'
import PromotionsActions from '../Stores/Promotions/Actions'
import OrderActions from '../Stores/Order/Actions'

import { fcmService } from './FCMService'
import { localNotificationService } from './LocalNotificationService'

export default function NotificationService() {
    const dispatch = useDispatch()
    const fcmToken = useSelector(state => state.app.fcmToken)

    useEffect(() => {
        if(!fcmToken) {
            fcmService.registerAppWithFCM()
        }
        fcmService.register(onRegister, onNotification, onOpenNotification)
        localNotificationService.configure(onOpenNotification)

        function onRegister(fcmToken) {
            console.log("[App] onRegister: FCM Token: ", fcmToken)

            dispatch(AppActions.registerFCM(fcmToken))
        }

        function onNotification(notification, data) {
            console.log("[App] onNotification: ", notification)
            console.log("[App] onNotification: ", data)

            if(notification !== null){
                const options = {
                    soundName: 'default',
                    sound: true,
                    id: data.id || 0,
                    navigation: data.navigation || ''
                }
                localNotificationService.showNotification(
                    0,
                    notification.title,
                    notification.body,
                    notification,
                    options
                )
            }
        } 

        function onOpenNotification(notification, data) {
            console.log("[App] onOpenNotification body: ", notification.body)
            console.log("[App] onOpenNotification data: ", data)


            //Open from background
            if(data.navigation) {
                if(data.navigation == 'OrderScreen'){
                    dispatch(OrderActions.viewOrder(parseInt(data.id)))
                } else if(data.navigation == 'PromotionsScreen') {
                    dispatch(PromotionsActions.goToPromotion(parseInt(data.id)))
                }
            }
            //Open from app
            else if(data.data.navigation) {
                if(data.data.navigation == 'OrderScreen'){
                    dispatch(OrderActions.viewOrder(parseInt(data.data.id)))
                } else if(data.data.navigation == 'PromotionsScreen') {
                    dispatch(PromotionsActions.goToPromotion(parseInt(data.data.id)))
                }
            }
        }

        return () => {
            fcmService.unRegister()
            localNotificationService.unregister()
        }
    }, [])

    return(null)
}