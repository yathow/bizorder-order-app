const docStatuses = {
  0: { label: 'pending', status: 'warning' },
  2: { label: 'void', status: 'error' },
  3: { label: 'draft', status: 'warning' },
  50: { label: 'processing', status: 'info' },
  100: { label: 'complete', status: 'success' }
};

const getDocStatus = docStatusId => {
  var result = docStatuses[docStatusId];
  if (result === undefined) {
    result = { label: '-', status: '' };
  }
  return result;
};

const isDocVoided = docStatusId => {
  return docStatusId === 2;
};

export { getDocStatus, isDocVoided };
