import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  resetAddress: null,
  showAddress: ['addressId'],
  deleteAddress: ['addressId'],
  setModalDeleteVisible: ['modalDeleteVisible', 'addressId'],
  fetchAddress: ['addressId'],
  fetchAddressSuccess: ['address'],
  fetchAddressLoading: ['boolean'],
  addAddress: ['formikBag', 'values'],
  updateAddress: ['formikBag', 'values'],

  setAddress: ['address'],

  setAddressState: ['stateId'],
  fetchAddressStates: null,
  fetchAddressStatesSuccess: ['states'],
  fetchAddressStatesLoading: ['boolean'],

  setAddressArea: ['areaId'],
  fetchAddressAreas: ['stateId'],
  fetchAddressAreasSuccess: ['areas'],
  fetchAddressAreasLoading: ['boolean']
});

export const AddressTypes = Types;
export default Creators;
