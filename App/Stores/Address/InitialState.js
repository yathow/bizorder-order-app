/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  addressIsLoading: false,
  refreshAddress: false,
  addressId: 0,
  address: {
    id: 0,
    unitNo: '',
    addressLine1: '',
    addressLine2: '',
    postcode: '',
    attention: '',
    phoneNo1: '',
    state: '',
    stateId: 0,
    city: '',
    areaId: 0
  },
  modalDeleteVisible: false,
  states: [],
  stateIsLoading: false,

  areas: [],
  areaIsLoading: false
};
