/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { AddressTypes } from './Actions';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';

export const resetAddress = state => ({
  ...state
});

export const showAddress = (state, { addressId }) => ({
  ...state,
  addressId: addressId,
  address: INITIAL_STATE.address
});

export const deleteAddress = (state, { addressId }) => ({
  ...state,
  addresses: addresses.filter(address => address.id !== addressId),
  addressesIsLoading: true,
})

export const setAddress = (state, { address }) => {
  return {
    ...state,
    address
  };
};

export const setModalDeleteVisible = (state, {modalDeleteVisible, addressId}) => {
  return {
    ...state,
    addressId,
    modalDeleteVisible
  }
}

export const fetchAddress = (state, { addressId }) => ({
  ...state,
  addressIsLoading: true
});

export const fetchAddressSuccess = (state, { address }) => ({
  ...state,
  address,
  addressesIsLoading: false
});

export const fetchAddressLoading = (state, { boolean }) => ({
  ...state,
  addressIsLoading: boolean
});

export const fetchAddressStates = state => ({
  ...state,
  states: [],
  stateIsLoading: true
});

export const fetchAddressStatesSuccess = (state, { states }) => ({
  ...state,
  states,
  stateIsLoading: false
});

export const fetchAddressStatesLoading = (state, { boolean }) => ({
  ...state,
  stateIsLoading: boolean
});

export const setAddressState = (state, { stateId }) => {
  return {
    ...state,
    address: {
      ...state.address,
      stateId,
      areaId: 0
    }
  };
};

export const fetchAddressAreas = (state, { stateId }) => ({
  ...state,
  areas: [],
  stateIsLoading: true
});

export const fetchAddressAreasSuccess = (state, { areas }) => ({
  ...state,
  areas,
  stateIsLoading: false
});

export const fetchAddressAreasLoading = (state, { boolean }) => ({
  ...state,
  stateIsLoading: boolean
});

export const setAddressArea = (state, { areaId }) => ({
  ...state,
  address: {
    ...state.address,
    areaId
  }
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [AddressTypes.SHOW_ADDRESS]: showAddress,
  [AddressTypes.SET_ADDRESS]: setAddress,
  [AddressTypes.RESET_ADDRESS]: resetAddress,
  [AddressTypes.SET_MODAL_DELETE_VISIBLE]: setModalDeleteVisible,
  [AddressTypes.FETCH_ADDRESS]: fetchAddress,
  [AddressTypes.FETCH_ADDRESS_SUCCESS]: fetchAddressSuccess,
  [AddressTypes.FETCH_ADDRESS_LOADING]: fetchAddressLoading,

  [AddressTypes.SET_ADDRESS_STATE]: setAddressState,
  [AddressTypes.FETCH_ADDRESS_STATES]: fetchAddressStates,
  [AddressTypes.FETCH_ADDRESS_STATES_SUCCESS]: fetchAddressStatesSuccess,
  [AddressTypes.FETCH_ADDRESS_STATES_LOADING]: fetchAddressStatesLoading,

  [AddressTypes.SET_ADDRESS_AREA]: setAddressArea,
  [AddressTypes.FETCH_ADDRESS_AREAS]: fetchAddressAreas,
  [AddressTypes.FETCH_ADDRESS_AREAS_SUCCESS]: fetchAddressAreasSuccess,
  [AddressTypes.FETCH_ADDRESS_AREAS_LOADING]: fetchAddressAreasLoading
});
