import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  resetAddresses: null,
  fetchAddresses: null,
  fetchAddressesSuccess: ['addresses'],
  fetchAddressesLoading: ['boolean'],
  setRefreshAddresses: ['boolean']
});

export const AddressesTypes = Types;
export default Creators;
