/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  addresses: [],
  addressesIsLoading: false,
  refreshAddresses: false
};
