/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { AddressesTypes } from './Actions';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';

export const resetAddresses = state => ({
  ...state,
  addresses: [],
  refreshAddresses: false
});

export const fetchAddresses = state => ({
  ...state,
  addresses: [],
  addressesIsLoading: true,
  refreshAddresses: false
});

export const fetchAddressesSuccess = (state, { addresses }) => ({
  ...state,
  addresses: addresses,
  addressesIsLoading: false
});

export const fetchAddressesLoading = (state, { boolean }) => ({
  ...state,
  addressesIsLoading: boolean
});

export const setRefreshAddresses = (state, { boolean }) => ({
  ...state,
  refreshAddresses: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [AddressesTypes.RESET_ADDRESSES]: resetAddresses,
  [AddressesTypes.FETCH_ADDRESSES]: fetchAddresses,
  [AddressesTypes.FETCH_ADDRESSES_SUCCESS]: fetchAddressesSuccess,
  [AddressesTypes.FETCH_ADDRESSES_LOADING]: fetchAddressesLoading,
  [AddressesTypes.SET_REFRESH_ADDRESSES]: setRefreshAddresses
});
