import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  initApiUrl: [],
  updateApiUrl: ['apiUrl'],
  authenticate: ['formikBag', 'username', 'password'],
  authenticateSuccess: ['token', 'user', 'successMessage'],
  resetUsername: [],
  fetchUsername: [],
  registerFCM: ['fcmToken'],
  registerFCMSuccess: ['fcmToken'],
  unregisterFCMSuccess: [],
  setUsername: ['username'],
  setDivision: ['divisionId', 'divisionName'],
  changePassword: ['formikBag', 'currentPassword', 'newPassword'],
  changePasswordSuccess: ['successMessage'],
  showErrorMessage: ['errorMessage'],
  setHttpError: ['result'],
  setConnected: ['connected'],
  clearMessages: [],
  tokenExpired: ['errorMessage'],
  passwordExpired: ['errorMessage'],
  initLocale: [],
  setLocale: ['locale'],
  logout: [],
  logoutSuccess: [],
  popRoute: [],
  updateRoute: ['route'],
  clearRoutes: [],
  fetchVersionTypes: [],
  selectVersionType: ['versionType']
});

export const AppTypes = Types;
export default Creators;
