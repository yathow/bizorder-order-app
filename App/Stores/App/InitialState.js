/**
 * The initial values for the redux state.
 */
export const VERSION_TYPES = [
  {
    id: 0,
    name: 'live',
    url: 'https://www.bizorder.com.my/bizorder-backend/public/api/v1'
  },
  {
    id: 1,
    name: 'demo',
    url: 'http://inverze.dyndns.org:8000/bizorder-backend/public/api/v1'
  }
];

export const TEST_API_MODE = 1;

export const LIVE_API = 0;
export const TEST_API = 1;

export const INITIAL_STATE = {
  username: null,
  token: null,
  fcmToken: null,
  versionType: 0,
  versionTypes: [],
  apiUrl: VERSION_TYPES[0].url,
  user: {
    username: '',
    email: '',
    debtorId: 0,
    first_name: '',
    last_name: '',
    timezone: '',
    last_login: '',
    password_changed_at: ''
  },
  route: [],

  locale: 'en',
  refresh: false,
  divisionId: 0,
  divisionName: '',
  successMessage: '',
  errorMessage: '',
  connected: false
};
