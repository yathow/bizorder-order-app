/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { AppTypes } from './Actions';
import { INITIAL_STATE, VERSION_TYPES } from './InitialState';
import { createReducer } from 'reduxsauce';

export const initApiUrl = state => {
  if (state.versionType !== INITIAL_STATE.versionType || state.apiUrl !== INITIAL_STATE.apiUrl) {
    // api url have changed, need reset all values
    return {
      ...INITIAL_STATE,
      versionType: state.versionType,
      apiUrl: state.apiUrl
    };
  } else {
    return state;
  }
};

export const updateApiUrl = (state, { apiUrl }) => ({
  ...state,
  apiUrl: apiUrl
});

export const authenticateSuccess = (state, { token, user, successMessage }) => ({
  ...state,
  token: token,
  user: user,
  successMessage: successMessage,
  errorMessage: ''
});

export const registerFCMSuccess = (state, { fcmToken }) => ({
  ...state,
  fcmToken: fcmToken
});

export const unregisterFCMSuccess = state => ({
  ...state,
  fcmToken: null
});

export const resetUsername = state => ({
  ...state,
  username: ''
});

export const setUsername = (state, { username }) => ({
  ...state,
  username: username
});

export const setDivision = (state, { divisionId, divisionName }) => ({
  ...state,
  divisionId: divisionId,
  divisionName: divisionName,
  refresh: !state.refresh
});

export const setConnected = (state, { connected }) => ({
  ...state,
  connected: connected
});

export const initLocale = state => {
  if (state.locale === undefined) {
    return { ...state, locale: INITIAL_STATE.locale };
  } else {
    return state;
  }
};

export const setLocale = (state, { locale }) => {
  return {
    ...state,
    locale: locale
  };
};

export const showErrorMessage = (state, { errorMessage }) => ({
  ...state,
  successMessage: '',
  errorMessage: errorMessage
});

export const clearMessages = state => ({
  ...state,
  successMessage: '',
  errorMessage: ''
});

export const tokenExpired = (state, { errorMessage }) => ({
  ...state,
  successMessage: '',
  errorMessage: errorMessage
});

export const passwordExpired = (state, { errorMessage }) => ({
  ...state,
  successMessage: '',
  errorMessage: errorMessage
});

export const changePasswordSuccess = (state, { successMessage }) => ({
  ...state,
  successMessage: successMessage,
  errorMessage: ''
});

export const logout = state => ({
  ...state
});

export const logoutSuccess = state => ({
  ...INITIAL_STATE,
  versionType: state.versionType,
  apiUrl: state.apiUrl,
  locale: state.locale
});

export const updateRoute = (state, { route }) => {
  if (route == '') {
    let routes = state.route;
    routes.splice(-1, 1);
    return {
      ...state,
      route: routes
    };
  } else {
    return {
      ...state,
      route: [...state.route, route]
    };
  }
};

export const clearRoutes = state => ({
  ...state,
  route: []
});

export const fetchVersionTypes = state => {
  return {
    ...state,
    versionTypes: VERSION_TYPES
  };
};

export const selectVersionType = (state, { versionType }) => {
  return {
    ...state,
    versionType: versionType
  };
};

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [AppTypes.INIT_API_URL]: initApiUrl,
  [AppTypes.UPDATE_API_URL]: updateApiUrl,
  [AppTypes.AUTHENTICATE_SUCCESS]: authenticateSuccess,
  [AppTypes.SHOW_ERROR_MESSAGE]: showErrorMessage,
  [AppTypes.CLEAR_MESSAGES]: clearMessages,
  [AppTypes.TOKEN_EXPIRED]: tokenExpired,
  [AppTypes.RESET_USERNAME]: resetUsername,
  [AppTypes.REGISTER_FCM_SUCCESS]: registerFCMSuccess,
  [AppTypes.UNREGISTER_FCM_SUCCESS]: unregisterFCMSuccess,
  [AppTypes.SET_USERNAME]: setUsername,
  [AppTypes.SET_DIVISION]: setDivision,
  [AppTypes.SET_CONNECTED]: setConnected,
  [AppTypes.INIT_LOCALE]: initLocale,
  [AppTypes.SET_LOCALE]: setLocale,
  [AppTypes.PASSWORD_EXPIRED]: passwordExpired,
  [AppTypes.CHANGE_PASSWORD_SUCCESS]: changePasswordSuccess,
  [AppTypes.LOGOUT]: logout,
  [AppTypes.LOGOUT_SUCCESS]: logoutSuccess,
  [AppTypes.UPDATE_ROUTE]: updateRoute,
  [AppTypes.CLEAR_ROUTES]: clearRoutes,
  [AppTypes.FETCH_VERSION_TYPES]: fetchVersionTypes,
  [AppTypes.SELECT_VERSION_TYPE]: selectVersionType
});
