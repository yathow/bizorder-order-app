import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
    //token to identify the caller, now using timestamp
    startScanner: ['token'],
    scanSuccess: ['barcodes'],
    scanFailure: ['errorMessage']
})

export const BarcodeScannerTypes = Types
export default Creators
