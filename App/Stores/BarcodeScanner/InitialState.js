/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
    token: null,
    barcodes: [],
    scannerIsSuccess: false,
    scannerErrorMessage: null,
}
  