/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState'
import { createReducer } from 'reduxsauce'
import { BarcodeScannerTypes } from './Actions'

export const startScanner = (state, { token }) => ({
    ...state,
    token: token,
    barcodes: [],
    scannerIsSuccess: false,
    scannerErrorMessage: null
})

export const scanSuccess = (state, { barcodes }) => ({
    ...state,
    barcodes: barcodes,
    scannerIsSuccess: true,
})

export const scanFailure = (state, { errorMessage }) => ({
    ...state,
    scannerIsSuccess: false,
    scannerErrorMessage: errorMessage
})

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
    [BarcodeScannerTypes.START_SCANNER]: startScanner,
    [BarcodeScannerTypes.SCAN_SUCCESS]: scanSuccess,
    [BarcodeScannerTypes.SCAN_FAILURE]: scanFailure
})