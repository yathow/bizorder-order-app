import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  resetBrands: null,
  setRefreshBrands: ['boolean'],
  fetchBrands: ['divisionId'],
  fetchBrandsByCategory: [],
  fetchBrandsLoading: ['boolean'],
  fetchBrandsSuccess: ['brands']
});

export const BrandsTypes = Types;
export default Creators;
