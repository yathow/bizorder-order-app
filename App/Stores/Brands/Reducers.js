/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { BrandsTypes } from './Actions';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';

export const resetBrands = state => ({
  ...state,
  brands: []
});

export const fetchBrands = (state, { divisionId }) => ({
  ...state,
  brands: [],
  brandsIsLoading: true,
  refreshBrands: false
});

export const fetchBrandsByCategory = (state) => ({
  ...state,
  brands: [],
  brandsIsLoading: true,
  refreshBrands: false
});

export const setRefreshBrands = (state, { boolean }) => ({
  ...state,
  refreshBrands: boolean
});

export const fetchBrandsLoading = (state, { boolean }) => ({
  ...state,
  brandsIsLoading: boolean
});

export const fetchBrandsSuccess = (state, { brands }) => ({
  ...state,
  brands: [...state.brands, ...brands],
  brandsIsLoading: false
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [BrandsTypes.RESET_BRANDS]: resetBrands,
  [BrandsTypes.SET_REFRESH_BRANDS]: setRefreshBrands,
  [BrandsTypes.FETCH_BRANDS]: fetchBrands,
  [BrandsTypes.FETCH_BRANDS_BY_CATEGORY]: fetchBrandsByCategory,
  [BrandsTypes.FETCH_BRANDS_LOADING]: fetchBrandsLoading,
  [BrandsTypes.FETCH_BRANDS_SUCCESS]: fetchBrandsSuccess
});
