import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
    resetItems: null,
    fetchItems: ['page', 'sorts', 'filters', 'pageSize'],
    fetchItemsLoading: ['boolean'],
    fetchItemsSuccess: ['items', 'currentPage', 'lastPage', 'total', 'successMessage'],
    updateItemPhotoSuccess: ['item'],

    resetItemGroup01s: null,
    fetchAllItemGroup01s: null,
    fetchAllItemGroup01sLoading: ['boolean'],
    fetchAllItemGroup01sSuccess: ['itemGroup01s', 'successMessage'],

    setFilters: ['filters'],
    resetFilters: null,

    showErrorMessage: ['errorMessage']
})

export const BrochureTypes = Types
export default Creators
