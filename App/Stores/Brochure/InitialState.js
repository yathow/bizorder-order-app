/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
    items: [],
    itemsIsLoading: false,
    sorts: {},
    filters: {
        barcode: '',
        code: '',
        desc: '',
        item_group01_code_in: []
    },
    pageSize: '20',
    currentPage: 1,
    lastPage: 10,
    total: 100,
    itemGroup01s: [],
    successMessage: '',
    errorMessage: ''
}
  