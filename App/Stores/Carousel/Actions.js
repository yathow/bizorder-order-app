import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
    setCarouselIndex: ['index']
});

export const CarouselTypes = Types;
export default Creators;
