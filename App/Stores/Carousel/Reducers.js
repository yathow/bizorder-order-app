import { CarouselTypes } from './Actions';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';

export const setCarouselIndex = (state, { index }) => ({
    ...state,
    carouselIndex: index
});

export const reducer = createReducer(INITIAL_STATE, {
    [CarouselTypes.SET_CAROUSEL_INDEX]: setCarouselIndex
});
