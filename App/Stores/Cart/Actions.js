import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  addItemToCart: ['itemId', 'uomId'],
  addQtyItemInCart: ['detailId'],
  removeQtyItemInCart: ['detailId'],
  setItemQtyInCart: ['itemId', 'uomId', 'qty'],
  placeOrder: ['cartId'],
  placeOrderSuccess: ['orderId'],
  resetCart: null,
  fetchCart: ['divisionId'],
  fetchCartLoading: ['boolean'],
  fetchCartSuccess: ['cart'],
  goToDelivery: [],
  refreshCartPromotions: [],
  resolveConflict: ['id'],
  setRefreshCart: ['boolean'],
  setCheckoutAddress: ['addressId']
});

export const CartTypes = Types;
export default Creators;
