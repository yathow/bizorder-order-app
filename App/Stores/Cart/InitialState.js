/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  cartId: 0,
  cart: {
    id: 0,
    details: [],
    totalAmt: 0
  },
  orderId: 0,
  refreshCart: false,
  cartIsLoading: false
};
