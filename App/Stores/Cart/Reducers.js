/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { CartTypes } from './Actions';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';

export const resetCart = state => ({
  ...INITIAL_STATE
});

export const fetchCart = (state, { divisionId }) => ({
  ...state,
  cart: {
    ...INITIAL_STATE.cart
  },
  cartIsLoading: true,
  refreshCart: false
});

export const fetchCartSuccess = (state, { cart }) => ({
  ...state,
  cartId: cart.id,
  cart: { ...state.cart, ...cart },
  cartIsLoading: false
});

export const fetchCartLoading = (state, { boolean }) => ({
  ...state,
  cartIsLoading: boolean
});

export const setRefreshCart = (state, { boolean }) => ({
  ...state,
  refreshCart: boolean
});

export const placeOrderSuccess = (state, { orderId }) => ({
  ...state,
  orderId: orderId
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [CartTypes.RESET_CART]: resetCart,
  [CartTypes.FETCH_CART]: fetchCart,
  [CartTypes.SET_REFRESH_CART]: setRefreshCart,
  [CartTypes.FETCH_CART_LOADING]: fetchCartLoading,
  [CartTypes.FETCH_CART_SUCCESS]: fetchCartSuccess,
  [CartTypes.PLACE_ORDER_SUCCESS]: placeOrderSuccess
});
