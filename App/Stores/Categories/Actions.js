import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  resetCategories: null,
  setRefreshCategories: ['boolean'],
  fetchCategories: ['divisionId'],
  fetchCategoriesByBrand: [],
  fetchCategoriesLoading: ['boolean'],
  fetchCategoriesSuccess: ['categories']
});

export const CategoriesTypes = Types;
export default Creators;
