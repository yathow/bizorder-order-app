/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { CategoriesTypes } from './Actions';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';

export const resetCategories = state => ({
  ...state,
  categories: []
});

export const setRefreshCategories = (state, { boolean }) => ({
  ...state,
  refreshCategories: boolean
});

export const fetchCategories = (state, { divisionId }) => ({
  ...state,
  categories: [],
  categoriesIsLoading: true,
  refreshCategories: false
});

export const fetchCategoriesByBrand = (state) => ({
  ...state,
  categories: [],
  categoriesIsLoading: true,
  refreshCategories: false
});

export const fetchCategoriesLoading = (state, { boolean }) => ({
  ...state,
  categoriesIsLoading: boolean
});

export const fetchCategoriesSuccess = (state, { categories }) => ({
  ...state,
  categories: categories,
  categoriesIsLoading: false
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [CategoriesTypes.RESET_CATEGORIES]: resetCategories,
  [CategoriesTypes.SET_REFRESH_CATEGORIES]: setRefreshCategories,
  [CategoriesTypes.FETCH_CATEGORIES]: fetchCategories,
  [CategoriesTypes.FETCH_CATEGORIES_BY_BRAND]: fetchCategoriesByBrand,
  [CategoriesTypes.FETCH_CATEGORIES_LOADING]: fetchCategoriesLoading,
  [CategoriesTypes.FETCH_CATEGORIES_SUCCESS]: fetchCategoriesSuccess
});
