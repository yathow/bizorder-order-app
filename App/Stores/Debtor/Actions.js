import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  resetDebtor: null,
  fetchDebtor: ['debtorId'],
  fetchDebtorLoading: ['boolean'],
  fetchDebtorSuccess: ['debtor']
});

export const DebtorTypes = Types;
export default Creators;
