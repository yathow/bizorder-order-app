/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  debtorId: 0,
  debtor: null,

  debtorIsLoading: false
};
