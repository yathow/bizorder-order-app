/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { DebtorTypes } from './Actions';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';

export const resetDebtor = state => ({
  ...state,
  debtor: INITIAL_STATE.debtor
});

export const fetchDebtor = (state, { debtorId }) => ({
  ...state,
  debtorId: debtorId,
  debtorIsLoading: true
});

export const fetchDebtorSuccess = (state, { debtor }) => ({
  ...state,
  debtor: debtor,
  debtorIsLoading: false
});

export const fetchDebtorLoading = (state, { boolean }) => ({
  ...state,
  debtorIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [DebtorTypes.RESET_DEBTOR]: resetDebtor,
  [DebtorTypes.FETCH_DEBTOR]: fetchDebtor,
  [DebtorTypes.FETCH_DEBTOR_SUCCESS]: fetchDebtorSuccess
});
