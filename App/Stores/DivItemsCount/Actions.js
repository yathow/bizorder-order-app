import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  setRefreshDivItemsCount: ['boolean'],
  fetchDivItemsCount: ['divisionId'],
  fetchDivItemsCountLoading: ['boolean'],
  fetchDivItemsCountSuccess: ['divItemsCount']
});

export const DivItemsCountTypes = Types;
export default Creators;
