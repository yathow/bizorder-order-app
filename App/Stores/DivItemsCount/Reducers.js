/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { DivItemsCountTypes } from './Actions';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';

export const fetchDivItemsCount = (state, { divisionId }) => ({
  ...state,
  divItemsCount: 0,
  divItemsCountIsLoading: true,
  refreshDivItemsCount: false
});

export const setRefreshDivItemsCount = (state, { boolean }) => ({
  ...state,
  refreshDivItemsCount: boolean
});

export const fetchDivItemsCountLoading = (state, { boolean }) => ({
  ...state,
  divItemsCountIsLoading: boolean
});

export const fetchDivItemsCountSuccess = (state, { divItemsCount }) => ({
  ...state,
  divItemsCount: divItemsCount,
  divItemsCountIsLoading: false
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [DivItemsCountTypes.SET_REFRESH_DIV_ITEMS_COUNT]: setRefreshDivItemsCount,
  [DivItemsCountTypes.FETCH_DIV_ITEMS_COUNT]: fetchDivItemsCount,
  [DivItemsCountTypes.FETCH_DIV_ITEMS_COUNT_LOADING]: fetchDivItemsCountLoading,
  [DivItemsCountTypes.FETCH_DIV_ITEMS_COUNT_SUCCESS]: fetchDivItemsCountSuccess
});
