import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  resetDivisions: null,
  fetchDivisions: [],
  fetchDivisionsLoading: ['boolean'],
  fetchDivisionsSuccess: ['divisions']
});

export const DivisionsTypes = Types;
export default Creators;
