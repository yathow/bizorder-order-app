/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { DivisionsTypes } from './Actions';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';

export const resetDivisions = state => ({
  ...state,
  divisions: []
});

export const fetchDivisions = state => ({
  ...state,
  divisions: [],
  divisionsIsLoading: true
});

export const fetchDivisionsSuccess = (state, { divisions }) => ({
  ...state,
  divisions: [...state.divisions, ...divisions],
  divisionsIsLoading: false
});

export const fetchDivisionsLoading = (state, { boolean }) => ({
  ...state,
  divisionsIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [DivisionsTypes.RESET_DIVISIONS]: resetDivisions,
  [DivisionsTypes.FETCH_DIVISIONS_LOADING]: fetchDivisionsLoading,
  [DivisionsTypes.FETCH_DIVISIONS]: fetchDivisions,
  [DivisionsTypes.FETCH_DIVISIONS_SUCCESS]: fetchDivisionsSuccess
});
