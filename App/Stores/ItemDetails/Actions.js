import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  showItemDetails: ['itemId', 'item'],
  fetchItemDetails: ['itemId'],
  fetchItemDetailsSuccess: ['item'],
  fetchItemDetailsLoading: ['boolean'],
  showErrorMessage: ['errorMessage'],
});

export const ItemDetailsTypes = Types;
export default Creators;
