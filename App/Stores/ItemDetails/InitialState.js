/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  itemId: 0,
  item: null,
  itemIsLoading: false,

  itemPhotos: [],
  itemPhotosIsLoading: false,
  itemPhotosSorts: {},
  itemPhotosFilters: {},
  itemPhotosPageSize: 20,
  itemPhotosCurrentPage: 1,
  itemPhotosLastPage: 10,
  itemPhotosTotal: 100,
  selectedPhotos: [],

  successMessage: '',
  errorMessage: ''
};
