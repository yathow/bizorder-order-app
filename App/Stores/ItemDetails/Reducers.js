/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState';
import { ItemDetailsTypes } from './Actions';
import { createReducer } from 'reduxsauce';

export const showItemDetails = (state, { itemId, item }) => ({
  ...state,
  itemId: itemId,
  item: item,
  successMessage: '',
  errorMessage: ''
});

export const fetchItemDetails = (state, { itemId }) => ({
  ...state,
  itemId: itemId,
  item: null,
  itemIsLoading: true,
  successMessage: '',
  errorMessage: '',
  expandPromotions: [],
});

export const fetchItemDetailsLoading = (state, { boolean }) => ({
  ...state,
  itemIsLoading: boolean
});

export const fetchItemDetailsSuccess = (state, { item }) => ({
  ...INITIAL_STATE,
  item: item
});

export const showErrorMessage = (state, { errorMessage }) => ({
  ...state,
  successMessage: '',
  errorMessage: errorMessage
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [ItemDetailsTypes.SHOW_ITEM_DETAILS]: showItemDetails,
  [ItemDetailsTypes.FETCH_ITEM_DETAILS]: fetchItemDetails,
  [ItemDetailsTypes.FETCH_ITEM_DETAILS_SUCCESS]: fetchItemDetailsSuccess,
  [ItemDetailsTypes.FETCH_ITEM_DETAILS_LOADING]: fetchItemDetailsLoading,
  [ItemDetailsTypes.SHOW_ERROR_MESSAGE]: showErrorMessage,
});
