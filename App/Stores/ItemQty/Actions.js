import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  resetItemQty: [],
  showItemQty: ['itemId'],
  fetchItemQty: ['itemId'],
  setItemQty: ['cartId', 'item', 'uoms'],
  setUoms: ['uoms'],
  setTotalQty: ['totalQty'],
  setShowItemQty: ['boolean'],
  updateItemQty: ['uomId', 'qty'],
  saveItemQty: [],
  saveItemQtySuccess: []
});

export const ItemQtyTypes = Types;
export default Creators;
