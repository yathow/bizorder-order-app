/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  showItemQty: false,
  itemId: 0,
  itemQty: {
    cartId: 0,
    item: null,
    uoms: null,
    totalQty: 0
  },
  itemIsLoading: false,
  uomsIsLoading: false
};
