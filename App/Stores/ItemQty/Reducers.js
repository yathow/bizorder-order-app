/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState';
import { ItemQtyTypes } from './Actions';
import { createReducer } from 'reduxsauce';

export const resetItemQty = state => ({
  ...INITIAL_STATE
});

export const showItemQty = (state, { itemId }) => ({
  ...state,
  itemId: itemId,
  itemQty: {
    cartId: 0,
    item: null,
    uoms: null
  },
  itemIsLoading: true,
  uomsIsLoading: true,
  showItemQty: true
});

export const setUoms = (state, { uoms }) => ({
  ...state,
  itemQty: {
    ...state.itemQty,
    uoms: uoms ? [...uoms] : null
  }
});

export const fetchItemQty = (state, { itemId }) => ({
  ...state,
  itemIsLoading: true
});

export const setItemQty = (state, { cartId, item, uoms }) => ({
  ...state,
  itemQty: {
    ...state.itemQty,
    cartId: cartId,
    item: { ...item },
    uoms: uoms ? [...uoms] : null
  },
  itemIsLoading: false
});

export const setShowItemQty = (state, { boolean }) => ({
  ...state,
  showItemQty: boolean
});

export const saveItemQtySuccess = state => ({
  ...state,
  showItemQty: false
});

export const setTotalQty = (state, { totalQty }) => ({
  ...state,
  itemQty: {
    ...state.itemQty,
    totalQty: totalQty
  }
})

// export const updateItemQty = (state, { uomId, qty}) => {
//   const uoms = state.itemQty.uoms.map(uom => {
//     if (uom.id === uomId) {
//       return ({
//         ...uom,
//         qty: qty
//       });
//     } else {
//       return ({
//         ...uom
//       })
//     }
//   })
  
//   return {
//     ...state,
//     itemQty: {
//       ...state.itemQty,
//       uoms
//     }
//   }
// }

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [ItemQtyTypes.RESET_ITEM_QTY]: resetItemQty,
  [ItemQtyTypes.SHOW_ITEM_QTY]: showItemQty,
  [ItemQtyTypes.FETCH_ITEM_QTY]: fetchItemQty,
  [ItemQtyTypes.SET_ITEM_QTY]: setItemQty,
  [ItemQtyTypes.SET_UOMS]: setUoms,
  [ItemQtyTypes.SET_SHOW_ITEM_QTY]: setShowItemQty,
  [ItemQtyTypes.SAVE_ITEM_QTY_SUCCESS]: saveItemQtySuccess,
  [ItemQtyTypes.SET_TOTAL_QTY]: setTotalQty,
  // [ItemQtyTypes.UPDATE_ITEM_QTY]: updateItemQty
});
