import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  selectBrand: ['brandId', 'categoryIds'],
  selectCategory: ['categoryId', 'brandIds'],
  toggleBrandFilters: ['brandId'],
  toggleCategoryFilters: ['categoryId'],
  fetchSortTypes: [],
  selectSortType: ['sortType'],
  resetFilters: [],
  clearNewFilters: [],
  fetchFilters: ['reset'],
  applyFilters: [],
  updateKeyword: ['keyword'],
  resetItems: null,
  setRefreshItems: ['boolean'],
  fetchItems: [],
  fetchMoreItems: [],
  fetchItemsSuccess: ['items', 'currentPage', 'lastPage', 'total'],
  fetchItemsLoading: ['boolean'],
  showErrorMessage: ['errorMessage']
});

export const ItemsTypes = Types;
export default Creators;
