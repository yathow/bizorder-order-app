/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  refreshItems: false,
  items: [],
  pageSize: 20,
  currentPage: 1,
  lastPage: 1,
  total: 0,

  filters: {
    keyword: '',
    brandIds: [],
    categoryIds: [],
    sortType: 0
  },

  newFilters: {
    keyword: '',
    brandIds: [],
    categoryIds: [],
    sortType: 0
  },

  sortTypes: [],

  isLoading: false,

  successMessage: '',
  errorMessage: ''
};
