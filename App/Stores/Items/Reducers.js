/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState';
import { ItemsTypes } from './Actions';
import { createReducer } from 'reduxsauce';

export const selectBrand = (state, { brandId }) => ({
  ...state,
  filters: {
    keyword: '',
    brandIds: [brandId],
    categoryIds: [],
    sortType: 0
  }
});

export const selectCategory = (state, { categoryId }) => ({
  ...state,
  filters: {
    keyword: '',
    brandIds: [],
    categoryIds: [categoryId],
    sortType: 0
  }
});

export const resetFilters = state => {
  return {
    ...state,
    filters: INITIAL_STATE.filters
  };
};

export const clearNewFilters = state => {
  return {
    ...state,
    newFilters: {
      ...INITIAL_STATE.newFilters
    }
  };
};

export const toggleBrandFilters = (state, { brandId }) => {
  let index = state.newFilters.brandIds.indexOf(brandId);
  return {
    ...state,
    newFilters: {
      ...state.newFilters,
      brandIds: index >= 0 ? [] : [brandId]
    },
    filters: {
      ...state.filters,
      brandIds: index >= 0 ? [] : [brandId]
    }
  };
};

export const toggleCategoryFilters = (state, { categoryId }) => {
  let index = state.newFilters.categoryIds.indexOf(categoryId);
  return {
    ...state,
    newFilters: {
      ...state.newFilters,
      categoryIds: index >= 0 ? [] : [categoryId]
    },
    filters: {
      ...state.filters,
      categoryIds: index >= 0 ? [] : [categoryId]
    }
  };
};

export const fetchFilters = (state, { reset }) => {
  let cFilters = reset ? INITIAL_STATE.filters : state.filters;
  return {
    ...state,
    filters: cFilters,
    newFilters: cFilters
  };
};

export const fetchSortTypes = state => {
  return {
    ...state,
    sortTypes: [
      { id: 0, name: 'Name', order: 'asc' },
      { id: 1, name: 'Name', order: 'desc' }
    ]
  };
};

export const selectSortType = (state, { sortType }) => {
  return {
    ...state,
    newFilters: {
      ...state.newFilters,
      sortType: sortType
    }
  };
};

export const applyFilters = state => {
  const filters = {
    ...state.newFilters
  };
  return {
    ...state,
    filters: filters,
    newFilters: filters,
    refreshItems: true
  };
};

export const updateKeyword = (state, { keyword }) => {
  return {
    ...state,
    newFilters: {
      ...state.newFilters,
      keyword: keyword
    }
  };
};

export const resetItems = state => ({
  ...state,
  items: []
});

export const fetchItems = state => ({
  ...state,
  items: [],
  isLoading: true,
  refreshItems: false
});

export const fetchMoreItems = state => ({
  ...state,
  isLoading: true,
  refreshItems: false
});

export const fetchItemsSuccess = (state, { items, currentPage, lastPage, total }) => ({
  ...state,
  items: [...state.items, ...items],
  currentPage: currentPage,
  lastPage: lastPage,
  total: total,
  isLoading: false,
  errorMessage: ''
});

export const fetchItemsLoading = (state, { boolean }) => ({
  ...state,
  isLoading: boolean
});

export const showErrorMessage = (state, { errorMessage }) => ({
  ...state,
  successMessage: '',
  errorMessage: errorMessage
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [ItemsTypes.SELECT_BRAND]: selectBrand,
  [ItemsTypes.SELECT_CATEGORY]: selectCategory,
  [ItemsTypes.TOGGLE_BRAND_FILTERS]: toggleBrandFilters,
  [ItemsTypes.TOGGLE_CATEGORY_FILTERS]: toggleCategoryFilters,
  [ItemsTypes.SELECT_SORT_TYPE]: selectSortType,
  [ItemsTypes.FETCH_SORT_TYPES]: fetchSortTypes,
  [ItemsTypes.RESET_FILTERS]: resetFilters,
  [ItemsTypes.CLEAR_NEW_FILTERS]: clearNewFilters,
  [ItemsTypes.FETCH_FILTERS]: fetchFilters,
  [ItemsTypes.APPLY_FILTERS]: applyFilters,
  [ItemsTypes.UPDATE_KEYWORD]: updateKeyword,
  [ItemsTypes.RESET_ITEMS]: resetItems,
  [ItemsTypes.FETCH_ITEMS]: fetchItems,
  [ItemsTypes.FETCH_MORE_ITEMS]: fetchMoreItems,
  [ItemsTypes.FETCH_ITEMS_SUCCESS]: fetchItemsSuccess,
  [ItemsTypes.FETCH_ITEMS_LOADING]: fetchItemsLoading,
  [ItemsTypes.SHOW_ERROR_MESSAGE]: showErrorMessage
});
