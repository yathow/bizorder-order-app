import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  addSuccessMessage: ['successMessage'],
  addErrorMessage: ['errorMessage'],
  delayClearMessages: ['ms'],
  clearMessages: []
});

export const MessagesTypes = Types;
export default Creators;
