import { INITIAL_STATE } from './InitialState';
import { MessagesTypes } from './Actions';
import { createReducer } from 'reduxsauce';

export const addErrorMessage = (state, { errorMessage }) => ({
  ...state,
  errorMessages: [...state.errorMessages, errorMessage]
});

export const addSuccessMessage = (state, { successMessage }) => ({
  ...state,
  successMessages: [...state.successMessages, successMessage]
});

export const clearMessages = state => ({
  ...state,
  successMessages: [],
  errorMessages: []
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [MessagesTypes.ADD_ERROR_MESSAGE]: addErrorMessage,
  [MessagesTypes.ADD_SUCCESS_MESSAGE]: addSuccessMessage,
  [MessagesTypes.CLEAR_MESSAGES]: clearMessages
});
