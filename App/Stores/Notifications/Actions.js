import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
    fetchNotifications: [],
    fetchMoreNotifications: [],
    fetchNotificationsLoading: ['boolean'],
    fetchMoreNotificationsLoading: ['boolean'],
    fetchNotificationsSuccess: ['notifications', 'currentPage', 'lastPage', 'total'],
    readNotification: ['id'],
    readNotificationSuccess: ['notification'],
    markAllAsSeen: [],
    clearNotifications: [],
    setConfirmDialog: ['title', 'message', 'confirmType', 'boolean'],
    setConfirmDialogVisible: ['boolean'],
    deleteNotification: ['id'],
    deleteNotificationSuccess: ['id'],
    setDeleteNotification: ['id']
});

export const NotificationsTypes = Types;
export default Creators;
