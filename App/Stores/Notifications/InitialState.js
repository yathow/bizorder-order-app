/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  deleteNotificationId: 0,
  notifications: [],
  notificationsIsLoading: false,
  moreNotificationsIsLoading: false,

  modalConfirmVisible: false,
  modalConfirmType: '',
  confirmTitle: '',
  confirmMessage: '',

  pageSize: 20,
  currentPage: 1,
  lastPage: 1,
  total: 0,
};
