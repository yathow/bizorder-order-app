import { INITIAL_STATE } from './InitialState';
import { NotificationsTypes } from './Actions';
import { createReducer } from 'reduxsauce';

export const fetchNotificationsLoading = (state, { boolean }) => ({
  ...state,
  notificationsIsLoading: boolean
})

export const fetchMoreNotificationsLoading = (state, { boolean }) => ({
  ...state,
  moreNotificationsIsLoading: boolean
})

export const fetchNotificationsSuccess = (state, { notifications, currentPage, lastPage, total }) => ({
  ...state,
  notifications,
  currentPage,
  lastPage,
  total
});

export const readNotificationSuccess = (state, { notification }) => {
  let notifications = state.notifications
  
  let newNotifications = notifications.map(n => {
    if(n.id == notification.id)
      return notification
    else
      return n
  })

  return (
    {
      ...state,
      notifications: newNotifications
    }
  )
}

export const setConfirmDialog = (state, { title, message, confirmType, boolean }) => ({
  ...state,
  modalConfirmVisible: boolean,
  modalConfirmType: confirmType,
  confirmTitle: title,
  confirmMessage: message
})

export const setConfirmDialogVisible = (state, { boolean }) => ({
  ...state,
  modalConfirmVisible: boolean,
  modalConfirmType: '',
  confirmTitle: '',
  confirmMessage: ''
})

export const deleteNotificationSuccess = (state, { id }) => ({
  ...state,
  deleteNotificationId: 0,
  notifications: [...state.notifications.filter(data => data.id !== id)]
})

export const setDeleteNotification = (state, { id }) => ({
  ...state,
  deleteNotificationId: id
})

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [NotificationsTypes.FETCH_NOTIFICATIONS_LOADING]: fetchNotificationsLoading,
  [NotificationsTypes.FETCH_NOTIFICATIONS_SUCCESS]: fetchNotificationsSuccess,
  [NotificationsTypes.READ_NOTIFICATION_SUCCESS]: readNotificationSuccess,
  [NotificationsTypes.SET_CONFIRM_DIALOG]: setConfirmDialog,
  [NotificationsTypes.SET_CONFIRM_DIALOG_VISIBLE]: setConfirmDialogVisible,
  [NotificationsTypes.DELETE_NOTIFICATION_SUCCESS]: deleteNotificationSuccess,
  [NotificationsTypes.SET_DELETE_NOTIFICATION]: setDeleteNotification
});
