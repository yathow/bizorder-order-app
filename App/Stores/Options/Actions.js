import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  showOptions: []
});

export const OptionsTypes = Types;
export default Creators;
