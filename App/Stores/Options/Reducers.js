/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState';
import { OptionsTypes } from './Actions';
import { createReducer } from 'reduxsauce';

export const showOptions = (state, {}) => ({
  ...state
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [OptionsTypes.SHOW_OPTIONS]: showOptions
});
