import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  viewOrder: ['orderId'],
  fetchOrder: ['orderId'],
  fetchOrderSuccess: ['order'],
  fetchOrderLoading: ['boolean'],
  showErrorMessage: ['errorMessage']
});

export const OrderTypes = Types;
export default Creators;
