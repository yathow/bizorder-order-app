/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  orderId: 0,
  order: null,

  orderIsLoading: false
};
