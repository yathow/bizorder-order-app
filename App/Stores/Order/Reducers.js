/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState';
import { OrderTypes } from './Actions';
import { createReducer } from 'reduxsauce';

export const viewOrder = (state, { orderId }) => ({
  ...state,
  orderId: orderId,
  order: null,
  orderIsLoading: true
});

export const fetchOrder = (state, { orderId }) => ({
  ...state,
  orderId: orderId,
  orderIsLoading: true
});

export const fetchOrderSuccess = (state, { order }) => ({
  ...state,
  order: order,
  orderIsLoading: false
});

export const fetchOrderLoading = (state, { boolean }) => ({
  ...state,
  orderIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [OrderTypes.VIEW_ORDER]: viewOrder,
  [OrderTypes.FETCH_ORDER]: fetchOrder,
  [OrderTypes.FETCH_ORDER_SUCCESS]: fetchOrderSuccess
});
