import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  fetchOrders: [],
  fetchMoreOrders: [],
  fetchOrdersSuccess: ['orders', 'currentPage', 'lastPage', 'total'],
  fetchOrdersLoading: ['boolean'],
  showErrorMessage: ['errorMessage'],
  setFilterOrderDialogVisible: ['boolean'],
  toggleFilterOrderStatus: ['status'],
  applyFilterOrder: [],
  setRefreshOrders: ['boolean'],
  resetFilters: []
});

export const OrdersTypes = Types;
export default Creators;
