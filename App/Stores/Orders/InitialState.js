/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  orders: [],
  pageSize: 20,
  currentPage: 1,
  lastPage: 1,
  total: 0,
  filterOrderDialogVisible: false,
  filterStatus: [],
  newFilterStatus: [],
  refreshOrders: false,

  ordersIsLoading: false
};
