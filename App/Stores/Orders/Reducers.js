/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState';
import { OrdersTypes } from './Actions';
import { createReducer } from 'reduxsauce';

export const fetchOrders = (state, {}) => ({
  ...state,
  orders: [],
  ordersIsLoading: true
});

export const fetchOrdersSuccess = (state, { orders, currentPage, lastPage, total }) => ({
  ...state,
  orders: [...state.orders, ...orders],
  currentPage: currentPage,
  lastPage: lastPage,
  total: total,
  orderIsLoading: false
});

export const fetchOrdersLoading = (state, { boolean }) => ({
  ...state,
  ordersIsLoading: boolean
});

export const setFilterOrderDialogVisible = (state, { boolean }) => ({
  ...state,
  filterOrderDialogVisible: boolean
})

export const toggleFilterOrderStatus = (state, { status }) => {
  let index = state.newFilterStatus.indexOf(status);
  return {
    ...state,
    newFilterStatus: index >= 0 ? state.newFilterStatus.filter((prev) => prev != status) : [...state.newFilterStatus, status]
  }
}

export const applyFilterOrder = state => {
  const filters = state.newFilterStatus;

  return {
    ...state,
    newFilterStatus: filters,
    filterStatus: filters,
    refreshOrders: true,
  };
};

export const setRefreshOrders = (state, { boolean }) => ({
  ...state,
  refreshOrders: boolean
});

export const resetFilters = state => ({
  ...state,
  filterStatus: [],
  newFilterStatus: [],
  filterOrderDialogVisible: false
})

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [OrdersTypes.FETCH_ORDERS]: fetchOrders,
  [OrdersTypes.FETCH_ORDERS_SUCCESS]: fetchOrdersSuccess,
  [OrdersTypes.FETCH_ORDERS_LOADING]: fetchOrdersLoading,
  [OrdersTypes.SET_FILTER_ORDER_DIALOG_VISIBLE]: setFilterOrderDialogVisible,
  [OrdersTypes.TOGGLE_FILTER_ORDER_STATUS]: toggleFilterOrderStatus,
  [OrdersTypes.APPLY_FILTER_ORDER]: applyFilterOrder,
  [OrdersTypes.SET_REFRESH_ORDERS]: setRefreshOrders,
  [OrdersTypes.RESET_FILTERS]: resetFilters
});
