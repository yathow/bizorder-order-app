import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
    fetchPromotions: [],
    fetchPromotionsSuccess: ['promotions'],
    fetchPromotionsLoading: ['boolean'],
    goToPromotion: ['promotionId'],
    setRefreshPromotions: ['boolean'],
    fetchPromotion: ['promotionId'],
    fetchPromotionSuccess: ['promotion'],
    fetchPromotionItems: ['promotionId'],
    fetchPromotionItemsLoading: ['boolean'],
    fetchPromotionItemsSuccess: ['itemsList'],
    setPromotion: ['promotionId'],
    togglePromotionItemExpand: ['title'],
    setForceUpdate: ['boolean']
});

export const PromotionsTypes = Types;
export default Creators;
