/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
    promotions: [],
    promotionsIsLoading: false,
    refreshPromotions: false,
    promotionId: 0,
    promotion: [],
    items: [],
    itemsIsLoading: false,
    forceUpdate: false,
};