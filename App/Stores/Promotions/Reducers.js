/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState';
import { PromotionsTypes } from './Actions';
import { createReducer } from 'reduxsauce';

export const fetchPromotions = (state, {}) => ({
    ...state,
    promotions: [],
    promotionsIsLoading: true
});

export const fetchPromotionsSuccess = (state, { promotions }) => ({
    ...state,
    promotions: promotions.map(promotion => {
        return ({
            id: promotion.id,
            asset_url: promotion.asset_url,
        })
    })
})

export const fetchPromotionsLoading = (state, { boolean }) => ({
    ...state,
    promotionsIsLoading: boolean
})

export const setRefreshPromotions = (state, { boolean }) => ({
    ...state,
    refreshPromotions: boolean
});

export const setPromotion = (state, { promotionId }) => ({
    ...state,
    promotionId: promotionId
})

export const fetchPromotionSuccess = (state, { promotion }) => ({
    ...state,
    promotion: promotion
})

export const fetchPromotionItemsLoading = (state, { boolean }) => ({
    ...state,
    itemsIsLoading: boolean
})

export const fetchPromotionItemsSuccess = (state, { itemsList }) => ({
    ...state,
    items: itemsList
})

export const togglePromotionItemExpand = (state, { title }) => {
    let items_list = state.items

    items_list.forEach((item) => {
        if(item.title === title) {
            item.expanded === true
            ? (
                item.expanded = false
            ) : (
                item.expanded = true
            )
        }
    })

    return {
        ...state,
        items: items_list
    }
}

export const setForceUpdate = (state, { boolean }) => ({
    ...state,
    forceUpdate: boolean
})

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
    [PromotionsTypes.FETCH_PROMOTIONS]: fetchPromotions,
    [PromotionsTypes.FETCH_PROMOTIONS_SUCCESS]: fetchPromotionsSuccess,
    [PromotionsTypes.FETCH_PROMOTIONS_LOADING]: fetchPromotionsLoading,
    [PromotionsTypes.SET_REFRESH_PROMOTIONS]: setRefreshPromotions,
    [PromotionsTypes.SET_PROMOTION]: setPromotion,
    [PromotionsTypes.FETCH_PROMOTION_SUCCESS]: fetchPromotionSuccess,
    [PromotionsTypes.FETCH_PROMOTION_ITEMS_LOADING]: fetchPromotionItemsLoading,
    [PromotionsTypes.FETCH_PROMOTION_ITEMS_SUCCESS]: fetchPromotionItemsSuccess,
    [PromotionsTypes.TOGGLE_PROMOTION_ITEM_EXPAND]: togglePromotionItemExpand,
    [PromotionsTypes.SET_FORCE_UPDATE]: setForceUpdate,
});
