import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  reset: [],
  fetch1: ['id'],
  fetch2: ['id'],
  process1: [],
  process2: [],
  setValue1: ['value1'],
  setValue2: ['value2']
});

export const TestTypes = Types;
export default Creators;
