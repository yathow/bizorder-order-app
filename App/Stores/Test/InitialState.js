/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  id1: null,
  id2: null,
  value1: null,
  value2: null
};
