/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState';
import { TestTypes } from './Actions';
import { createReducer } from 'reduxsauce';

export const reset = state => {
  console.log(' > reset');
  return {
    ...INITIAL_STATE
  };
};

export const fetch1 = (state, { id }) => {
  console.log(' > fetch1');
  return {
    ...state,
    id1: id
  };
};

export const fetch2 = (state, { id }) => {
  console.log(' > fetch2');
  return {
    ...state,
    id2: id
  };
};

export const setValue1 = (state, { value1 }) => {
  console.log(' > setValue1');
  return {
    ...state,
    value1: { ...value1 }
  };
};

export const setValue2 = (state, { value2 }) => {
  console.log(' > setValue2');
  return {
    ...state,
    value2: { ...value2 }
  };
};

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [TestTypes.RESET]: reset,
  [TestTypes.FETCH1]: fetch1,
  [TestTypes.FETCH2]: fetch2,
  [TestTypes.SET_VALUE1]: setValue1,
  [TestTypes.SET_VALUE2]: setValue2
});
