import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  setTheme: ['theme']
});

export const ThemeTypes = Types;
export default Creators;
