/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { ThemeTypes } from './Actions';

export const setTheme = (state, { theme }) => ({
  ...state,
  theme: theme
});

export const reducer = createReducer(INITIAL_STATE, {
  [ThemeTypes.SET_THEME]: setTheme
});
