import { reducer as AddressReducer } from './Address/Reducers';
import { reducer as AddressesReducer } from './Addresses/Reducers';
import { reducer as AppReducer } from './App/Reducers';
import { reducer as BarcodeScannerReducer } from './BarcodeScanner/Reducers';
import { reducer as BrandsReducer } from './Brands/Reducers';
import { reducer as BrochureReducer } from './Brochure/Reducers';
import { reducer as CarouselReducer } from './Carousel/Reducers';
import { reducer as CartReducer } from './Cart/Reducers';
import { reducer as CategoriesReducer } from './Categories/Reducers';
import { reducer as DebtorReducer } from './Debtor/Reducers';
import { reducer as DivItemsCountReducer } from './DivItemsCount/Reducers';
import { reducer as DivisionsReducer } from './Divisions/Reducers';
import { reducer as ItemDetailsReducer } from './ItemDetails/Reducers';
import { reducer as ItemQtyReducer } from './ItemQty/Reducers';
import { reducer as ItemsReducer } from './Items/Reducers';
import { reducer as MessageReducer } from './Messages/Reducers';
import { reducer as NotificationsReducer } from './Notifications/Reducers'
import { reducer as OrderReducer } from './Order/Reducers';
import { reducer as OrdersReducer } from './Orders/Reducers';
import { reducer as PromotionsReducer } from './Promotions/Reducers';
import { reducer as TestReducer } from './Test/Reducers';
import { reducer as OptionsReducer } from './Options/Reducers';
import { reducer as ThemeReducer } from './Theme/Reducers';
import { combineReducers } from 'redux';
import configureStore from './CreateStore';
import rootSaga from '../Sagas';

export default () => {
  const rootReducer = combineReducers({
    /**
     * Register your reducers here.
     * @see https://redux.js.org/api-reference/combinereducers
     */
    addresses: AddressesReducer,
    app: AppReducer,
    address: AddressReducer,
    barcodeScanner: BarcodeScannerReducer,
    brands: BrandsReducer,
    brochure: BrochureReducer,
    carousel: CarouselReducer,
    cart: CartReducer,
    categories: CategoriesReducer,
    debtor: DebtorReducer,
    divisions: DivisionsReducer,
    divItemsCount: DivItemsCountReducer,
    itemDetails: ItemDetailsReducer,
    items: ItemsReducer,
    itemQty: ItemQtyReducer,
    messages: MessageReducer,
    notifications: NotificationsReducer,
    options: OptionsReducer,
    order: OrderReducer,
    orders: OrdersReducer,
    promotions: PromotionsReducer,
    theme: ThemeReducer,
    test: TestReducer
  });
  return configureStore(rootReducer, rootSaga);
};
