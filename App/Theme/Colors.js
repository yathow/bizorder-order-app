import { Colors } from '@merchstack/react-native-ui';

export default {
  ...Colors,
  blue1: '#0091ea',
  orange1: '#ff6f00',
  orange4: '#fed8b1',
  darkgray1: '#808080',
  darkgray2: '#222222',
  red1: '#d9534f'
};
