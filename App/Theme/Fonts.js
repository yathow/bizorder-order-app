import { Fonts, Metrics } from '@merchstack/react-native-ui';

var size = {
  h1: 21,
  h2: 19,
  h3: 17,
  h4: 16,
  input: 15,
  regular: 14,
  medium: 14,
  small: 12
};

if (Metrics.screenWidth() < 400) {
  size = {
    ...size,
    h1: 19,
    h2: 17,
    h3: 15,
    h4: 14,
    input: 13,
    regular: 12,
    medium: 12,
    small: 10
  };
} else if (Metrics.screenWidth() < 480) {
  size = {
    ...size,
    h1: 19,
    h2: 17,
    h3: 15,
    h4: 14,
    input: 14,
    regular: 13,
    medium: 13,
    small: 11
  };
}

const style = {
  h1: {
    fontSize: size.h1
  },
  h2: {
    fontSize: size.h2
  },
  h3: {
    fontSize: size.h3
  },
  h4: {
    fontSize: size.h4
  },
  normal: {
    fontSize: size.regular
  }
};

export default {
  ...Fonts,
  ...size,
  ...style
};
