import { Helpers } from '@merchstack/react-native-ui';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  ...Helpers
});
