/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

export default {
  logo: require('App/Assets/Images/logo.png'),
  noPhotoIcon: require('App/Assets/Images/no-photo-icon.png'),
  noPhotoMedium: require('App/Assets/Images/no-photo-medium.png'),
  promotionsNotification: require('App/Assets/Images/megaphone.jpg')
};
