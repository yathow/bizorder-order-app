import { Metrics } from '@merchstack/react-native-ui';

var baseMetrics = {
  ...Metrics,
  headerHeight: 50,

  list: {
    brand: {
      width: 100,
      height: 100
    },
    category: {
      width: 100,
      height: 100
    }
  },
  catalog: {
    items: {
      width: 125,
      height: 125
    },
    uom: {
      height: 20
    },
    promo: { height: 24 }
  }
};

if (Metrics.screenWidth() < 480) {
  baseMetrics = {
    ...baseMetrics,
    xl: 22,
    lg: 18,
    md: 8,
    sm: 4,
    xs: 1,

    smBorderRadius: 3,
    borderRadius: 8,
    spacer: 80,

    catalog: {
      ...baseMetrics.catalog,
      items: {
        width: 110,
        height: 110
      },
      uom: {
        height: 16
      },
      promo: { height: 20 }
    }
  };
}

export default baseMetrics;
