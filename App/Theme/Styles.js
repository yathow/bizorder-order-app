import Colors from './Colors';
import Metrics from './Metrics';
import { Styles } from '@merchstack/react-native-ui';

export default {
  ...Styles,

  smallBtn: {
    padding: Metrics.sm,
    paddingLeft: Metrics.md,
    paddingRight: Metrics.md,
    margin: 0,
    borderRadius: Metrics.smBorderRadius
  }
};
