import Colors from './Colors';
import Fonts from './Fonts';
import Metrics from './Metrics';
import { createTheme } from '@merchstack/react-native-ui';

const baseLight = createTheme({
  colors: {
    primary: Colors.blue1,
    secondary: Colors.orange1,
    danger: Colors.red1,
    inverse: Colors.white,
    inputBorder: Colors.darkgray1,
    highlight: Colors.orange1,
    link: Colors.blue1,
    lightGrey: Colors.gray4,
    grey: Colors.gray1,
    darkGrey1: Colors.darkgray1,
    darkGrey2: Colors.darkgray2,

    info: Colors.blue1,
    success: Colors.green1,
    error: Colors.red1,
    warning: Colors.orange2
  },
  backgroundColors: {
    main: Colors.white,
    primary: Colors.blue1,
    secondary: Colors.orange1,

    suggestion: Colors.orange4,
    lightGray: Colors.gray4,
    disabledLight: Colors.gray1,
    disabledDark: Colors.darkgray1
  },
  fonts: Fonts,
  metrics: Metrics
});

const baseDark = createTheme({
  colors: {
    primary: Colors.red1,
    secondary: Colors.orange3,
    inverse: Colors.black,
    inputBorder: Colors.gray3,
    highlight: Colors.orange3,
    link: Colors.red1,

    text: Colors.white,
    input: Colors.white,

    info: Colors.blue1,
    success: Colors.green1,
    error: Colors.red1,
    warning: Colors.orange2
  },
  backgroundColors: {
    main: Colors.black,
    primary: Colors.red1,
    secondary: Colors.blue1,
    panel: Colors.gray3,

    input: Colors.black
  },
  fonts: Fonts,
  metrics: Metrics
});

const Themes = {
  light: {
    ...baseLight,
    // Add generic customization code here
    Input: {
      ...baseLight.Input,
      floatingTitle: true
    }
  },
  dark: {
    ...baseDark,
    // Add generic customization code here
    Input: {
      ...baseLight.Input,
      floatingTitle: true
    }
  }
};

export default Themes;
