import * as RNLocalize from 'react-native-localize';

import { I18nManager } from 'react-native';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize';

const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require('./en.json'),
  bm: () => require('./bm.json')
};

export const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

export const loadLocale = locale => {
  translate.cache.clear();
  I18nManager.forceRTL(false);

  const effLocale = findBestAvailableLanguage(locale);

  i18n.translations = { [locale]: translationGetters[effLocale]() };
  i18n.locale = effLocale;
};

const findBestAvailableLanguage = locale => {
  if (translationGetters[locale]) {
    return locale;
  } else {
    return 'en';
  }
};

export const setI18nConfig = () => {};

/*
export const setI18nConfig = () => {
  // fallback if no available language fits
  const fallback = { languageTag: 'en', isRTL: false };

  const { languageTag, isRTL } =
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) || fallback;

  // clear translation cache
  translate.cache.clear();
  // update layout direction
  I18nManager.forceRTL(isRTL);
  // set i18n-js config
  i18n.translations = { [languageTag]: translationGetters[languageTag]() };
  i18n.locale = languageTag;
};
*/

export const addLocaleChangedListener = handler => {
  RNLocalize.addEventListener('change', handler);
};

export const removeLocaleChangedListener = handler => {
  RNLocalize.removeEventListener('change', handler);
};
