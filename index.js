/**
 * @format
 */

import React from 'react';
import App from './App/App';
import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';
import messaging from '@react-native-firebase/messaging';

/* Hide this section and in RootScreen.js to turn off notification handling */
// messaging().setBackgroundMessageHandler(async remoteMessage => {
//     console.log("Message handled in the background!", remoteMessage);
// });

// function HeadlessCheck({ isHeadless }) {
//     if(isHeadless) {
//         //App has been launched in the background by iOS, ignore
//         return null 
//     } 

//     return <App />;
// }

// AppRegistry.registerComponent(appName, () => HeadlessCheck);
/*------------------------------------------------------*/

AppRegistry.registerComponent(appName, () => gestureHandlerRootHOC(App));
